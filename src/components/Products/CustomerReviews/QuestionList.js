import React from 'react';

const QuestionList = () => {

    return (
        <React.Fragment>
            <div className="card-header">
                <div className="d-flex justify-content-between align-items-center">
                    <div className="d-flex justify-content-between align-items-center">
                        <div className="mr-2">
                            <div className="stamped-review-avatar-content">EC</div>
                        </div>
                        <div className="ml-2">
                            <div className="h5 m-0">EMILY C </div>

                        </div>
                    </div>
                    <div>
                        <div className="dropdown">
                            <span> 26/06/2020 </span>
                        </div>
                    </div>
                </div>
            </div>
            <div className="card-body">
                <a className="card-link" href="#">
                    <h5 className="card-title">ALLIES OF SKIN</h5>
                </a>
                <p className="card-text">
                    My first time using this cleanser was great! I noticed a difference to my skin right away
                </p>
            </div>
        </React.Fragment>
    )
}

export default QuestionList