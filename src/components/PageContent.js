import React, { Component } from 'react';
import '../styles/Home.scss';
import { HTTP } from '../Utils';

class PageContent extends Component {
	constructor(props){
		super(props);
		this.state = {
			pagesData: ''
		}
	}

	componentDidMount(){
		return HTTP().get('/api/catalog/vue_storefront_catalog/cms_page/_search?from=0&request=%7B%7D&size=50&sort=')
    	.then(res => {
	        const pageData = res.data.hits.hits.reduce((result, set, index)=>{
	          result.push(set._source);
	          return result;
	        },[]).find((page)=>{
	        	if(page.id === this.props.pageId){
	        		return page;
	        	}
	        });
	        this.setState({
	        	pageData: pageData.content
	        });
		});
	}

	scrollToTop =()=> {

		window.scrollTo({
		  top: 0,
		  behavior: "smooth"
		});
	
	  }

	createMarkup(){
	    return {
	      __html: this.state.pageData
	    }
	  }

	  clickHandler(e) {

		if (e.target.className == "back-to-top") {
			window.scrollTo({
				top: 0,
				behavior: "smooth"
			  });
		}
	}

	render(){
		return (<div onClick={this.clickHandler} dangerouslySetInnerHTML={this.createMarkup()} ></div>)
	}
}


export default PageContent;
