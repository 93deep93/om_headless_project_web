import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap'; 
import CustomerReviews from './CustomerReviews/CustomerReviews';
import ProductVideo from './ProductVideo';
import '../../styles/ProductDetails.scss';
// import 
import {  increaseQty } from '../../actions/increaseQty';
import {  decreaseQty } from '../../actions/decreaseQty';
import AlliesEthoes from '../../components/AlliesEthoes';
import { connect } from 'react-redux';
import {  addBasket } from '../../actions/addAction';
import { HTTP } from '../../Utils';
import Modal from './Models/Model';
import ModalWhite from './Models/Model2';
import SimilarProducts from './SimilarProducts';

class ProductDetails extends Component {


		constructor(props){
		super(props);
		this.state={
			products_list : [],
			product:{},
			isLoaded: false,
			productInfomodelState : false,
			productUsageState : false,
			product_error :  this.props.location.state ? this.props.location.state.error : ''
			}
	 	}

	 	increaseQty(product){
			 if(product.stock.qty>product.numbers)
 				product.numbers += 1;
 			this.setState({ 
					product:product,
					isLoaded: true
             })

	 	}

	 	decreaseQty(product){
	 		product.numbers -= 1;
	 		product.numbers = product.numbers < 1 ?  1 : product.numbers;
 			this.setState({ 
					product:product,
					isLoaded: true
             })
	 	}


   htmlStripTags(string){
		string = string || '';
		return string.replace(/<[^>]*>?/gm, '');
	}

	addDefaultSrc(ev){
       ev.target.src = 'http://demo43.laraveldevloper.in/pub/media/catalog/product/placeholder/default/placeholder_3.png'
    }


	  componentDidMount(){

		return HTTP().get('/api/catalog/vue_storefront_catalog/product/_search')
		.then(res => {
		    const product = res.data.hits.hits.reduce((result, set, index)=>{
		    	result.push(set._source);
		    	for(let s in set){
	    			let options = set[s].attributes_metadata || [];
	    			// option = options ? options.find((attr)=>attr.default_frontend_label === "Product Size") : [];
	    			let productSizeOption = options.length ? options.find((attr)=>attr.default_frontend_label === "Product Size") : null;
	    			productSizeOption = productSizeOption ? productSizeOption.options[0] || [] : [];
	    			set._source.productSizeOptions = productSizeOption;
				}
				this.setState({products_list : result});
		    	return result;
		    },[]).find((prod)=>prod.id ==this.props.match.params.id);
			product.numbers = 1;

			if( this.props.basketProps.products && this.props.basketProps.products.productList && this.props.basketProps.products.productList.length>0 ){
				if( this.props.basketProps.products.productList.find(x=> x.sku === product.sku)){
					product.isCartProduct = true;
				}else{
					product.isCartProduct = false;
				}
			}else{
				product.isCartProduct = false;
			}
			
            this.setState({ 
					product:product,
					isLoaded: true
             })
		})
		.catch(e => { 
		   console.log('Error:' + e)
		})


	}


	componentWillReceiveProps( props){
		if(props.basketProps.unAuthorizeUser){
			localStorage.removeItem('x-access-token');
			localStorage.removeItem('x-access-token-expiration');
			localStorage.removeItem('user-info-allies');
			sessionStorage.removeItem('cart-id');
			this.props.history.push({
			pathname :'/Login'
			})
		}
		let product = this.state.product;
		if( props.basketProps.products && props.basketProps.products.productList && props.basketProps.products.productList.length>0 ){
			
			if( props.basketProps.products.productList.find(x=> x.sku === product.sku)){
				product.isCartProduct = true;
				this.setState({ product : product })
			}else{
				product.isCartProduct = false;
			}
		}else if(props.basketProps.products && props.basketProps.products.productList && props.basketProps.products.productList.length === 0){
			product.isCartProduct = false;
		}
	}

    render(){

    	console.log(this.state);
		let {product, isLoaded, products_list, product_error} = this.state;
	    if(!isLoaded){
			return(<div className="text-center mt-5 mb-5">loading...</div>)
		}

		else {
           
           return (
       <React.Fragment>
		   {product_error ? <div className="product-error">{product_error}</div> : ''}
	    <div className="container-fluid product_detail_page pl-5 pr-5">
	      <div className="row">

	         <div className ="col-xl-6 col-lg-6 col-md-5 col-sm-12 co-12">
        	   <div className ="product_img">
               <img onError={this.addDefaultSrc} className="img-responsive" src={'http://139.180.155.160/img/310/300/resize'+ product.image}/>
	           </div>
	         </div>

	         <div className ="col-xl-6 col-lg-6 col-md-7 col-sm-12 co-12">

	              <div className="product_content">
	                 <h2 className="product-name">{product.name}</h2>

	                  <div className="stamped-badge">
	                     <span>
	                      <i className="fa fa-star"></i>
	                      <i className="fa fa-star"></i>
	                      <i className="fa fa-star"></i>
	                      <i className="fa fa-star"></i>
	                      <i className="fa fa-star"></i>
	                      </span>
	                      <span className="reviews"><Link to="review">{product.product_review}</Link></span>
	                  </div>  
	              </div>
              
                  <div className="product_content">
                   <h2 className="attribute-title">WHAT IT IS</h2>

                   <div className="attribute-value">

                       <p>{this.htmlStripTags(product.description)}</p>
                        <strong>USE DAILY (AM/PM)</strong>
                   </div>
                  </div>

	              <div  className ="product_content infobox_sec" onClick={ ()=>{ this.setState({productInfomodelState : true})}}>
	                   <Link className="learn_more"  >Product Information</Link>  
	              </div>

	              <div  className ="product_content infobox_sec" onClick={ ()=>{ this.setState({productUsageState : true})}}>
	                   <Link className="learn_more" >Usage and benefits</Link>  
	              </div>

	              <div className = "product_content">
	                <h2 className="attribute-title">SUITABLE FOR</h2>

	                <div className="attribute-value">

                      <p>{this.htmlStripTags(product.suitable_for)}</p>
	                </div>

                    <div className="custom-social-sharing-icon-container">
		                <a target="_blank" href = "https://www.facebook.com/alliesofskin"> <img src={require('../../icons/face-book.png')} /> <span>Share</span> </a>
		                <a target="_blank" href = "https://twitter.com/"> <img src={require('../../icons/twitter.png')} /> <span>Tweet</span> </a>
		                <a target="_blank" href = "https://in.pinterest.com/"> <img src={require('../../icons/pinterest.png')} /> <span>Pin It</span> </a>
		                <a target="_blank" href = "https://fancy.com/"> <img src={require('../../icons/fancy.png')} /> <span>Fancy</span> </a>
		                <a target="_blank" href = "https://myaccount.google.com/"> <img src={require('../../icons/google-plus.png')} /> <span></span> </a>
	                </div>
	              </div>               

	         </div>

	      </div>
	    </div> 

	    <div className="product-add-form">
		    <div className="container-fluid">
		    <div className ="row cart_details">
                <div className="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">      
                </div>
                <div className="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12">

                   <div className="row cart_detail">
                     <div className="col-3 cart">
                        <span className="cart_title_name">Size</span>
                        <span className="cart_title_value">{product.productSizeOptions ? product.productSizeOptions.label : ''}  </span>
                     </div>
                     <div className="col-3 cart">
                     <span className="cart_title_name">Price</span>
                        <span className="cart_title_value">{'$' + product.price}  </span>
                     </div>
                     <div className="col-3 cart">
					 {product.stock.qty > 0 ?
					 <div className="qnatity">
					<span className="cart_title_name">Qantity</span>
					<span className="quantity" onClick = { product.isCartProduct ? ()=>{} : () => this.decreaseQty(product)} ><i className="fa fa-minus" aria-hidden="true"></i></span>
					<span className="cart_title_value qty"> {product.numbers} </span>
					<span className="quantity" onClick = { product.isCartProduct ? ()=>{} : () => this.increaseQty(product)} ><i className="fa fa-plus" aria-hidden="true"></i></span>
					</div>
					 :
					<div className="qyanity">

					</div>
					 } 
                     
						</div>
                     <div className="col-3 cart-btn">
						 {product.stock.qty > 0 ?
						 <Button variant="primary" disabled = { product.isCartProduct ? true : false} onClick={() => this.props.addBasket (product)} > {product.isCartProduct ? 'Added': 'Add to Cart'}</Button>
						 :
						<span></span>
						//  <Button variant="primary" > {'Notify me'} </Button>
						 }
                      
                     </div>           
                   </div>
                </div>                 
                </div>

		    </div>
	    </div>
		{product.product_video ? <ProductVideo product_video={ product.product_video} />: ''}
	    

	    {/* < CustomerReviews /> */}
		<SimilarProducts product={product} products_list={products_list} basketProps ={this.props.basketProps} addBasket={this.props.addBasket}/>
		{/* <div dangerouslySetInnerHTML={{__html: product.complete_your_regimen_with}}></div> */}
	    <AlliesEthoes />
		<div>
			<Modal product ={product} isShow={this.state.productInfomodelState}  closeModal={()=>{ this.setState({ productInfomodelState: false})}}/>
		</div>
		<div>
			<ModalWhite product ={product} isShow={this.state.productUsageState} closeModal={()=>{ this.setState({ productUsageState : false})}} />
		</div>
		
    </React.Fragment>
  );
		}
       
       

    }
	


}

const mapStateToProps = state => ({
	basketProps : state.basketState
});

export default connect(mapStateToProps, { addBasket,increaseQty, decreaseQty }) (ProductDetails);