import React, { Component } from 'react';
import { HTTP } from '../Utils';



class Fetch2 extends Component {

	constructor(props){
		super(props);
		this.state={
			items:[],
			isLoaded:false,
		}
	}

	componentDidMount(){

		return HTTP().get('/V1/customers/19358')
		.then(res => {
			console.log('res: ', res)
		    return res.data
		})
		.catch(e => {
		   console.log('Error:' + e)
		})
			
	}

	render() {

		let {isLoaded, items} = this.state;
		let myItems = Object.assign([],items.data);

		if(!isLoaded){
			return(<div className="loading">Loading...</div>)
		}else{
			return(

    		<div className="mx-4 my-4">
		        {myItems.map((postDetail, index)=>{
		        	return <div>
		        	<h3>Employee Name: {postDetail.employee_name}</h3>
		        	<span>Employee Salary: {postDetail.employee_salary}</span>
		        	<p>Employee Age: {postDetail.employee_age}</p>
		        	<img src={postDetail.profile_image} height="100px"/>
		        	</div>
		        })}
    		</div>
    		);	
		}
		

		
	}
  
}

export default Fetch2;