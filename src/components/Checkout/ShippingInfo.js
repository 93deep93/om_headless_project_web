import React, {Fragment, Component} from 'react';
import {Form, Button, Tooltip, Accordion, Card } from 'react-bootstrap';
import '../../styles/Checkout.scss';
import ShippingAddress from './ShippingAddress/shippingAddress';
import { Redirect, Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import countriesList from '../../data/countries_reagions.json';
import equal from 'fast-deep-equal';
import ShippingView from './ShippingForm/ShippingView';


class ShippingInfo extends Component {

   constructor(props) {
      super(props);
      this.state = { 
        shipping_data : {},
        shipping_method_data : [],
        cart_id : sessionStorage.getItem('cart-id'),
        previous_shipping_data : sessionStorage.getItem('checkout_shipping_data') ? JSON.parse(sessionStorage.getItem('checkout_shipping_data')) : {}
        
       }
      }
      
      render() {
      
        return (
            <React.Fragment>
                <div className="shipping_form">
                    <h2 className="step_title">Shipping Address</h2>
                    { this.props.user_info_address.length > 0?
                        <ShippingAddress 
                          previous_checkout_shipping_data= {this.state.previous_shipping_data}
                          gotoPaymentPage={this.props.gotoPaymentPage} 
                          user_info_address = {this.props.user_info_address}  
                          showLoader={this.props.showLoader}
                          closeLoader={this.props.closeLoader}
                          />
                        :
                        <ShippingView
                          previous_checkout_shipping_data= {this.state.previous_shipping_data}
                          gotoPaymentPage={this.props.gotoPaymentPage} />
                    }     
               </div>
           </React.Fragment >
        );

    }

}


export default ShippingInfo;