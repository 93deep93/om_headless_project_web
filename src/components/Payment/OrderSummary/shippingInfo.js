import React, { useState, Fragment, Component } from 'react';
import countriesList from '../../../data/countries_reagions.json';
import { Form, Button, Tooltip, Accordion, Card } from 'react-bootstrap';
// import '../../styles/Checkout.scss';
import { Redirect, Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import equal from 'fast-deep-equal';
const Shippinginfo = (props) => {
    const shipmentAddress = props.shippingAddress;

    return (
        <React.Fragment>        
             <div className="ship-to">
                    <div className="shipping-information-title">
                        <span>Ship To:</span>
                    </div>
                    {shipmentAddress ? 
                        <div className="shipping-information-content">
                        <span>{ shipmentAddress.firstname +''+ shipmentAddress.lastname}</span><br/>
                        <span>{ shipmentAddress.address}</span><br/>
                        <span>{ shipmentAddress.city}</span><br/>
                        <span>{ shipmentAddress.region}</span><br/>
                        <span>{countriesList.length>0 ? countriesList.map( country => { 
                                                if(country.id === shipmentAddress.country_id)  
                                                return country.full_name_english; }):'' }</span><br/>
                        <span>{ shipmentAddress.telephone}</span><br/>
                    </div>
                    :''}
                    
                </div>
                <div className="ship-via">
                    <div className="shipping-information-title">
                        <span>Shipping Method:</span>
                        <button onClick={ props.gotoCheckoutPage} className="action action-edit">
                            <span>edit</span>
                        </button>
                    </div>
                    <div className="shipping-information-content">
                        <span className="value">Shipping - {props.shipping_method_Details ? props.shipping_method_Details.method_title : ''}</span>
                    </div>
                </div>
            
          </React.Fragment >
    );
};

export default Shippinginfo;