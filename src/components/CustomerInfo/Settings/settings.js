import React, { Component } from 'react';
import Pagination from 'react-js-pagination';
import '../../../styles/AccountInfo.scss';
import AccountDashMenu from '../myAccountDashboardMenu';

class Settings extends Component {

    constructor(props) {
        super(props);
        this.state = {
            anonymise_check_agree : false,
            delete_check : false
        };
    }

    handleDeleteCheckboxChange =()=>{
        this.setState( { delete_check: !this.state.delete_check})
    }
    handleAnonymiseCheckboxChange =()=>{
        this.setState( { anonymise_check_agree : !this.state.anonymise_check_agree})
    }

    render() {

        return (
            <React.Fragment>
                <section className="page_banner">
                    <div className="rowsec-account" id="maincontent">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-md-4">
                                    <AccountDashMenu/>
                                </div>
                                <div className="col-md-8">
                                    <div className="frmsec full-form">
                                    <form className="form form-edit-account" action="#" method="post" id="personaldata" encType="multipart/form-data">
                                        <fieldset className="fieldset info">

                                            <legend className="legend"><span>Download personal data</span></legend>
                                            <p className="policy-content">Here you can download a copy of your personal data which we store for your account in CSV format.</p>
                                            <div className="field field-name-password required">
                                                <label className="label" htmlFor="password">
                                                    <span>Current Password</span>
                                                </label>

                                                <div className="control">
                                                    <input type="text" id="current_password" name="currentpassword" value="" title="Current Password" className="input-text required-entry" />
                                                </div>
                                            </div>
                                        </fieldset>  
                                            <div className="actions-toolbar">
                                                <div className="primary">
                                                    <button type="submit" className="action save primary" title="Download"><span>Download</span></button>
                                                </div>                                          
                                            </div>
                                    </form>

                                </div>
                                <div className="frmsecnext full-form">
                                    <form className="form form-edit-account" action="#" method="post" id="anonymise-personal-data" encType="multipart/form-data">
                                        <fieldset className="fieldset info">

                                            <legend className="legend"><span>Anonymise personal data</span></legend>
                                            <p className="policy-content">Anonymising your personal data means that it will be replaced with non-personal anonymous information and before that you will get your new login and password to your e-mail address. After this process, your e-mail address and all other personal data will be removed from the website.</p>
                                            <p className="control checkbox">
                                                <label className="accept addon">
                                                    <input type="checkbox" name="anonymise_check_agree" checked={this.state.anonymise_check_agree} onChange={this.handleAnonymiseCheckboxChange}/>
                                                    I understand and I want to delete my account                       
                                                </label>
                                            </p>
                                            {this.state.anonymise_check_agree ?
                                            <div className="field field-name-password required">
                                                <label className="label" htmlFor="password">
                                                    <span>Current Password</span>
                                                </label>

                                                <div className="control">
                                                    <input type="text" id="current_password" name="currentpassword" value="" title="Current Password" className="input-text required-entry" />
                                                </div>
                                            </div> : ''}
                                        </fieldset>  
                                            <div className="actions-toolbar">
                                                <div className="primary">
                                                    <button type="submit" className="action save primary" title="Proceed"><span>Proceed</span></button>
                                                </div>                                          
                                            </div>
                                    </form>

                                </div>
                                <div className="frmsecnext full-form padbt">
                                    <form className="form form-edit-account" action="#" method="post" id="form-validate" encType="multipart/form-data">
                                        <fieldset className="fieldset info">

                                            <legend className="legend"><span>Delete account</span></legend>
                                            <p className="policy-content">Request to remove your account, together with all your personal data, will be processed by our staff. Deleting your account will remove all the purchase history, discounts, orders, invoices and all other information that might be related to your account or your purchases. All your orders and similar information will be lost.You will not be able to restore access to your account after we approve your removal request.</p>
                                            <p className="control checkbox">
                                                <label className="accept addon">
                                                    <input type="checkbox" name="delete_check" checked={this.state.delete_check} onChange={this.handleDeleteCheckboxChange}/>
                                                    I understand and I want to delete my account                       
                                                    </label>
                                            </p>
                                            {this.state.delete_check ? 
                                                <div className="field field-name-password required">
                                                    <label className="label" htmlFor="password">
                                                        <span>Current Password</span>
                                                    </label>

                                                    <div className="control">
                                                        <input type="text" id="current_password" name="currentpassword" value="" title="Current Password" className="input-text required-entry" />
                                                    </div>
                                                </div> : ''}
                                        </fieldset>  
                                            <div className="actions-toolbar">
                                                <div className="primary">
                                                    <button type="submit" className="action save primary" title="Submit request"><span>Submit request</span></button>
                                                </div>                                          
                                            </div>
                                    </form>

                                </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
            </React.Fragment>
        );

    }

}

export default Settings;