import React, { Component } from 'react';
import Pagination from 'react-js-pagination';
import '../../../styles/AccountInfo.scss';
import AccountDashMenu from '../myAccountDashboardMenu';

class Subscription extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }


    render() {

        return (
            <React.Fragment>
                <section className="page_banner">
                    <div className="rowsec-account" id="maincontent">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-md-4">
                                    <AccountDashMenu/>
                                </div>
                                <div className="col-md-8">
                                    <div className="frmsec full-form">
                                    <form className="form form-edit-account" action="#" method="post" id="subscription" encType="multipart/form-data">
                                        <fieldset className="fieldset info">

                                            <legend className="legend"><span>Subscription option</span></legend>
                                            <div className="field choice">
                                                <input type="checkbox" name="is_subscribed" id="subscription" value="1" title="General Subscription" className="checkbox"/>
                                                <label for="subscription" className="label"><span>General Subscription</span></label>
                                            </div>
                                        </fieldset>  
                                            <div className="actions-toolbar">
                                                <div className="primary">
                                                    <button type="submit" className="action save primary" title="Save"><span>Save</span></button>
                                                </div>                                          
                                            </div>
                                    </form>

                                </div>
                                
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
            </React.Fragment>
        );

    }

}

export default Subscription;