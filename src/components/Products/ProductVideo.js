import React from 'react';
import '../../styles/ProductVideo.scss';
import { Player } from 'video-react';



const ProductVideo = (props) => {
	console.log('http://demo43.laraveldevloper.in/pub/media/catalog/product/video/'+props.product_video);
	return (
		   <>
		    <div className ="container-fluid product-video-panel">
                <div className = "row">
                    <div className="product-video-panel col-2"></div>
					<div className="product-center-panel col-8 pl-0 pr-0">
					<div className='player-wrapper'>
				       {/* <Player>
                       <source src={require('../../video/peptides-antioxidant-firming-daily-treatment.mp4')} />
                       </Player> */}
					    <video style={{ width:"100%"}}
							controls 
							src={('http://demo43.laraveldevloper.in/pub/media/catalog/product/video/'+ props.product_video)}
							 />
				      </div>
					</div>
					<div className="product-video-panel col-2"></div>
                </div>
		    </div>
		  

		   </>
		);
}

export default ProductVideo;
