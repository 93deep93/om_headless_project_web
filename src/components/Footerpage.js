import React, { Component } from 'react';
import '../styles/Footer.scss';
import {Link} from 'react-router-dom';
import DurationDisplay from 'video-react/lib/components/time-controls/DurationDisplay';


class  Footerpage extends Component  {
  constructor(props) {
    super(props);
    this.state ={
      email : '',
      error : '',
    }
  }

  setEmail=(e)=>{
    this.setState({email : e.target.value});
  }
  validateEmail=( email)=>{
    const regex_email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex_email.test(String(email).toLowerCase());
  }

  validate=() =>{
    let flag = false;
    let error = '';
    if( this.state.email === "" || this.state.email=== null || this.state.email === undefined){
      error = 'This is a required field.';
      flag = true;
    }else if( this.state.email !== "" || this.state.email !== null || this.state.email !== undefined){
      if(!this.validateEmail( this.state.email)){
        error = 'Please enter a valid email address (Ex: johndoe@domain.com).'
        flag = true;
      }
    }
    this.setState({ error : error});
    return flag;
  }

  subscribe=()=>{
    if(!this.validate()){

    }
  }
  render() {
    return (
      <React.Fragment>
      <div className="page-footer">
        <div className="container pb-4">
          <div className="row">
          <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12">
            <div className="footer-logo">
              <img src={require('../icons/aos-foot-logo.svg')}/>
            </div>  
            <div className="newsletter" style={{display: "none"}}>
              <label className="title">Sign Up for Our Newsletter:</label>        
              <div className=" ">
                  <input name="email" type="email" className="input-design" value={this.state.email} onChange={ this.setEmail} placeholder="Email Address"/>
                  <button className="subscribe-btn" onClick={this.subscribe}>
                    <span>  </span>
                  </button>
                </div> 
                {this.state.error ? <div className="error">{ this.state.error}</div> : ''}          
            </div>
          </div>
          <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12">
            <div className="social-link">
            <a  target="_blank" href="https://www.facebook.com/alliesofskin"><img src={require('../icons/facebook.png')} /></a>
            <a  target="_blank" href="https://www.instagram.com/alliesofskin/"><img src={require('../icons/instagram.svg')} height="32px;"/></a>
            </div>

            <div className="info-page">
            <ul>
            <li><Link to="/Contact">Contact Us</Link></li>
            <li><Link to="/TermOfService">Terms of Service</Link></li>
            <li><Link to="/Privacypolicy">Privacy Policy</Link></li>
            <li><Link to="/Faq">FAQs</Link></li>
            </ul>
            </div>

          </div>
          </div>
        </div>

      </div>
      </React.Fragment>
    );
  }
}

export default Footerpage;