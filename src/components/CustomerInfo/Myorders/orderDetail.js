import React, { Component } from 'react';
import Pagination from 'react-js-pagination';
import '../../../styles/AccountInfo.scss';
import AccountDashMenu from '../myAccountDashboardMenu';
import Addressinfo from '../assets/address_info'   
import {getUserMyOrdersApi} from '../../../account/customerInfoApi';
import { isAuthenticated } from '../../../account/Repository';
import { Redirect} from 'react-router-dom';
const month =[]

class OrderDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            orderDetails : this.props.location.state.orderDetails? this.props.location.state.orderDetails : ''
        };
    }

    componentDidMount(){
        if(!this.props.location.state.orderDetails && this.props.location.state.orderPlace){
            getUserMyOrdersApi().then((res)=>{
                let orderDetails = res.result.items.filter(x=> x.increment_id === this.props.location.state.orderPlaceData.orderNumber);
                if(orderDetails.length>0){
                    this.setState({ orderDetails : orderDetails[0]})
                }

            })
        }
    }

    formatDate(d) 
        {
          var date = new Date(d);

         if ( isNaN( date .getTime() ) ) 
         {  return d;
        }else
        {          
          return    new Date(date).toLocaleString('default', { month: 'long' }) +" "+ date.getDate()  + " " + date.getFullYear();
          }
            
         }

    render() {
        if (!isAuthenticated()) return (<Redirect to="/login" />);
        const {orderDetails} = this.state;
        if(orderDetails !=''){
            return (
                <React.Fragment>
                    <section className="page_banner">
                        <div className="rowsec-account" id="maincontent">
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-md-4">
                                        <AccountDashMenu/>
                                    </div>
                                    <div className="col-md-8">
                                        <div className="page-title-wrapper">
                                            <h1 className="page-title">
                                            <span className="base">Order # {orderDetails.increment_id}</span> </h1>
                                            <span className="order-status">Pending</span>
                                            <div className="order-date">
                                                {/* <span className="label">Order Date:</span> */}
                                                <date>{  this.formatDate(orderDetails.created_at)}</date>
                                            </div>
                                            <div className="actions-toolbar order-actions-toolbar"><div className="actions">
                                                <a href="#" className="action order">
                                                    {/* <span>Reorder</span> */}
                                                </a>
                                                <a href="#" className="action print" target="_blank" rel="noopener">
                                                    {/* <span>Print Order</span> */}
                                                </a>
                                            </div>
                                            </div>
                                        </div>
                                        <ul className="items order-links">
                                            <li className="nav item current"><strong>Items Ordered</strong></li>
                                        </ul>
                                        <div className="order-details-items ordered">
    
                                            <div className="order-title">
                                                <strong>Items Ordered</strong>
                                            </div>
    
                                            <div className="table-wrapper order-items">
                                                <table className="data table table-order-items" id="my-orders-table" summary="Items Ordered">
    
                                                    <thead>
                                                        <tr>
                                                            <th className="col name">Product Name</th>
                                                            <th className="col sku">SKU</th>
                                                            <th className="col price">Price</th>
                                                            <th className="col qty">Qty</th>
                                                            <th className="col subtotal">Subtotal</th>
                                                        </tr>
                                                    </thead>
                                                    {
                                                        orderDetails.items ? orderDetails.items.map(( item , index) =>{
                                                           return(
                                                            <tbody>
                                                                <tr id="order-item-row-20726">
                                                                    <td className="col name" data-th="Product Name">
                                                                        <strong className="product name product-item-name">{item.name}</strong>
                                                                    </td>
                                                                    <td className="col sku" data-th="SKU">{item.sku}</td>
                                                                    <td className="col price" data-th="Price">
    
                                                                        <span className="price-excluding-tax" data-label="Excl. Tax">
                                                                            <span className="cart-price">
                                                                                <span className="price">${item.price_incl_tax.toFixed(2)}</span></span>
    
                                                                        </span>
                                                                    </td>
                                                                    <td className="col qty" data-th="Qty">
                                                                        <ul className="items-qty">
                                                                            <li className="item">
                                                                                <span className="title">Ordered: </span>
                                                                                    <span className="content">{item.qty_ordered}</span>
                                                                            </li>
                                                                        </ul>
                                                                    </td>
                                                                    <td className="col subtotal" data-th="Subtotal">
    
                                                                        <span className="price-excluding-tax" data-label="Excl. Tax">
                                                                            <span className="cart-price">
                                                                            <span className="price">${item.row_total_incl_tax}</span>            </span>
    
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                           )
                                                       }) :''
                                                    }
                                                    
                                                    <tfoot>
                                                        <tr className="subtotal">
                                                            <th colspan="4" className="mark" scope="row">
                                                                Subtotal                    </th>
                                                            <td className="amount" data-th="Subtotal">
                                                            <span className="price">${  orderDetails.subtotal ?orderDetails.subtotal.toFixed(2) : '0.00'}</span>                    </td>
                                                        </tr>
                                                        <tr className="shipping">
                                                            <th colspan="4" className="mark" scope="row">
                                                                Shipping &amp; Handling                    </th>
                                                            <td className="amount" data-th="Shipping &amp; Handling">
                                                            <span className="price">${orderDetails.shipping_incl_tax? orderDetails.shipping_incl_tax.toFixed(2) : '0.00'}</span>                    </td>
                                                        </tr>
                                                        <tr className="grand_total">
                                                            <th colspan="4" className="mark" scope="row">
                                                                <strong>Grand Total</strong>
                                                            </th>
                                                            <td className="amount" data-th="Grand Total">
                                                            <strong><span className="price">${orderDetails.grand_total ? orderDetails.grand_total.toFixed(2): '0.00'}</span></strong>
                                                            </td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
    
    
                                            <div className="actions-toolbar">
                                                <div className="secondary">
                                                    <a className="action back" href="#">
                                                        <span>Back to My Orders</span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div className="block block-order-details-view">
                                                <div className="block-title">
                                                    <strong>Order Information</strong>
                                                </div>
                                                <div className="block-content">
                                                    <div className="box box-order-shipping-address">
                                                        <strong className="box-title"><span>Shipping Address</span></strong>
                                                        <div className="box-content">
                                                        {orderDetails.extension_attributes?
                                                           <Addressinfo billing_address ={orderDetails.extension_attributes.shipping_assignments[0].shipping.address}/>:
                                                           ''}
                                                        </div>
                                                    </div>
    
                                                    <div className="box box-order-shipping-method">
                                                        <strong className="box-title">
                                                            <span>Shipping Method</span>
                                                        </strong>
                                                        <div className="box-content">
                                                            {orderDetails.shipping_description}                            </div>
                                                    </div>
    
                                                    <div className="box box-order-billing-address">
                                                        <strong className="box-title">
                                                            <span>Billing Address</span>
                                                        </strong>
                                                        <div className="box-content">
                                                        {orderDetails.billing_address ?
                                                            <Addressinfo billing_address ={orderDetails.billing_address}/>:
                                                            ''}
                                                        </div>
                                                    </div>
                                                    <div className="box box-order-billing-method">
                                                        <strong className="box-title">
                                                            <span>Payment Method</span>
                                                        </strong>
                                                        <div className="box-content">
                                                            <dl className="payment-method">
                                                            <dt className="title">{ orderDetails.payment ? orderDetails.payment.additional_information[0] : ''}</dt>
                                                            </dl>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
    
                    </section>
                </React.Fragment>
            );
        }else{
            return <div className="text-center mt-5 mb-5">loading...</div>
        }
        

    }

}

export default OrderDetails;