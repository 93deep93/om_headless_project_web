import React, {Component} from 'react';
import './Login.scss';
import {Modal} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import { forgetPassword } from './Repository';

class Forgetpassword extends Component {

  constructor() {
        super();
        this.state = { email: '', message: '', error: '' };
      }

      handleClose = () => {
         this.setState({isPopup: false})
      }


      handleInputChange = (event) => 
                this.setState({[event.target.name]: event.target.value})
      forgetPassword = (event) => {
        event.preventDefault();
        forgetPassword(this.state)
          .then(token => {
            // window.location = '/';
           if(token.code === 200) {           
            this.setState({isPopup:true, message: "We've sent password reset instructions to your email. Check your inbox and follow the link.!", error: ''});
           }
          }).catch(err => {
            console.log(err)
          });
      }
 
 render () {

   let {isPopup, error, message} = this.state;
   
   return (
    <React.Fragment> 
        <div className="login-container container mt-5 mb-5">
            <div className="row">
               <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 ml-auto mr-auto forget_loginpswd">
                   <div className="customer-login">
                 
                       <form onSubmit={this.forgetPassword}>
                          <p>Please enter your email address below to receive a password reset link.</p>
                            
                            <div className="form_group">
                              <label for="email">Email</label>
                                <input type="email"  name="email" placeholder="" onChange={this.handleInputChange} />
                            </div> 

                            <input type="submit" value="Reset my password" />    
                             <span className="login-account"><Link to="/Login">Return to Login?</Link></span>    
                       </form>
                    </div>  
                </div>           
            </div> 
        </div>


        <Modal className="message_popup" show={isPopup} onHide={this.handleClose} animation={false}>
          <Modal.Header closeButton> </Modal.Header>
            <Modal.Body className= "text-center mt-5 mb-5">{message}</Modal.Body> 
        </Modal>
 
    </React.Fragment>
  );

 }
  
}

export default Forgetpassword;