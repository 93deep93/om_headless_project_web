import React, {useState} from 'react';

const ModelAlertDelete = ( props) => {  
    const dynammicModalClass = () => (props.showModelDelete ? 'block' : 'none');
    return (
        <div className="modal-alert" style={{ display : dynammicModalClass()}}>
            <span className="alert-close" onClick={ props.closeDeleteModel} title="Close Modal">×</span>
            <form className="modal-alert-content" action="#">
                <div className="container-alert">
                <h4>Delete Address</h4>
                <p>Are you sure you want to delete your Address?</p>
                <div className="clearfix">
                    <button type="button" onClick={ props.closeDeleteModel} className="cancelbtn">Cancel</button>
                    <button type="button"onClick={ props.deleteAddressConfirm} className="deletebtn">Delete</button>
                </div>
                </div>
            </form>
        </div>
    )
        
}
      
export default ModelAlertDelete;