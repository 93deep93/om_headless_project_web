import React, { Component } from 'react';
import { connect} from 'react-redux';
import '../../styles/rewardsPoint.scss';
import SwellRewardsPointView from './SwellRewardsPointView';

class SwellRewardsPoint extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }


    render() {

        // alert(this.props.basketProps.isLogin)

        return (
            <React.Fragment>
                <SwellRewardsPointView  basketProps={this.props.basketProps}/>
            </React.Fragment>
        );

    }

}

const mapStateToProps = state => ({
    basketProps:state.basketState
  })
  export default connect(mapStateToProps)(SwellRewardsPoint);