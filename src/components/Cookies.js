import React, { useState } from 'react';
import '../styles/CookiePopup.scss';

const CookiePopup = () => {

    const [ showCookies, SetShowCookies ] = useState( sessionStorage.getItem('aliesCookies') ? sessionStorage.getItem('aliesCookies') : true)

    return (
        <React.Fragment>
            {showCookies === true ? 
            <div data-role="gdpr-cookie-container">
                <div role="alertdialog" class="message global cookie am-cookie">
                    <div role="document" class="content" tabindex="0">
                        <p>We use cookies to help improve our services, make personal offers, and enhance your experience. If you do not accept optional cookies below, your experience may be affected. If you want to know more, please read the  <a href="/cookie-policy" target="_blank">Cookie Policy</a></p>
                        <div class="actions">
                            <button onClick={()=>{SetShowCookies(false); sessionStorage.setItem('aliesCookies', false)}} class="action allow primary cookiebar">
                                <span>Allow Cookies</span>
                            </button>

                            <button onClick={()=> {SetShowCookies(false); sessionStorage.setItem('aliesCookies', false)}} class="action disallow primary cookiebar">
                                <span>Disallow Cookies</span>
                            </button>

                        </div>
                    </div>
                </div>
            </div>:''}
        </React.Fragment>
    );
}

export default CookiePopup;