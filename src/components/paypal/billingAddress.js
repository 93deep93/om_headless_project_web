import React, {Fragment, Component} from 'react';
import {Form, Button, Tooltip } from 'react-bootstrap';
import { isAuthenticated, getProducts, pay } from '../../account/Repository';
// import Payment from './paypal/Payment';
// import PayPalBtn from './paypal/PayPalBtn';
import './BillingAddress.scss';
import { Redirect, Link } from 'react-router-dom';
import { connect } from 'react-redux';


class BillingAddress extends Component {

   constructor(props) {
      super(props);
        this.state = { 
          productItem: [], 
          total: 0,
          formData: {},
          errors:{ },
          validated: false,
          copy_address : false,
         }
      }

      componentDidMount(){
        // if(sessionStorage.getItem( 'shipmentAddress')){
          // console.log(sessionStorage.getItem( 'shipmentAddress'))
          // this.setState({ formData : sessionStorage.getItem( 'shipmentAddress')})  
        // } 
      }


      handleInputChangeText =(event) => {
        const { formData, errors} = this.state;
        formData[event.target.name] = event.target.value;
        errors[event.target.name] = '';
        this.setState ({formData: formData, errors : errors});
      }

      handleCheckChange=(event)=>{
          if(!this.state.copy_address){
            // set data in form data 
            if(this.props.shipmentAddress){
                const shipmentAddress = this.props.shipmentAddress;
                const formData = this.state.formData;
                formData.email= shipmentAddress.email;
                formData.fname= shipmentAddress.fname;
                formData.lname= shipmentAddress.lname;
                formData.address = shipmentAddress.address;
                formData.city = shipmentAddress.city;
                formData.company = shipmentAddress.company;
                formData.state = shipmentAddress.state;
                formData.country = shipmentAddress.country;
                formData.phone = shipmentAddress.phone;
                formData.postcode = shipmentAddress.postcode
                this.setState({ formData : formData});
            }
          }else{
              this.setState({ formData : {}});
          }
          this.setState({ copy_address  : !this.state.copy_address})
      }

      // pay = () => pay(this.state.formData).then(data => alert(data)).catch(err => console.log(err))
      pay = () => {
        if(!this.validate()){
          sessionStorage.setItem('shipmentAddress' ,  JSON.stringify(this.state.formData));
         this.props.history.push({
           pathname :'/Payment',
           state :{ shipmentAddress: this.state.formData}
         })
        }
      };

      
   render () {

    


  return (
    <React.Fragment>
     <section>

      <div >  
         <div className="row">

            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div className="billing_form">
              <div className = "shipping-information">
              <div className="ship-to">
                    <div className="shipping-information-title"> <span>Billing Address :</span></div>
                </div></div>

               
                    <Form  noValidate validated={this.state.validated}>
                        <Form.Group>
                            <Form.Check
                                name="copy_address"
                                label="Copy address data from shipping"
                                onChange={this.handleCheckChange}
                                isInvalid={!!errors.copy_address}
                                feedback={errors.copy_address}
                                inline
                               checked = {this.state.copy_address}
                            />
                        </Form.Group>
                      <Form.Group controlId="formBasicEmail" className="billing_info_detils">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control name="email" value={formData.email } type="email" onChange={this.handleInputChangeText} placeholder=""  isInvalid={!!errors.email}/>
                           <span className="fa fa-question"></span>
                           <Form.Control.Feedback type="invalid"> {errors.email} </Form.Control.Feedback>
                        <Form.Text className="text-muted">
                          You can create an account after checkout.
                        </Form.Text>
                      </Form.Group>

                      <Form.Group controlId="formBasicPassword">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control type="text" name="fname" value={formData.fname } placeholder="" onChange={this.handleInputChangeText} isInvalid={!!errors.fname}/>
                        <Form.Control.Feedback type="invalid"> {errors.fname} </Form.Control.Feedback>
                      </Form.Group>

                      <Form.Group controlId="formBasicLastName">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control type="text" name="lname" value={formData.lname } placeholder="" onChange={this.handleInputChangeText} isInvalid={!!errors.lname}/>
                        <Form.Control.Feedback type="invalid"> {errors.lname} </Form.Control.Feedback>
                      </Form.Group>

                      <Form.Group controlId="formBasicCompany">
                        <Form.Label>Company</Form.Label>
                        <Form.Control type="text" name="company" value={formData.company} placeholder="" onChange={this.handleInputChangeText} isInvalid={!!errors.company} />
                        <Form.Control.Feedback type="invalid"> {errors.company} </Form.Control.Feedback>
                      </Form.Group>

                      <Form.Group controlId="formBasicAddress">
                        <Form.Label>Street Address</Form.Label>
                        <Form.Control className="mb-4" type="text" name="address" value={formData.address} placeholder="" onChange={this.handleInputChangeText}  isInvalid={!!errors.address}/>
                        <Form.Control.Feedback type="invalid"> {errors.address} </Form.Control.Feedback>
                        <Form.Control className="mb-4" type="text" name="address" placeholder="" onChange={this.handleInputChangeText} />
                      </Form.Group>

                      <Form.Group controlId="formBasicCity">
                      <Form.Label>City</Form.Label>
                        <Form.Control type="text" name="city" value={formData.city} onChange={this.handleInputChangeText} isInvalid={!!errors.city} />
                        <Form.Control.Feedback type="invalid"> {errors.city} </Form.Control.Feedback>
                      </Form.Group>

                        <Form.Group controlId="formBasicState">
                          <Form.Label>State/Province</Form.Label>
                          <Form.Control type="text" name="state" value={ formData.state} placeholder="" onChange={this.handleInputChangeText} isInvalid={!!errors.state}/>
                          <Form.Control.Feedback type="invalid"> {errors.state} </Form.Control.Feedback>
                        </Form.Group>

                         <Form.Group controlId="formBasicCountry">
                          <Form.Label>Country/Province</Form.Label>
                          <Form.Control type="text" name="country" placeholder="" value={ formData.country} onChange={this.handleInputChangeText} isInvalid={!!errors.country}/>
                          <Form.Control.Feedback type="invalid"> {errors.country} </Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group controlId="formBasicPhone" className="billing_info_detils">
                          <Form.Label>Phone Number</Form.Label>
                          <Form.Control type="tel" name="phone" placeholder="" value={ formData.phone} onChange={this.handleInputChangeText} isInvalid={!!errors.phone}/>
                           <span className="fa fa-question"></span>
                           <Form.Control.Feedback type="invalid"> {errors.phone} </Form.Control.Feedback>
                        </Form.Group>
                      
                    </Form>
                 </div>
            </div>
         </div>
      </div> 
      
      </section>
    </React.Fragment>
  );


   }

     
}


const mapStateToProps = state => ({
  basketProps : state.basketState
})

export default connect(mapStateToProps) (BillingAddress);