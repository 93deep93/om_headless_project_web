import React, { useState } from 'react';
import styled from 'styled-components'
import {Link} from 'react-router-dom';
import { Button, Modal} from 'react-bootstrap';
import '../styles/WidgetPopup.scss';


const RewardPoint = (props) => {

	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);


		return (

		   <div className = "modal_reward_popup">	
			 <Button className="widget-popup" onClick={handleShow} >My Points</Button>

			 <Modal className="reward-popup" show={show} onHide={handleClose} animation={false}>
			        <Modal.Header closeButton>
			          <Modal.Title>Reward Points</Modal.Title>
			        </Modal.Header>
				        <Modal.Body>
	                        <div className = "row">
	                           <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
	                              <h4>EARN POINTS FOR ACTIONS</h4>

	                              <div className="earn_reward-point">
	                                 <div className="col-sm-12">
					                  <div className="row swell-auth-panel">
					                    
					                      <div className="col-xs-3 swell-auth-item-icon fa-stack fa-2x">
					                        <i className="fa fa-circle fa-stack-2x"></i>
					                         <i className="fa fa-heart fa-stack-1x fa-inverse primary-color"></i>
					                      </div>
					                    
					                     <div className="col-xs-9 swell-auth-item-details">
					                      <div className="swell-auth-item-title secondary-color">Refer a friend</div>
					                      <div className="swell-auth-item-description secondary-color">1000 Points</div>
					                    </div>
					                  </div>


			                       <div className="row swell-auth-panel">
				                    
				                      <div className="col-xs-3 swell-auth-item-icon fa-stack fa-2x">
				                        <i className="fa fa-circle fa-stack-2x"></i>
				                        <i className="fa fa-dollar fa-stack-1x fa-inverse primary-color"></i>
				                      </div>
				                    
				                    <div className="col-xs-9 swell-auth-item-details">
				                      <div className="swell-auth-item-title secondary-color">Make a purchase</div>
				                      <div className="swell-auth-item-description secondary-color">5 Points Per $1.00 Spent</div>
				                    </div>
			                    </div>

			                  <div className="row swell-auth-panel">
                    
		                      <div className="col-xs-3 swell-auth-item-icon fa-stack fa-2x">
		                        <i className="fa fa-circle fa-stack-2x"></i>
		                        <i className="fa fa-user fa-stack-1x fa-inverse primary-color"></i>
		                      </div>
		                    
		                    <div className="col-xs-9 swell-auth-item-details">
		                      <div className="swell-auth-item-title secondary-color">Create an account</div>
		                      <div className="swell-auth-item-description secondary-color">100 Points</div>
		                    </div>
                           </div>

			                  <div className="row swell-auth-panel">
			                    
			                      <div className="col-xs-3 swell-auth-item-icon fa-stack fa-2x">
			                        <i className="fa fa-circle fa-stack-2x"></i>
			                        <i className="fa fa-pencil fa-stack-1x fa-inverse primary-color"></i>
			                      </div>
			                    
			                    <div className="col-xs-9 swell-auth-item-details">
			                      <div className="swell-auth-item-title secondary-color">Write a Review</div>
			                      <div className="swell-auth-item-description secondary-color">500 Points</div>
			                    </div>
			                  </div>

					                </div>
	                              </div>

	                           </div>
	                           <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                   <h4>REDEEM POINTS FOR DISCOUNTS</h4>
                                  
                                   <div className="earn_reward-point">

                                     
					                <div className="col-sm-12">

					                  <div className="row swell-auth-panel">
					                      <div className="col-xs-3 swell-auth-item-icon fa-stack fa-2x">
					                        <i className="fa fa-circle fa-stack-2x"></i>
					                        <i className="fa fa-dollar fa-stack-1x fa-inverse primary-color"></i>
					                      </div>
					                    
					                    <div className="col-xs-9 swell-auth-item-details">
					                      <div className="swell-auth-item-title secondary-color">$5.00 Off</div>
					                      <div className="swell-auth-item-description secondary-color">500 Points</div>
					                    </div>
					                  </div>

					                  <div className="row swell-auth-panel">
					                      <div className="col-xs-3 swell-auth-item-icon fa-stack fa-2x">
					                        <i className="fa fa-circle fa-stack-2x"></i>
					                        <i className="fa fa-dollar fa-stack-1x fa-inverse primary-color"></i>
					                      </div>
					                    
					                    <div className="col-xs-9 swell-auth-item-details">
					                      <div className="swell-auth-item-title secondary-color">$10.00 Off</div>
					                      <div className="swell-auth-item-description secondary-color">1000 Points</div>
					                    </div>
					                  </div>


					                  <div className="row swell-auth-panel">
					                      <div className="col-xs-3 swell-auth-item-icon fa-stack fa-2x">
					                        <i className="fa fa-circle fa-stack-2x"></i>
					                        <i className="fa fa-dollar fa-stack-1x fa-inverse primary-color"></i>
					                      </div>
					                    
					                    <div className="col-xs-9 swell-auth-item-details">
					                      <div className="swell-auth-item-title secondary-color">$20.00 Off</div>
					                      <div className="swell-auth-item-description secondary-color">2000 Points</div>
					                    </div>
					                  </div>


					                  <div className="row swell-auth-panel">
					                      <div className="col-xs-3 swell-auth-item-icon fa-stack fa-2x">
					                        <i className="fa fa-circle fa-stack-2x"></i>
					                        <i className="fa fa-dollar fa-stack-1x fa-inverse primary-color"></i>
					                      </div>
					                    
					                    <div className="col-xs-9 swell-auth-item-details">
					                      <div className="swell-auth-item-title secondary-color">$30.00 Off</div>
					                      <div className="swell-auth-item-description secondary-color">3000 Points</div>
					                    </div>
					                  </div>

					                </div>
	                              </div>
	                           </div>

	                        </div>
					         
				        </Modal.Body>
			        <Modal.Footer>
			           <h3 className="text-center w-100 my-2">This Feature is Coming Soon</h3>
			          {/* <a href ="Login"> Log In </a>
			          <a href = "Create"> Create Account</a> */}
			        </Modal.Footer>
			      </Modal>
		   </div>	
		);

}

export default RewardPoint; 