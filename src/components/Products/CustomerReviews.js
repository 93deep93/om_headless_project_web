import React from 'react';
import { Link } from 'react-router-dom';
import '../../styles/CustomerReviews.scss';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';


const CustomerReviews = () => {

    return (
        <React.Fragment>

            <div className="container-fluid customer-review-section pl-5 pr-5">
                <div className="stamped-header-title"> Customer Reviews </div>
                <div className="row">
                    <div className="col-xl-6 col-lg-6 col-md-5 col-sm-12 co-12">
                        <div className="customer-review-left-section">
                            <div className="stamped-summary">
                                <span className="stamped-summary-num"> 4.8 </span>
                                <span className="stamped-summary-start">
                                    <i className="fa fa-star"></i>
                                    <i className="fa fa-star"></i>
                                    <i className="fa fa-star"></i>
                                    <i className="fa fa-star"></i>
                                    <i className="fa fa-star"></i>
                                </span>
                                <span className="stamped-summary-txt">Based on 263 Reviews</span>
                            </div>

                            <div className="stamped-summary-action">
                                <Link to="">Write a review</Link>
                                <Link to="">Ask a Question</Link>
                            </div>

                        </div>
                    </div>
                    <div className="col-xl-6 col-lg-6 col-md-7 col-sm-12 co-12">
                        <div className="customer-review-right-section">

                            <Tabs>
                                <TabList>
                                    <Tab>Reviewes (263) </Tab>
                                    <Tab> Questions (14) </Tab>
                                </TabList>

                                <TabPanel>

                                    <div className="card gedf-card">
                                        <div className="review-form">
                                            <div className="question-form-wrapper">
                                                <div className="stamped-form-message stamped-form-message-success">
                                                    <p>Thank you for posting a question!<br />Your question will appear on the site once we've answered it.</p></div>
                                                <form method="post" id="new-review-form" className="new-question-form stamped-visible">
                                                    <div className="spr-form-title stamped-form-title">Write a review</div>
                                                    <fieldset className="spr-form-contact stamped-form-contact">

                                                        <legend>Author</legend>
                                                        <div className="spr-form-contact-name stamped-form-contact-name">
                                                            <label className="spr-form-label stamped-form-label" forHtml="question_author_31">Name</label>
                                                            <input className="spr-form-input spr-form-input-text stamped-form-input stamped-form-input-text" id="question_author_31" type="text" name="name" value="" placeholder="Enter your name" required="" />
                                                        </div>
                                                        <div className="spr-form-contact-email stamped-form-contact-email">
                                                            <label className="spr-form-label stamped-form-label" forHtml="question_email_31">Email</label>
                                                            <input className="spr-form-input spr-form-input-email stamped-form-input stamped-form-input-email" id="question_email_31" type="email" name="email" value="" placeholder="john.smith@example.com" required="" />
                                                        </div>
                                                    </fieldset>
                                                    <fieldset className="stamped-form-review">
                                                        <legend>Rating</legend>
                                                        <div className="stamped-form-review-rating">
                                                            <label className="stamped-form-label" htmlFor="reviewRating">Rating</label>
                                                            <input type="text" id="reviewRating" name="reviewRating" required="" />

                                                            <div className="stamped-form-input stamped-starrating">
                                                                <a className="fa fa-star" ><span>1</span></a>
                                                                <a className="fa fa-star" ><span>2</span></a>
                                                                <a className="fa fa-star-o"><span>3</span></a>
                                                                <a className="fa fa-star-o"><span>4</span></a>
                                                                <a className="fa fa-star-o"><span>5</span></a>
                                                            </div>
                                                        </div>
                                                        <div className="stamped-form-review-title">
                                                            <label className="stamped-form-label" htmlFor="review_title_31">Title of Review</label>
                                                            <input className="stamped-form-input stamped-form-input-text" id="review_title_31" type="text" name="reviewTitle" required="" value="" placeholder="Give your review a Title" />
                                                        </div>
                                                        <div className="stamped-form-review-body">
                                                            <label className="stamped-form-label" htmlFor="review_body_31">How was your overall experience?</label>
                                                            <div className="stamped-form-input">
                                                                <textarea className="stamped-form-input stamped-form-input-textarea" id="review_body_31" name="reviewMessage" required="" rows="3" maxlength="5000"></textarea>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                    
                                                    <fieldset className="spr-form-actions stamped-form-actions">
                                                        <legend>Submit</legend>
                                                        <input type="submit" className="stamped-button stamped-button-primary button button-primary btn btn-primary" value="Submit" />
                                                    </fieldset>
                                                </form>
                                            </div>
                                        </div>
                                        <div className="card-header">
                                            <div className="d-flex justify-content-between align-items-center">
                                                <div className="d-flex justify-content-between align-items-center">
                                                    <div className="mr-2">
                                                        <div className="stamped-review-avatar-content">EC</div>
                                                    </div>
                                                    <div className="ml-2">
                                                        <div className="h5 m-0">EMILY C <span>Verified Buyer</span></div>
                                                        <div className="h7 text-muted">United States</div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="dropdown">
                                                        <span> 26/06/2020 </span>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div className="card-body">

                                            <a className="card-link" href="#">
                                                <h5 className="card-title">FAVORITE NEW CLEANSER!</h5>
                                            </a>

                                            <p className="card-text">
                                                My first time using this cleanser was great! I noticed a difference to my skin right away
                                            </p>
                                        </div>
                                        <div className="card-footer">
                                            <a href="#" className="card-link"><i className="fa fa-mail-forward"></i> Share</a>
                                            <span className="float-right social">
                                                <span className="stamped-rating-holder">Was this review helpful?</span>
                                                <span className="thumb"><i className="fa fa-thumbs-up"></i> 1 </span>
                                                <span><i className="fa fa-thumbs-down"></i> 0 </span>
                                            </span>
                                        </div>
                                    </div>


                                    <div className="card gedf-card">

                                        <div className="card-header">
                                            <div className="d-flex justify-content-between align-items-center">
                                                <div className="d-flex justify-content-between align-items-center">
                                                    <div className="mr-2">
                                                        <div className="stamped-review-avatar-content">JT</div>
                                                    </div>

                                                    <div className="ml-2">
                                                        <div className="h5 m-0">JACQUELINE T <span>Verified Buyer</span></div>
                                                        <div className="h7 text-muted">Singapore</div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="dropdown">
                                                        <span> 26/06/2020 </span>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div className="card-body">

                                            <a className="card-link" href="#">
                                                <h5 className="card-title">AVERAGE CLEANSER</h5>
                                            </a>

                                            <p className="card-text">
                                                My first time using this cleanser was great! I noticed a difference to my skin right away
                                                One thing to note is that is doesn't lather into a foam
                                            </p>
                                        </div>
                                        <div className="card-footer">
                                            <a href="#" className="card-link"><i className="fa fa-mail-forward"></i> Share</a>
                                            <span className="float-right social">
                                                <span className="stamped-rating-holder">Was this review helpful?</span>
                                                <span className="thumb"><i className="fa fa-thumbs-up"></i> 1 </span>
                                                <span><i className="fa fa-thumbs-down"></i> 0 </span>
                                            </span>
                                        </div>
                                    </div>

                                </TabPanel>

                                <TabPanel>
                                    <div className="card gedf-card">
                                        <div className="question-form">
                                            <div className="question-form-wrapper">
                                                <div className="stamped-form-message stamped-form-message-success">
                                                    <p>Thank you for posting a question!<br />Your question will appear on the site once we've answered it.</p></div>
                                                <form method="post" id="new-question-form" className="new-question-form stamped-visible">
                                                    <div className="spr-form-title stamped-form-title">Ask a Question</div>
                                                    <fieldset className="spr-form-contact stamped-form-contact">

                                                        <legend>Author</legend>
                                                        <div className="spr-form-contact-name stamped-form-contact-name">
                                                            <label className="spr-form-label stamped-form-label" forHtml="question_author_31">Name</label>
                                                            <input className="spr-form-input spr-form-input-text stamped-form-input stamped-form-input-text" id="question_author_31" type="text" name="name" value="" placeholder="Enter your name" required="" />
                                                        </div>
                                                        <div className="spr-form-contact-email stamped-form-contact-email">
                                                            <label className="spr-form-label stamped-form-label" forHtml="question_email_31">Email</label>
                                                            <input className="spr-form-input spr-form-input-email stamped-form-input stamped-form-input-email" id="question_email_31" type="email" name="email" value="" placeholder="john.smith@example.com" required="" />
                                                        </div>
                                                    </fieldset>
                                                    <fieldset className="spr-form-review stamped-form-review">
                                                        <legend>Question</legend>
                                                        <div className="spr-form-review-body stamped-form-review-body">
                                                            <label className="spr-form-label stamped-form-label" forHtml="question_body_31">Question</label>
                                                            <div className="spr-form-input stamped-form-input">
                                                                <textarea className="spr-form-input spr-form-input-textarea stamped-form-input stamped-form-input-textarea" id="question_body_31" name="reviewBody" rows="3" maxLength="1000" placeholder="Write your question here" required=""></textarea>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                    <fieldset className="spr-form-actions stamped-form-actions">
                                                        <legend>Submit</legend>
                                                        <input type="submit" className="stamped-button stamped-button-primary button button-primary btn btn-primary" value="Submit Question" />
                                                    </fieldset>
                                                </form>
                                            </div>
                                        </div>
                                        <div className="card-header">
                                            <div className="d-flex justify-content-between align-items-center">
                                                <div className="d-flex justify-content-between align-items-center">
                                                    <div className="mr-2">
                                                        <div className="stamped-review-avatar-content">EC</div>
                                                    </div>
                                                    <div className="ml-2">
                                                        <div className="h5 m-0">EMILY C </div>

                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="dropdown">
                                                        <span> 26/06/2020 </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="card-body">
                                            <a className="card-link" href="#">
                                                <h5 className="card-title">ALLIES OF SKIN</h5>
                                            </a>
                                            <p className="card-text">
                                                My first time using this cleanser was great! I noticed a difference to my skin right away
                                            </p>
                                        </div>
                                    </div>

                                </TabPanel>

                            </Tabs>
                        </div>
                    </div>
                </div>

            </div>

        </React.Fragment>
    );
}

export default CustomerReviews;