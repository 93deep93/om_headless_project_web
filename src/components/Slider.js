import React, {Component} from 'react';
import '../styles/Home.scss';
import Carousel from 'react-bootstrap/Carousel';
import { Link } from 'react-router-dom';

class Slider extends Component {

	constructor(props) {
            super(props);
            this.state = {
                index: 1,  
               
                nextIcon: <span className="fa fa-angle-right"></span>,
                prevIcon: <span className="fa fa-angle-left"></span>
            }
        }

        handleSelect = (selectedIndex, e) => {
            this.setState({
                index: selectedIndex
                
            });
        }

   render() {
 
	const {nextIcon,prevIcon}=this.state;
    return (
      <React.Fragment> 
        <div className = "vogue-section">
			<Carousel nextIcon ={nextIcon} prevIcon={prevIcon}  index={this.state.index} direction={this.state.direction} onSelect={this.handleSelect}>
			 
			  <Carousel.Item>
			     <img src={require('../images/elegraph.png')}/>
			      <Link to ="">Allies Of Skin Molecular Silk Amino Hydrating Cleanser FEAT. Molecular Silk Amino Hydrating Cleanser</Link>		    
			  </Carousel.Item>
			  
			  <Carousel.Item>			    
			      <img src={require('../images/elle.png')}/>
			       <Link to ="">Allies Of Skin Molecular Silk Amino Hydrating Cleanser FEAT. Molecular Silk Amino Hydrating Cleanser</Link>
			  </Carousel.Item>

			  <Carousel.Item>
			      <img src={require('../images/vogue.png')}/>
			       <Link to ="">Allies Of Skin Molecular Silk Amino Hydrating Cleanser FEAT. Molecular Silk Amino Hydrating Cleanser</Link>
			  </Carousel.Item>

			</Carousel>
			<div className="view-features"><Link to ="Press">View all Press</Link></div>
		</div>
			 
    </React.Fragment> 
  );
}
}

export default Slider;