import React, { Component } from 'react';
import '../styles/Home.scss';
import {Link} from 'react-router-dom';
import { HTTP } from '../Utils';
import Slider from './Slider';
class Homebanner extends Component {

   constructor(props){
    super(props);
    this.state={
      homepage:[],
      isLoaded:false,
    }
  }


  componentDidMount(){

    return HTTP().get('/api/catalog/vue_storefront_catalog/cms_page/_search?from=0&request=%7B%7D&size=50&sort=')
    .then(res => {
        const homepage = res.data.hits.hits.reduce((result, set, index)=>{
          result.push(set._source);
          return result;
        },[]);
        console.log('my page content', homepage);


            this.setState({ homepage });
          })
          .catch(e => { 
             console.log('Error:' + e)
          })
            
        }

   

  render () {

    let homepage = this.state;
    console.log(`home page data`, homepage);

   
    return (
    <React.Fragment> 
    
    <div className="home_banner">
       <img className="w-100 h-100" src={require('../images/slider1.jpg')}/>
      <div className="banner_container">
           <h1>WHO SAYS<br/> YOU CANT DO IT ALL </h1>
           <p>Our new Molecular Silk Amino Hydrating Cleanser is a pH 5.5, sulfate-free universal cleanser that infuses skin with essential moisture and nutrients, as it removes daily grime and makeup.</p>
          <Link to ="Shop">Shop Now</Link>
      </div>

    </div>

 
    </React.Fragment> 
  );

  }
 
}

export default Homebanner;
