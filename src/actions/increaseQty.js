import { INCREASE_QUANTITY,SET_SPINNER_STARTED, SET_SPINNER_COMPLETED} from './types';
import { createCart, AddToCart} from '../account/Repository';

export const increaseQty = (product) => {
	return dispatch => {
		// createCart(product)
		// .then(res => {
			dispatch({ type : SET_SPINNER_STARTED});
		// if(res.code === 200) { 
			let productDetails ={};
			productDetails.sku =  product.sku;
			productDetails.quoteId= sessionStorage.getItem('cart-id');
			productDetails.qty= (product.qty ||product.numbers)+1;
			if(product.item_id)
				productDetails.item_id = product.item_id;
			AddToCart(productDetails , sessionStorage.getItem('cart-id') )
			.then(res => {
			if(res.code === 200) { 
					dispatch({
						type: INCREASE_QUANTITY,
						payload: product
					}); 
					dispatch({ type : SET_SPINNER_COMPLETED});	
				}
			}).catch(err => {
				if( err.response && err.response.data.result)
				alert(err.response.data.result)
			});
		// }
		// }).catch(err => {
		// console.log(err)
		// });
	}
}