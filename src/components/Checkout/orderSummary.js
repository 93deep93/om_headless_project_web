import React, {Fragment, Component} from 'react';
import {Form, Button, Tooltip, Accordion, Card } from 'react-bootstrap';
const OrderSummary =(props)=>{
 
        return (
            <React.Fragment>
              <Accordion defaultActiveKey="0">
                <Card >
                  <Accordion.Toggle as={Card.Header} eventKey="0">
                  <div className="itemin_cart"><span>{props.products.length > 1 ? (props.products.length +' Items'): (props.products.length +' Item')} in Cart</span></div>
                  </Accordion.Toggle>
                  <Accordion.Collapse eventKey="0">
                    <Card.Body>
                    <div className="order_section">
                      {
                      props.products ? 
                      props.products.map(( product, index)=>{
                        return(
                          <div className="order-details col-12">
                            <div className="row">
                              <div className="col-3">
                                <img src={'http://139.180.155.160/img/310/300/resize'+ product.image} />
                              </div>
                              <div className="col-9">
                                  <h4 className="product_title">
                                      {product.name}
                                      <span className="product_price">{'$'+product.price.toFixed(2) }</span>
                                  </h4>
                                  <span className="product_quantity">Qty :  {product.numbers || product.qty} </span>
                                  {/* <p className="note">{product.amasty_preorder_note}</p> */}
                              </div>
                          </div>
                        </div>
                        )
                      })
                      :''
                      }</div>
                    {/* <div className="container-fluid">
                      <div className="order-summary checkout-summary">
                        <h2 className="text-center">Summary</h2>
                        <div className="col-12">
                          <div className="row total">
                            <div className="col-6 cart-totals">
                              <span className="order-price-name">Sub Total</span>
                              </div>
                            <div className="col-6 cart-totals text-right">
                            <span className="order-price-value">1000</span>
                            </div>
                          </div>

                            <div className="row total mb-5">
                            <div className="col-6 cart-totals">
                              <span className="order-price-name">Order Total</span>
                              </div>
                            <div className="col-6 cart-totals text-right">
                            <span className="order-price-value">{'1000'}</span>
                            </div>
                          </div>
                      </div>
                    </div>
                    </div> */}
                    </Card.Body>
                  </Accordion.Collapse>
                </Card>
              </Accordion>
            </React.Fragment >
        );
}


export default OrderSummary;