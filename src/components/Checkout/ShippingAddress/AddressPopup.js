import React, {useState, useRef, Component} from 'react';
import {Form, Button, Tooltip, Accordion, Card } from 'react-bootstrap';
// import '../../styles/Checkout.scss';
import { Redirect, Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import ShippingForm from '../ShippingForm/shippingForm';
import countriesList from '../../../data/countries_reagions.json';
import equal from 'fast-deep-equal';
const AddAddress = ( props) => {
    const [activeTab, setIactiveTabs] = useState('description');

    const childRef = useRef(null);
  
    const dynammicModalClass = () => (props.isShow ? { display: 'block' } : { display: 'none' });

    const shipHere=()=>{
      if(!childRef.current.validate()){
        props.setShippingAddress( childRef.current.getMyState()) ;
      }
      
    }
  
    return  (
  
      <div className="modal classshow"  style={dynammicModalClass()} >
        <div className="modal-dialog  modal-lg addaddress" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button
                onClick={ props.closeModal}
                style={{ color: '#000' }}
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">X</span>
              </button>
              <h3>Shipping Address</h3>
            </div>
  
            <div className="modal-body">
            <ShippingForm ref={childRef} setShippingMethod={()=>{}}/>
            </div>
            <div className="modal-footer">
                <button type="button" onClick={ props.closeModal} className="action secondary action-hide-popup" data-dismiss="modal">Cancel</button>
                <button type="button" onClick={shipHere} className="action primary action-save-address">Ship Here</button>
            </div>
          </div>
        </div>
      </div>
    );
  };
  
  export default AddAddress;