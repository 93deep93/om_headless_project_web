import React, {Fragment, Component} from 'react';
import {Form, Button, Tooltip, Accordion, Card } from 'react-bootstrap';
// import '../../styles/Checkout.scss';
import { Redirect, Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import countriesList from '../../../data/countries_reagions.json';
import equal from 'fast-deep-equal';
import { setUserProfileApi, isAuthenticated, getProducts, pay } from '../../../account/Repository';
import ShippingMethod from '../shippingMethod';
class ShippingForm extends Component {

   constructor(props) {
      super(props);
      this.state = { 
        productItem: [], 
        total: 0,
        formData: {},
        errors:{ },
        validated: false,
        state_arr:[],
        countriesList : countriesList,
        cartCost:'', products:[],
        email_desibled : true,
        cart_id : sessionStorage.getItem('cart-id'),
        user_info_address: localStorage.getItem('user-info-allies') ? 
          JSON.parse(localStorage.getItem('user-info-allies')).isUserInfo ?
            JSON.parse(localStorage.getItem('user-info-allies')).isUserInfo.addresses
            : JSON.parse(localStorage.getItem('user-info-allies')).addresses 
            : [],

            user_info: localStorage.getItem('user-info-allies') ? 
          JSON.parse(localStorage.getItem('user-info-allies')).isUserInfo ?
            JSON.parse(localStorage.getItem('user-info-allies')).isUserInfo
            : JSON.parse(localStorage.getItem('user-info-allies')) 
            : '',
       }
       
      
      }

      componentDidMount(){

        if( localStorage.getItem('x-access-token') ){
          if( this.state.user_info== ''){
            setUserProfileApi().then(res => {
              let user_info = res.result;
              this.setState({user_info : user_info});
              const { formData} = this.state;
              formData.email = user_info.email;
              this.setState({ formData : formData });
            })
          }else{
            const { formData} = this.state;
            formData.email = this.state.user_info.email;
            this.setState({ formData : formData });
            
          }
          if(this.props.address_exists){
            this.setState({ formData: this.props.address_exists});
          }
        }else if( window.location.href.includes('/Checkout')){
          if(this.props.previous_checkout_shipping_data && Object.keys(this.props.previous_checkout_shipping_data).length > 0){
              this.setState({ formData :  this.props.previous_checkout_shipping_data.shippingAddress});
          }
          console.log( this.props.shippingPropsData);
          this.setState({ email_desibled : false})
        }else if(window.location.href.includes('/Payment')){
          const { formData} = this.state;
              formData.email = this.props.shippingEmail;
              this.setState({ formData : formData });
        }

        

        
      }

      handleInputChangeText =(event) => {
        const { formData, errors} = this.state;
        formData[event.target.name] = event.target.value;
        errors[event.target.name] = '';
        this.setState ({formData: formData, errors : errors});
        if(event.target.name === 'country_id'){
          formData.state = '';
          let stateArr = this.state.countriesList.filter((data)=> data.id=== event.target.value).length>0? this.state.countriesList.filter((data)=> data.id=== event.target.value)[0].available_regions : [];
          this.setState({ formData: formData, state_arr : stateArr}) //set regions
          this.props.setShippingMethod( event.target.value);
        }
      }

      handleInputChangeState=(e)=>{
        const formData = this.state.formData;
        let region_info = this.state.state_arr.filter(x=> x.code === e.target.value);
        if( region_info.length>0){
          region_info = region_info[0]; 
          formData.region = region_info.name;
          formData.region_code = region_info.code;
          formData.region_id = region_info.id;
        }else{
          formData.region = '';
          formData.region_code = '';
          formData.region_id = '';
        }
        formData.state = e.target.value;
        const errors= this.state.errors;
        errors[e.target.name] = '';
        this.setState({ formData : formData, errors : errors});
      }

      validateEmail=( email)=>{
        const regex_email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex_email.test(String(email).toLowerCase());
      }

      validate=() =>{
        let flag = false;
        const { errors, formData} = this.state;
        if( formData.email === "" || formData.email=== null || formData.email === undefined){
          errors.email = 'This field is required';
          flag = true;
        }else if( formData.email !== "" || formData.email !== null || formData.email !== undefined){
          if(!this.validateEmail( formData.email)){
            errors.email = 'Please enter valid email id !'
            flag = true;
          }
        }
        if( formData.firstname === "" || formData.firstname=== null || formData.firstname === undefined){
          errors.firstname = 'This field is required';
          flag = true;
        }
        if( formData.lastname === "" || formData.lastname=== null || formData.lastname === undefined){
          errors.lastname = 'This field is required';
          flag = true;
        }
        if( formData.company === "" || formData.company=== null || formData.company === undefined){
          errors.company = 'This field is required';
          flag = true;
        }
        if( formData.address === "" || formData.address=== null || formData.address === undefined){
          errors.address = 'This field is required';
          flag = true;
        }
        if( formData.city === "" || formData.city=== null || formData.city === undefined){
          errors.city = 'This field is required';
          flag = true;
        }
        if( formData.state === "" || formData.state=== null || formData.state === undefined){
          errors.state = 'This field is required';
          flag = true;
        }
        if( formData.country_id === "" || formData.country_id=== null || formData.country_id === undefined){
          errors.country_id = 'This field is required';
          flag = true;
        }
        if( formData.telephone === "" || formData.telephone=== null || formData.telephone === undefined){
          errors.telephone = 'This field is required';
          flag = true;
        }
        if( formData.postcode === "" || formData.postcode=== null || formData.postcode === undefined){
          errors.postcode = 'This field is required';
          flag = true;
        }
        
        this.setState({ errors : errors});
        return flag;
      }

      getMyState=()=>{
        return this.state.formData;
      }

      render() {
        const {  errors, formData, validated, email_desibled} =  this.state;

        return (
            <React.Fragment>
                    <Form  noValidate validated={this.state.validated}>
                       <Form.Group controlId="formBasicEmail" >   {/*className="info_detils" */}
                        <Form.Label>Email address</Form.Label>
                        <Form.Control disabled ={ email_desibled } name="email" type="email" value={formData.email} onChange={this.handleInputChangeText} placeholder=""  isInvalid={!!errors.email}/>
                           {/* <span className="fa fa-question"></span> */}
                           <Form.Control.Feedback type="invalid"> {errors.email} </Form.Control.Feedback>
                        <Form.Text className="text-muted">
                          You can create an account after checkout.
                        </Form.Text>
                      </Form.Group>
                        
                      <Form.Group controlId="formBasicPassword">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control type="text" name="firstname" value={formData.firstname} placeholder="" onChange={this.handleInputChangeText} isInvalid={!!errors.firstname}/>
                        <Form.Control.Feedback type="invalid"> {errors.firstname} </Form.Control.Feedback>
                      </Form.Group>

                      <Form.Group controlId="formBasicLastName">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control type="text" name="lastname" value={formData.lastname} placeholder="" onChange={this.handleInputChangeText} isInvalid={!!errors.lastname}/>
                        <Form.Control.Feedback type="invalid"> {errors.lastname} </Form.Control.Feedback>
                      </Form.Group>

                      <Form.Group controlId="formBasicCompany">
                        <Form.Label>Company</Form.Label>
                        <Form.Control type="text" name="company" value={formData.company} placeholder="" onChange={this.handleInputChangeText} isInvalid={!!errors.company} />
                        <Form.Control.Feedback type="invalid"> {errors.company} </Form.Control.Feedback>
                      </Form.Group>

                      <Form.Group controlId="formBasicAddress">
                        <Form.Label>Street Address</Form.Label>
                        <Form.Control className="mb-45" type="text" name="address" value={formData.address} placeholder="" onChange={this.handleInputChangeText}  isInvalid={!!errors.address}/>
                        <Form.Control.Feedback type="invalid"> {errors.address} </Form.Control.Feedback>
                        <Form.Control className="mb-4" type="text" name="address2" placeholder="" onChange={this.handleInputChangeText} />
                      </Form.Group>

                      <Form.Group controlId="formBasicCity">
                      <Form.Label>City</Form.Label>
                        <Form.Control type="text" name="city" value={formData.city} onChange={this.handleInputChangeText} isInvalid={!!errors.city} />
                        <Form.Control.Feedback type="invalid"> {errors.city} </Form.Control.Feedback>
                      </Form.Group>

                        <Form.Group controlId="formBasicState">
                          <Form.Label>State/Province</Form.Label>
                          { ( this.state.state_arr === null || this.state.state_arr.length===0)?
                            <Form.Control type="text" name="state" placeholder="" value={ formData.state} onChange={this.handleInputChangeText} isInvalid={!!errors.state}/>:
                            <Form.Control as="select" name="state" placeholder="" onChange={this.handleInputChangeState} isInvalid={!!errors.state}>
                              <option value="">Please select a region, state or province</option>
                              { this.state.state_arr.map( ( state, index) => {
                                return <option value = {state.code}>{state.name}</option>
                              })}
                            </Form.Control>
                            }
                          <Form.Control.Feedback type="invalid"> {errors.state} </Form.Control.Feedback>
                        </Form.Group>

                         <Form.Group controlId="formBasicCountry">
                          <Form.Label>Country/Province</Form.Label>
                          <Form.Control as="select" name="country_id" placeholder="" onChange={this.handleInputChangeText} isInvalid={!!errors.country_id}>
                          <option value="">Select Country</option>
                          { countriesList.map( ( country, index) => {
                            return <option value = {country.id}>{country.full_name_english}</option>
                          })}
                          </Form.Control>
                          <Form.Control.Feedback type="invalid"> {errors.country_id} </Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group controlId="formBasicPhone" >
                          <Form.Label>Phone Number</Form.Label>
                          <Form.Control type="tel" name="telephone" value={formData.telephone} onChange={this.handleInputChangeText} isInvalid={!!errors.telephone}/>
                           {/* <span className="fa fa-question"></span> */}
                           <Form.Control.Feedback type="invalid"> {errors.telephone} </Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group controlId="formBasicAddress">
                        <Form.Label>Post Code</Form.Label>
                        <Form.Control className="mb-4" type="text" name="postcode" value={formData.postcode} onChange={this.handleInputChangeText}  isInvalid={!!errors.postcode}/>
                        <Form.Control.Feedback type="invalid"> {errors.postcode} </Form.Control.Feedback>
                      </Form.Group>
                    </Form> 
            </React.Fragment >
        );

    }

}


export default ShippingForm;