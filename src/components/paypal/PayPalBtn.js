import React from 'react';
import { PayPalButton } from "react-paypal-button-v2";
class PayPalBtn extends React.Component {
    render() {
      const { amount, onSuccess, currency } = this.props;
        return (
         <PayPalButton 
           amount={amount}
            currency={currency} 
            onSuccess={(details, data) => onSuccess(details, data)}
              options={{
                clientId: "AR4QmiLo_pYFTLmNUArk2QbO3-FVdeoOJxd7mCfPqZOvhECIU6JnNS0awZYMyRkhK8onIrHNqzKezX32"
              }}
          />
        );
    }
}
export default PayPalBtn;