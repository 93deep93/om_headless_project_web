import React from 'react';
const Description = (description) =>{
    return(
        <div className="tab-text">
            <h3 className="pophead">MOLECULAR SILK AMINO HYDRATING CLEANSER</h3>
            <div dangerouslySetInnerHTML={{__html: description.description}}></div>
            
        </div>
    )
} 

export default Description