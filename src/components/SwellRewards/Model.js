import React, { useState, useEffect } from 'react';

const Modal = () => {
  const [isShown, setIsShown] = useState(true);

  const showModal = () => {
    setIsShown(true);
  };

  const closeModal = () => {
    setIsShown(false);
  };

  const dynammicModalClass = () => (isShown ? { display: 'block' } : '');

  useEffect(() => {
    if (!sessionStorage.popupModal) {
      const timer = setTimeout(() => {
        setIsShown(false);
        sessionStorage.popupModal = 1;
      }, 3000);

      return () => clearTimeout(timer);
    }
  }, []);

  // return isShown ? <h3>Modal content</h3> : null;
  return isShown ? (

    <div className="modal " style={dynammicModalClass()} id="channelModal">
      <div className="modal-dialog modal-dialog-slideout" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <button
              onClick={closeModal}
              style={{ color: '#fff' }}
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div className="modal-body">
            <div className="row">
              <div className="col-12">
                <div className="pouptext">
                <h3>MOLECULAR SILK AMINO HYDRATING CLEANSER</h3>
                <p>A pH 5.5, sulfate-free universal cleanser that is supercharged with a hydrating blend of Antioxidants, 
                Phospholipids and Hydrators like Silk Amino Acids, Hyaluronic Acid, 
                Organic Safflower + Moringa Oil to gently remove dirt, 
                grime and make-up without stripping the skin.</p> 
                <p>Silk Amino Acids is a potent antioxidant that provides trace nutrients and nourishes the skin. 
                <p>It functions as a humectant that draws water and enhances moisture absorption to revitalize and hydrate skin cells. </p>
                <p>Organic Safflower Oil and Moringa Oil break down grime and impurities while nourishing the skin with excellent reparative benefits. 
                Ethylated L-Ascorbic Acid helps brighten dull skin, while Lecithin Phospholipids forms a protective film on the skin's surface to seal in essential moisture, 
                keeping the skin healthy and supple. With daily use, skin feels silkier with a more refined texture, and complexion looks brighter.</p>
                </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  ) : null;
};

export default Modal;
