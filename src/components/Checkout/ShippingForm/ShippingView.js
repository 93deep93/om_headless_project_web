import React, {Fragment, Component} from 'react';
import {Form, Button, Tooltip, Accordion, Card } from 'react-bootstrap';
// import '../../styles/Checkout.scss';
import { Redirect, Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import countriesList from '../../../data/countries_reagions.json';
import equal from 'fast-deep-equal';
import { getShipmentMethods, isAuthenticated, getProducts, pay } from '../../../account/Repository';
import ShippingMethod from '../shippingMethod';
import ShippingForm from './shippingForm';
class ShippingView extends Component {

   constructor(props) {
      super(props);
      this.child = React.createRef();
      this.state = { 
        productItem: [], 
        total: 0,
        formData: {},
        errors:{ },
        validated: false,
        selected_shipping_method : '',
        selected_shipping_method_error : '',
        shipping_method_data : [],
        cart_id : sessionStorage.getItem('cart-id'),
        user_info_address: localStorage.getItem('user-info-allies') ? 
          JSON.parse(localStorage.getItem('user-info-allies')).isUserInfo ?
            JSON.parse(localStorage.getItem('user-info-allies')).isUserInfo.addresses
            : JSON.parse(localStorage.getItem('user-info-allies')).addresses 
            : [],
        }
      }

      UNSAFE_componentWillMount(){
        if(this.props.previous_checkout_shipping_data && Object.keys(this.props.previous_checkout_shipping_data).length > 0){
          this.setShippingMethod( this.props.previous_checkout_shipping_data.shippingAddress.country_id);
          this.setState({ selected_shipping_method : this.props.previous_checkout_shipping_data.shipping_method_code });
        }
      }

      handleShippingMethodOnChange =( val)=>{
        this.setState({ selected_shipping_method : val , selected_shipping_method_error: '' });
      }

      setShippingMethod =(country_code)=>{
        getShipmentMethods( { country_code : country_code , cart_id : this.state.cart_id })
        .then(res => {
          if(res.code === 200) {   
            this.setState({ shipping_method_data : res.result });
          }
        }).catch(err => {
          console.log(err)
        });
      }

      validateEmail=( email)=>{
        const regex_email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex_email.test(String(email).toLowerCase());
      }

      validate=() =>{
        let flag = false;
        let selected_shipping_method_error_val ='';
        const { selected_shipping_method} = this.state;
        
        if( selected_shipping_method === "" || selected_shipping_method ===null || selected_shipping_method === undefined){
          selected_shipping_method_error_val = 'Please select shipping method';
          flag = true
        }
        this.setState({ selected_shipping_method_error : selected_shipping_method_error_val});
        return flag;
      }

      gotoPaymentPage=()=>{

        if(!this.child.current.validate() && !this.validate( ) ){
          let data = {};
          let formData = this.child.current.getMyState();
          data.cart_id=this.state.cart_id;
          data.shipping_carrier_code = this.state.shipping_method_data.filter(item=> item.method_code === this.state.selected_shipping_method)[0].carrier_code;
          data.shipping_method_code = this.state.selected_shipping_method;
          data.shippingAddress= formData;
          data.shipping_method_Details = this.state.shipping_method_data.filter(item=> item.method_code === this.state.selected_shipping_method)[0];
          localStorage.setItem('shipmentAddress' ,  JSON.stringify(data));
          this.props.gotoPaymentPage(data);
        }
      }

      render() {
        const { shipping_method_data, selected_shipping_method, selected_shipping_method_error} =  this.state;

        return (
            <React.Fragment>
                <ShippingForm
                  setShippingMethod={this.setShippingMethod}
                  ref={this.child}
                  previous_checkout_shipping_data={this.props.previous_checkout_shipping_data}
                /> 
                <div className="shipping_method">
                    { this.state.shipping_method_data.length>0 ? 
                    <Fragment>
                        <ShippingMethod  
                            shipping_method_data ={ shipping_method_data}
                            selected_shipping_method={ selected_shipping_method}
                            selected_shipping_method_error={ selected_shipping_method_error}
                            handleShippingMethodOnChange={this.handleShippingMethodOnChange}
                        />
                        
                        <div className="actions-toolbar" id="shipping-method-buttons-container">
                            <div className="primary">
                            <button onClick={this.gotoPaymentPage} className="button action continue primary">
                                <span>Next</span>
                            </button>
                            </div>
                        </div>
                    </Fragment>
                    :
                    <div className="shipping_method">
                        <h3 className="step_title">Shipping Method</h3>        
                        <span>Sorry, no quotes are available for this order at this time</span>
                    </div>
                    } 
                </div>

                    
            </React.Fragment >
        );

    }

}


export default ShippingView;