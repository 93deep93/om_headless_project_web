import React, { useState, Fragment, Component } from 'react';
import { Form, Button, Tooltip, Accordion, Card } from 'react-bootstrap';
// import '../../styles/Checkout.scss';
import { Redirect, Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import equal from 'fast-deep-equal';
const Summary = (props) => {

    return (
        <React.Fragment>        
            <div className="opc-block-summary">
                <span className="title">Order Summary</span>

                <table className="data table table-totals">
                    <tbody>
                        <tr className="totals sub">
                            <th className="marks" scope="row">Cart Subtotal</th>
                            <td className="amount">
                            <span className="price">${props.cartCost}</span>
                            </td>
                        </tr>

                        <tr className="totals shipping excl">
                            <th className="marks" scope="row">
                                <span className="label">Shipping</span>
                                <span className="value">Shipping - {props.shipping_method_Details ? props.shipping_method_Details.method_title : ''}</span>
                            </th>
                            <td className="amount">
                                <span className="price">${props.shipping_method_Details ? props.shipping_method_Details.price_incl_tax : ''}</span>
                            </td>
                        </tr>

                        <tr className="grand totals">
                            <th className="marks" scope="row">
                                <strong data-bind="i18n: title">Order Total</strong>
                            </th>
                            <td className="amount">
                                <strong><span className="price">${ Number(props.cartCost) + Number(props.shipping_method_Details ? props.shipping_method_Details.price_incl_tax:0) }</span></strong>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </React.Fragment >
    );
};

export default Summary;