import React from 'react';
import '../styles/Privacyplocy.scss';
import PageContent from "./PageContent";

const Privacypolicy = () => {
  return (
    <React.Fragment>
      <section className="privacy-policy">
           <PageContent pageId={5} />    
      </section>
      
    </React.Fragment>
  );
}

export default Privacypolicy;