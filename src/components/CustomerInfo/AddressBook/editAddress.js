import React, { Component } from 'react';
import AccountDashMenu from '../myAccountDashboardMenu';
import {  setUserProfileApi, isAuthenticated} from '../../../account/Repository';
import { Redirect} from 'react-router-dom';
import '../../../styles/AccountInfo.scss';
import countriesList from '../../../data/countries_reagions.json';
import { getUserInfoApi, postUserInfoApi, updateUserPasswordApi} from '../../../account/customerInfoApi';
import { CHECK_UNAUTHORIZED , SET_SPINNER_STARTED, SET_SPINNER_COMPLETED } from '../../../actions/types';
import { connect } from 'react-redux';

class EditAddress extends Component {

    constructor(props) {
        super(props);
        this.state = {
            countriesList : countriesList,
            user_info: '',
            default_billing_check : false,
            default_shipping_check : false,
            address_details : this.props.location.state? this.props.location.state.address_details : 
            {   id:'',
                customer_id:'',
                region:{region_code:"",region:"",region_id:''},
                region_id:'',
                country_id:'',
                street:[],
                company:'',
                telephone:'',
                postcode:'',
                city:'',
                firstname:'',
                lastname:''
            },
            errors : {   
                region_error : '',
                region_id:'',
                country_id:'',
                street:[],
                company:'',
                telephone:'',
                postcode:'',
                city:'',
                firstname:'',
                lastname:''
            },
            error_api_res : '',
            state_arr:[],
            newAddress : true
        };
    }
    componentDidMount(){
        this.props.dispatch({ type : SET_SPINNER_STARTED});
        setUserProfileApi().then(res => {
            let user_info = res.result;
            this.setState({user_info : user_info});
            this.props.dispatch({ type : SET_SPINNER_COMPLETED});
          });
        if(this.props.location && this.props.location.state && this.props.location.state.address_details){
            if(this.props.location.state.address_details.default_billing === true)
            this.setState({ default_billing_check: true});
            if(this.props.location.state.address_details.default_shipping === true)
            this.setState({ default_shipping_check: true});

            this.setState({ newAddress: false});
        }
        let available_regions = this.state.countriesList.filter((data)=> data.id=== this.state.address_details.country_id);
        if(available_regions.length>0)
        this.setState({state_arr: this.state.address_details ? available_regions[0].available_regions  : []});
    }
    handleInputChangeText =(event) => {
        
        const { address_details, errors} = this.state;
        address_details[event.target.name] = event.target.value;
        errors[event.target.name] = '';
        this.setState ({address_details: address_details, errors : errors});
        if(event.target.name === 'country_id')
        this.setState({ state_arr :  this.state.countriesList.filter((data)=> data.id=== event.target.value).length>0 ? this.state.countriesList.filter((data)=> data.id=== event.target.value)[0].available_regions ? this.state.countriesList.filter((data)=> data.id=== event.target.value)[0].available_regions : [] : []})
      }

      handleCheck=(e)=>{
          
            const address_details = this.state.address_details;
            address_details[e.target.name] = ! address_details[e.target.name]
            this.setState({ address_details : address_details })
      }

      handleInputChangeState=(e)=>{
        
        let region_info = this.state.state_arr.filter(x=> x.id == e.target.value)[0];
        const address_details = this.state.address_details;
        let region ={};
        if(region_info){
            region.region = region_info.name
            region.region_code = region_info.code
            region.region_id = region_info.id
        }
        address_details.region = region;
        address_details.region_id = region_info.id;
        const errors= this.state.errors;
        errors.region_error = '';
        this.setState({ address_details : address_details, errors : errors});
      }

      handleInputChangeStreet=(e)=>{
        const { address_details, errors} = this.state;
        errors.address='';
        address_details.street[ e.target.name.match(/\d/g).join('') ] = e.target.value;
        this.setState({ address_details : address_details , errors : errors});
      }

      validateStreet=()=>{
          let flag= true;
          const address_details = this.state.address_details;
          
          if(  address_details.street.length == 0 || address_details.street=== null || address_details.street === undefined){
              flag = false;
          }else if( address_details.street.join().replaceAll(',','')===''){
            flag = false;
          }
          return flag
      }

      
      validate=() =>{
        let flag = false;
        const { errors, address_details } = this.state;
        
        if( address_details.firstname === "" || address_details.firstname=== null || address_details.firstname === undefined){
          errors.firstname = 'This field is required';
          flag = true;
        }
        if( address_details.lastname === "" || address_details.lastname=== null || address_details.lastname === undefined){
          errors.lastname = 'This field is required';
          flag = true;
        }
        if( address_details.company === "" || address_details.company=== null || address_details.company === undefined){
          errors.company = 'This field is required';
          flag = true;
        }
        
        if(!this.validateStreet() ){
        // if(  address_details.street.length == 0 || address_details.street=== null || address_details.street === undefined){
          errors.address = 'This field is required';
          flag = true;
        }
        if( address_details.city === "" || address_details.city=== null || address_details.city === undefined){
          errors.city = 'This field is required';
          flag = true;
        }
        if( address_details.region_id === "" || address_details.region_id=== null || address_details.region_id === undefined){
          errors.region_error = 'This field is required';
          flag = true;
        }
        if( address_details.country_id === "" || address_details.country_id=== null || address_details.country_id === undefined){
          errors.country_id = 'This field is required';
          flag = true;
        }
        if( address_details.telephone === "" || address_details.telephone=== null || address_details.telephone === undefined){
          errors.telephone = 'This field is required';
          flag = true;
        } 
        if( address_details.postcode === "" || address_details.postcode=== null || address_details.postcode === undefined){
          errors.postcode = 'This field is required';
          flag = true;
        }
        this.setState({ errors : errors});
        return flag;
      }

      saveAddress =()=>{
        if(!this.validate()){
            if(this.state.newAddress){
                let address_details =  this.state.address_details;
                delete address_details["id"];
                address_details.customer_id = this.state.user_info.id;
                this.state.user_info.addresses.push(address_details);
                this.updateAddress( this.manageStreetArray( this.state.user_info ))
            }else{
                let addresses = this.state.user_info.addresses;
                let address_details =  this.state.address_details;
                if( address_details.default_shipping === true && address_details.default_billing === true){
                    for(let i= 0; i< addresses.length ; i++){
                        delete addresses[i]['default_billing'];
                        delete addresses[i]['default_shipping'];
                    }
                }else if(address_details.default_shipping === true){
                    for(let i= 0; i< addresses.length ; i++){
                        delete addresses[i]['default_shipping'];
                    }
                }else if(address_details.default_billing === true){
                    for(let i= 0; i< addresses.length ; i++){
                        delete addresses[i]['default_billing'];
                    }
                }
                delete address_details['country_name'];
                
                this.state.user_info.addresses = addresses.map(u => u.id !== address_details.id ? u : address_details);
                this.updateAddress( this.manageStreetArray( this.state.user_info ))
              }
          }
       }

       manageStreetArray=(usrInfo)=>{
           for( let i=0; i< usrInfo.addresses.length ; i++){
               if( usrInfo.addresses[i].street.length<2){
                usrInfo.addresses[i].street.push(" ");
               }else{
                   for( let k=0; k< usrInfo.addresses[i].street.length; k++){
                    if(usrInfo.addresses[i].street[k] === ""){
                        usrInfo.addresses[i].street[k] = " ";
                    }
                   }
               }
           }
           return usrInfo
       }

       updateAddress =(user_info)=>{
        this.props.dispatch({ type : SET_SPINNER_STARTED});
        
        postUserInfoApi(user_info).then(res => {
            this.props.dispatch({ type : SET_SPINNER_COMPLETED});
            if(res.code === 200){
                this.props.history.push('/address');
             }
        }).catch(err => {
            this.props.dispatch({ type : SET_SPINNER_COMPLETED});
            if(err.response && err.response.data.code === 401 && err.response.data.result === "The consumer isn't authorized to access self."){
                this.props.dispatch({ type : CHECK_UNAUTHORIZED, payload: true});
            }else{
                    this.setState({ error_api_res : err.response.data.result.errorMessage})
                }
            this.setState({ error_api_res : err.response.data.result.errorMessage});
        });
       }

    render() {
        if (!isAuthenticated()) return (<Redirect to="/login" />);
        // 
        const {address_details , errors, error_api_res, default_billing_check, default_shipping_check} = this.state;
        return (
            <React.Fragment>
                {error_api_res ?<div className="product-error">{error_api_res} </div>:''}  
                <section className="page_banner">
                    <div className="rowsec-account" id="maincontent">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-md-4">
                                    <AccountDashMenu/>
                                </div>
                                <div className="col-md-8">
                                    <div className="addfrm-sec">
                                        <div className="form-address-edit" >
                                            <div className="form-row">
                                                <fieldset className="fieldset">
                                                    <legend className="legend"><span>Contact Information</span></legend>
                                                    <div className="field field-name-firstname required">
                                                        <label className="label"> <span>First Name</span> </label>
                                                        <div className="control">
                                                            <input type="text" id="firstname" name="firstname" onChange={this.handleInputChangeText} value={address_details.firstname} title="First Name" className="input-text required-entry" /> </div>
                                                            { errors.firstname ?  <div  className = "error">{errors.firstname}</div> : '' }
                                                        </div>
                                                        
                                                    <div className="field field-name-lastname required">
                                                        <label className="label"> <span>Last Name</span> </label>
                                                        <div className="control">
                                                            <input type="text" name="lastname" onChange={this.handleInputChangeText} value={address_details.lastname} title="Last Name" className="input-text required-entry" /> </div>
                                                            { errors.lastname ?  <div  className = "error">{errors.lastname}</div> : '' }
                                                    </div>
                                                    <div className="field company ">
                                                        <label className="label"> <span>
                                                            Company        </span> </label>
                                                        <div className="control">
                                                            <input type="text" name="company" onChange={this.handleInputChangeText} value={address_details.company} title="Company" className="input-text" />
                                                            { errors.company ?  <div  className = "error">{errors.company}</div> : '' }
                                                        </div>
                                                    </div>
                                                    <div className="field telephone required">
                                                        <label className="label"> <span>
                                                            Phone Number        </span> </label>
                                                        <div className="control">
                                                            <div className="validator validator-phone validation-none">
                                                                <input type="text" name="telephone" onChange={this.handleInputChangeText} value={address_details.telephone} title="Phone Number" className="input-text required-entry" />
                                                                { errors.telephone ?  <div  className = "error">{errors.telephone}</div> : '' }
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <fieldset className="fieldset">
                                                    <legend className="legend"><span>Address</span></legend>

                                                    <div className="field street required">
                                                        <label className="label"> <span>Street Address</span> </label>
                                                        <div className="control">
                                                            <input type="text" name="street0" onChange={this.handleInputChangeStreet} value={address_details.street ?address_details.street[0] : ''} className="input-text required-entry" autocomplete="off" />
                                                            <div className="nested">
                                                                <div className="field additional">
                                                                    <label className="label" htmlFor="street_1"> <span>Street Address 2</span> </label>
                                                                    <div className="control">
                                                                        <input type="text" name="street1" onChange={this.handleInputChangeStreet} value={ address_details.street ? address_details.street[1]: ''} title="Street Address 1" id="street_1" className="input-text " aria-required="true" /> </div>
                                                                </div>
                                                                <div className="field additional">
                                                                    <label className="label" htmlFor="street_2"> <span>Street Address 3</span> </label>
                                                                    <div className="control">
                                                                        <input type="text" name="street2" onChange={this.handleInputChangeStreet} value={address_details.street ? address_details.street[2]:''} title="Street Address 2" id="street_2" className="input-text " aria-required="true" /> </div>
                                                                </div>
                                                            </div>
                                                            { errors.address ?  <div  className = "error">{errors.address}</div> : '' }
                                                        </div>
                                                    </div>
                                                    <div className="field city required">
                                                        <label className="label"><span>City</span></label>
                                                        <div className="control">
                                                            <input type="text" name="city" onChange={this.handleInputChangeText} value={address_details.city} title="City" className="input-text required-entry" id="city" aria-required="true" />
                                                            { errors.city ?  <div  className = "error">{errors.city}</div> : '' }
                                                        </div>
                                                    </div>
                                                    <div className="field region required">
                                                        <label className="label"> <span>State/Province</span> </label>
                                                        <div className="control">
                                                            <select name="region_id" onChange={this.handleInputChangeState} value={address_details.region_id} title="State/Province" className="validate-select required-entry" defaultvalue="41" aria-required="true">
                                                            <option value="">Please select a region, state or province</option>
                                                            { this.state.state_arr.length>0 ?this.state.state_arr.map( ( state, index) => {
                                                            return <option value = {state.id}>{state.name}</option>
                                                            }):''}
                                                            </select>
                                                            { errors.region_error ?  <div  className = "error">{errors.region_error}</div> : '' }
                                                        </div>
                                                    </div>
                                                    <div className="field zip required">
                                                        <label className="label"> <span>Zip/Postal Code</span> </label>
                                                        <div className="control">
                                                            <input type="text" name="postcode" onChange={this.handleInputChangeText} value={address_details.postcode} className="input-text validate-zip-international required-entry" aria-required="true" autocomplete="off" /> </div>
                                                            { errors.postcode ?  <div  className = "error">{errors.postcode}</div> : '' }
                                                    </div>
                                                    <div className="field country required">
                                                        <label className="label" htmlFor="country"><span>Country</span></label>
                                                        <div className="control">
                                                            <select id="region_id" name="country_id" title="country" onChange={this.handleInputChangeText} value={address_details.country_id} className="validate-select required-entry" defaultvalue="41" aria-required="true">
                                                                <option value="">Select Country</option>
                                                                { countriesList.map( ( country, index) => {
                                                                    return <option value = {country.id}>{country.full_name_english}</option>
                                                                })}
                                                            </select>
                                                            {errors.country_id ?  <div  className = "error">{errors.country_id}</div> : '' }
                                                        </div>
                                                    </div>
                                                    { default_billing_check === true ? 
                                                    <div className="field choice set billing">
                                                        <label className="message info"> <span>! It's a default billing address.</span> </label>
                                                    </div>:
                                                    
                                                    <div className="field choice set billing">
                                                        <input type="checkbox" name="default_billing" onChange={this.handleCheck} checked={ address_details.default_billing} className="checkbox" />
                                                        <label className="label"> <span>Use as my default billing address</span> </label>
                                                    </div>}

                                                    { default_shipping_check === true ? 
                                                    <div className="field choice set billing">
                                                        <label className="message info"> <span>! It's a default shipping address.</span> </label>
                                                    </div>:
                                                    <div className="field choice set shipping">
                                                        <input type="checkbox" name="default_shipping" onChange={this.handleCheck} checked={ address_details.default_shipping} className="checkbox" />
                                                        <label className="label"> <span>Use as my default shipping address</span> </label>
                                                    </div>}
                                                </fieldset>
                                            </div>
                                            <div className="actions-toolbar">
                                                <div className="primary">
                                                    <button className="action save primary"  onClick={ this.saveAddress}> <span>Save Address</span> </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section >
            </React.Fragment >
        );

    }

}

export default connect() (EditAddress);