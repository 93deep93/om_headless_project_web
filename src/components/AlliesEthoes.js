import React from 'react';
import '../styles/AlliesEthoes.scss';




const AlliesEthoes = () => {

  return (
    <React.Fragment>
     <section className="product-allies-ethos">
       <div className="container-fluid pl-5 pr-5">
          <h2 className="footBanner_title">Allies Ethos</h2>
         <div className="footBanner_content"> 
          <div className="footBanner_icon">
            <span className="ethos_ico">
	          <img src={require('../icons/allies1.svg')}/></span> 
	          <span className="ethos_text">
	          <label><strong>Peta-Certified</strong> Cruelty Free</label>
            </span>
          </div>

          <div className="footBanner_icon">
            <span className="ethos_ico">
	          <img src={require('../icons/allies2.svg')}/></span> 
	          <span className="ethos_text">
	          <label>NOSilicones</label>
            </span>
          </div>

          <div className="footBanner_icon">
            <span className="ethos_ico">
	          <img src={require('../icons/allies3.svg')}/></span> 
	          <span className="ethos_text">
	          <label> NOSulfates</label>
            </span>
          </div>

          <div className="footBanner_icon">
            <span className="ethos_ico">
	          <img src={require('../icons/allies4.svg')}/></span> 
	          <span className="ethos_text">
	          <label> NOParabens</label>
            </span>
          </div>

          <div className="footBanner_icon">
            <span className="ethos_ico">
	          <img src={require('../icons/allies5.svg')}/></span> 
	          <span className="ethos_text">
	          <label> NODrying Alcohol</label>
            </span>
          </div>

          <div className="footBanner_icon">
            <span className="ethos_ico">
	          <img src={require('../icons/allies6.svg')}/></span> 
	          <span className="ethos_text">
	          <label> NOMineral Oil</label>
            </span>
          </div>

          <div className="footBanner_icon">
            <span className="ethos_ico">
	          <img src={require('../icons/allies7.svg')}/></span> 
	          <span className="ethos_text">
	          <label> NOPhtalates</label>
            </span>
          </div>



          <div className="footBanner_icon">
            <span className="ethos_ico">
	          <img src={require('../icons/allies8.svg')}/></span> 
	          <span className="ethos_text">
	          <label> NOPEG-Numbered</label>
            </span>
          </div>

          <div className="footBanner_icon">
            <span className="ethos_ico">
	          <img src={require('../icons/allies9.svg')}/></span> 
	          <span className="ethos_text">
	          <label> NOSynthetic Dyes</label>
            </span>
          </div>

          <div className="footBanner_icon">
            <span className="ethos_ico">
	          <img src={require('../icons/allies10.svg')}/></span> 
	          <span className="ethos_text">
	          <label><strong>NOSynthetic</strong> Fragrances</label>
            </span>
          </div>
       </div>


       </div> 

     </section>
    </React.Fragment>
  );
}

export default AlliesEthoes;