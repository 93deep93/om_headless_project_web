import React from 'react';
import Footerpage from './Footerpage';
import Backtotop from './Backtotop';
import '../styles/Footer.scss';
import CookiePopup from './Cookies';

const Footer = () => {
  return (
    <React.Fragment>
     <Footerpage />
     <Backtotop />
    <div className="footer_btm">
    
    <footer>
      <div className="copyright"><span>©2020 Allies Group Pte Ltd - All rights reserved</span></div>
      </footer>
      <CookiePopup/>
      </div>
    </React.Fragment>
  );
}

export default Footer;