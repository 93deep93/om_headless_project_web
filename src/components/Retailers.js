import React, { Component } from 'react';
import '../styles/Home.scss';
import '../styles/Retailer.scss'; 
import ReatailerMap from './Map/RetailerMap';


class Retailers extends Component {

  render() {

   
   return (
    <React.Fragment>
      <section className="page_banner ">
      <div><img className="image" src={require('../images/retailer_page.jpg')}/></div>
    <div className="retailer-section">
      <div className="container-fluid retailer-section pl-5 pr-5 mb-5">
{/* 
       <div className ="row">
         <div className="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <div className="location_selector">
           <span><label>FIND STORES NEAR:</label></span>
           <span className="location"><a href=""><i className="fa fa-location-arrow" aria-hidden="true"></i> My Location</a></span>
           </div>
         </div>
         <div className="col-xl-9 col-lg-9 col-md-8 col-sm-8 col-xs-12">
            <input type="text" name="storelocator-search_address" placeholder="Enter an address" aria-label="Search Address Input" autocomplete="off" className="ui-autocomplete-input"/>
         </div>
       </div>

        <div className ="row mt-4">
         <div className="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <div className="location_selector">
           <span><label>COUNTRY:</label></span>
           </div>
         </div>
         <div className="col-xl-9 col-lg-9 col-md-8 col-sm-8 col-xs-12">
                <select id="cars" className="ui-autocomplete-input">
                  <option value="contry">Select Country</option>
                  <option value="India" selected>India</option>
                  <option value="America">America</option>
                  <option value="Japan">Japan</option>
                 </select>
         </div>
       </div> */}

       <div className ="row mt-4">
         {/* <div className="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <div className="location_selector">

           </div>
         </div> */}
         <div className="col-md-12 ">
          <h2 style={{ textAlign:'center'}}>Stores </h2>
         </div>
         
         <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <ReatailerMap />
         </div>
       </div>

      </div>
      </div>
      </section>

    </React.Fragment>
  );

  }
  
}

export default Retailers;