import React, { useState, Fragment, Component, useEffect } from 'react';
import { Form, Button, Tooltip, Accordion, Card } from 'react-bootstrap';
// import '../../styles/Checkout.scss';
import { Redirect, Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import equal from 'fast-deep-equal';
import Summary from './summary';
import Shippinginfo from './shippingInfo';
import OrderSummary from '../../Checkout/orderSummary';
const OrderSummaryView = (props) => {

    const [shipping_method_Details, setShipping_method_Details] = useState('');
    const [shippingAddress, setShippingAddress] = useState({});
    const [cartCost, setCartCost] = useState(0);
    const [products, setProducts] = useState([]);

    useEffect( ()=>{
        console.log(props.shippingData.shipping_method_Details);
        setShipping_method_Details(props.shippingData.shipping_method_Details)
        setCartCost(props.basketProps.cartCost);
        setProducts(props.basketProps.products.productList);
        setShippingAddress(props.shippingData.shippingAddress)
    },[])
    return (
        <React.Fragment>        
            <div className="opc-block-summary">
                <Summary  shipping_method_Details={shipping_method_Details} cartCost ={cartCost}/>
            </div>
            <div className="incartitem">
                <div className ="order_summary">
                    <OrderSummary products={ products}/>
                </div>
            </div>
            <div className="shipping-information">
                <Shippinginfo 
                    shippingAddress={shippingAddress} 
                    shipping_method_Details ={shipping_method_Details}
                    gotoCheckoutPage={props.gotoCheckoutPage}
                />
            </div>
        </React.Fragment >
    );
};

export default OrderSummaryView;