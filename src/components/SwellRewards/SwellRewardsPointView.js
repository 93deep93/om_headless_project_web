import React, { Component } from 'react';
import { connect} from 'react-redux';
import '../../styles/rewardsPoint.scss';

class SwellRewardsPointView extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }


    render() {
        const isLogin = this.props.basketProps.isLogin ? this.props.basketProps.isLogin : false;
        // alert(this.props.basketProps.isLogin)

        return (
            <React.Fragment>
                <section className="page_banner">
                    <div className="rowsec-account-awards" id="swell-bootstrap">
                        <div className="container">
                            <div className="row">
                                <div className="rewards-sec rewards-templete">
                                    <div className="banner">
                                        <div className="caption">
                                            <h1 id="hero_header">Rewards Your Way</h1>
                                            <p id="hero_subtitle">Choose how you want to earn and redeem</p>
                                        </div>
                                        <div className="point-circle swell-authenticated">
                                            <h2 className="divider-text swell-point-balance">0</h2>
                                            <p className="divider-text">Points</p>
                                        </div>
                                    </div>
                                </div>
                                { isLogin ? '':
                                <div className="swell-unauthenticated referral-block divider-block" style={{ marginTop : '0px'}}>
                                    <div className="btn-block">
                                        <ul>
                                            <li className="login-btn active"><a href="/Login">Sign In</a></li>
                                            <li className="register-btn"><a href="/Signup">Create an Account</a></li>
                                        </ul>
                                    </div>
                                </div>
                                }
                            </div>
                        </div>
                    </div>
                    <div className="main-block earn-section rewards-page-campaigns-section-image rewards-page-campaigns-section-color">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-12 ">
                                    <div className="block-holder">
                                        { isLogin ? 
                                        <div className="heading-holder">
                                            <h2 id="earn_header" className="swell-authenticated">Earn</h2>
                                            <p id="earn_subtitle" className="swell-authenticated">Get points for every action you take</p>
                                        </div> :
                                        <div className="heading-holder">
                                            <h2 id="guest_earn_header" className="swell-unauthenticated">Sign In To Start Earning</h2>
                                            <p id="guest_earn_subtitle" className="swell-unauthenticated">Get points for every action you take</p>
                                        </div>
                                        }
                                    </div>

                                </div>
                            </div>
                            <div id="rp-campaigns-region">


                                <div className="row">
                                    <div className="campaign col-md-4 ">
                                        { isLogin ? 
                                            <div className="swell-campaign-link swell-authenticated" data-display-mode="modal" data-campaign-id="454705">
                                                <div className="item-box" id="gi-background">
                                                    <div className="icon-holder"  id="gi-icon-background">
                                                        <i className="fa fa-dollar" id="gi-icon-foreground"></i>
                                                    </div>
                                                    <h3 id="gi-desc-color">
                                                        Make a purchase
                                                                </h3>
                                                    <h2 id="gi-cost-color">
                                                        5 Points Per $1.00 Spent
                                                    </h2>
                                                </div>
                                            </div>
                                            :
                                        
                                            <div className="swell-campaign-link swell-authenticated" data-display-mode="modal" data-campaign-id="454705">
                                                <div className="item-box" id="gi-background">
                                                    <div className="icon-holder"  id="gi-icon-background">
                                                        <i className="fa fa-dollar" id="gi-icon-foreground"></i>
                                                    </div>
                                                    <h3 id="gi-desc-color">
                                                        Make a purchase
                                                                </h3>
                                                    <h2 id="gi-cost-color">
                                                        5 Points Per $1.00 Spent
                                                    </h2>
                                                    <div className="hover-logout-box">
                                                        <div className="btn-block">
                                                            <ul>
                                                            <li className="login-btn"><a href="/Login">Log In</a></li>
                                                            <li className="register-btn"><a href="/Signup">Create Account</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        }
                                    </div>
                                    <div className="campaign col-md-4">
                                        {isLogin ? 
                                        <div className="swell-campaign-link swell-authenticated" data-display-mode="modal" data-campaign-id="454706">
                                            <div className="item-box" id="gi-background">
                                                <div className="icon-holder" id="gi-icon-background">
                                                    <i className="fa fa-user" id="gi-icon-foreground"></i>
                                                </div>
                                                <h3 id="gi-desc-color">
                                                    Create an account
                                                                </h3>
                                                <h2 id="gi-cost-color">
                                                    100 Points
                                                </h2>
                                            </div>
                                        </div>
                                        :
                                        <div className="swell-campaign-link swell-authenticated" data-display-mode="modal" data-campaign-id="454706">
                                            <div className="item-box" id="gi-background">
                                                <div className="icon-holder" id="gi-icon-background">
                                                    <i className="fa fa-user" id="gi-icon-foreground"></i>
                                                </div>
                                                <h3 id="gi-desc-color">
                                                    Create an account
	                                                             </h3>
                                                <h2 id="gi-cost-color">
                                                    100 Points
	                                            </h2>
                                                <div className="hover-logout-box">
                                                    <div className="btn-block">
                                                        <ul>
                                                        <li className="login-btn"><a href="/Login">Log In</a></li>
                                                        <li className="register-btn"><a href="/Signup">Create Account</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        }
                                    </div>
                                    <div className="campaign col-md-4">
                                        { isLogin ? 
                                        <div className="swell-campaign-link swell-authenticated" data-display-mode="modal" data-campaign-id="454925">
                                            <div className="item-box" id="gi-background" >
                                                <div className="icon-holder" id="gi-icon-background">
                                                    <i className="fa fa-pencil" id="gi-icon-foreground"></i>
                                                </div>
                                                <h3 id="gi-desc-color">
                                                    Write a Review
                                                                </h3>
                                                <h2 id="gi-cost-color">
                                                    500 Points
                                                </h2>
                                            </div>
                                        </div>
                                        :
                                        <div className="swell-campaign-link swell-authenticated" data-display-mode="modal" data-campaign-id="454925">
                                            <div className="item-box" id="gi-background" >
                                                <div className="icon-holder" id="gi-icon-background">
                                                    <i className="fa fa-pencil" id="gi-icon-foreground"></i>
                                                </div>
                                                <h3 id="gi-desc-color">
                                                    Write a Review
	                                                            </h3>
                                                <h2 id="gi-cost-color">
                                                    500 Points
	                                             </h2>
                                                 <div className="hover-logout-box">
                                                    <div className="btn-block">
                                                        <ul>
                                                        <li className="login-btn"><a href="/Login">Log In</a></li>
                                                        <li className="register-btn"><a href="/Signup">Create Account</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        }
                                    </div>
                                </div>
                            </div>

                            <div id="rp-referral-campaign-region">
                                <div className="referral_campaign">
                                    {isLogin ?
                                    <div className="swell-authenticated referral-block divider-block">
                                        <h2 id="referral_header" className="divider-text">Refer a Friend</h2>
                                        <p id="referral_subtitle" className="divider-text">Share our store to earn points for you and a friend!</p>
                                        <h3 className="divider-text swell-referral-link">http://rwrd.io/ig6ryhs</h3>
                                        <div className="social-icon">
                                            <ul>
                                                <li className="swell-share-referral-facebook"><i className="fa fa-facebook"></i></li>
                                                <li className="swell-share-referral-twitter"><i className="fa fa-twitter"></i></li>
                                                <li className="swell-share-referral-email"><i className="fa fa-envelope-o"></i></li>
                                                <li className="swell-share-referral-messenger"><svg className="svg-inline--fa fa-facebook-messenger fa-w-14" aria-hidden="true" data-prefix="fab" data-icon="facebook-messenger" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M224 32C15.9 32-77.5 278 84.6 400.6V480l75.7-42c142.2 39.8 285.4-59.9 285.4-198.7C445.8 124.8 346.5 32 224 32zm23.4 278.1L190 250.5 79.6 311.6l121.1-128.5 57.4 59.6 110.4-61.1-121.1 128.5z"></path></svg></li></ul>
                                        </div>
                                    </div>
                                    :
                                    <div className="swell-unauthenticated referral-block divider-block">
                                        <h2 id="guest_referral_header" className="divider-text">Sign In To Refer a Friend</h2>
                                        <p id="guest_referral_subtitle" className="divider-text">Share our store to earn points for you and a friend!</p>
                                        <div className="btn-block">
                                            <ul>
                                                <li className="login-btn"><a href="/Login">Sign In</a></li>
                                                <li className="registModaler-btn"><a href="/Signup">Create an Account</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    }
                                </div>
                            </div>
                            <div id="rp-campaigns-redeem" >
                                <div className="row">
                                    <div className="col-md-12 ">
                                        <div className="block-holder">
                                            <div className="heading-holder">
                                                <h2 id="reedeem_header" className="swell-authenticated">Redeem</h2>
                                                <p id="redeem_subtitle" className="swell-authenticated">Use your points towards any of the offers below</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div className="row">
                                    <div className="campaign col-md-4">
                                        {isLogin ? 
                                        <div className="swell-campaign-link swell-authenticated" data-display-mode="modal" data-campaign-id="454706">
                                            <div className="item-box" id="gi-background">
                                                <div className="icon-holder" id="gi-icon-background">
                                                    <i className="fa fa-dollar" id="gi-icon-foreground"></i>
                                                </div>
                                                <h3 id="gi-desc-color">$5.00 Off</h3>
                                                <h2 id="gi-cost-color">500 Points</h2>
                                            </div>
                                        </div>
                                        :
                                        <div className="swell-campaign-link swell-authenticated" data-display-mode="modal" data-campaign-id="454706">
                                            <div className="item-box" id="gi-background">
                                                <div className="icon-holder" id="gi-icon-background">
                                                    <i className="fa fa-dollar" id="gi-icon-foreground"></i>
                                                </div>
                                                <h3 id="gi-desc-color">$5.00 Off</h3>
                                                <h2 id="gi-cost-color">500 Points</h2>
                                                <div className="hover-logout-box">
                                                    <div className="btn-block">
                                                        <ul>
                                                        <li className="login-btn"><a href="/Login">Log In</a></li>
                                                        <li className="register-btn"><a href="/Signup">Create Account</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        }
                                    </div>
                                    <div className="campaign col-md-4">
                                        {isLogin ? 
                                        <div className="swell-campaign-link swell-authenticated" data-display-mode="modal" data-campaign-id="454706">
                                            <div className="item-box" id="gi-background">
                                                <div className="icon-holder" id="gi-icon-background">
                                                    <i className="fa fa-dollar" id="gi-icon-foreground"></i>
                                                </div>
                                                <h3 id="gi-desc-color"> $10.00 Off </h3>
                                                <h2 id="gi-cost-color">1000 Points</h2>
                                            </div>
                                        </div>
                                        :
                                        <div className="swell-campaign-link swell-authenticated" data-display-mode="modal" data-campaign-id="454706">
                                            <div className="item-box" id="gi-background">
                                                <div className="icon-holder" id="gi-icon-background">
                                                    <i className="fa fa-dollar" id="gi-icon-foreground"></i>
                                                </div>
                                                <h3 id="gi-desc-color"> $10.00 Off </h3>
                                                <h2 id="gi-cost-color">1000 Points</h2>
                                                    <div className="hover-logout-box">
                                                    <div className="btn-block">
                                                        <ul>
                                                        <li className="login-btn"><a href="/Login">Log In</a></li>
                                                        <li className="register-btn"><a href="/Signup">Create Account</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        }
                                    </div>
                                    <div className="campaign col-md-4">
                                        {isLogin ? 
                                        <div className="swell-campaign-link swell-authenticated" data-display-mode="modal" data-campaign-id="454706">
                                            <div className="item-box" id="gi-background">
                                                <div className="icon-holder" id="gi-icon-background">
                                                    <i className="fa fa-dollar" id="gi-icon-foreground"></i>
                                                </div>
                                                <h3 id="gi-desc-color"> $20.00 Off </h3>
                                                <h2 id="gi-cost-color">   2000 Points   </h2>
                                            </div>
                                        </div>
                                        :
                                        <div className="swell-campaign-link swell-authenticated" data-display-mode="modal" data-campaign-id="454706">
                                            <div className="item-box" id="gi-background">
                                                <div className="icon-holder" id="gi-icon-background">
                                                    <i className="fa fa-dollar" id="gi-icon-foreground"></i>
                                                </div>
                                                <h3 id="gi-desc-color"> $20.00 Off </h3>
                                                <h2 id="gi-cost-color">   2000 Points   </h2>
                                                <div className="hover-logout-box">
                                                    <div className="btn-block">
                                                        <ul>
                                                        <li className="login-btn"><a href="/Login">Log In</a></li>
                                                        <li className="register-btn"><a href="/Signup">Create Account</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        }
                                    </div>
                                    <div className="campaign col-md-4">
                                        {isLogin ? 
                                        <div className="swell-campaign-link swell-authenticated" data-display-mode="modal" data-campaign-id="454706">
                                            <div className="item-box" id="gi-background">
                                                <div className="icon-holder" id="gi-icon-background">
                                                    <i className="fa fa-dollar" id="gi-icon-foreground"></i>
                                                </div>
                                                <h3 id="gi-desc-color"> $30.00 Off </h3>
                                                <h2 id="gi-cost-color"> 3000 Points </h2>
                                            </div>
                                        </div>
                                        :
                                        <div className="swell-campaign-link swell-authenticated" data-display-mode="modal" data-campaign-id="454706">
                                            <div className="item-box" id="gi-background">
                                                <div className="icon-holder" id="gi-icon-background">
                                                    <i className="fa fa-dollar" id="gi-icon-foreground"></i>
                                                </div>
                                                <h3 id="gi-desc-color"> $30.00 Off </h3>
                                                <h2 id="gi-cost-color"> 3000 Points </h2>
                                                    <div className="hover-logout-box">
                                                    <div className="btn-block">
                                                        <ul>
                                                        <li className="login-btn"><a href="/Login">Log In</a></li>
                                                        <li className="register-btn"><a href="/Signup">Create Account</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        }
                                    </div>
                                    <div className="campaign col-md-4">
                                        {isLogin ?
                                        <div className="swell-campaign-link swell-authenticated" data-display-mode="modal" data-campaign-id="454706">
                                            <div className="item-box" id="gi-background">
                                                <div className="icon-holder" id="gi-icon-background">
                                                    <i className="fa fa-dollar" id="gi-icon-foreground"></i>
                                                </div>
                                                <h3 id="gi-desc-color">  $40.00 Off   </h3>
                                                <h2 id="gi-cost-color"> 4000 Points </h2>
                                            </div>
                                        </div>
                                        :
                                        <div className="swell-campaign-link swell-authenticated" data-display-mode="modal" data-campaign-id="454706">
                                            <div className="item-box" id="gi-background">
                                                <div className="icon-holder" id="gi-icon-background">
                                                    <i className="fa fa-dollar" id="gi-icon-foreground"></i>
                                                </div>
                                                <h3 id="gi-desc-color">  $40.00 Off   </h3>
                                                <h2 id="gi-cost-color"> 4000 Points </h2>
                                                    <div className="hover-logout-box">
                                                    <div className="btn-block">
                                                        <ul>
                                                        <li className="login-btn"><a href="/Login">Log In</a></li>
                                                        <li className="register-btn"><a href="/Signup">Create Account</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        }
                                    </div>
                                    <div className="campaign col-md-4">
                                        {isLogin ? 
                                        <div className="swell-campaign-link swell-authenticated" data-display-mode="modal" data-campaign-id="454706">
                                            <div className="item-box" id="gi-background">
                                                <div className="icon-holder" id="gi-icon-background">
                                                    <i className="fa fa-dollar" id="gi-icon-foreground"></i>
                                                </div>
                                                <h3 id="gi-desc-color">$50.00 Off</h3>
                                                <h2 id="gi-cost-color">5000 Points</h2>
                                            </div>
                                        </div>    
                                        :
                                        <div className="swell-campaign-link swell-authenticated" data-display-mode="modal" data-campaign-id="454706">
                                            <div className="item-box" id="gi-background">
                                                <div className="icon-holder" id="gi-icon-background">
                                                    <i className="fa fa-dollar" id="gi-icon-foreground"></i>
                                                </div>
                                                <h3 id="gi-desc-color">$50.00 Off</h3>
                                                <h2 id="gi-cost-color">5000 Points</h2>
                                                    <div className="hover-logout-box">
                                                    <div className="btn-block">
                                                        <ul>
                                                        <li className="login-btn"><a href="/Login">Log In</a></li>
                                                        <li className="register-btn"><a href="/Signup">Create Account</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        }
                                    </div>
                                </div>
                                <div id="rp-referral-campaign-region">
                                    <div className="referral_campaign">
                                        {isLogin ?
                                        <div className="swell-authenticated referral-block divider-block">
                                            <h2 id="referral_header" className="divider-text">Transaction History</h2>
                                            <p id="referral_subtitle" className="divider-text">View past earnings and redemptions</p>
                                            <div className="btn-block">
                                                <ul>
                                                    <li id="view-history-btn" className="swell-history-link">View History</li>
                                                </ul>
                                            </div>
                                        </div>
                                        :
                                        <div className="swell-unauthenticated referral-block divider-block">
                                            <h2 id="guest_referral_header" className="divider-text">Sign In To Refer a Friend</h2>
                                            <p id="guest_referral_subtitle" className="divider-text">Share our store to earn points for you and a friend!</p>
                                            <div className="btn-block">
                                                <ul>
                                                    <li className="login-btn"><a href="/Login">Sign In</a></li>
                                                    <li className="register-btn"><a href="/Signup">Create an Account</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        }
                                    </div>
                                </div>
                                <div className="reward-faq">
                                <div className="faq-block">
                                    <div className="container">
                                        <div className="row">
                                            <div className="block-holder">
                                                <div className="heading-holder">
                                                    <h2 id="faq-header" >FAQ</h2>
                                                    <p id="faq-subtitle" >Subtitle</p>
                                                </div>
                                                <div id="faq-text" className="content-block">
                                                    <h2 className="swell-question" >How do I participate?</h2>
                                                    <p className="swell-answer" >Joining is easy! Just click the Create An Account button to get started. Once you're registered with our store, you'll have the opportunity to take part in all of the exciting ways we currently offer to earn points!</p>
                                                    <h2 className="swell-question" >How can I earn points?</h2>
                                                    <p className="swell-answer" >You can earn points by participating in any of our innovative promotions! Simply click on the 'Earn Points' tab to view and take part in our current opportunities. In addition, make sure to check back often, as we're adding great new ways for you to earn points all the time!</p>
                                                    <h2 className="swell-question" >What can I redeem my points for?</h2>
                                                    <p className="swell-answer" >Glad you asked! We want to make it easy and fun to redeem your hard-earned points. Just visit the 'Get Rewards' tab to view all of our exciting reward options.</p>
                                                    <h2 className="swell-question" >How do I redeem my points?</h2>
                                                    <p className="swell-answer" >Exchanging your points for great rewards couldn't be easier! Simply visit the 'Get Rewards' tab to view all of our great reward options and click the 'Redeem' button to redeem your reward.</p>
                                                    <h2 className="swell-question" >How do I check my points balance?</h2>
                                                    <p className="swell-answer" >Your up-to-date points balance is always displayed in the top of this popup.</p>
                                                    <h2 className="swell-question" >Does it cost anything to begin earning points?</h2>
                                                    <p className="swell-answer" >Absolutely not! Sign up is 100% free, and it will never cost you anything to earn points. Make sure to visit the 'Earn Points' tab to get started.</p>
                                                    <h2 className="swell-question" >Do I have to enroll or register in individual promotions?</h2>
                                                    <p className="swell-answer" >Once you register for an account, you're all set – we don't require you to register for individual promotions in order to be eligible. Just fulfill the requirements of a promotion, and we'll post the points to your account immediately!</p>
                                                    <h2 className="swell-question" >How long will it take for points to post to my account?</h2>
                                                    <p className="swell-answer" >You should receive points in your account instantly once you complete a promotion!</p>
                                                    <h2 className="swell-question" >Do my points expire?</h2>
                                                    <p className="swell-answer" >Nope! Your points will never expire.</p>
                                                    <h2 className="swell-question" >What happens to my points if I make a return?</h2>
                                                    <p className="swell-answer" >When you return an item, you lose the associated credit you originally earned by buying the item in the first place.  Sound kind of confusing? Let's take an example: let's say you had previously spent $50 towards a 'spend $100, earn 500 points' promotion, and you decide to buy a $20 item, which bumps you up to $70. If you decide to return that item, your progress would also go back down to $50 – it's just like you hadn't bought the item in the first place.</p>
                                                    <h2 className="swell-question" >How do I contact support if I have questions about my points?</h2>
                                                    <p className="swell-answer" >Our team is ready and waiting to answer your questions about our rewards program! Just send us an email and we'll be in touch.</p>
                                                    <h2 className="swell-question" >I'm very close to earning a reward. Can I buy extra points to get there?</h2>
                                                    <p className="swell-answer" >We currently require you to have enough points to redeem any of the awards you see listed on the 'Get Rewards' tab.</p>
                                                    <h2 className="swell-question" >What if I don't want to receive promotional emails?</h2>
                                                    <p className="swell-answer" >From time to time, you'll receive program-related emails from us. If you'd prefer to not receive those types of emails anymore, just click the 'Unsubscribe' button when you receive your next email.</p>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                </section>
            </React.Fragment>
        );

    }

}

  export default SwellRewardsPointView;