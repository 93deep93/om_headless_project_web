import React, { useEffect, useRef, useState } from "react";
import Slider from "react-slick";
import { Button } from 'react-bootstrap'; 
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import '../../styles/SimilarProducts.scss';
const SimilarProducts = (props) => {
    const [ similerProductlist, setSimilerProductlist] = useState([]);
    const settings = {
        dots: false,
        arrow: true,
        infinite: false,
        speed: 800,
        autoplay: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: false,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };

    useEffect ( ()=>{
        var ids = new Set( props.product.product_links.map(d => d.linked_product_sku));
		var similerProducts = props.products_list.filter(d => ids.has(d.sku));
        console.log(similerProducts);
        let data = props.basketProps.products.productList;
        for( let i=0; i< data.length ;i++){
            let index = similerProducts.findIndex( x=> x.sku === data[i].sku);
            if(index ==0|| index >0)
            similerProducts[index].isCartProduct = true;
        }
        debugger
        setSimilerProductlist(similerProducts);

    }, [])

    function addDefaultSrc(ev){
        ev.target.src = 'http://demo43.laraveldevloper.in/pub/media/catalog/product/placeholder/default/placeholder_3.png'
    }

    function htmlStripTags(string){
		string = string || '';
		return string.replace(/<[^>]*>?/gm, '');
	}

    return (
        similerProductlist.length > 0 ?
        <React.Fragment>

            <div className="Similar-product-section">
                <div className="rowowl">
                <div className="container-fluid my-5 pl-5 pr-5 product padtop-60">
                    <Slider {...settings}>
                        <div className="item product-item-info ">

                            <div className="regimen-left-section">
                            <h2 className="complete_your_regimen_with-heading">Complete Your Regimen With</h2>
                            <div dangerouslySetInnerHTML={{__html: props.product.complete_your_regimen_with}}></div>
                                {/* 
                                <h3>To tone and soothe stressed skin</h3>
                                <p>Mist with <a href="http://demo43.laraveldevloper.in/products/molecular-saviour-probiotics-mist">Molecular Saviour™ Probiotics Repair Mist</a></p>
                                <h3>To promote extra hydration</h3>
                                <p>Layer with <a href="http://demo43.laraveldevloper.in/products/triple-hyaluronic-antioxidant-hydration-serum">Triple Hyaluronic Antioxidant Hydration Serum</a></p>
                                <h3>To attain an extra brightening effect</h3>
                                <p>Layer with <a href="http://demo43.laraveldevloper.in/products/35-vitamin-c-perfecting-serum">35% Vitamin C+ Perfecting Serum</a></p>
                                <h3>To combat breakouts, whiteheads, blackheads and hydrate skin</h3>
                                <p>Moisturise with&nbsp;<a href="http://demo43.laraveldevloper.in/products/promise-keeper-blemish-facial">Promise Keeper™ Blemish Sleeping Facial</a>&nbsp;nightly</p> */}
                            </div>


                        </div>
                        { similerProductlist.map( product => {
                            return <div className="item product-item-info ">
                            <div className="w-100 text-center card">
                                <a href="/productDetails/5">
                                   
                                    <img className="img-responsive" onError={addDefaultSrc} src={'http://139.180.155.160/img/310/300/resize'+ product.image} /></a>
                                    <div className="card-body">
                                        <div className="card-title h5"> {product.name} </div>
                                        <p className="card-text">
                                            <div className="stamped-badge">
                                                <span className="porduct_desc"> { htmlStripTags(product.description)} </span><span className="rating">
                                                    <a href="/rating"><i className="fa fa-star"></i><i className="fa fa-star"></i><i className="fa fa-star"></i><i className="fa fa-star"></i><i className="fa fa-star"></i></a>
                                                </span>
                                                <span className="reviews"><a href="/review">  </a></span>
                                            </div>
                                            <div className="attribute">
                                                <span className="product-volume-attr"> 
                                                { product.attributes_metadata.findIndex( (x)=> x.attribute_code === "product_size") != -1 ? 
                                                  product.attributes_metadata[ product.attributes_metadata.findIndex( (x)=> x.attribute_code === "product_size") ].options[0].label :' 0 ml'} </span>
                                                <span className="separator"></span>
                                                <span className="product-price">${ product.price}</span>
                                            </div>
                                        </p>
                                        {product.stock.qty > 0 ?
                                        <Button variant="primary" disabled = { product.isCartProduct ? true : false} onClick={() => this.props.addBasket (product)} > {product.isCartProduct ? 'Added': 'Add to Cart'}</Button>
                                        :
                                        <span> OUT OF STOCK </span>
                                        //  <Button variant="primary" > {'Notify me'} </Button>
                                        }
                                    </div>
                                </div>
                            </div>
                        } )}
                        
                        {/* <div className="item product-item-info ">
                        <div className="w-100 text-center card">
                                <a href="/productDetails/5">
                                    <img className="img-responsive" alt="Bright Future Sleeping Facial" src="http://139.180.155.160/img/310/300/resize/t/_/t_bright-future-sleeping-facial_1.jpg" /></a>
                                <div className="card-body">
                                    <div className="card-title h5"> Bright Future Sleeping Facial </div>
                                    <p className="card-text">
                                        <div className="stamped-badge"><span className="porduct_desc"> Brightens dull and uneven complexion. Reduces breakouts and blemishes. </span><span className="rating"><a href="/rating"><i className="fa fa-star"></i><i className="fa fa-star"></i><i className="fa fa-star"></i><i className="fa fa-star"></i><i className="fa fa-star"></i></a></span>
                                            <span
                                                className="reviews"><a href="/review">  </a></span>
                                        </div>
                                        <div className="attribute"><span className="product-volume-attr"> 50 ml </span><span className="separator"></span><span className="product-price">$120.00</span></div>
                                    </p><button type="button" className="btn btn-primary"> Add to Cart</button></div>
                            </div>
                        </div>
                        <div className="item product-item-info ">
                        <div className="w-100 text-center card">
                                <a href="/productDetails/5">
                                    <img className="img-responsive" alt="Bright Future Sleeping Facial" src="http://139.180.155.160/img/310/300/resize/t/_/t_bright-future-sleeping-facial_1.jpg" /></a>
                                <div className="card-body">
                                    <div className="card-title h5"> Bright Future Sleeping Facial </div>
                                    <p className="card-text">
                                        <div className="stamped-badge"><span className="porduct_desc"> Brightens dull and uneven complexion. Reduces breakouts and blemishes. </span><span className="rating"><a href="/rating"><i className="fa fa-star"></i><i className="fa fa-star"></i><i className="fa fa-star"></i><i className="fa fa-star"></i><i className="fa fa-star"></i></a></span>
                                            <span
                                                className="reviews"><a href="/review">  </a></span>
                                        </div>
                                        <div className="attribute"><span className="product-volume-attr"> 50 ml </span><span className="separator"></span><span className="product-price">$120.00</span></div>
                                    </p><button type="button" className="btn btn-primary"> Add to Cart</button></div>
                            </div>
                        </div> */}
                        {/* <div className="item product-item-info ">
                        <div className="w-100 text-center card">
                                <a href="/productDetails/5">
                                    <img className="img-responsive" alt="Bright Future Sleeping Facial" src="http://139.180.155.160/img/310/300/resize/t/_/t_bright-future-sleeping-facial_1.jpg" /></a>
                                <div className="card-body">
                                    <div className="card-title h5"> Bright Future Sleeping Facial </div>
                                    <p className="card-text">
                                        <div className="stamped-badge"><span className="porduct_desc"> Brightens dull and uneven complexion. Reduces breakouts and blemishes. </span><span className="rating"><a href="/rating"><i className="fa fa-star"></i><i className="fa fa-star"></i><i className="fa fa-star"></i><i className="fa fa-star"></i><i className="fa fa-star"></i></a></span>
                                            <span
                                                className="reviews"><a href="/review">  </a></span>
                                        </div>
                                        <div className="attribute"><span className="product-volume-attr"> 50 ml </span><span className="separator"></span><span className="product-price">$120.00</span></div>
                                    </p><button type="button" className="btn btn-primary"> Add to Cart</button></div>
                            </div>
                        </div> */}
                    </Slider>
                </div>
                </div>

            </div>


        </React.Fragment>:''
    );
}

export default SimilarProducts;