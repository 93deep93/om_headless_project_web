import React, {PureComponent} from 'react';
import countriesList from '../../../data/countries_reagions.json';

export default class Addressinfo extends PureComponent{
   getCounteryName=(country_id)=>{
      return countriesList.filter(x=> x.id == country_id)[0].full_name_english;
  }
   render(){

     
      const billing_address = this.props.billing_address;
      return <>
        <address>
            {billing_address.firstname+' '+ billing_address.lastname} <br/>
            {billing_address.company}<br/>
            {billing_address.street ? billing_address.street.join(' ') : ''} <br/>
            {/* {billing_address.street[0]+' '+ billing_address.street.length ? billing_address.street[1] : ''} <br/> */}
            {billing_address.city}<br/>
             { billing_address.region ? <>{billing_address.region.region}<br/></> : ''}
            {  billing_address.country_id ? this.getCounteryName( billing_address.country_id):'' }<br/>
            T: <a href={"tel:" + billing_address.telephone}>{billing_address.telephone}</a>
        </address>
      </>
   }
}