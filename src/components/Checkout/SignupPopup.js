import React, { useState } from 'react';
import { Link } from 'react-router-dom';

const SignupPopup = ( props) => {
    const regex_email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const [ isShow, setIsShow ] = useState( false);
    const [ email, setEmail ] = useState ('');
    const [ password, setPassword ] = useState('');
    const [ email_error, setEmailError ] = useState ('');
    const [ password_error, setPasswordError ] = useState('');

    

    function validateEmail( email){
        return regex_email.test(String(email).toLowerCase());
    }

    function validate(){
        let flag = false;
        if( email=== "" || email=== null || email === undefined){
            setEmailError('This field is required');
            flag = true;
        }else if( email !== "" || email !== null || email !== undefined){
            if(!validateEmail( email)){
                setEmailError('Please enter valid email id !');
                flag = true;
            }
        }
        if( password === "" || password === null || password === undefined){
            setPasswordError( 'This field is required');
            flag = true;
        }
        return flag;
    }

    function submitLogin() {
        if(!validate()){
            props.submitLogin( { email : email, password : password } )   
        }
    }
    
    return (
        <React.Fragment>
            <div className="authentication-wrapper" >
                <button onClick={()=> setIsShow(!isShow)} type="button" className="action action-auth-toggle">
                    <span>Sign In</span>
                </button>

                <div className={"modal-custom authentication-dropdown custom-slide " + (isShow ? '_show' : '')} >

                    <div className="modal-inner-wrap">
                        <header className="modal-header">

                            <button  onClick={()=> setIsShow(false)} className="action-close" type="button">
                                {/* <span>Close</span> */}
                            </button>
                        </header>
                        <div id="modal-content-15" className="modal-content-signup">
                            <div className="block-authentication">
                            {props.signin_error ? <div className="product-error">{props.signin_error}</div> : ''}
                                <div className="block block-customer-login">
                                    <div className="block-title">
                                        <strong id="block-customer-login-heading">Sign In</strong>
                                    </div>
                                    <div className="messages"></div>
                                    <div className="block-content">
                                        <div>
                                            <div className="fieldset">
                                                <div className="field field-email required">
                                                    <label className="label" htmlFor="login-email">
                                                        <span>Email Address</span>
                                                    </label>
                                                    <div className="control">
                                                        <input type="email" name='email' onChange={(e)=>{setEmail(e.target.value)}} className="input-text" name="username" />
                                                    </div>
                                                    {email_error ? <div  className = "error">{email_error}</div> : ''}
                                                </div>
                                                <div className="field field-password required">
                                                    <label htmlFor="login-password" className="label">
                                                        <span>Password</span></label>
                                                    <div className="control">
                                                        <input type="password" className="input-text" name='password' onChange={(e)=>{setPassword(e.target.value)}} name="password" />
                                                    </div>
                                                    {password_error ? <div  className = "error">{password_error}</div> : ''}
                                                </div>

                                            </div>
                                            <div className="actions-toolbar">
                                                <div className="primary">
                                                    <button onClick={()=> submitLogin()} ><span>Sign In</span></button>
                                                </div>
                                                <div className="secondary">
                                                    <Link to="/Forgetpassword" className="action action-remind">
                                                        <span>Forgot Your Password?</span>
                                                    </Link>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </React.Fragment>
    );
}

export default SignupPopup;