import React, {Fragment, Component} from 'react';
import {Form, Button, Tooltip } from 'react-bootstrap';
import { getShipmentMethods, isAuthenticated, getProducts, pay } from '../account/Repository';
import Payment from './paypal/Payment';
import PayPalBtn from './paypal/PayPalBtn';
import '../styles/Checkout.scss';
import { Redirect, Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import countriesList from '../data/countries_reagions.json';
import equal from 'fast-deep-equal';

class Checkout extends Component {

   constructor(props) {
      super(props);
        this.state = { 
          productItem: [], 
          total: 0,
          formData: {},
          errors:{ },
          validated: false,
          selected_shipping_method : '',
          selected_shipping_method_error : '',
          cart_id : '48654',
          shipping_method_data : [],
          state_arr:[],
          countriesList : countriesList,
          cartCost:'', products:[]
         }
         
      }

      componentDidMount(){
        if(localStorage.getItem('user-info-allies')){
          let info = JSON.parse(localStorage.getItem('user-info-allies'));
          let formdata = { firstname : info.firstname, lastname: info.lastname, email : info.email};
          this.setState({ formData: formdata });
        }
      }

      componentDidUpdate( preProps){
        if( this.props.basketProps.products && this.props.basketProps.products.productList){
          if (!equal(this.props.basketProps.products.productList, this.state.products)){
            this.setState({cartCost : this.props.basketProps.cartCost, 
              products : this.props.basketProps.products.productList ? this.props.basketProps.products.productList: []});
          }
        }
      }

      getShippingMethod=( country_code)=>{  
        this.setState({ state_arr : this.state.countriesList.filter((data)=> data.id=== country_code)[0].available_regions}) //set regions
        let data={};
        data.country_code = country_code;
        data.cart_id = this.state.cart_id;
          getShipmentMethods(data)
            .then(res => {
             if(res.code === 200) {   
               this.setState({ shipping_method_data : res.result});
             }
            }).catch(err => {
              console.log(err)
            });
      }

      handleInputChangeText =(event) => {
        const { formData, errors} = this.state;
        formData[event.target.name] = event.target.value;
        errors[event.target.name] = '';
        this.setState ({formData: formData, errors : errors});
        if(event.target.name === 'country_id')
          this.getShippingMethod( event.target.value);
      }

      pay = () => {
        if(!this.validate()){
          sessionStorage.setItem('shipmentAddress' ,  JSON.stringify(this.state.formData));
         this.props.history.push({
           pathname :'/Payment',
           state :{ shipmentAddress: this.state.formData}
         })
        }
      };

      validateEmail=( email)=>{
        const regex_email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex_email.test(String(email).toLowerCase());
      }

      validate=() =>{
        let flag = false;
        let selected_shipping_method_error_val ='';
        const { errors, formData, selected_shipping_method , selected_shipping_method_error} = this.state;
        if( formData.email === "" || formData.email=== null || formData.email === undefined){
          errors.email = 'This field is required';
          flag = true;
        }else if( formData.email !== "" || formData.email !== null || formData.email !== undefined){
          if(!this.validateEmail( formData.email)){
            errors.email = 'Please enter valid email id !'
            flag = true;
          }
        }
        if( formData.firstname === "" || formData.firstname=== null || formData.firstname === undefined){
          errors.firstname = 'This field is required';
          flag = true;
        }
        if( formData.lastname === "" || formData.lastname=== null || formData.lastname === undefined){
          errors.lastname = 'This field is required';
          flag = true;
        }
        if( formData.company === "" || formData.company=== null || formData.company === undefined){
          errors.company = 'This field is required';
          flag = true;
        }
        if( formData.address === "" || formData.address=== null || formData.address === undefined){
          errors.address = 'This field is required';
          flag = true;
        }
        if( formData.city === "" || formData.city=== null || formData.city === undefined){
          errors.city = 'This field is required';
          flag = true;
        }
        if( formData.state === "" || formData.state=== null || formData.state === undefined){
          errors.state = 'This field is required';
          flag = true;
        }
        if( formData.country_id === "" || formData.country_id=== null || formData.country_id === undefined){
          errors.country_id = 'This field is required';
          flag = true;
        }
        if( formData.telephone === "" || formData.telephone=== null || formData.telephone === undefined){
          errors.telephone = 'This field is required';
          flag = true;
        }
        if( formData.postcode === "" || formData.postcode=== null || formData.postcode === undefined){
          errors.postcode = 'This field is required';
          flag = true;
        }
        if( selected_shipping_method === "" || selected_shipping_method ===null || selected_shipping_method === undefined){
          selected_shipping_method_error_val = 'Please select shipping method';
          flag = true
        }
        this.setState({ errors : errors, selected_shipping_method_error : selected_shipping_method_error_val});
        return flag;
      }

      handleShippingMethodOnChange =( val)=>{
        this.setState({ selected_shipping_method : val , selected_shipping_method_error: ''});
      }

      gotoPaymentPage=()=>{
        if(!this.validate()){
        let data = {};
          let formData = this.state.formData;
          formData.postcode= formData.postcode;
          formData.street = [ formData.address, formData.address];
          data.cart_id=this.state.cart_id;
          data.shipping_carrier_code = this.state.shipping_method_data.filter(item=> item.method_code === this.state.selected_shipping_method)[0].carrier_code;
          data.shipping_method_code = this.state.selected_shipping_method;
          data.shippingAddress= formData;
          localStorage.setItem('shipmentAddress' ,  JSON.stringify(data));
          this.props.history.push({ pathname:'/Payment', state : { shippingData: data}});
        }
      }

      handleInputChangeState=(e)=>{
        
        let region_info = this.state.state_arr.filter(x=> x.code === e.target.value)[0];
        const formData = this.state.formData;
        formData.region = region_info.name;
        formData.region_code = region_info.code;
        formData.region_id = region_info.id;
        formData.state = e.target.value;
        const errors= this.state.errors;
        errors[e.target.name] = '';
        this.setState({ formData : formData, errors : errors});
      }

   render () {
    if (!isAuthenticated()) return (<Redirect to={{pathname :"/login", state: { pre_page_url: '/Checkout' }}} />);
    const { productItem, total, errors, formData, selected_shipping_method_error } =  this.state;

    let { cartCost, products } = this.state;
    let productsInCart = products; 
    
    if(!productsInCart.length){
      return <Fragment>
      <span className="view_editcart">YOU HAVE NO ITEMS SELECTED FOR ORDER!</span>
      </Fragment>
    }
  

  productsInCart = productsInCart.map( (productList, index) => {

     return (
         
          <Fragment>     
              <div className="order-details col-12">
                     <div className="row">
                         <div className="col-3">
                           <img src={'http://139.180.155.160/img/310/300/resize'+ productList.image} />
                         </div>
                         <div className="col-9">
                            <h4 className="product_title">
                                {productList.name}
                                <span className="product_price">{'$'+productList.price.toFixed(2) }</span>
                            </h4>
                            <span className="product_quantity">Qty :  {productList.numbers || productList.qty} </span>
                            <p className="note">{productList.amasty_preorder_note}</p>
                         </div>
                     </div>
                  </div>
          </Fragment>
      )
  })

  let shipping_method_list = '';
  if(this.state.shipping_method_data.length >0){
    shipping_method_list = this.state.shipping_method_data.map( ( methodlist, index) => {

      return (
          
           <tr>     
              <td className='col-method'> < input
                            type="radio"
                            value="paymethodlist.method_codepal" 
                            checked={this.state.selected_shipping_method === methodlist.method_code ? true : false} 
                            onChange={this.handleShippingMethodOnChange.bind(this,methodlist.method_code)} />
              </td>
              <td className='col-price'> {'$'+ methodlist.amount} </td>
              <td className='col-method'>  { methodlist.method_title} </td>
              <td className='col-carrier'>{ methodlist.carrier_title} </td>          
           </tr>
       )
   })
  }


  return (
    <React.Fragment>
     <section>

      <div className="container-fluid checkout_page pl-5 pr-5 mt-5 mb-5">  
         <div className="row">

            <div className="col-xl-8 col-lg-8 col-md-7 col-sm-12 col-12">
              <div className="shipping_form">
               <h2 className="step_title">Shipping Address</h2>
                    <Form  noValidate validated={this.state.validated}>
                       <Form.Group controlId="formBasicEmail" >   {/*className="info_detils" */}
                        <Form.Label>Email address</Form.Label>
                        <Form.Control name="email" type="email" value={formData.email} onChange={this.handleInputChangeText} placeholder=""  isInvalid={!!errors.email}/>
                           {/* <span className="fa fa-question"></span> */}
                           <Form.Control.Feedback type="invalid"> {errors.email} </Form.Control.Feedback>
                        <Form.Text className="text-muted">
                          You can create an account after checkout.
                        </Form.Text>
                      </Form.Group>

                      <Form.Group controlId="formBasicPassword">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control type="text" name="firstname" value={formData.firstname} placeholder="" onChange={this.handleInputChangeText} isInvalid={!!errors.firstname}/>
                        <Form.Control.Feedback type="invalid"> {errors.firstname} </Form.Control.Feedback>
                      </Form.Group>

                      <Form.Group controlId="formBasicLastName">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control type="text" name="lastname" value={formData.lastname} placeholder="" onChange={this.handleInputChangeText} isInvalid={!!errors.lastname}/>
                        <Form.Control.Feedback type="invalid"> {errors.lastname} </Form.Control.Feedback>
                      </Form.Group>

                      <Form.Group controlId="formBasicCompany">
                        <Form.Label>Company</Form.Label>
                        <Form.Control type="text" name="company" placeholder="" onChange={this.handleInputChangeText} isInvalid={!!errors.company} />
                        <Form.Control.Feedback type="invalid"> {errors.company} </Form.Control.Feedback>
                      </Form.Group>

                      <Form.Group controlId="formBasicAddress">
                        <Form.Label>Street Address</Form.Label>
                        <Form.Control className="mb-4" type="text" name="address" placeholder="" onChange={this.handleInputChangeText}  isInvalid={!!errors.address}/>
                        <Form.Control.Feedback type="invalid"> {errors.address} </Form.Control.Feedback>
                        <Form.Control className="mb-4" type="text" name="address" placeholder="" onChange={this.handleInputChangeText} />
                      </Form.Group>

                      <Form.Group controlId="formBasicCity">
                      <Form.Label>City</Form.Label>
                        <Form.Control type="text" name="city" value={formData.city} onChange={this.handleInputChangeText} isInvalid={!!errors.city} />
                        <Form.Control.Feedback type="invalid"> {errors.city} </Form.Control.Feedback>
                      </Form.Group>

                        <Form.Group controlId="formBasicState">
                          <Form.Label>State/Province</Form.Label>
                          { ( this.state.state_arr === null || this.state.state_arr.length===0)?
                            <Form.Control type="text" name="state" placeholder="" onChange={this.handleInputChangeText} isInvalid={!!errors.state}/>:
                            <Form.Control as="select" name="state" placeholder="" onChange={this.handleInputChangeState} isInvalid={!!errors.state}>
                              <option value="">Please select a region, state or province</option>
                              { this.state.state_arr.map( ( state, index) => {
                                return <option value = {state.code}>{state.name}</option>
                              })}
                            </Form.Control>
                            }
                          <Form.Control.Feedback type="invalid"> {errors.state} </Form.Control.Feedback>
                        </Form.Group>

                         <Form.Group controlId="formBasicCountry">
                          <Form.Label>Country/Province</Form.Label>
                          <Form.Control as="select" name="country_id" placeholder="" onChange={this.handleInputChangeText} isInvalid={!!errors.country}>
                          <option value="">Select Country</option>
                          { countriesList.map( ( country, index) => {
                            return <option value = {country.id}>{country.full_name_english}</option>
                          })}
                          </Form.Control>
                          <Form.Control.Feedback type="invalid"> {errors.country_id} </Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group controlId="formBasicPhone" >
                          <Form.Label>Phone Number</Form.Label>
                          <Form.Control type="tel" name="telephone" placeholder="" onChange={this.handleInputChangeText} isInvalid={!!errors.telephone}/>
                           {/* <span className="fa fa-question"></span> */}
                           <Form.Control.Feedback type="invalid"> {errors.telephone} </Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group controlId="formBasicAddress">
                        <Form.Label>Post Code</Form.Label>
                        <Form.Control className="mb-4" type="text" name="postcode" placeholder="" onChange={this.handleInputChangeText}  isInvalid={!!errors.postcode}/>
                        <Form.Control.Feedback type="invalid"> {errors.postcode} </Form.Control.Feedback>
                      </Form.Group>
                      
                    </Form>

                    <div className="shipping_method">
                        <h3 className="step_title">Shipping Method</h3>                   
                        { this.state.shipping_method_data.length>0 ? 
                          <Fragment>
                          <table className="table-checkout-shipping-method">
                            <tbody>
                              {shipping_method_list}
                              
                            </tbody>
                          </table>
                          {selected_shipping_method_error.length !== ''?
                                <div style={{ color: 'red'}}>{selected_shipping_method_error}</div>
                              :''}
                          <div className="shipping-method-buttons-container proceed-checkout"><button onClick={this.gotoPaymentPage} className='button-action-continue'><span>Next</span></button></div>
                          </Fragment>
                          :
                            <span>Sorry, no quotes are available for this order at this time</span>
                        }
                       
                       
                       
                       
                   </div>
                 </div>

            </div>

             <div className="col-xl-4 col-lg-4 col-md-5 col-sm-12 col-12">
                
                <div className ="order_summary">
                     <h2 className="step_title">Order Summary</h2>
                  <div className="order_section">{productsInCart}</div>

                   <div className="container-fluid">
                           <div className="order-summary checkout-summary">
                             <h2 className="text-center">Summary</h2>
                             <div className="col-12">
                               <div className="row total">
                                <div className="col-6 cart-totals">
                                  <span className="order-price-name">Sub Total</span>
                                  </div>
                                 <div className="col-6 cart-totals text-right">
                                 <span className="order-price-value">{cartTotal(cartCost)}</span>
                                 </div>
                              </div>

                                <div className="row total mb-5">
                                <div className="col-6 cart-totals">
                                  <span className="order-price-name">Order Total</span>
                                  </div>
                                 <div className="col-6 cart-totals text-right">
                                 <span className="order-price-value">{cartTotal(cartCost)}</span>
                                 </div>
                               </div>
                           </div>

                             <div className="col-12 text-center">
                               {/* <Link to="/Payment" className="proceed-checkout" onClick={this.pay}>Proceed to Pay</Link> */}
                               {/* <button className="proceed-checkout" onClick={this.pay}>Proceed to Pay</button> */}
                            </div>

                        </div>
                        </div>
                  
              </div>

            </div>

         </div>

      </div> 
      
      </section>
    </React.Fragment>
  );


   }

     
}

function cartTotal(amount){
  return '$'+amount.toFixed(2);
}

const mapStateToProps = state => ({
  basketProps : state.basketState
})

export default connect(mapStateToProps) (Checkout);