import React, {Component, useEffect, useState} from 'react'; 
import { Link } from 'react-router-dom'; 
import '../styles/Regimen.scss'; 

const Regimen =()=>{

    const [ intro_container, setIntroContainer] = useState('activate');
    const [ regimen_form_container, setRegimenFormContainer] = useState('deactivate');
    
    useEffect( ()=>{
        setTimeout(function(){ setIntroContainer('deactivate'); setRegimenFormContainer('activate') }, 2000);
    },[])

return(
<React.Fragment>
    <div className="regimen-section">
        <div className="regimen-container">
            <div className="regimen-header">
                <a href="http://demo43.laraveldevloper.in/">
                    <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/microsite-logo.png" alt="logo" />
                </a>
            </div>

            <div className="regimen-body">
                <div className={"intro-container "+ intro_container}>
                    <div className="intro-text-container">
                      <h1 className="intro-text">Hello There</h1>
                    </div>
                    <div className="intro-image-container">
                        <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/hello-pic.png" alt="hello" />
                    </div>
                </div>

                <div className={"regimen-form-container "+ regimen_form_container}>
                    <form id="recommender-container" method="post" action="http://demo43.laraveldevloper.in/regimen-recommender/result/index/">
                        <div className="step-ques-1 question-container ">
                            <span className="qustn-number">01</span>
                            <p className="qustn">What's your first name</p>
                            <div className="ques-container">
                                <input type="text" name="fname" placeholder="TYPE YOUR NAME HERE" id="your-name" />
                                <span className="field-arrow next-qustn" id="next-qustn-click-event">{'>'}</span>
                            </div>
                        </div>

                        <div className="name-display-container question-container show-name ">
                            <span>Hello</span>
                            <p>Nice to meet you</p>
                            <h2 id="show-name-display"></h2>
                            <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/heart-icon.png" alt="heart" />
                        </div>

                        <div className="thanks-message-conatiner question-container more-intro">
                            <span>Thank you so much</span>
                            <p>Let's get to know you</p>
                            <h3>
                                Don't worry, it will be less <br />
                                than 2 minutes ;)
                            </h3>
                        </div>

                        <div className="step-ques-2 question-container age-container">
                            <span className="qustn-number">02</span>
                            <p className="qustn">What’s your age?</p>
                            <div className="ques-container">
                                <div className="radio-box no-btn">
                                    <input type="radio" name="age" value="18-25" attr="onclick" className="custom-radio" /> <span>18-25 <b>years young</b></span>
                                    <span className="opt-icon">
                                      <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/age-option-1.png" /></span>
                                </div>
                                <div className="radio-box no-btn">
                                    <input type="radio" name="age" value="26-38" attr="onclick" className="custom-radio" /> <span>26-38 <b>years young</b></span>
                                    <span className="opt-icon">
                                      <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/age-option-2.png" /></span>
                                </div>
                                <div className="radio-box no-btn">
                                    <input type="radio" name="age" value="39-50" attr="onclick" className="custom-radio" /> <span>39-50 <b>years young</b></span>
                                    <span className="opt-icon">
                                      <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/age-option-3.png" /></span>
                                </div>
                                <div className="radio-box no-btn">
                                    <input type="radio" name="age" value="51 and above" attr="onclick" className="custom-radio" />
                                    <span> 51
                                        <b>
                                            years young<br />
                                            and above
                                        </b>
                                    </span>
                                    <span className="opt-icon">
                                      <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/age-option-4.png" /></span>
                                </div>
                            </div>
                        </div>

                        <div className="step-ques-3 question-container">
                            <span className="qustn-number">03</span>
                            <p className="qustn">Are you?</p>

                            <div className="ques-container">
                                <div className="radio-box no-btn">
                                    <input type="radio" name="gender" value="M" attr="onclick" className="custom-radio" />
                                    <span className="opt-icon">
                                      <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/male-option.png" /></span>
                                    <span className="middle-align"><b>Male</b></span>
                                </div>
                                <div className="radio-box no-btn">
                                    <input type="radio" name="gender" value="F" attr="onclick" className="custom-radio" />
                                    <span className="opt-icon">
                                      <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/female-option.png" /></span>
                                    <span className="middle-align"><b>Female</b></span>
                                </div>
                                <div className="radio-box no-btn">
                                    <input type="radio" name="gender" value="F" attr="onclick" className="custom-radio" />
                                    <span className="opt-icon">
                                      <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/nc-gender-option.png" /></span>
                                    <span className="middle-align">
                                        <b>
                                            Gender <br />
                                            Non-Conforming
                                        </b>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div className="step-ques-4 question-container" id="dont-show-for-male">
                            <span className="qustn-number">04</span>
                            <p className="qustn">Are you pregnant or breastfeeding?</p>

                            <div className="ques-container">
                                <div className="radio-box"><input type="radio" name="pregnant or breastfeeding" value="P" attr="onclick" className="custom-radio" /> <span>Yes</span></div>
                                <div className="radio-box"><input type="radio" name="pregnant or breastfeeding" value="NP" attr="onclick" className="custom-radio" /> <span>No</span></div>
                            </div>
                        </div>

                        <div className="step-ques-5 question-container">
                            <span className="qustn-number">05</span>
                            <p className="qustn">Do you smoke?</p>

                            <div className="ques-container">
                                <div className="radio-box"><input type="radio" name="smoke" value="yes" attr="onclick" className="custom-radio" /> <span>Yes</span></div>
                                <div className="radio-box"><input type="radio" name="smoke" value="no" attr="onclick" className="custom-radio" /> <span>No</span></div>
                            </div>
                        </div>

                        <div className="step-ques-6 question-container">
                            <span className="qustn-number">06</span>
                            <p className="qustn">Alcohol consumption per week?</p>

                            <div className="ques-container">
                                <div className="radio-box no-btn">
                                    <input type="radio" name="Alcohol consumption" value="no" attr="onclick" className="custom-radio" />
                                    <span>
                                        <div className="no-value-pic">
                                          <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/no-value-icon.png" /></div>
                                        <b>I don’t drink</b>
                                    </span>
                                    <span className="opt-icon">
                                      <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/alcohol-option-1.png" /></span>
                                </div>
                                <div className="radio-box no-btn">
                                    <input type="radio" name="Alcohol consumption" value="less than 4 drinks" attr="onclick" className="custom-radio" /> <span>4 <b>drinks or less</b></span>
                                    <span className="opt-icon">
                                      <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/alcohol-option-2.png" /></span>
                                </div>
                                <div className="radio-box no-btn">
                                    <input type="radio" name="Alcohol consumption" value="More than 4 drinks" attr="onclick" className="custom-radio" /> <span>4 + <b>drinks or more</b></span>
                                    <span className="opt-icon">
                                      <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/alcohol-option-3.png" /></span>
                                </div>
                                <div className="radio-box no-btn">
                                    <input type="radio" name="Alcohol consumption" value="Too many to count" attr="onclick" className="custom-radio" /> <span>? <b>Too many to count</b></span>
                                    <span className="opt-icon">
                                      <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/alcohol-option-4.png" /></span>
                                </div>
                            </div>
                        </div>

                        <div className="step-ques-7 question-container">
                            <span className="qustn-number">07</span>
                            <p className="qustn">
                                How many hours do you use <br />
                                your computer or smartphone per day?
                            </p>

                            <div className="ques-container">
                                <div className="radio-box no-btn">
                                    <input type="radio" name="hours spent on computer" value="Less than 5 hours" attr="onclick" className="custom-radio" /> <span>5 <b>hours or less</b></span>
                                    <span className="opt-icon">
                                       <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/phone-option-1.png" /></span>
                                </div>
                                <div className="radio-box no-btn">
                                    <input type="radio" name="hours spent on computer" value="5-8 hours" attr="onclick" className="custom-radio" /> <span>5 - 8 <b>hours</b></span>
                                    <span className="opt-icon">
                                       <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/phone-option-2.png" /></span>
                                </div>
                                <div className="radio-box no-btn">
                                    <input type="radio" name="hours spent on computer" value="More than 8 hours" attr="onclick" className="custom-radio" /> <span>8 + <b>hours or more</b></span>
                                    <span className="opt-icon">
                                      <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/phone-option-3.png" /></span>
                                </div>
                            </div>
                        </div>

                        <div className="step-ques-8 question-container">
                            <span className="qustn-number">08</span>
                            <p className="qustn">How many hours do you sleep per night?</p>

                            <div className="ques-container">
                                <div className="radio-box no-btn">
                                    <input type="radio" name="sleep per night" value="1-5 hours" attr="onclick" className="custom-radio" /> <span>1 - 5 <b>hours</b></span>
                                    <span className="opt-icon">
                                      <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/sleep-option-1.png" /></span>
                                </div>
                                <div className="radio-box no-btn">
                                    <input type="radio" name="sleep per night" value="More than 6 hours" attr="onclick" className="custom-radio" /> <span>6 - 8 <b>hours</b></span>
                                    <span className="opt-icon">
                                      <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/sleep-option-2.png" /></span>
                                </div>
                                <div className="radio-box no-btn">
                                    <input type="radio" name="sleep per night" value="I struggle with insomnia" attr="onclick" className="custom-radio" />
                                    <span>
                                        <div className="no-value-pic">
                                            <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/no-value-icon.png" />
                                        </div>
                                        <b>
                                            I struggle with <br />
                                            insomnia
                                        </b>
                                    </span>
                                    <span className="opt-icon">
                                      <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/sleep-option-3.png" /></span>
                                </div>
                            </div>
                        </div>

                        <div className="step-ques-9 question-container">
                            <span className="qustn-number">09</span>
                            <p className="qustn">How regularly do you exercise per week?</p>

                            <div className="ques-container">
                                <div className="radio-box no-btn">
                                    <input type="radio" name="exercise per week" value="I don’t exercise" attr="onclick" className="custom-radio" /> <span>0 <b>I don’t exercise</b></span>
                                    <span className="opt-icon">
                                      <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/exercise-option-1.png" /></span>
                                </div>
                                <div className="radio-box no-btn">
                                    <input type="radio" name="exercise per week" value="0-1 Barely" attr="onclick" className="custom-radio" /> <span>0-1 <b>Barely</b></span>
                                    <span className="opt-icon">
                                      <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/exercise-option-2.png" /></span>
                                </div>
                                <div className="radio-box no-btn">
                                    <input type="radio" name="exercise per week" value="1-3 Times Per Week" attr="onclick" className="custom-radio" /> <span>1-3 <b>Times Per Week</b></span>
                                    <span className="opt-icon"> 
                                    <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/exercise-option-3.png" /></span>
                                </div>
                                <div className="radio-box no-btn">
                                    <input type="radio" name="exercise per week" value="3-5 Times Per Week" attr="onclick" className="custom-radio" />
                                    <span>3-5 <b>Times Per Week</b></span>
                                    <span className="opt-icon"> 
                                    <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/exercise-option-4.png" /></span>
                                </div>
                            </div>
                        </div>

                        <div className="step-ques-10 question-container">
                            <span className="qustn-number">10</span>
                            <p className="qustn">
                                How many products do you use <br />
                                in your current skincare regimen?
                            </p>

                            <div className="ques-container">
                                <div className="radio-box no-btn">
                                    <input type="radio" name="products on skin" value="1-3" attr="onclick" className="custom-radio" /> <span>1-3 <b></b></span>
                                    <span className="opt-icon"> 
                                    <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/skincare-option-1.png" /></span>
                                </div>
                                <div className="radio-box no-btn">
                                    <input type="radio" name="products on skin" value="4-6" attr="onclick" className="custom-radio" /> <span>4-6 <b></b></span>
                                    <span className="opt-icon"> 
                                    <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/skincare-option-2.png" /></span>
                                </div>
                                <div className="radio-box no-btn">
                                    <input type="radio" name="products on skin" value="7-10" attr="onclick" className="custom-radio" /> <span>7-10 <b></b></span>
                                    <span className="opt-icon"> 
                                    <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/skincare-option-3.png" /></span>
                                </div>
                                <div className="radio-box no-btn">
                                    <input type="radio" name="products on skin" value="10 or More" attr="onclick" className="custom-radio" />
                                    <span>10 <b>or More</b></span>
                                    <span className="opt-icon">
                                       <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/skincare-option-4.png" /></span>
                                </div>
                            </div>
                        </div>

                        <div className="step-ques-11 question-container">
                            <span className="qustn-number">11</span>
                            <p className="qustn">
                                What other skincare <br />
                                brands do you use?
                            </p>
                            <p className="type-example">(FOR EXAMPLE: LA MER, KIEHLS, ORIGINS)</p>

                            <div className="ques-container">
                                <input type="text" name="skin-brands" placeholder="list your brand(s) here" id="brand-list" />
                                <span className="field-arrow next-qustns" id="brands-click"> > </span>
                            </div>
                        </div>

                        <div className="step-ques-12 question-container final-step-already-loggedin">
                            <span className="qustn-number">12</span>
                            <p className="qustn">What are your main skin concerns? (pick 3)</p>

                            <div className="ques-container">
                                <div className="top-opt-list">
                                    <div className="radio-box no-btn">
                                        <input name="skin-problem[]" type="checkbox" value="acne" className="LCheckbox custom-radio" attr="skin" />
                                        <span className="opt-icon">
                                          <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/skin-concerns-option-1.png" /></span>
                                        <span><b>Acne</b></span>
                                    </div>

                                    <div className="radio-box no-btn">
                                        <input name="skin-problem[]" type="checkbox" value="pores" className="LCheckbox custom-radio" attr="skin" />
                                        <span className="opt-icon">
                                          <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/skin-concerns-option-2.png" /></span>
                                        <span><b>Pores</b></span>
                                    </div>

                                    <div className="radio-box no-btn">
                                        <input name="skin-problem[]" type="checkbox" value="wrinkles" className="LCheckbox custom-radio" attr="skin" />
                                        <span className="opt-icon">
                                          <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/skin-concerns-option-3.png" /></span>
                                        <span><b>Fine Lines / Wrinkles</b></span>
                                    </div>

                                    <div className="radio-box no-btn">
                                        <input name="skin-problem[]" type="checkbox" value="damage" className="LCheckbox custom-radio" attr="skin" />
                                        <span className="opt-icon">
                                          <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/skin-concerns-option-4.png" /></span>
                                        <span><b>Pollution Damage</b></span>
                                    </div>

                                    <div className="radio-box no-btn">
                                        <input name="skin-problem[]" type="checkbox" value="ageing" className="LCheckbox custom-radio" attr="skin" />
                                        <span className="opt-icon">
                                          <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/skin-concerns-option-5.png" /></span>
                                        <span><b>Signs of Aging</b></span>
                                    </div>
                                </div>

                                <div className="bottom-opt-list">
                                    <div className="radio-box no-btn">
                                        <input name="skin-problem[]" type="checkbox" value="darkspots" className="LCheckbox custom-radio" attr="skin" />
                                        <span className="opt-icon">
                                          <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/skin-concerns-option-6.png" /></span>
                                        <span><b>Dark Spots / Pigmentation</b></span>
                                    </div>

                                    <div className="radio-box no-btn">
                                        <input name="skin-problem[]" type="checkbox" value="dehydration" className="LCheckbox custom-radio" attr="skin" />
                                        <span className="opt-icon">
                                          <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/skin-concerns-option-7.png" /></span>
                                        <span><b>Dehydration</b></span>
                                    </div>

                                    <div className="radio-box no-btn">
                                        <input name="skin-problem[]" type="checkbox" value="redness" className="LCheckbox custom-radio" attr="skin" />
                                        <span className="opt-icon">
                                          <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/skin-concerns-option-8.png" /></span>
                                        <span><b>Redness</b></span>
                                    </div>

                                    <div className="radio-box no-btn">
                                        <input name="skin-problem[]" type="checkbox" value="sensitivity" className="LCheckbox custom-radio" attr="skin" />
                                        <span className="opt-icon"> 
                                        <img src="http://demo43.laraveldevloper.in/pub/static/version1603882761/frontend/OM/alliesskin-v2/en_US/images/quiz-icons/skin-concerns-option-9.png" /></span>
                                        <span><b>Sensitivity</b></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="routine-container-after-registration underground-registration-success  " style={{ display:'none'}}>
                            <p className="thankstext">Thanks for sign up</p>
                            <p className="info">
                                Your personalized routine will be saved <br />
                                in your Allies Underground account, <br />
                                and also emailed to you shortly.
                            </p>
                            <input type="hidden" id="register-customer-email" name="email" value="rawat.girdhar@orangemantra.in" />
                            <button type="submit" className="viewroutine">View routine</button>
                        </div>

                        <div className="step-ques-13 question-container" style={{ display:'none'}}>
                            <span className="qustn-number">Finally</span>
                            <p className="qustn">
                                What's your email address? <br />
                                so, we can send you your <br />
                                personalized skincare regimen
                            </p>

                            <div className="ques-container">
                                <input type="email" placeholder="type your email here" name="email" required />
                                <button className="field-arrow" type="submit">></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div className="back-to-quiz-container"><span className="quiz-back-button">back</span></div>
            </div>
        </div>
    </React.Fragment>
    );
 } 
 export default Regimen;
