import React, { Fragment, Component } from 'react';
import { Redirect, Link, withRouter } from 'react-router-dom';
import { Form, Button, Tooltip, Accordion, Card } from 'react-bootstrap';
import '../../styles/Checkout.scss';

class PaymentSucess extends Component {

    constructor(props) {
        super(props);
        this.state = { 
            orderDetails : this.props.location.state ? this.props.location.state.orderPlaceData? this.props.location.state.orderPlaceData : '':'',
            orderPlaceEmail : this.props.location.state ? this.props.location.state.orderPlaceEmail ? this.props.location.state.orderPlaceEmail : '':''
        }
    }

    render() {
        
        const { orderDetails, orderPlaceEmail } = this.state;
        return (
            <React.Fragment>
                <div className="payment-sucess">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="checkout-success">
                                    <p>Your order # is: <span>{orderDetails.orderNumber}</span>.</p>
                                    <p>We'll email you an order confirmation with details and tracking info.</p>
                                    <div className="actions-toolbar">
                                        <div className="primary">
                                            <Link className="action primary continue" to="/Shop"><span>Continue Shopping</span></Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="createbutton">
                                    <p >You can track your order status by creating an account.</p>
                                    <p><span>Email Address</span>: <span>{orderPlaceEmail}</span></p>
                                    <Link class="action primary" to="/Signup">
                                        <span>Create an Account</span>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default PaymentSucess;