import React, { useState, useEffect } from 'react';
import '../../../styles/ProductModalwhite.scss';

const ModalWhite = ( props) => {
  const [activeTab, setIactiveTabs] = useState('usage');

  const dynammicModalClass = () => (props.isShow ? { display: 'block' } : '');
  return (

    <div className={"modal " + (props.isShow ? 'classshowwhite' : 'classhidewhite')} id="channelModalwhite">
      <div className="modal-dialog modal-dialog-slideout-white" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <button
              onClick={ props.closeModal}
              style={{ color: '#000' }}
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
              
            >
              <span aria-hidden="true">X</span>
            </button>
          </div>

          <div className="modal-body">
            <div className="row">
              <div className="col-12">
                <div className="pouptext">
                  <div className="navlink">
                    <ul className="tablink">
                        <li className={ activeTab === 'usage' ? "active":''}><a onClick={()=>{setIactiveTabs( 'usage')}}>usage</a></li>
                        <li className={ activeTab === 'benifits' ? "active":''}><a onClick={()=>{setIactiveTabs( 'benifits')}}>benifits</a></li>
                        <li className={ activeTab === 'caution' ? "active":''}><a onClick={()=>{setIactiveTabs( 'caution')}}>caution</a></li>
                    </ul>
                  </div>
                  <div className="tab-content-section" style={ { display:  activeTab === 'usage' ? 'block':'none'}}>
                    <div className="tab-text">
                        <div dangerouslySetInnerHTML={{__html: props.product.suggested_usage}}></div>
                    </div>
                  </div>
                  <div className="tab-content-section" style={ { display:  activeTab === 'benifits' ? 'block':'none'}}>
                    <div className="row" dangerouslySetInnerHTML={{__html: props.product.benefits}}></div>
                  </div>
                  <div className="tab-content-section" style={ { display:  activeTab === 'caution' ? 'block':'none'}}>
                    <div className="tab-text">
                      <div dangerouslySetInnerHTML={{__html: props.product.layering}}></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModalWhite;
