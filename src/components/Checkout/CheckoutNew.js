import React, {Fragment, Component} from 'react';
import {Form, Button, Tooltip, Accordion, Card } from 'react-bootstrap';
import '../../styles/Checkout.scss';
import { Redirect, Link, withRouter } from 'react-router-dom';
import { isAuthenticated } from '../../account/Repository';
import {  SET_SPINNER_STARTED, SET_SPINNER_COMPLETED } from '../../actions/types';
import { connect } from 'react-redux';
import OrderSummary from './orderSummary';
import CheckoutProcess from './CheckoutProcess';
import SignupPopup from './SignupPopup';
import ShippingInfo from './ShippingInfo';
import { login , setUserProfileApi} from '../../account/Repository';
import { changeLoginStatus, setUserProfile } from '../../actions/changeLoginStatus';
import { getNumbers, checkOldCartAndUpdate } from '../../actions/getAction';
class CheckoutNew extends Component {

   constructor(props) {
      super(props);
      this.state = { 
        user_info: localStorage.getItem('user-info-allies') ? 
          JSON.parse(localStorage.getItem('user-info-allies')).isUserInfo ?
            JSON.parse(localStorage.getItem('user-info-allies')).isUserInfo
            : JSON.parse(localStorage.getItem('user-info-allies')) 
            : '',
            signin_error : '',
            user_info_address: localStorage.getItem('user-info-allies') ? 
          JSON.parse(localStorage.getItem('user-info-allies')).isUserInfo ?
            JSON.parse(localStorage.getItem('user-info-allies')).isUserInfo.addresses
            : JSON.parse(localStorage.getItem('user-info-allies')).addresses 
            : [],
       }
      }

      componentWillMount(){
        if(localStorage.getItem('x-access-token')){
          this.props.dispatch(setUserProfile());

          if(this.state.user_info_address.length == 0){
             setUserProfileApi().then(res => {
             this.setState({ user_info_address :  res.result.addresses });
           })
        }
         
        }
      }
      
      gotoPaymentPage=( data)=>{
        sessionStorage.setItem('checkout_shipping_data', JSON.stringify(data));
        data.email = this.state.user_info.email;
        this.props.history.push({ pathname:'/Payment', state : { shippingData: data}});
      }

      closeLoader=()=>{
        this.props.dispatch({ type : SET_SPINNER_COMPLETED});
      }

      showLoader=()=>{
        this.props.dispatch({ type : SET_SPINNER_STARTED});
      }

      submitLogin=( data)=> {
        this.showLoader()
            login(data)
            .then(token => {
                if(token.code === 200) {   
                  setUserProfileApi().then(res => {
                    this.setState({ user_info_address :  res.result.addresses });
                  })
                  this.props.dispatch(changeLoginStatus(true));
                if( this.props.basketProps && this.props.basketProps.products && this.props.basketProps.products.productList && this.props.basketProps.products.productList.length>0){
                    this.props.dispatch(checkOldCartAndUpdate( this.props.basketProps.products.productList));
                }else{
                    sessionStorage.removeItem('cart-id')
                    this.props.dispatch(getNumbers());
                }

                //
                
                }
                setTimeout( ()=>{
                  this.closeLoader();
                } , 4000);

                
            }).catch(err => {
              
              this.setState({ signin_error: err})
              this.closeLoader();
            });
        
    }

     
      render() {
        // if (!isAuthenticated()) return (<Redirect to={{pathname :"/login", state: { pre_page_url: '/Checkout' }}} />);
        if(this.props.basketProps.products && this.props.basketProps.products.productList.length>0){
          return (
            <React.Fragment>
              { localStorage.getItem('x-access-token') ? '' : <SignupPopup  signin_error={this.state.signin_error} submitLogin={ this.submitLogin }/> }
                <CheckoutProcess view="checkout"/>
                <section>
                    <div className="container-fluid checkout_page pl-5 pr-5 mt-5 mb-5">  
                      <div className="row">
                          <div className="col-xl-8 col-lg-8 col-md-7 col-sm-12 col-12">
                            <ShippingInfo  
                              showLoader = {this.showLoader}
                              closeLoader = {this.closeLoader}
                              products = {this.props.basketProps.products.productList} 
                              user_info_address={ this.state.user_info_address}
                              gotoPaymentPage={this.gotoPaymentPage}/>
                          </div>
                          <div className="col-xl-4 col-lg-4 col-md-5 col-sm-12 col-12">
                            <div className ="order_summary">
                              <h2 className="step_title">Order Summary</h2> 
                              <OrderSummary products = {this.props.basketProps.products.productList} />
                            </div>
                          </div>
                      </div>
                    </div> 
            </section>
          </React.Fragment >
        );
        }else{ return (

          <section>
          <div className="container-fluid checkout_page pl-5 pr-5 mt-5 mb-5">  
            <div className="row">
                <div className="col-xl-8 col-lg-8 col-md-7 col-sm-12 col-12">
                  <div>You have no items in your shopping cart.</div>
                  <div>Click <Link to ='/'>here</Link> to continue shopping.</div>
                </div>
            </div>
          </div>
          </section>
        )}

        

    }

}

function cartTotal(amount){
  return '$'+amount.toFixed(2);
}

const mapStateToProps = state => ({
  basketProps : state.basketState
})

export default connect(mapStateToProps) (CheckoutNew);