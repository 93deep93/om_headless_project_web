import React, { Component } from 'react';
import PaginationView from '../assets/pagination';
import '../../../styles/AccountInfo.scss';
import AccountDashMenu from '../myAccountDashboardMenu';
import Addressinfo from '../assets/address_info';
import { Link } from 'react-router-dom';
import countriesList from '../../../data/countries_reagions.json';
import { getUserInfo} from '../../../actions/customerInfo';
import {  isAuthenticated} from '../../../account/Repository';
import { Redirect} from 'react-router-dom';
import { connect } from 'react-redux';
import { getUserInfoApi, postUserInfoApi, updateUserPasswordApi} from '../../../account/customerInfoApi';
import { CHECK_UNAUTHORIZED , SET_SPINNER_STARTED, SET_SPINNER_COMPLETED } from '../../../actions/types';
import ModelAlertDelete from '../assets/model_alert_delete';

const limit=10;
class AddressBook extends Component {

    constructor(props) {
        super(props);
        this.state = {
            countriesList : countriesList,
            default_billing_address : '',
            default_shipping_address : '',
            additionalAddress : [],
            currentPage: 1,
            error_api_res : '',
            showModelDelete: false,
            deleteAddressValue : ''
        };
    }

    componentDidMount(){
        this.props.dispatch( getUserInfo());
    }

    getCounteryName=(country_id)=>{
        return this.state.countriesList.filter(x=> x.id == country_id)[0].full_name_english;
    }

    UNSAFE_componentWillReceiveProps(){
        if( this.props.propsData && this.props.propsData.isUserInfo && this.props.propsData.isUserInfo.addresses){
            const address = this.props.propsData.isUserInfo.addresses;
            var default_shipping = address.filter(function (entry) { return entry.default_shipping === true; });
             this.setState({ default_shipping_address : default_shipping.length>0 ? default_shipping[0] : ''});
             var default_billing = address.filter(function (entry) { return entry.default_billing === true; });
             this.setState({ default_billing_address : default_billing.length>0 ? default_billing[0] : ''});

            var additionalAddress = address.filter(function (entry) { return (entry.default_shipping !== true && entry.default_billing !== true); });
           
            this.setState({ additionalAddress : additionalAddress});
        }
    }

    handlePageChange = (page, e) => {
        this.setState({
        currentPage: page
        });
    };

    closeDeleteModel =()=>{
        this.setState({ showModelDelete:false, deleteAddressValue: ''});
    }

    manageStreetArray=(usrInfo)=>{
        for( let i=0; i< usrInfo.addresses.length ; i++){
            if( usrInfo.addresses[i].street.length<2){
             usrInfo.addresses[i].street.push(" ");
            }
        }
        return usrInfo
    }

    
    manageCountryObj=(usrInfo)=>{
        for( let i=0; i< usrInfo.addresses.length ; i++){

            usrInfo.addresses.map(item => ({ 
                 id : item.id,
                 customer_id : item.customer_id,
                 region : item.region,
                 region_id : item.region_id,
                 country_id : item.country_id,
                 street : item.street,
                 company : item.company,
                 telephone : item.telephone,
                 postcode : item.postcode,
                 city : item.city,
                 firstname : item.firstname,
                 lastname : item.lastname,
            
            }));
        }
        return usrInfo
    }

    deleteAddress = ( address )=>{
        this.setState({ showModelDelete:true, deleteAddressValue : address });
    }

    deleteAddressConfirm =()=>{
        const userInfo = this.props.propsData.isUserInfo;
        userInfo.addresses.map((pd, index)=>{
			if( this.state.deleteAddressValue.id === pd.id){
				userInfo.addresses.splice(index,1);
			}
		});
        this.deleteAddressApi( this.manageStreetArray(userInfo));
        this.setState({ showModelDelete: false, deleteAddressValue : '' });
    }

    deleteAddressApi =(info)=>{
        this.props.dispatch({ type : SET_SPINNER_STARTED});
        
        postUserInfoApi(info).then(res => {
            this.props.dispatch({ type : SET_SPINNER_COMPLETED});
            if(res.code === 200){
                this.props.dispatch( getUserInfo());
             }
        }).catch(err => {
            this.props.dispatch({ type : SET_SPINNER_COMPLETED});
            if(err.response && err.response.data.code === 401 && err.response.data.result === "The consumer isn't authorized to access self."){
                this.props.dispatch({ type : CHECK_UNAUTHORIZED, payload: true});
            }else{
                    this.setState({ error_api_res : err.response.data.result.errorMessage})
                }
            this.setState({ error_api_res : err.response.data.result.errorMessage});
        });
    }
    
    render() {
        
        if (!isAuthenticated()) return (<Redirect to="/login" />);

        const {default_billing_address, default_shipping_address, error_api_res , additionalAddress, currentPage, showModelDelete} = this.state;
        let start = Number( (currentPage-1) *limit);
        let end = Number(currentPage*limit);
        return (
            <React.Fragment>
                {error_api_res ?<div className="product-error">{error_api_res} </div>:''}  
                <section className="page_banner">
                    <div className="rowsec-account" id="maincontent">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-md-4">

                                <AccountDashMenu/>

                                </div>
                                <div className="col-md-8">
                                    <div className="block block-dashboard-info">
                                        <div className="block-title"><strong>Default Addresses</strong></div>
                                        <div className="block-content">
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <div className="box box-information">
                                                        <strong className="box-title">
                                                            <span>Default Billing Address</span>
                                                        </strong>
                                                        {default_billing_address === ''? <div className="box-content">You have no default billing address in your address book. </div>:
                                                        <>
                                                            <div className="box-content">
                                                        
                                                                < Addressinfo billing_address={default_billing_address}/>
                                                            
                                                            </div>
                                                            <div className="box-actions">
                                                                <Link  to= { default_billing_address === ''? { pathname: 'address-new'} : {pathname: 'address-edit', state : { address_details : default_billing_address }} } >
                                                                    <span>Change Billing Address</span> 
                                                                </Link>
                                                            </div>
                                                        </>}
                                                    </div>
                                                </div>
                                                <div className="col-md-6">
                                                    <div className="box box-newsletter">
                                                        <strong className="box-title">
                                                            <span>Default Shipping Address</span>
                                                        </strong>
                                                        {default_shipping_address === ''? <div className="box-content">You have no default shipping address in your address book.</div>:
                                                           <>
                                                           <div className="box-content">
                                                            
                                                            < Addressinfo billing_address={default_shipping_address}/>
                                                            
                                                            </div>
                                                            <div className="box-actions">
                                                                <Link  to= { default_shipping_address === ''? { pathname: 'address-new'} : {pathname: 'address-edit', state : { address_details : default_shipping_address }} } >
                                                                    <span>Change Shipping Address</span> 
                                                                </Link>
                                                            </div>
                                                            </>
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="block block-dashboard-addresses">
                                        
                                        <div className="block-title">
                                            <strong>Additional Address Entries</strong>

                                        </div>

                                            

                                        <div className="block-content">
                                        <ol className="items addresses">
                                            {additionalAddress.length === 0 ? <li className="item">No record found!</li>
                                            :
                                            additionalAddress.map( (address, index) => {

                                                return(
                                                    <li className="item">
                                                        < Addressinfo billing_address={address}/>
                                                        <div className="item actions">
                                                            <Link className="action edit"  to={{pathname: 'address-edit', state : { address_details : address } }} >
                                                                <span>Edit Address</span> 
                                                            </Link>
                                                            <a className="action delete" onClick={ this.deleteAddress.bind(this,address ) } role="delete-address" data-address="6328"><span>Delete Address</span></a>
                                                        </div>
                                                    </li>
                                                )
                                            })

                                               
                                            }
                                            </ol>
                                        </div>

                                        <div className="block block-addresses-list">
                                            <div className="actions-toolbar">
                                                <div className="primary">
                                                    <Link to={{pathname: 'address-new' }} >
                                                        <button className="action save primary" title="Add New Address"><span>Add New Address</span></button>
                                                    </Link>
                                                </div>                                          
                                            </div>
                                            <div className="block-title"><strong>Additional Address Entries</strong></div>
                                            {additionalAddress.length > 0 ?
                                            <div className="block-content">

                                                <div className="table-wrapper additional-addresses">
                                              
                                                    <table className="data table table-additional-addresses-items history" id="additional-addresses-table">
                                                        <caption className="table-caption">Additional addresses</caption>
                                                        <thead>
                                                            <tr>
                                                                <th scope="col" className="col firstname">First Name</th>
                                                                <th scope="col" className="col lastname">Last Name</th>
                                                                <th scope="col" className="col streetaddress">Street Address</th>
                                                                <th scope="col" className="col city">City</th>
                                                                <th scope="col" className="col country">Country</th>
                                                                <th scope="col" className="col state">State</th>
                                                                <th scope="col" className="col zip">Zip/Postal Code</th>
                                                                <th scope="col" className="col phone">Phone</th>
                                                                <th scope="col" className="col actions"> </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        {additionalAddress.slice( start, end).map( (address, index) => {

                                                        return(
                                                            <tr>
                                                                <td data-th="First Name" className="col firstname">{address.firstname}</td>
                                                                <td data-th="Last Name" className="col lastname">{address.lastname}</td>
                                                                <td data-th="Street Address" className="col streetaddress">{address.street.join(' ')} </td>
                                                                <td data-th="City" className="col city">{address.city}</td>
                                                                <td data-th="Country" className="col country">{ this.getCounteryName( address.country_id)}</td>
                                                                <td data-th="State" className="col state">{address.region.region}</td>
                                                                <td data-th="Zip/Postal Code" className="col zip">{address.postcode}</td>
                                                                <td data-th="Phone" className="col phone">{address.telephone}</td>
                                                                <td data-th="Actions" className="col actions">
                                                                    <Link className="action edit"  to={{pathname: 'address-edit', state : { address_details : address } }} >
                                                                        <span>Edit</span> 
                                                                    </Link>
                                                                    <a className="action delete" onClick={ this.deleteAddress.bind(this,address ) }  role="delete-address" data-address="6328"><span>Delete</span></a>
                                                                </td>
                                                            </tr>
                                                            )}
                                                        )}
                                                        </tbody>
                                                    </table> 
                                                </div>
                                                <div className="customer-addresses-toolbar toolbar bottom">
                                                    <div className="pager">

                                                        <p className="toolbar-amount">
                                                            <span className="toolbar-number">
                                                            Items  { ((this.state.currentPage-1)*limit)+1 } to { ((this.state.currentPage-1)*limit) + (additionalAddress.slice( start, end).length)} of {additionalAddress.length} total </span>
                                                        </p>
                                                       
                                                        <div className="limiter">
                                                        {additionalAddress.length > limit ? 
                                                            <PaginationView 
                                                            total={ additionalAddress.length} 
                                                            pageCount ={ Number(additionalAddress.length/limit)} 
                                                            limit ={limit} 
                                                            currentPage ={this.state.currentPage}
                                                            handlePageChange={this.handlePageChange}
                                                            />:''}
                                                        </div>
                                                    </div>

                                                </div>

                                                <div className="actions-toolbar">
                                                    <div className="primary">
                                                        <Link to={{pathname: 'address-new' }} >
                                                            <button className="action save primary" title="Add New Address"><span>Add New Address</span></button>
                                                        </Link>
                                                    </div>                                          
                                                </div>
                                            </div>
                                        
                                            : <span className="item">No record found!<br/></span>}
                                        </div>


                                        <ModelAlertDelete showModelDelete={showModelDelete} closeDeleteModel={ this.closeDeleteModel} deleteAddressConfirm={this.deleteAddressConfirm}/>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
            </React.Fragment>
        );

    }

}

const mapStateToProps = state => ({
    propsData : state.userinfoState,
  })
  
  export default connect(mapStateToProps) (AddressBook);