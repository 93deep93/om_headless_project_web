import React, { Component } from 'react';
import Pagination from 'react-js-pagination';
import '../../styles/AccountInfo.scss';
import { Accordion, Card, Button } from 'react-bootstrap';

class AccountDashMenu extends Component {

  constructor(props) {
    super(props);
    this.state = {
    };
  }


  render() {

    return (
        <React.Fragment>
            <div className="left-fixed-menu">
                    <div className="sidebar sidebar-main">
                      <div className="block block-collapsible-nav">
                        <>
                          <Accordion>
                            <Card>
                              <Accordion.Toggle as={Card.Header} eventKey="0">
                                <div className="title block-collapsible-nav-title">
                                  <strong>Account Dashboard</strong>
                                </div>
                              </Accordion.Toggle>
                              <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                  <div className="content block-collapsible-nav-content" id="block-collapsible-nav">
                                    <ul className="nav items">
                                      <li className="nav item current"><a href="/accountinfo"><strong>Account Dashboard</strong></a></li>
                                      <li className="nav item"><a href="/myorders">My Orders</a></li>
                                      <li className="nav item"><a href="/address">Address Book</a></li>
                                      <li className="nav item"><a href="/accountinfo-edit">Update Your Information</a></li>
                                      {/* <li className="nav item"><a href="/settings">Settings</a></li>
                                      <li className="nav item"><a href="#">My Saved Cards</a></li>
                                      <li className="nav item"><a href="#">My Product Reviews</a></li>
                                      <li className="nav item"><a href="/newslettermanage">Newsletter Subscriptions</a></li>
                                      <li className="nav item"><a href="#">My Returns</a></li>
                                      <li className="nav item"><a href="#">My Rewards and Points</a></li>
                                      <li className="nav item"><a href="#">My Routine</a></li> */}
                                    </ul>
                                  </div>
                                </Card.Body>
                              </Accordion.Collapse>
                            </Card>
                          </Accordion>
                          <style type="text/css">
                            {`
                          .card{
                            border:none;
                          }
                      .collapse:not(.show) {
                        display: block;
                        height: 100%;
                      }
                      .left-fixed-menu .card-header, .left-fixed-menu .card-body {
                        padding: 0;
                        border: none;
                      }
                      .accordion, .accordion>.card, .card-body {
                        height: 100%;
                    }
                    .accordion>.card>.card-header {

                      margin-bottom: 0px;
                  }
                      @media only screen and (max-width: 767px){
                        .collapse:not(.show) {
                          display: none;
                        }
                      }
                          `}

                          </style>
                        </>
                      </div>
                    </div>

                  </div>
        </React.Fragment>
        );

    }

}

export default AccountDashMenu;