import React from 'react';
import '../styles/About.scss';
import PageContent from "./PageContent";
import AOS from 'aos';
import 'aos/dist/aos.css';
AOS.init();
const About = () => {
  return (
    <React.Fragment>
      <div className ="about-us">
        <PageContent pageId={25} />
      </div>
    </React.Fragment>
  );
}

export default About;