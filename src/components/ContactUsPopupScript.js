import React, { Component } from 'react';
// import ScriptTag from 'react-script-tag';
 
class ContactUsPopup extends Component {
     createMarkup =()=> {
        return {__html: '<script type="text/javascript" src="https://s3.amazonaws.com/assets.freshdesk.com/widget/freshwidget.js"></script>\
        <script type="text/javascript">\
        FreshWidget.init("", {"queryString": "&widgetType=popup&formTitle=Contact+Us&submitTitle=EMAIL+US&submitThanks=Thank+you.%0D%0AYour+message+has+been+sent.&captcha=yes&searchArea=no", "utf8": "✓", "widgetType": "popup", "buttonType": "text", "buttonText": "CONTACT US", "buttonColor": "white", "buttonBg": "#000000", "alignment": "2", "offset": "500px", "submitThanks": "Thank you.\r\nYour message has been sent.", "formHeight": "500px", "captcha": "yes", "url": "https://alliesofskin.freshdesk.com"} );\
        </script>'};
      }
    render() {
        return (<div dangerouslySetInnerHTML={ this.createMarkup()} />);
    }
}

export default ContactUsPopup;