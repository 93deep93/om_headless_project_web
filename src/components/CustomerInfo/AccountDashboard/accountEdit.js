import React, { Component } from 'react';
import Pagination from 'react-js-pagination';
import '../../../styles/AccountInfo.scss';
import AccountDashMenu from '../myAccountDashboardMenu';
import {  isAuthenticated } from '../../../account/Repository';
import { getUserInfoApi, postUserInfoApi, updateUserPasswordApi} from '../../../account/customerInfoApi';
import { CHECK_UNAUTHORIZED , SET_SPINNER_STARTED, SET_SPINNER_COMPLETED } from '../../../actions/types';
import { Redirect} from 'react-router-dom';
import { connect } from 'react-redux';

class AccountInfoEdit extends Component {

    constructor(props) {
        super(props);
        this.state = {
            orderDetails : this.props.location.state? this.props.location.state.userInfo : '',
            changeEmailCheck : this.props.location.state? this.props.location.state.changeEmailCheck : false,
            changePasswordCheck : this.props.location.state? this.props.location.state.changePasswordCheck : false,
            firstname : this.props.location.state ? this.props.location.state.userInfo.firstname : '',
            firstname_error: '',
            lastname_error: '',
            new_email_error: '',
            lastname: this.props.location.state ? this.props.location.state.userInfo.lastname : '',
            new_email: this.props.location.state ? this.props.location.state.userInfo.email : '',
            current_password:'',
            current_password_error:'',
            current_password : '',
            confirm_password: '',
            confirm_password_error : '',
            new_password : '',
            new_password_error: '',
            error_api_res : '',
        };
    }

    componentDidMount(){
        debugger
        if(!this.props.location.state){
            // dispatch({ type : SET_SPINNER_STARTED});
                getUserInfoApi()
                .then(res => {
                if(res.code === 200) { 
                    this.setState({
                        orderDetails : res.result ? res.result.userInfo ? res.result.userInfo : res.result :'',
                        firstname : res.result ? res.result.firstname : '',
                        lastname: res.result ? res.result.lastname : '',
                        new_email: res.result ? res.result.email : '',
                    })
                    // dispatch({ type : SET_SPINNER_COMPLETED});			
                }
                }).catch(err => {
                    // dispatch({ type : SET_SPINNER_COMPLETED});
                console.log(err)
                });
        }
    }

    handleChange=(e)=>{
        this.setState({ [ e.target.name]: e.target.value, [e.target.name+'_error'] : ''});
    }

    handleChangeEmailCheck=()=>{ this.setState({ changeEmailCheck : !this.state.changeEmailCheck, new_email_error:''}) }
    handleChangePasswordCheck=()=>{ this.setState({ changePasswordCheck : !this.state.changePasswordCheck, current_password_error:'',confirm_password_error : '',new_password_error:""}) }

    validateEmail=( email)=>{
        const regex_email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex_email.test(String(email).toLowerCase());
    }

    validatePassword=( password)=>{
        const regex_password = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/;
        return regex_password.test(password);
      }

    validate=() =>{
        let flag = false;
        var { changePasswordCheck, changeEmailCheck, firstname, lastname, new_email, firstname_error, lastname_error, new_email_error, current_password, current_password_error, confirm_password, confirm_password_error, new_password, new_password_error} = this.state;
        
        if( firstname === "" || firstname=== null || firstname === undefined){
            firstname_error = 'This field is required';
            this.setState({firstname_error : firstname_error})
            flag = true;
        }
        if( lastname === "" || lastname=== null || lastname === undefined){
            lastname_error = 'This field is required';
            this.setState({lastname_error : lastname_error})
            flag = true;
        }
        if( (new_email=== "" || new_email=== null || new_email === undefined) && changeEmailCheck ){
            new_email_error = 'This field is required';
            this.setState({new_email_error : new_email_error})
            flag = true;
        }else if(( new_email !== "" || new_email !== null || new_email !== undefined) && changeEmailCheck){
            if(!this.validateEmail( new_email)){
                new_email_error = 'Please enter valid email id !'
                this.setState({new_email_error : new_email_error})
                flag = true;
            }
        }

        if( (new_password === "" || new_password=== null || new_password === undefined) && changePasswordCheck){
            new_password_error = 'This field is required';
            this.setState({ new_password_error : new_password_error})
            flag = true;
        }else if( (new_password !== "" || new_password !== null || new_password !== undefined) && changePasswordCheck){
            if(!this.validatePassword(new_password)){
                new_password_error = 'Password must have at least 8 letters, one uppercase character, one lowercase character, one digit and one special character !'
              this.setState({ new_password_error : new_password_error})
              flag = true;
            }
        }

        if( (confirm_password === "" || confirm_password=== null || confirm_password === undefined) && changePasswordCheck){
            confirm_password_error = 'This field is required';
            this.setState({ confirm_password_error : confirm_password_error})
            flag = true;
        }

        if( (current_password === "" || current_password=== null || current_password === undefined) && changePasswordCheck){
            current_password_error = 'This field is required';
            this.setState({current_password_error : current_password_error})
            flag = true;
        }

        if( (new_password !== "") &&  (confirm_password !== "") && changePasswordCheck){
            if( confirm_password  !== new_password){
              confirm_password_error = "Password and confirm password didn't match !";
              this.setState({ confirm_password_error : confirm_password_error})
              flag = true;
            }else if(current_password === new_password){
                confirm_password_error = "New password must be different than current password !";
                this.setState({ confirm_password_error : confirm_password_error})
                flag = true;
            }
          }
        return flag
    }

    updateUserInfo=()=>{
        debugger
        if(!this.validate()){
        const { orderDetails , changeEmailCheck, changePasswordCheck, firstname, lastname, new_email, current_password, new_password} = this.state;
        orderDetails.firstname = firstname;
        orderDetails.lastname = lastname;
        if(changeEmailCheck)
        orderDetails.email = new_email;
        delete orderDetails.addresses;
        this.props.dispatch({ type : SET_SPINNER_STARTED});
        if(changePasswordCheck){
            let data = { currentPassword : current_password, newPassword :  new_password}
            updateUserPasswordApi( data).then(res=>{
                if(res.code === 200){
                    this.props.dispatch({ type : SET_SPINNER_COMPLETED});
                    postUserInfoApi(orderDetails).then(res => {
                        if(res.code === 200){
                            this.props.history.push('/accountinfo');
                         }
                    }).catch(err => {
                        this.props.dispatch({ type : SET_SPINNER_COMPLETED});
                    if(err.response && err.response.data.code === 401 && err.response.data.result === "The consumer isn't authorized to access self."){
						this.props.dispatch({ type : CHECK_UNAUTHORIZED, payload: true});
					  }else{
                            this.setState({ error_api_res : err.response.data.result.errorMessage})
                        }
                    });
                }
            }).catch(err=>{
                this.props.dispatch({ type : SET_SPINNER_COMPLETED});
                if(err.response && err.response.data.code === 401 && err.response.data.result === "The consumer isn't authorized to access self."){
                    this.props.dispatch({ type : CHECK_UNAUTHORIZED, payload: true});
                  }else{
                      this.setState({ error_api_res : err.response.data.result.errorMessage})
                  }
                });
        }else{
            postUserInfoApi(orderDetails).then(res => {
                this.props.dispatch({ type : SET_SPINNER_COMPLETED});
                if(res.code === 200){
                    this.props.history.push('/accountinfo');
                 }
            }).catch(err => {
                this.props.dispatch({ type : SET_SPINNER_COMPLETED});
                if(err.response && err.response.data.code === 401 && err.response.data.result === "The consumer isn't authorized to access self."){
                    this.props.dispatch({ type : CHECK_UNAUTHORIZED, payload: true});
                }else{
                    this.setState({ error_api_res : err.response.data.result.errorMessage})
                }
            });
        }    
    }
    }

    render() {
        if (!isAuthenticated()) return (<Redirect to="/login" />);
        const{ error_api_res,  firstname_error, lastname_error, new_email_error, current_password_error, new_password_error,confirm_password_error } = this.state;
        return (
            <React.Fragment>
                {error_api_res ?<div className="product-error"> The password doesn't match this account. Verify the password and try again. </div>:''}
                <section className="page_banner">
                    <div className="rowsec-account" >
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-md-4">
                                    <AccountDashMenu/>
                                </div>
                                <div className="col-md-8">
                                    <div className="frmsec">
                                        <form className="form form-edit-account" >
                                           <div class="form-row">
                                            <fieldset className="fieldset info">

                                                <legend className="legend"><span>Update Your Information</span></legend>

                                                <div className="field field-name-firstname required">
                                                    <label className="label" >
                                                        <span>First Name</span>
                                                    </label>

                                                    <div className="control">
                                                        <input type="text"  name="firstname" onChange={this.handleChange} value={this.state.firstname}  className="input-text required-entry" />
                                                    </div>
                                                    {firstname_error ? <div  className = "error">{firstname_error}</div> : ''}
                                                </div>
                                                <div className="field field-name-lastname required">
                                                    <label className="label" >
                                                        <span>Last Name</span>
                                                    </label>

                                                    <div className="control">
                                                        <input type="text"  name="lastname" onChange={this.handleChange} value={this.state.lastname}  className="input-text required-entry" />
                                                    </div>
                                                    {lastname_error ? <div  className = "error">{lastname_error}</div> : ''}
                                                </div>


                                                <div className="field choice">
                                                    <input type="checkbox" name="changeEmailCheck" onChange={this.handleChangeEmailCheck} checked={this.state.changeEmailCheck}  className="checkbox" />
                                                    <label className="label" ><span>Change Email</span></label>
                                                </div>
                                                <div className="field choice">
                                                    <input type="checkbox" name="changePasswordCheck"  onChange={this.handleChangePasswordCheck} checked={this.state.changePasswordCheck} className="checkbox" />
                                                    <label className="label" ><span>Change Password</span></label>
                                                </div>
                                            </fieldset>
                                            { (this.state.changePasswordCheck || this.state.changeEmailCheck) ? 
                                            <fieldset className="fieldset password" >
                                                <legend className="legend"><span>
                                                {(this.state.changePasswordCheck && this.state.changeEmailCheck) ? 'Change Email and Password': 
                                                this.state.changeEmailCheck ? 'Change Email':'Change Password'}
                                                    </span></legend>
                                                {this.state.changeEmailCheck ?
                                                <div className="field email required" >
                                                    <label className="label" ><span>Email</span></label>
                                                    <div className="control">
                                                        <div className="validator validator-email validation-none">
                                                            <input type="email" name="new_email"  autocomplete="email" onChange={this.handleChange} value={this.state.new_email} className="input-text"  />
                                                        </div>
                                                        {new_email_error ? <div  className = "error">{new_email_error}</div> : ''}
                                                    </div>
                                                </div>:''}
                                                {this.state.changePasswordCheck ?<>
                                                <div className="field password current required">
                                                    <label className="label"><span>Current Password</span></label>
                                                    <div className="control">
                                                        <input type="password" className="input-text" name="current_password" onChange={this.handleChange} value={this.state.current_password} id="current-password" data-input="current-password" autocomplete="off" aria-required="true" />
                                                    </div>
                                                    {current_password_error ? <div  className = "error">{current_password_error}</div> : ''}
                                                </div>
                                                <div className="field new password required" data-container="new-password">
                                                    <label className="label" ><span>New Password</span></label>
                                                    <div className="control">
                                                        <input type="password" className="input-text" name="new_password" id="password" onChange={this.handleChange} value={this.state.new_password}/>
                                                        <div >
                                                            {/* <div id="password-strength-meter" className="password-strength-meter">
                                                                Password Strength:<span >
                                                                    No Password</span>
                                                            </div> */}
                                                        </div>
                                                        {new_password_error ? <div  className = "error">{new_password_error}</div> : ''}
                                                    </div>
                                                </div>
                                                <div className="field confirm password required">
                                                    <label className="label" htmlFor="password-confirmation"><span>Confirm New Password</span></label>
                                                    <div className="control">
                                                        <input type="password" className="input-text" name="confirm_password" onChange={this.handleChange} value={this.state.confirm_password} id="password-confirmation" autocomplete="off" />
                                                    </div>
                                                {confirm_password_error ? <div  className = "error">{confirm_password_error}</div> : ''}
                                                </div>
                                                </>: ''}
                                            </fieldset>
                                            :''}
                                            </div>
                                            <div className="field password-info">
                                                <p>If you created this account using Amazon Pay, you might not know your site password.
                                                    <a href="#">Request a password to change your account password.</a>
                                                </p>
                                            </div>
                                            
                                        </form>
                                        <div className="actions-toolbar">
                                            <div className="primary">
                                                <button  onClick={ this.updateUserInfo}  ><span>Save</span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
            </React.Fragment>
        );

    }

}

export default connect() (AccountInfoEdit);