import React, { useState } from 'react';
import { Redirect, Link, withRouter } from 'react-router-dom';
import '../styles/OfferpopupHome.scss';


const OfferpopupHome = () => {
  const [ show_popup, setShoPopUp]= useState( sessionStorage.getItem('isFreshFacedPopup')=== true ? false: true);

  if(sessionStorage.getItem('isFreshFacedPopup') ){
    return '';
  }else{
    return (
      <React.Fragment>
          <div id="popup-box"><a className="close" onClick={()=>{ sessionStorage.setItem('isFreshFacedPopup', true); setShoPopUp(false); }} >X</a>
              <h2 className="pop-up-heading">Fresh-Faced</h2>
              <p className="pop-up-text">We've not met, but we have a feeling that we're going to become fast Allies. Let's celebrate that with 15% off your first purchase .*</p>
              
              <p className="pop-up-text">Simply enter <a href="#">FRESHFACED</a> before checkout.</p>
              
              <p className="pop-up-text">While you're at it, if receiving the latest intel on skincare and earning points with purchases is your thing, come join us at Allies Underground</p>
               <Link onClick={()=>{ sessionStorage.setItem('isFreshFacedPopup', true) }} 
               to={ localStorage.getItem('x-access-token') ? {pathname :"/accountinfo"}:{ pathname :"/login", state: { pre_page_url: '/accountinfo' }}} className="pop-up-btn"  >
                Secret Handshake
               </Link> 
              <p className="pop-up-text note">* Not combinable with other discount offers.</p>
              <p className="pop-up-text note">** Allies of Skin reserves the right to terminate this promotion without prior notice.</p>
          </div>
      </React.Fragment>
    );
  }
}

export default OfferpopupHome;