import React, { Component, useState } from 'react';
import Carousel from 'react-multi-carousel';
import { getProducts } from '../account/Repository';
import '../styles/Shop.scss';
import 'react-multi-carousel/lib/styles.css';
import {Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {  addBasket } from '../actions/addAction';
import { HTTP } from '../Utils';



class ProductShop extends Component {

   
	constructor(props){
		super(props);
		this.state={
			items:[],
            isLoaded:false,
            postDataState : [],
			selected_Product :''
		}
	}

	setPrice(price){
		let newPrice = price || 0;
		return '$'+newPrice.toFixed(2);
	}

	htmlStripTags(string){
		string = string || '';
		return string.replace(/<[^>]*>?/gm, '');
	}

	addDefaultSrc(ev){
       ev.target.src = 'http://demo43.laraveldevloper.in/pub/media/catalog/product/placeholder/default/placeholder_3.png'
    }


	// componentDidMount(){

	// 		return HTTP().get('/api/catalog/vue_storefront_catalog/product/_search')
	// 		.then(res => {
	// 		    const items = res.data.hits.hits.reduce((result, set, index)=>{
	// 	    		for(let s in set){
	// 	    			let options = set[s].attributes_metadata || [];
	// 	    			// option = options ? options.find((attr)=>attr.default_frontend_label === "Product Size") : [];
	// 	    			let productSizeOption = options.length ? options.find((attr)=>attr.default_frontend_label === "Product Size") : null;
	// 	    			productSizeOption = productSizeOption ? productSizeOption.options[0] || [] : [];
	// 	    			set._source.productSizeOptions = productSizeOption;
	// 	    		}
	// 		    	result.push(set._source);
	// 		    	return result;
	// 		    },[]);

	//             getProducts().then((items) => this.setState({ 
	//                     isLoaded:true,
	// 					items:items,
	//              }))
	// 	})
	// 	.catch(e => { 
	// 	   console.log('Error:' + e)
	// 	})
			
	// }

    UNSAFE_componentWillReceiveProps( props,nextProps){
		if( props.postData &&props.basketProps.products && props.basketProps.products.productList && props.basketProps.products.productList.length>0 ){
			if(props.postData.length>0){
				props.postData.forEach(function(v){ delete v.isCartProduct });
				let postDataState = props.postData;
			let data = props.basketProps.products.productList;
				for( let i=0; i< data.length ;i++){
					let index = postDataState.findIndex( x=> x.sku === data[i].sku);
					if(index ==0|| index >0)
					postDataState[index].isCartProduct = true;
				}
				this.setState({ isLoaded: false, postDataState : postDataState})
			}
		}else if( props.postData && props.basketProps.products && props.basketProps.products.productList && props.basketProps.products.productList.length ==0){
			props.postData.forEach(function(v){ delete v.isCartProduct });
			this.setState({ isLoaded: false, postDataState : props.postData})
		}
		if( props.postData != nextProps.postData){
			// props.postData.forEach(function(v){ delete v.isCartProduct });
			this.setState({ isLoaded: true, postDataState : props.postData})
		}
		if(props.basketProps.addBasketError && props.basketProps.addBasketError.error && this.state.selected_Product){
			this.props.gotoReviewPage(this.state.selected_Product.id, props.basketProps.addBasketError.error)
		}
	}

	addToCart( productList ){
		this.props.addBasket (productList)
		this.setState({ selected_Product : productList})
	}
	
   
   render (){

   	let {isLoaded, items} = this.state;
	let PostData = this.props.postData || []; //Object.assign([],items);
	console.log('Product List', PostData);

	  if(!isLoaded){
			return(<div className="text-center mt-5 mb-5">loading...</div>)
		}

		else {



			 return (
    <React.Fragment>


    <div className="container-fluid my-5 pl-5 pr-5 product">
       
       <div className="row">
         <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
          <h2 className="homepage-product-heading">ALLIES KEEP THEIR PROMISES</h2>
           <span className="homepage-product-text">To create smart, effective products for the modern day multi-hyphenate using potent antioxidants and intelligent nutrients.</span>
         </div>
         <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
            <Link  to ="/Shop" className="view_btn">View All Section + </Link>
         </div>
       </div>

         <div className="row">
			{PostData&& PostData.length>0 ? 
             PostData.map((productList, index)=>{
             if(index > 2 && !this.props.page) return;
        	  return<div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
           <Card className="w-100 text-center">
			  <Link to ={{pathname: `/productDetails/${productList.id}`}}>
               <img onError={this.addDefaultSrc} className="img-responsive" alt={productList.name} src={'http://139.180.155.160/img/310/300/resize'+ productList.image}/>
			   </Link>
			  <Card.Body>
			    <Card.Title> {productList.name} </Card.Title>
			    <Card.Text>
			     <div className="stamped-badge">
			       <span className="porduct_desc"> {this.htmlStripTags(productList.category_below_content)} </span>
				     <span className="rating">
				     <Link to ="rating">
		                      <i className="fa fa-star"></i>
		                      <i className="fa fa-star"></i>
		                      <i className="fa fa-star"></i>
		                      <i className="fa fa-star"></i>
		                      <i className="fa fa-star"></i>
		                 
				     </Link>
				     </span>
			     <span className="reviews"><Link to="review"> {productList.product_review} </Link></span>
			     </div>
				      <div className="attribute">
				        <span className="product-volume-attr"> {productList.productSizeOptions ? productList.productSizeOptions.label : ''} </span>

				        <span className="separator"></span> 
				        <span className="product-price">{this.setPrice(productList.price)}</span>
				      </div>
			    </Card.Text>
			 	<Button variant="primary" disabled = { productList.isCartProduct ? true : false} onClick={this.addToCart.bind(this, productList)}> {productList.isCartProduct ? 'Added': 'Add to Cart'}</Button>
			  </Card.Body>
			</Card>
           </div> 
		}) :
		<div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
			<br/><h2 className="homepage-product-heading">Product not available !</h2></div>}

           
         </div>
    </div>  
    
    </React.Fragment> 
  );

	} 


 }
   
  
}
const mapStateToProps = state => ({
	basketProps : state.basketState,
});


export default connect(mapStateToProps, { addBasket })(ProductShop);
