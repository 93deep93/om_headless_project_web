import React from 'react';
import './Orderinfo.scss';

const Orderinfo = () => {
  return (
    <React.Fragment>
      
  <section className="order-form col-xl-6 col-lg-6 col-md-10 col-sm-12 col-xs-12 mx-auto mt-5 mb-5">
    <div className="container pt-4">

      <div className="row">
        <div className="col-12">
          <h1>Order Information</h1>
          <hr className="mt-1"/>
        </div>
        <div className="col-12 p-0">
          <div className="row mt-3 mx-4">
            <div className="col-12 p-0">
              <label className="order-form-label">Order ID</label>
            </div>
            <div className="col-12 p-0">
              <input className="order-form-input" placeholder=" "/>
            </div>
          </div>

          <div className="row mt-3 mx-4">
            <div className="col-12 p-0">
              <label className="order-form-label">Billing Last Name</label>
            </div>
            <div className="col-12 p-0">
              <input className="order-form-input" placeholder=" "/>
            </div>
          </div>

          <div className="row mt-3 mx-4">
            <div className="col-12 p-0">
              <label className="order-form-label">Find Order By</label>
            </div>
            <div className="col-12 p-0">
              <input className="order-form-input" placeholder=" "/>
            </div>
          </div>

          <div className="row mt-3 mx-4">
            <div className="col-12 p-0">
              <label className="order-form-label">Email</label>
            </div>
            <div className="col-12 p-0">
              <input className="order-form-input" placeholder=" "/>
            </div>
          </div>       
          

          <div className="row mt-3 mx-4">
            <div className="col-12 p-0">
              <button type="button" className="btn d-block btn-submit">Continue</button>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>
    </React.Fragment>
  );
}

export default Orderinfo;