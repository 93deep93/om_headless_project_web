import React, { Component } from 'react';
import '../styles/Products.scss';
import Homebanner from './Homebanner';
import MobileBanner from './MobileBanner';
import Product from './Product';
import Recommend from './Recommend';
import Slider from './Slider';
import DeviceIdentifier from 'react-device-identifier';
import AlliesEthoes from './AlliesEthoes';
import { HTTP } from '../Utils';
import { connect } from 'react-redux';
import { getCartAddedProducts } from '../account/Repository';
import OfferpopupHome from './OfferpopupHome';
class Home extends Component {

	constructor(props){
		super(props);
	}

	componentDidMount(){

	    HTTP().get('/api/catalog/vue_storefront_catalog/category/_search')
	    .then(res => {
	        const cats = res.data.hits.hits.reduce((result, cat, index)=>{
	          result.push(cat._source);
	          return result;
	        },[]);

	        if(this.props.match.params.id){
		        const pageCategory = cats.find((cat)=>cat.id ==this.props.match.params.id);
		            this.setState({ 
		                pageCategory:pageCategory,
		             });
	        }

	        HTTP().get('/api/catalog/vue_storefront_catalog/product/_search')
	        .then(res => {
				
	        var items = res.data.hits.hits.reduce((result, set, index)=>{
	          for(let s in set){
	            let options = (set[s].attributes_metadata || [] );
	            let productSizeOption = options.length ? options.find((attr)=>attr.default_frontend_label === "Product Size") : null;
	            productSizeOption = productSizeOption ? productSizeOption.options[0] || [] : [];
	            set._source.productSizeOptions = productSizeOption;
	          }
			//   if(set._source && set._source.extension_attributes && set._source.extension_attributes.website_ids){
				// if(set._source.extension_attributes.website_ids.length > 0 && set._source.extension_attributes.website_ids[0] === 1){
					result.push(set._source);
					return result;
					
			// 	}
			//   }
			  	
			},[]);

			items = items.filter(x=> x.extension_attributes.website_ids[0] === 1);
			this.setState({ 
				items: items,
				categories: cats
			})
			// getCartAddedProducts(JSON.parse(sessionStorage.getItem('cart-id'))).then((cardItems) =>{
			// 	let allProduct = items;
			// 	const data = cardItems.result
			// 		if(data.length>0){
			// 			for( let i=0; i< data.length ;i++){
			// 				let index = allProduct.findIndex( x=> x.sku === data[i].sku);
			// 				allProduct[index].isCartProduct = true;
			// 			}
			// 		}
			// 		this.setState({ 
			// 			items:allProduct,
			// 			categories: cats
			// 		})

			// 	})
				
	        });
	    });
	}


	componentDidUpdate(){

	}

	gotoReviewPage=( product_id , error)=>{
		this.props.history.push( {pathname: `/productDetails/${product_id}` , state : { error : error}});
	}

	componentWillReceiveProps( props){
		if(props.basketProps.unAuthorizeUser){
			localStorage.removeItem('x-access-token');
			localStorage.removeItem('x-access-token-expiration');
			localStorage.removeItem('user-info-allies');
			sessionStorage.removeItem('cart-id');
			this.props.history.push({
			pathname :'/Login'
			})
		}
	}
	

 
 render() {
 	// let {items, categories} = this.state; 
 	//postData={this.props.items}
 	const {items} = {...this.state};
   	console.log('my items', this.state);
    return (
    <React.Fragment> 
		<DeviceIdentifier isDesktop={true}>
		<Homebanner />
            </DeviceIdentifier>
            <DeviceIdentifier isMobile={true}>
			<MobileBanner/> 
          </DeviceIdentifier>
          <DeviceIdentifier isTablet={true}>
		  <MobileBanner/> 
          </DeviceIdentifier>
	    <Product postData={items} gotoReviewPage={this.gotoReviewPage} />
	    <Recommend />
	    <Slider />
	    <AlliesEthoes />
		<OfferpopupHome />
    </React.Fragment> 
  );

 }
}

const mapStateToProps = state => ({
	basketProps : state.basketState,
});


export default connect(mapStateToProps)(Home);