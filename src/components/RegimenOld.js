import React, {Component} from 'react'; 
import { Link } from 'react-router-dom'; 
import '../styles/Regimen.scss'; 
class Regimen extends Component { constructor(props) { super(props); this.state = {value: ''};
 this.handleChange =
this.handleChange.bind(this); this.handleSubmit = this.handleSubmit.bind(this); 
}
 handleChange(event) { this.setState({value: event.target.value}); }
  handleSubmit(event) { alert('A name was submitted: ' + this.state.value);
event.preventDefault(); } render() { return (
<React.Fragment>
    
    <section className="regimen-section">
        <div className="container-fluid pl-5 pr-5">
            <div className="regimen_container">
                <div className="regimen-header">
                    <link to="/" <img src="https://alliesofskin.com/pub/static/version1590851859/frontend/OM/alliesskin-v2/en_US/images/microsite-logo.png" />/>
                </div>

                <div className="regimen-body">
                    <form action="" role="form" className="text-center">
                        <input id="step2" type="checkbox" />
                        <input id="step3" type="checkbox" />

                        <div id="part1" className="form-group">
                            <div className="step_first_question">
                                <div className="panel-heading">
                                    <h4 className="qustn-number text-white">01</h4>
                                    <p className="qustn">WHAT'S YOUR FIRST NAME</p>
                                </div>
                                <div className="ques-container" onSubmit="{this.handleSubmit}">
                                    <input type="text" id="name" value="{this.state.value}" onChange="{this.handleChange}" className="form-control" placeholder="TYPE YOUR NAME HERE" aria-describedby="sizing-addon1" />
                                    <div className="btn-group btn-group-lg" role="group" aria-label="...">
                                        <label for="step2" id="continue-step2">
                                            <div className="btn btn-default btn-lg" value="Submit">&gt;</div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="part2" className="form-group">
                            <div className="step_two">
                                <div className="panel-heading">
                                    <h4 className="qustn-number text-white">Hello</h4>
                                </div>

                                <h2 className="text-white mt-4">NICE TO MEET YOU</h2>

                                <h1 className="text-white">{this.state.value}</h1>

                                <div className="btn-group btn-group-lg btn-group-justified" role="group" aria-label="...">
                                    <label for="step2" id="back-step2" className="back">
                                        <div className="btn btn-default btn-lg" role="button">Back</div>
                                    </label>

                                    <label for="step3" id="continue-step3" className="continue">
                                        <div className="btn btn-default btn-lg" role="button">Continue</div>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div id="part3" className="form-group">
                            <div className="step_three">
                                <div className="panel-heading">
                                    <h4 className="qustn-number text-white">Thank you so much</h4>
                                </div>

                                <h2 className="text-white mt-4">LET'S GET TO KNOW YOU</h2>

                                <h1 className="text-white">Don't Worry, It Will Be Less Than 2 Minutes ;)</h1>
                                <div className="btn-group btn-group-lg" role="group" aria-label="...">
                                    <label for="step3" id="back-step3" className="back">
                                        <div className="btn btn-default btn-lg">Back</div>
                                    </label>
                                    <label className="continue">
                                        <button type="submit" className="btn btn-default btn-lg">Submit</button>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    


</React.Fragment>
);
 }
 } 
 export default Regimen;
