import React from 'react';
import '../styles/Header.scss';
import '../styles/MediaQuery.scss';
import Menu from './Menu';
import MobileMenu from './MobileMenu';
import DeviceIdentifier from 'react-device-identifier';

const Header = () => {
  return (
    <React.Fragment> 
     
            <DeviceIdentifier isDesktop={true}>
             <Menu />
            </DeviceIdentifier>
            <DeviceIdentifier isMobile={true}>
            <MobileMenu />
           </DeviceIdentifier>
           <DeviceIdentifier isTablet={true}>
           <Menu />
          </DeviceIdentifier>
    </React.Fragment> 
  );
}


export default Header;