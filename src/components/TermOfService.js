import React from 'react';
import '../styles/Privacyplocy.scss';
import PageContent from "./PageContent";

const TermOfService = () => {
  return (
    <React.Fragment>
     <div className ="privacy-policy">
        <PageContent pageId={32} />
      </div>
    </React.Fragment>
  );
}

export default TermOfService;