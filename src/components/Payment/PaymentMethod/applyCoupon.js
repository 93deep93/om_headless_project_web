import React, { useState, Fragment, Component } from 'react';
import ShippingForm from '../../Checkout/ShippingForm/shippingForm';
import { Form, Button, Tooltip, Accordion, Card } from 'react-bootstrap';
const ApplyCoupon = (props) => {

    return (
        <React.Fragment> 
            <div className="applycoupon order_summary">
                <Accordion defaultActiveKey="1">
                    <Card>
                        <Accordion.Toggle as={Card.Header} eventKey="1">
                            <div className="itemin_cart"><span>Apply Discount Code</span></div>
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="1">
                            <Card.Body>
                                <div className="payment-option-content">
                                    <form className="form form-discount" id="discount-form">
                                        <div className="payment-option-inner">
                                            <div className="field">
                                                <label className="label" for="discount-code">
                                                    <span>Enter discount code</span>
                                                </label>
                                                <div className="control">
                                                    <input className="input-text" type="text" id="discount-code" name="discount_code" placeholder="Enter discount code" />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="actions-toolbar">
                                            <div className="primary">
                                                <button className="action action-apply" type="submit" value="Apply Discount">
                                                    <span><span>Apply Discount</span></span>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </Card.Body>
                        </Accordion.Collapse>
                    </Card>
                </Accordion>
            </div>       
        </React.Fragment>
    )
}

export default ApplyCoupon