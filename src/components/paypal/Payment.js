import React, { Component } from 'react';
import PayPalBtn from './PayPalBtn';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';
import { isAuthenticated, placeOrderApi, pay } from '../../account/Repository';
import { placeOrder} from '../../actions/getAction';
import './Payment.scss';
import countriesList from '../../data/countries_reagions.json';
import {Form, Button, Tooltip } from 'react-bootstrap';
import PaypalExpressBtn from 'react-paypal-express-checkout';
import { HTTP } from '../../Utils';
import { SET_USER_CART_DATA, SET_SPINNER_STARTED, SET_SPINNER_COMPLETED } from '../../actions/types';
const client = {
  sandbox:    'AR4QmiLo_pYFTLmNUArk2QbO3-FVdeoOJxd7mCfPqZOvhECIU6JnNS0awZYMyRkhK8onIrHNqzKezX32',
  production: 'YOUR-PRODUCTION-APP-ID',
  }
 class Payment extends Component {

   constructor(props) {
      super(props);
        this.state ={
          paymentMethod : '',
          shippingData : this.props.location.state ? this.props.location.state.shippingData : '',
          shipmentAddress: this.props.location.state ? this.props.location.state.shippingData.shippingAddress:'',
          state_arr:[],
          countriesList : countriesList,
          formData: {},
          errors:{ },
          validated: false,
          copy_address : false,
          order_error : '',
          user_info: localStorage.getItem('user-info-allies') ? 
          JSON.parse(localStorage.getItem('user-info-allies')).isUserInfo ?
            JSON.parse(localStorage.getItem('user-info-allies')).isUserInfo
            : JSON.parse(localStorage.getItem('user-info-allies')) 
            : '',
        }
      }

      componentDidMount(){
        const { formData} = this.state;
        formData.email = this.state.user_info.email;
        this.setState({ formData : formData });
      }

    paymentHandler = (details, data) => {
      /** Here you can call your backend API
        endpoint and update the database */
      console.log(details, data);
    }

 handleInputChangeText =(event) => {
  const { formData, errors} = this.state;
  formData[event.target.name] = event.target.value;
  errors[event.target.name] = '';
  this.setState ({formData: formData, errors : errors});
  if(event.target.name === 'country_id'){
    formData.state='';
    this.setState({ formData: formData, state_arr : this.state.countriesList.filter((data)=> data.id=== event.target.value)[0].available_regions})
  }
  
}

handleCheckChange=(event)=>{
  if(!this.state.copy_address){
    // set data in form data 
    if(this.state.shipmentAddress){
        const shipmentAddress = this.state.shipmentAddress;
        const formData = this.state.formData;
        formData.postcode= shipmentAddress.postcode;
        formData.firstname= shipmentAddress.firstname;
        formData.lastname= shipmentAddress.lastname;
        formData.address = shipmentAddress.address;
        formData.city = shipmentAddress.city;
        formData.company = shipmentAddress.company;
        formData.state = shipmentAddress.state;
        formData.street = shipmentAddress.street;
        formData.country_id = shipmentAddress.country_id;
        formData.telephone = shipmentAddress.telephone;
        formData.region = shipmentAddress.region;
        formData.region_code = shipmentAddress.region_code;
        formData.region_id = shipmentAddress.region_id;
        this.setState({ formData : formData, errors : {}, state_arr : this.state.countriesList.filter((data)=> data.id=== shipmentAddress.country_id)[0].available_regions});
    }
  }else{
      // this.setState({ formData : {}});
  }
  this.setState({ copy_address  : !this.state.copy_address})
}

validateEmail=( email)=>{
  const regex_email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regex_email.test(String(email).toLowerCase());
}

validate=() =>{
  let flag = false;
  const { errors, formData } = this.state;
  if( formData.email === "" || formData.email=== null || formData.email === undefined){
    errors.email = 'This field is required';
    flag = true;
  }else if( formData.email !== "" || formData.email !== null || formData.email !== undefined){
    if(!this.validateEmail( formData.email)){
      errors.email = 'Please enter valid email id !'
      flag = true;
    }
  }
  if( formData.firstname === "" || formData.firstname=== null || formData.firstname === undefined){
    errors.firstname = 'This field is required';
    flag = true;
  }
  if( formData.lastname === "" || formData.lastname=== null || formData.lastname === undefined){
    errors.lastname = 'This field is required';
    flag = true;
  }
  if( formData.company === "" || formData.company=== null || formData.company === undefined){
    errors.company = 'This field is required';
    flag = true;
  }
  if( formData.address === "" || formData.address=== null || formData.address === undefined){
    errors.address = 'This field is required';
    flag = true;
  }
  if( formData.city === "" || formData.city=== null || formData.city === undefined){
    errors.city = 'This field is required';
    flag = true;
  }
  if( formData.state === "" || formData.state=== null || formData.state === undefined){
    errors.state = 'This field is required';
    flag = true;
  }
  if( formData.country_id === "" || formData.country_id=== null || formData.country_id === undefined){
    errors.country_id = 'This field is required';
    flag = true;
  }
  if( formData.telephone === "" || formData.telephone=== null || formData.telephone === undefined){
    errors.telephone = 'This field is required';
    flag = true;
  } 
  if( formData.postcode === "" || formData.postcode=== null || formData.postcode === undefined){
    errors.postcode = 'This field is required';
    flag = true;
  }
  this.setState({ errors : errors});
  return flag;
}

 handlePaymentChange = (val)=>{
  this.setState({ paymentMethod : val, order_error :''});
 }

 orderplace =()=>{
  if(!this.validate()){
    this.orderplaceSubmit('');
    }
 }

 getAddressDataFormat=( data)=>{
  return  {
      city: data.city,
      company: data.company,
      country_id: data.country_id,
      email: data.email,
      firstname: data.firstname,
      lastname: data.lastname,
      postcode: '07416', //data.postcode,
      region: data.region,
      region_id: data.region_id,
      street: data.street,
      telephone: data.telephone
  }
 }

 orderplaceSubmit =( payment )=>{
  
    let info = JSON.parse(localStorage.getItem('user-info-allies'));
    let data ={};
    let finelArrProducts = [];
    let arrProducts =this.props.basketProps.products.productList;
    for(let i=0; i< arrProducts.length; i++){
      let obj ={};
      obj.qty = (arrProducts[i].qty || arrProducts[i].numbers);
      obj.sku = arrProducts[i].sku;
      finelArrProducts.push(obj);
    }
    data.addressInformation = { 
      shippingAddress : this.getAddressDataFormat(this.state.shipmentAddress), 
      billingAddress : this.state.paymentMethod==='cashondelivery'? this.getAddressDataFormat(this.state.formData): this.getAddressDataFormat(this.state.shipmentAddress),
      
      // payment_method_additional:  { payment}, 
      payment_method_code: this.state.paymentMethod,
      shipping_carrier_code : this.state.shippingData.shipping_carrier_code,
      shipping_method_code : this.state.shippingData.shipping_method_code,
    };
    data.products= finelArrProducts;
    // data.transmited = true;
    data.user_id = info.id? info.id:info.isUserInfo.id;
    data.cart_id = sessionStorage.getItem('cart-id');
    this.props.dispatch({ type : SET_SPINNER_STARTED});
    return HTTP().post('/api/order',{  
      addressInformation : data.addressInformation,
      products: data.products,
      transmited : true,
      user_id : data.user_id,
      cart_id : data.cart_id
    })
    .then(res => {
      this.props.dispatch({ type: SET_USER_CART_DATA, payload:  []}); 
        this.props.dispatch({ type : SET_SPINNER_COMPLETED});
        sessionStorage.removeItem('cart-id');
        this.props.history.push({
          pathname: '/Orderdetails', state: { orderDetails : false, orderPlace: true, orderPlaceData : res.data.result
          //   orderPlaceData : {
          //   "magentoOrderId": "11535",
          //   "orderNumber": "3000005108",
          //   "backendOrderId": "11535",
          //   "transferedAt": "2020-10-02T06:24:59.687Z"
          // }
        } 
      })
    }).catch(err => {
      this.props.dispatch({ type : SET_SPINNER_COMPLETED});
      if( err.response && err.response.data.result)
      this.setState({ order_error : err.response.data.result});
    })

    //call order api
    // this.props.dispatch(placeOrder(data));
    // this.props.dispatch({ type : SET_SPINNER_STARTED});
    // placeOrderApi.then((res) =>{
    //   // this.props.dispatch({ type : SET_SPINNER_COMPLETED});
    //   if(res.code === 200) { 
    //     
    //     this.props.dispatch({
    //       type: SET_USER_CART_DATA,
    //       payload:  []
    //     }); 
    //   }
    // })
    
  
}

  onSuccess = (payment) => {
  // Congratulation, it came here means everything's fine!
      console.log("The payment was succeeded!", payment);
      this.orderplaceSubmit(payment);
      // You can bind the "payment" object's value to your state or props or whatever here, please see below for sample returned data
  }

  onCancel = (data) => {
  // User pressed "cancel" or close Paypal's popup!
  this.setState({ order_error : 'The payment was cancelled!'});
  console.log('The payment was cancelled!', data);
  // You can bind the "data" object's value to your state or props or whatever here, please see below for sample returned data
  }

  onError = (err) => {
    // alert('Error!');
    this.setState({ order_error : 'Error!'});
  // The main Paypal's script cannot be loaded or somethings block the loading of that script!
  console.log("Error!", err);
  // Because the Paypal's main script is loaded asynchronously from "https://www.paypalobjects.com/api/checkout.js"
  // => sometimes it may take about 0.5 second for everything to get set, or for the button to appear
  }

  
  handleInputChangeState=(e)=>{
        
    let region_info = this.state.state_arr.filter(x=> x.code === e.target.value)[0];
    const formData = this.state.formData;
    formData.region = region_info.name;
    formData.region_code = region_info.code;
    formData.region_id = region_info.id;
    formData.state = e.target.value;
    const errors= this.state.errors;
    errors[e.target.name] = '';
    this.setState({ formData : formData, errors : errors});
  }
  

    render() {

      let env = 'sandbox'; // you can set here to 'production' for production
    let currency = 'USD'; // or you can set this value from your props or state
    let total = 100; // same as above, this is the total amount (based on currency) to be paid by using Paypal express checkout
    // Document on Paypal's currency code: https://developer.paypal.com/docs/classic/api/currency_codes/

      if (!isAuthenticated()) return (<Redirect to="/login" />);
    const { errors, formData , copy_address} =  this.state;

        let { cartCost, products } = this.props.basketProps;
        let shipmentAddress = this.state.shipmentAddress;
        let productsInCart = products.productList; 


      productsInCart = productsInCart.map( (productList, index) => {
 
        return ( 

                 <div className="col-12">
                  <div className="row total-details">
                     
                     <div className="col-2">
                       <div className="items-img"><img src={'http://139.180.155.160/img/310/300/resize'+ productList.image} /></div>
                     </div> 
                    <div className="col-10">
                      <div className="items-details">
                        <h3> {productList.name}</h3>
                        <span className="quantity">Qty :  {productList.numbers}</span>

                          <span className="price">Price : {'$'+productList.price.toFixed(2)}</span>
                        
                      </div>
                    </div>                     
                </div>
                </div>
        );
    });

      return (
          <React.Fragment>
                
                <div className="container-fluid payment-section pr-5 pl-5">
                  {/* <h1 className="text-center">Payment to Paypal</h1> */}
                  <h1 className="text-center">Payment By</h1>
                <div className ="row">
                  <div className="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12 payment-system">
                     <div>{productsInCart}</div>

                  </div>
                  <div className="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12 pay-right">
                     <span className="total-price">Total Price : {cartTotal(cartCost)}</span>
                     <div className = "shipping-information">
                            <div className="ship-to">
                              <div className="shipping-information-title"> <span>Ship To :</span></div>
                            </div>
                            <div className="shipping-information-content">
                              <span>{ shipmentAddress.firstname +''+ shipmentAddress.lastname}</span><br/>
                              <span>{ shipmentAddress.address}</span><br/>
                              <span>{ shipmentAddress.city}</span><br/>
                              <span>{ shipmentAddress.state}</span><br/>
                              <span>{ shipmentAddress.country}</span><br/>
                              <span>{ shipmentAddress.telephone}</span><br/>
                            </div>
                          </div>
                          <span className="total-price">Payment Method </span>
                          <div className="payment-method">
                          {/* <div  className="payment-method-box">
                              < input
                                type="radio"
                                value="paypal_express" 
                                checked={this.state.paymentMethod === "paypal_express"} 
                                onChange={this.handlePaymentChange.bind(this,'paypal_express')} 
                              />
                              <span > Paypal</span>
                            </div> */}
                            <div className="payment-method-box">
                              <input
                                type="radio"
                                value="cashondelivery"
                                checked={this.state.paymentMethod === "cashondelivery"}
                                onChange={this.handlePaymentChange.bind(this,'cashondelivery')}
                              />
                              <span> COD</span>
                            </div>
                          </div>
                            <div>
                               {/* here in billing address */}
                               {this.state.paymentMethod === 'cashondelivery' ?
                               <div className="row">
                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                  <div className="billing_form">
                                  <div className = "shipping-information">
                                  <div className="ship-to">
                                        <div className="shipping-information-title"> <span>Billing Address :</span></div>
                                    </div></div>

                                  
                                        <Form  noValidate validated={this.state.validated}>
                                            <Form.Group>
                                                <Form.Check
                                                    name="copy_address"
                                                    label="Copy address data from shipping"
                                                    onChange={this.handleCheckChange}
                                                    isInvalid={!!errors.copy_address}
                                                    feedback={errors.copy_address}
                                                    inline
                                                  checked = {this.state.copy_address}
                                                />
                                            </Form.Group>
                                          <Form.Group controlId="formBasicEmail" className="billing_info_detils">
                                            <Form.Label>Email address</Form.Label>
                                            <Form.Control name="email" disabled={ copy_address ? true : false} value={formData.email } type="email" onChange={this.handleInputChangeText} placeholder=""  isInvalid={!!errors.email}/>
                                              {/* <span className="fa fa-question"></span> */}
                                              <Form.Control.Feedback type="invalid"> {errors.email} </Form.Control.Feedback>
                                            <Form.Text className="text-muted">
                                              You can create an account after checkout.
                                            </Form.Text>
                                          </Form.Group>

                                          <Form.Group controlId="formBasicPassword">
                                            <Form.Label>First Name</Form.Label>
                                            <Form.Control type="text" name="firstname" disabled={ copy_address ? true : false} value={formData.firstname } placeholder="" onChange={this.handleInputChangeText} isInvalid={!!errors.firstname}/>
                                            <Form.Control.Feedback type="invalid"> {errors.firstname} </Form.Control.Feedback>
                                          </Form.Group>

                                          <Form.Group controlId="formBasicLastName">
                                            <Form.Label>Last Name</Form.Label>
                                            <Form.Control type="text" name="lastname" disabled={ copy_address ? true : false} value={formData.lastname } placeholder="" onChange={this.handleInputChangeText} isInvalid={!!errors.lastname}/>
                                            <Form.Control.Feedback type="invalid"> {errors.lastname} </Form.Control.Feedback>
                                          </Form.Group>

                                          <Form.Group controlId="formBasicCompany">
                                            <Form.Label>Company</Form.Label>
                                            <Form.Control type="text" name="company" disabled={ copy_address ? true : false} value={formData.company} placeholder="" onChange={this.handleInputChangeText} isInvalid={!!errors.company} />
                                            <Form.Control.Feedback type="invalid"> {errors.company} </Form.Control.Feedback>
                                          </Form.Group>

                                          <Form.Group controlId="formBasicAddress">
                                            <Form.Label>Street Address</Form.Label>
                                            <Form.Control className="mb-4" type="text" disabled={ copy_address ? true : false} name="address" value={formData.address} placeholder="" onChange={this.handleInputChangeText}  isInvalid={!!errors.address}/>
                                            <Form.Control.Feedback type="invalid"> {errors.address} </Form.Control.Feedback>
                                            <Form.Control className="mb-4" type="text" disabled={ copy_address ? true : false} name="address" placeholder="" onChange={this.handleInputChangeText} />
                                          </Form.Group>

                                          <Form.Group controlId="formBasicCity">
                                          <Form.Label>City</Form.Label>
                                            <Form.Control type="text" name="city" disabled={ copy_address ? true : false} value={formData.city} onChange={this.handleInputChangeText} isInvalid={!!errors.city} />
                                            <Form.Control.Feedback type="invalid"> {errors.city} </Form.Control.Feedback>
                                          </Form.Group>

                                          <Form.Group controlId="formBasicState">
                                            <Form.Label>State/Province</Form.Label>
                                            { ( this.state.state_arr === null || this.state.state_arr.length===0)?
                                              <Form.Control type="text" disabled={ copy_address ? true : false} value={formData.state}  name="state" placeholder="" onChange={this.handleInputChangeText} isInvalid={!!errors.state}/>:
                                              <Form.Control as="select" disabled={ copy_address ? true : false} value={formData.state}  name="state" placeholder="" onChange={this.handleInputChangeState} isInvalid={!!errors.state}>
                                                <option value="">Please select a region, state or province</option>
                                                { this.state.state_arr.map( ( state, index) => {
                                                  return <option value = {state.code}>{state.name}</option>
                                                })}
                                              </Form.Control>
                                              }
                                            <Form.Control.Feedback type="invalid"> {errors.state} </Form.Control.Feedback>
                                          </Form.Group>

                                            <Form.Group controlId="formBasicCountry">
                                            <Form.Label>Country/Province</Form.Label>
                                            <Form.Control as="select" name="country_id" disabled={ copy_address ? true : false} value={formData.country_id} placeholder="" onChange={this.handleInputChangeText} isInvalid={!!errors.country}>
                                            <option value="">Select Country</option>
                                            { countriesList.map( ( country, index) => {
                                              return <option key={country.id} value = {country.id}>{country.full_name_english}</option>
                                            })}
                                            </Form.Control>
                                            <Form.Control.Feedback type="invalid"> {errors.country_id} </Form.Control.Feedback>
                                          </Form.Group>

                                            <Form.Group controlId="formBasictelephone" className="billing_info_detils">
                                              <Form.Label>telephone Number</Form.Label>
                                              <Form.Control type="tel" name="telephone" disabled={ copy_address ? true : false} placeholder="" value={ formData.telephone} onChange={this.handleInputChangeText} isInvalid={!!errors.telephone}/>
                                              {/* <span className="fa fa-question"></span> */}
                                              <Form.Control.Feedback type="invalid"> {errors.telephone} </Form.Control.Feedback>
                                            </Form.Group>

                                            <Form.Group controlId="formBasictelephone" className="billing_info_detils">
                                              <Form.Label>Post Code</Form.Label>
                                              <Form.Control type="tel" name="postcode" disabled={ copy_address ? true : false} placeholder="" value={ formData.postcode} onChange={this.handleInputChangeText} isInvalid={!!errors.postcode}/>
                                              {/* <span className="fa fa-question"></span> */}
                                              <Form.Control.Feedback type="invalid"> {errors.postcode} </Form.Control.Feedback>
                                            </Form.Group>
                                        </Form>
                                    </div>
                                </div>
                              </div> : ''}
                            </div>
                          
                     
                    {  this.state.paymentMethod === 'paypal_express' ?
                        <div style={{ paddingLeft:'20%'}}>
                          <PaypalExpressBtn env={env} client={client} currency={currency} total={cartCost} onError={this.onError} onSuccess={this.onSuccess} onCancel={this.onCancel} />
                        </div>
                        :
                        <div>  
                          <div className="col-12 text-center">                          
                          {this.state.paymentMethod === 'cashondelivery' ? <button className="proceed-payment" onClick={ this.orderplace}>Continue to checkout </button>:''}
                        </div>
                      </div>
                    }
                    { this.state.order_error != ''? 
                      <div style={{ color : 'red', textAlign : "center"}}>{this.state.order_error}</div>:''
                    }
                   
                  </div>
                  </div>
                </div>
             
                  
          </React.Fragment>
        )
 }
}


function cartTotal(amount){
  return '$'+amount.toFixed(2);
}

const mapStateToProps = state => ({
  basketProps : state.basketState
})

export default connect(mapStateToProps) (Payment); 