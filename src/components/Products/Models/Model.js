import React, { useState, useEffect } from 'react';
import '../../../styles/ProductModalinfo.scss';
import KeyProducts from './KeyProduct';

const Modal = ( props) => {
  const [activeTab, setIactiveTabs] = useState('description');

  const dynammicModalClass = () => (props.isShow ? { display: 'block' } : '');

  return  (

    <div className={"modal " + (props.isShow ? 'classshow' : 'classhide')}  id="channelModal">
      <div className="modal-dialog modal-dialog-slideout" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <button
              onClick={ props.closeModal}
              style={{ color: '#fff' }}
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">X</span>
            </button>
          </div>

          <div className="modal-body">
            <div className="row">
              <div className="col-12">
                <div className="pouptext">
                  <div className="navlink">
                      <ul className="tablink">
                        <li className={ activeTab === 'description' ? "active":''}><a onClick={()=>{setIactiveTabs( 'description')}}>Description</a></li>
                        <li className={ activeTab === 'key' ? "active":''}><a onClick={()=>{setIactiveTabs( 'key')}}>Key Allies</a></li>
                        <li className={ activeTab === 'ingregient_list' ? "active":''}><a onClick={()=>{setIactiveTabs( 'ingregient_list')}}>Full Ingredient List</a></li>
                      </ul>
                  </div>
                  <div className="tab-content-section" style={ { display:  activeTab === 'description' ? 'block':'none'}}>
                    <div className="tab-text">
                    <h3 className="pophead">{props.product.name}</h3>
                    <div dangerouslySetInnerHTML={{__html: props.product.description}}></div>
                    </div>
                  </div>
                  <div className="tab-content-section" style={ { display:  activeTab === 'key' ? 'block':'none'}}>
                    <KeyProducts attributes_key_data = {props.product.attributes_metadata} product={props.product} />  
                  </div>
                  <div className="tab-content-section" style={ { display:  activeTab === 'ingregient_list' ? 'block':'none'}}>
                    <div className="tab-text">
                      <div dangerouslySetInnerHTML={{__html: props.product.full_ingredient_list}}></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Modal;
