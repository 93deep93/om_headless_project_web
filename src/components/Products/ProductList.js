import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Product from '../Product';
import { connect } from 'react-redux';
import {  addBasket } from '../../actions/addAction';
import ProductShop from '../ProductShop';

class ProductList extends Component {

	constructor(props){
		super(props);

	}

	state = {
  activeId: null  // nothing selected by default, but this is up to you...
}

handleClick(event, id) {
  this.setState({ activeId: id })
}

gotoReviewPage=( product_id , error)=>{
	this.props.gotoReviewPage(product_id, error);
}
	
	render() {
		const all = true;
      
       return (
	    <React.Fragment>

		    <div className="container-fluid main_all_product pl-0 pr-0">
		       <ProductShop page={all} postData={this.props.postData}  gotoReviewPage={this.gotoReviewPage}/>
		    </div> 
	           
	    </React.Fragment>
	  );
	}
 
}

export default connect(null, { addBasket })(ProductList);