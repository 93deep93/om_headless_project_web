import React, { useState } from 'react';
import '../styles/Faq.scss';
import PageContent from "./PageContent";


const Faq = () => {

    const [ DELIVERY_SHIPPING, SET_DELIVERY_SHIPPING ] = useState( false);
    const [ ALLIES_OF_SKIN, SET_ALLIES_OF_SKIN ] = useState( false);
    const [ PRODUCTS, SET_PRODUCTS ] = useState( false);
    const [ RETURN_EXCHANGE, SET_RETURN_EXCHANGE ] = useState( false);
    const [ BILLING, SET_BILLING ] = useState( false);
    const [ ORDER_QUESTIONS, SET_ORDER_QUESTIONS ] = useState( false);
  return (
    <React.Fragment>
      <div className="faq-page-section">
        {/* <PageContent pageId={14} /> */}
        <div className="faq-page">
          <div className="faq-section-sec">
            <div className="content-heading top-full-banner">
              <img src={require('../images/FAQ_Header.jpg')} />
            </div>
            <div className="faq-content cms-content">
              <div className="faq-desc">
                <h2 className="faq-desc-title">This is what you came here for.</h2>
                <span className="faq-desc-subtitle">How can we help?</span>
                <p className="faq-heading col-md-6">Drop us an email at <a href="mailto:ask@alliesofskin.com">ask@alliesofskin.com </a> or text us at +65 9646 9631 (we check our Whatsapp regularly) to leave feedback, ask questions about your order, our products, our website, or just Allies of Skin in general. Help us help you. We believe regularly touching base with our allies will help us serve you and your skin better, so feedback is always appreciated!</p>
              </div>
              <div onClick={()=>{ SET_DELIVERY_SHIPPING(!DELIVERY_SHIPPING)}} className={"accordion "+( DELIVERY_SHIPPING === true ? " active": "")}><span className="faq-margin">DELIVERY/SHIPPING</span></div>
              <div className={"panel "+( DELIVERY_SHIPPING === true ? " active": "")}>
                <div className="row">
                <div className="faq-leftsec col-md-6">
                  <div className="faq-block"><span className="faq-ques">Where does Allies of Skin ship?</span>
                    <div className="faq-ans">
                      <p>We ship globally to selected countries, restrictions apply. Alternatively, please refer to our list of <a href="https://alliesofskin.com/retailers/">Authorized Retailers</a>.</p>
                      <p>Do note that all orders are subjected to local custom fees, taxes, tariffs, or duties.</p>
                      <p>All prices reflected on our site are displayed in US Dollars (US$).</p>
                    </div>
                  </div>
                  <div className="faq-block">
                    <span className="faq-ques">What are my delivery/shipping options?</span></div>
                  <div className="faq-block">
                    <div className="faq-ans"><span className="faq-title">SINGAPORE</span>
                      <p>We offer free Courier Delivery for all Singapore orders US$75 and above.</p>
                      <p>For all orders below US$75, the following shipping rates applies:</p>
                      <p><strong>Courier Delivery:</strong> 1 Business Day*: US$10</p>
                      <p><strong>Standard Delivery:</strong>3 - 4 Business Days: US$5</p>
                      <p>*We aim to get your order delivered to you within 24 hours of you placing the order.<br />All orders placed after 9:30 am (GMT+8) will be processed the next working day.</p>
                      <span className="faq-title">ASIA AND OCEANIA (BRUNEI, HONG KONG, JAPAN, MACAO, MALAYSIA, SOUTH KOREA, TAIWAN, AUSTRALIA AND NEW ZEALAND)</span>
                      <p>We offer free DHL Express (3 to 4 business days) delivery for all orders placed within the following countries stated above:</p>
                      <p>Free delivery for orders US$200 and above.</p>
                      <p>US$25delivery charge for orders below US$200.</p>
                      <p><span className="faq-title">UNITED KINGDOM</span></p>
                      <p>Free delivery for orders GBP70 and above.</p>
                      <p>GBP5 delivery charge for orders below GBP70.</p>
                      <p><span className="faq-title">REST OF EUROPE (BELGIUM, DENMARK, FRANCE, GERMANY, IRELAND, ITALY, LUXEMBOURG, MONCATO, NETHERLANDS, NORWAY, PORTUGAL, SWEDEN, SWITZERLAND)</span></p>
                      <p>Free delivery for orders EUR70and above.</p>
                      <p>EUR5 delivery charge for orders below EUR70.</p>
                      <span className="faq-title">UNITED STATES</span>
                      <p>We offer USPS Priority and USPS First Class Mail delivery to all states within the United States**</p>
                      <p><strong>USPS Priority:</strong></p>
                      <p>US$5 delivery charge for orders US$75 and above.</p>
                      <p>US$10 delivery charge for orders below US$75.</p>
                      <p><strong>USPS First Class Mail:</strong></p>
                      <p>Free delivery for orders US$75 and above.</p>
                      <p>US$5 delivery charge for orders below US$75.</p>
                      <p>**For orders to Alaska and Hawaii, a flat shipping rate (USPS First Class Mail) applies to:</p>
                      <p>US$5 delivery charge for orders US$75 and above.</p>
                      <p>US$10 delivery charge for orders below US$75.</p>
                      <span className="faq-title">CANADA</span>
                      <p>We offer USPS First Class Mail International delivery to all states within Canada.</p>
                      <p><strong>USPS First Class Mail International:</strong></p>
                      <p>US$12 delivery charge for orders US$250 and below.</p>
                      <p>Free delivery for orders USD$250 and above.</p>
                    </div>
                  </div>
                </div>
                <div className="faq-rightsec col-md-6">
                  <div className="faq-block"><span className="faq-ques">When will my order ship?</span>
                    <div className="faq-ans">
                      <p>We aim to process orders placed before 9.30 am (GMT+8) within 24 hours of you placing it.* Orders placed after 9.30 am (GMT+8) will be processed the next working day. A confirmation email will be sent to you after an order has been placed. Once your order has been shipped, you will receive an email with the tracking information for your parcel.</p>
                      <p>*This excludes US orders, which may take up to 48 hours to be processed.</p>
                    </div>
                  </div>
                  <div className="faq-block"><span className="faq-ques">I’m having problems with my international tracking number.</span>
                    <div className="faq-ans">
                      <p>It may take 1-2 working days for your shipment status to be updated on our carrier’s system. For any enquiries regarding your order, please reach out to us at <a href="mailto:ask@alliesofskin.com">ask@alliesofskin.com</a> with your order number and we will endeavor to be in touch within 1 working day.</p>
                    </div>
                  </div>
                  <div className="faq-block"><span className="faq-ques">How can I edit my shipping address?</span>
                    <div className="faq-ans">
                      <p>If you wish to make any changes to your order, please email us at<a href="mailto:ask@alliesofskin.com">ask@alliesofskin.com</a>with your order number as soon as possible.</p>
                    </div>
                  </div>
                  <div className="faq-block"><span className="faq-ques">Other enquiries.</span>
                    <div className="faq-ans">
                      <p>For any enquiries, please email us at<a href="mailto:ask@alliesofskin.com">ask@alliesofskin.com</a> and we will endeavor to be in touch within 1 working day.</p>
                    </div>
                  </div>
                </div>
                </div>
              </div>
              <div onClick={()=>{ SET_ALLIES_OF_SKIN(!ALLIES_OF_SKIN)}} className={"accordion "+( ALLIES_OF_SKIN === true ? " active": "")}><span className="faq-margin">ALLIES OF SKIN</span></div>
              <div className={"panel "+( ALLIES_OF_SKIN === true ? " active": "")}>
                <div className="faq-leftsec col-md-6">
                  <div className="faq-block"><span className="faq-ques">Where is your company and where are your products made?</span>
                    <div className="faq-ans">
                      <p>We have offices in Singapore and Berlin. All our products are made in the United States.</p>
                    </div>
                  </div>
                  <div className="faq-block"><span className="faq-ques">I would love to carry Allies of Skin in my store/spa, who should I speak to?</span>
                    <div className="faq-ans">
                      <p>Please send all wholesale and retail queries to <a href="maitto:partners@alliesofskin.com">partners@alliesofskin.com</a>.</p>
                    </div>
                  </div>
                </div>
                <div className="faq-rightsec col-md-6">
                  <div className="faq-block"><span className="faq-ques">Do you test on animals?</span>
                    <div className="faq-ans">
                      <p>Never. We are 100% cruelty-free and PETA-certified. Moreover, we find that busy go-getters make for the best test subjects. They are demanding and honest; traits that go a long way in helping to create the best products.</p>
                    </div>
                  </div>
                  <div className="faq-block"><span className="faq-ques">I would like to feature Allies of Skin and its products. How do I get in touch?</span>
                    <div className="faq-ans">
                      <p>Please send all press and media enquiries to <a href="maitto:press@alliesofskin.com">press@alliesofskin.com</a>.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div onClick={()=>{ SET_PRODUCTS(!PRODUCTS)}} className={"accordion "+( PRODUCTS === true ? " active": "")}><span className="faq-margin">PRODUCTS</span></div>
              <div className={"panel "+( PRODUCTS === true ? " active": "")}>
                <div className="faq-leftsec col-md-6">
                  <div className="faq-block"><span className="faq-ques">What skin types are Allies of Skin products suitable for?</span>
                    <div className="faq-ans">
                      <p>Allies of Skin products were created to bring comfort to all skin types in all of life’s lemonade-making moments. Be it dullness or breakouts caused by stress from pulling that all-nighter at work, or skin dryness sustained from a night out living your best life, we’d like to think that we have you covered.</p>
                      <p>To ensure we fulfil all terms of our alliance with you (to give you happy skin!), all of our products are created in small batches under the strictest ISO &amp; FDA guidelines. They are also Dermatologist-Tested, Hypoallergenic and Non-Clogging.</p>
                      <p>In fact, through our testing processes, we’ve found that our products are proven to help balance and improve problematic skin. So here’s to happy skin indeed!</p>
                    </div>
                  </div>
                  <div className="faq-block"><span className="faq-ques">Can I use Allies of Skin products during pregnancy?</span>
                    <div className="faq-ans">
                      <p>Congratulations! While we endeavor to be your trusted skincare expert, we strongly recommend you speak to your doctor for the best advice on what products you should and should not use when pregnant.</p>
                    </div>
                  </div>
                  <div className="faq-block"><span className="faq-ques">I reordered a product and I noticed that it seems to smell a little different, why is that so?</span>
                    <div className="faq-ans">
                      <p>The nose has it, the aroma you detect is a natural by-product of the bioactive plant oils we use in our products. It is part of the intrinsic nature of plant oils and extracts that their aroma changes over time.</p>
                      <p>Our sole goal for every product is maximum potency and efficacy so we do not formulate for scent. The final aroma of every product is the result of a clean, meticulous formulation process where every product smells as per nature intended.</p>
                      <p>So, you can rest assured that not a drop of that synthetic perfume goes into the creation of Allies of Skin products.</p>
                    </div>
                  </div>
                </div>
                <div className="faq-rightsec col-md-6">
                  <div className="faq-block"><span className="faq-ques">How do I know when my product will expire?</span>
                    <div className="faq-ans">
                      <p>Each product lasts for 6 - 12 months from the time of opening. Look out for the icon with the open jar on the back of every product (example: 6M / 9M / 12M).</p>
                      <p>We intend for our products to weather all of life’s many seasons with you, and while our products can be left in extreme heat or cold for a short period of time, without affecting their efficacy, they do get cold feet or become too hot to handle which may result in a slight change in product texture.</p>
                    </div>
                  </div>
                  <div className="faq-block"><span className="faq-ques">When is your next product launching?</span>
                    <div className="faq-ans">
                      <p>We have plenty of new formulas to share. To be the first in the know, simply join our mailing list and receive the latest updates.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div onClick={()=>{ SET_RETURN_EXCHANGE(!RETURN_EXCHANGE)}} className={"accordion "+( RETURN_EXCHANGE === true ? " active": "")}><span className="faq-margin">RETURN/EXCHANGE</span></div>
              <div className={"panel "+( RETURN_EXCHANGE === true ? " active": "")}>
                <div className="faq-leftsec col-md-6">
                  <div className="faq-block"><span className="faq-ques">Can I return/exchange my product?</span>
                    <div className="faq-ans">
                      <p>Your satisfaction is our top priority. If you are not completely contented with our products within 30 days of you placing the order, please refer to the instructions on the <a href="https://alliesofskin.com/alliesofskin/guest/login/">Order Returns Form</a>. We endeavor to be in touch within 3 business days. Simply return the unused product, and we will be happy to refund you the full amount of your purchase, excluding shipping and any discount applied.</p>
                    </div>
                  </div>
                </div>
                <div className="faq-rightsec col-md-6">
                  <div className="faq-block"><span className="faq-ques">What should I do if I received the wrong product?</span>
                    <div className="faq-ans">
                      <p>Please contact us at <a href="mailto:ask@alliesofskin.com">ask@alliesofskin.com</a>with your order number and we will endeavor to offer you assistance within 1 working day.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div onClick={()=>{ SET_BILLING(!BILLING)}} className={"accordion "+( BILLING === true ? " active": "")}><span className="faq-margin">BILLING</span></div>
              <div className={"panel "+(BILLING === true ? " active": "")}>
                <div className="faq-leftsec col-md-6">
                  <div className="faq-block"><span className="faq-ques">When will my credit card be charged?</span>
                    <div className="faq-ans">
                      <p>Your card will be charged once the order has been accepted and processed.</p>
                    </div>
                  </div>
                  <div className="faq-block"><span className="faq-ques">What forms of payment do you accept?</span>
                    <div className="faq-ans">
                      <p>We accept all major credit cards and PayPal.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div onClick={()=>{ SET_ORDER_QUESTIONS(!ORDER_QUESTIONS)}} className={"accordion "+( ORDER_QUESTIONS === true ? " active": "")}><span className="faq-margin">ORDER QUESTIONS</span></div>
              <div className={"panel "+( ORDER_QUESTIONS === true ? " active": "")}>
                <div className="faq-leftsec col-md-6">
                  <div className="faq-block"><span className="faq-ques">Can I send my order as a gift?</span>
                    <div className="faq-ans">
                      <p>That is an affirmative yes. After checking out, simply email us at<a href="mailto:ask@alliesofskin.com">ask@alliesofskin.com</a>with your request, together with the order number. If you have a personalised message for the recipient of the gift, please indicate it in your request.</p>
                    </div>
                  </div>
                  <div className="faq-block"><span className="faq-ques">How do I update my order?</span>
                    <div className="faq-ans">
                      <p>Please contact us at <a href="mailto:ask@alliesofskin.com">ask@alliesofskin.com</a> as soon as possible, with your order number, if you would like to cancel or modify your order. We usually process orders as soon as we receive them but we’ll try our best to fulfill your request.</p>
                    </div>
                  </div>
                  <div className="faq-block"><span className="faq-ques">How can I check the status of my order?</span>
                    <div className="faq-ans">
                      <p>You will receive a confirmation email once your order has been placed. We’ll send you a second email with the tracking information once your order has been shipped. If you have any further questions regarding your order, please contact us at <a href="mailto:ask@alliesofskin.com">ask@alliesofskin.com</a></p>
                    </div>
                  </div>
                </div>
                <div className="faq-rightsec col-md-6">
                  <div className="faq-block"><span className="faq-ques">How do I use a promo code?</span>
                    <div className="faq-ans">
                      <p>Simply apply your promo code during checkout. Please note that all Allies of Skin promo codes cannot be used in conjunction with one another and only one promo code can be used at any one time.*</p>
                      <p>*Offers that are advertised on our website without a promo code will be automatically applied at checkout. In this instance, separate promo codes can be used in conjunction with the aforementioned advertised offers.</p>
                      <p><span className="faq-ques">How do I place a preorder?</span></p>
                      <div className="faq-ans">
                        <p>You can add the items into your cart and checkout as usual. Kindly note that you must select the “preorder” payment method for your order to be processed. A representative will be in touch with you regarding payment when the product(s) is/are ready for fulfillment.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

export default Faq;