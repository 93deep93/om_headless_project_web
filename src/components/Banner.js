import React from 'react';
import '../styles/Banner.scss';


const Banner = () => {
  return (
    <React.Fragment>
      <div className=" top_banner">
        <p>PRICES DISPLAYED ARE IN USD. ENJOY FREE SHIPPING ON ALL ORDERS ABOVE $75. *SELECTED COUNTRIES ONLY*</p>
      </div>
    </React.Fragment>
  );
}

export default Banner;