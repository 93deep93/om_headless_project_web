import React, {Component} from 'react';
import './Login.scss';
import {Modal} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import { login, isAuthenticated } from './Repository';
import { changeLoginStatus, setUserProfile } from '../actions/changeLoginStatus';
import { getNumbers, checkOldCartAndUpdate } from '../actions/getAction';
import { connect } from 'react-redux';


class Login extends Component {


  constructor(props) {
        super();
        console.log(props);
        this.state = { email: '', password: '', email_error: '', password_error: '', isPopup: false, message: '', error: '', pre_page_url : '' };
      }

      componentDidMount(){
        if( this.props.location && this.props.location.state)
        this.setState({ pre_page_url : this.props.location.state.pre_page_url })
      }

      handleClose = () => {
         this.setState({isPopup: false})
      }

      validateEmail=( email)=>{
        const regex_email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex_email.test(String(email).toLowerCase());
      }

      validatePassword=( password)=>{
        const regex_password = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/;
        return regex_password.test(password);
      }


      handleInputChange = (event) => 
          this.setState({[event.target.name]: event.target.value, [event.target.name + '_error']: ''})


    validateForm=()=>{
      let flag = false;
      var { email, password, email_error, password_error} = this.state;
      if( email === "" || email=== null || email === undefined){
        email_error = 'This field is required';
        this.setState({ email_error : email_error})
        flag = true;
      }else if( email !== "" || email !== null || email !== undefined){
        if(!this.validateEmail(email)){
          email_error = 'Please enter valid email id !'
          this.setState({ email_error : email_error})
          flag = true;
        }}

        if( password === "" || password=== null || password === undefined){
          password_error = 'This field is required';
          this.setState({ password_error : password_error})
          flag = true;
        } 
        return flag
      }
          
      submitLogin = (event) => {
        event.preventDefault();
        if(!this.validateForm()){
        login(this.state)
          .then(token => {
            // window.location = '/';
           if(token.code === 200) {   
             this.props.setUserProfile();
            this.props.changeLoginStatus(true);
            if( this.props.basketProps && this.props.basketProps.products && this.props.basketProps.products.productList && this.props.basketProps.products.productList.length>0){
              this.props.checkOldCartAndUpdate( this.props.basketProps.products.productList);
            }else{
              sessionStorage.removeItem('cart-id')
              this.props.getNumbers();
            }
            this.setState({isPopup:true, message: "You are successfully logged in!", error: ''});
            let pre_page_url = this.state.pre_page_url;
            this.props.history.push(pre_page_url != ''? pre_page_url :'/');
           }
          }).catch(err => {
            this.setState({error: err});
            console.log(err)
          });
        }
      }



 render() {

  let {isPopup, error, message,  email_error, password_error } = this.state;
  console.log('print popup',isPopup);

  return (
    <React.Fragment> 
    <div className="login-container container mt-5 mb-5">
      <div className="row">

        <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 p-0 mob-pad">

            <div className="customer-login">
                     
                  <form onSubmit={this.submitLogin}>

                     <h2>I`M ALREADY AN ALLY</h2>
                       <p>If you have an account, sign in with your email address.</p>

                        <div className="form_group">
                            <label for="email">Email</label>
                            <input type="email" name="email" placeholder="" onChange={this.handleInputChange}/>
                            {email_error ? <div  className = "error">{email_error}</div> : ''}
                        </div>
                            
                        <div className="form_group">
                            <label for="password">Password</label>
                            <input type="password" name="password" placeholder="" onChange={this.handleInputChange} />  
                            {password_error ? <div  className = "error">{password_error}</div> : ''}
                        </div> 
                        <p className="invalid_userid">{error}</p>

                        <input type="submit" value="Sign In"/>
                        <span className="forget-pswd"><Link to="/Forgetpassword">Forgot Your Password?</Link></span>
                  </form>
            </div>  
        </div> 

           <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 p-0 mob-pad"> 

                <div className="block block-new-customer">

                    <div className="block-title">
                       <h2 role="heading" aria-level="2">New Customers</h2>
                    </div>

                    <div class="block-content">
                        <p>Creating an account has many benefits: check out faster, keep more than one address, track orders and more.</p>
                         
                         <div className="primary">
                             <Link to="/Signup" className="action primary"><span>Create an Account</span></Link>
                          </div>
                    </div>

                </div> 

          </div> 

      </div> 

    </div>

            <Modal className="message_popup" show={isPopup} onHide={this.handleClose} animation={false}>
              <Modal.Header closeButton> </Modal.Header>
                <Modal.Body className= "text-center mt-5 mb-5">{message}</Modal.Body>
            </Modal>
 
    </React.Fragment>
  );

 }
  
}


const mapStateToProps = state => ({
  basketProps:state.basketState
})
export default connect(mapStateToProps, {changeLoginStatus, getNumbers, checkOldCartAndUpdate, setUserProfile})(Login);