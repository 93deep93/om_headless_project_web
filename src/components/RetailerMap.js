import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
 
const AnyReactComponent = ({ text }) => 
<div className="map" style={{
    color: 'white', 
    background: 'grey',
    padding: '15px 10px',
    display: 'inline-flex',
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '100%',
    transform: 'translate(-50%, -50%)'
  }}>
    {text}
  </div>;
 
class ReatailerMap extends Component {
  static defaultProps = {
    center: {
        lat:36.778259,
        lng: -119.417931
    },
    zoom: 3
  };
 
  render() {
    return (
      // Important! Always set the container height explicitly
      <div className="retailemap" style={{ height: '400px', width: '100%' }}>
        <GoogleMapReact googleApiKey=''
          // bootstrapURLKeys={{ key: 'AIzaSyCucmsUlipBcNS2M5XplegdUdcWwR9jLes'}}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
        >
          <AnyReactComponent
            lat={36.778259}
            lng={-119.417931}
            text="California"
          />
          <AnyReactComponent
            lat={41.676388}
            lng={-86.250275}
            text="South Bend"
          />
          <AnyReactComponent
            lat={40.058323}
            lng={-74.405663}
            text="New Jersey"
          />
        </GoogleMapReact>
      </div>
    );
  }
}
 
export default ReatailerMap;