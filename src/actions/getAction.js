import { SET_USER_CART_DATA, SET_SPINNER_STARTED, SET_SPINNER_COMPLETED, CHECK_UNAUTHORIZED } from './types';
import { getCartAddedProducts, createCart, AddToCart, placeOrderApi, CheckStockProductApi} from '../account/Repository'

export const getNumbers =()=> {
	return dispatch => {
		dispatch({ type : SET_SPINNER_STARTED});
		createCart()
		.then(res => {
		if(res.code === 200) { 
			if(!sessionStorage.getItem('cart-id'))
			sessionStorage.setItem('cart-id', res.result);
			let cartID = sessionStorage.getItem('cart-id') ? sessionStorage.getItem('cart-id') : res.result;
			getCartAddedProducts(  cartID)
			.then(res => {
				if(res.code === 200) { 
					
				let allProducts = JSON.parse(localStorage.getItem('allProductsAlliesProducts'));
					for(let i =0; i< res.result.length; i++){
						 res.result[i] = Object.assign(res.result[i], allProducts.filter( item => item.sku === res.result[i].sku)[0]);
					}
					dispatch({
						type: SET_USER_CART_DATA,
						payload:  res.result
					}); 
					dispatch({ type : SET_SPINNER_COMPLETED});
					}
				}).catch(err => {
					dispatch({ type : SET_SPINNER_COMPLETED});
					if(err.response && err.response.data.code === 401 && err.response.data.result === "The consumer isn't authorized to access self."){
						dispatch({ type : CHECK_UNAUTHORIZED, payload: true});
					  }
				})
			}
			}).catch(err => {
				dispatch({ type : SET_SPINNER_COMPLETED});
				if( err.response && err.response.data.code === 401 && err.response.data.result === "The consumer isn't authorized to access self."){
					dispatch({ type : CHECK_UNAUTHORIZED, payload: true});
				  }
			});
		}
}

export const checkOldCartAndUpdate =( oldCartproducts)=> {
	return dispatch => {
		dispatch({ type : SET_SPINNER_STARTED});
		createCart()
			.then(res => {
			if(res.code === 200) { 
				let cart_id= res.result;
				sessionStorage.setItem('cart-id', res.result);
				getCartAddedProducts( res.result)
				.then(res => {
					if(res.code === 200) { 	
						let finalDataAllCart = getFinalCartData( oldCartproducts, res.result, cart_id);
				
							updateAllCartProducts( finalDataAllCart, cart_id, function( updateRespose ){
								console.log(updateRespose)
								dispatch({ type : SET_SPINNER_COMPLETED});
								// dispatch({
								// 		type: SET_USER_CART_DATA,
								// 		payload:  finalDataAllCart
								// 	}); 
								dispatch( getNumbers())
							} )

							// console.log(res.result);
							// dispatch({
							// 	type: SET_USER_CART_DATA,
							// 	payload:  res.result
							// }); 
					
				}
				}).catch(err => {
					dispatch({ type : SET_SPINNER_COMPLETED});
					if( err.response && err.response.data.result)
					alert(err.response.data.result)
				})
			}
			}).catch(err => {
				dispatch({ type : SET_SPINNER_COMPLETED});
				if(err.response.status === 401 && err.response.statusText === "Unauthorized"){
					dispatch({ type : CHECK_UNAUTHORIZED, payload: true});
				  }
			});
		}
}

function getFinalCartData( oldkartData, newKartData, cart_id){

	let finalDataAllCart = [];
	let sameData =[];
	let allCartData =[];
	if(newKartData.length>0){
		var ids = new Set( newKartData.map(d => d.sku));
		var uniqueOldProducts = oldkartData.filter(d => !ids.has(d.sku));
		for( let i=0; i<oldkartData.length; i++){
			let matchdata = newKartData.filter( item=> item.sku === oldkartData[i].sku );
			if(matchdata.length>0){
				oldkartData[i].qty= Number(oldkartData[i].qty  || oldkartData[i].numbers) + Number(matchdata[0].qty|| matchdata[0].numbers);
				oldkartData[i].item_id = matchdata[0].item_id;
				oldkartData[i].isAddIteamId = true;
				sameData.push(oldkartData[i]);
			}
		}
		allCartData =  [ ...uniqueOldProducts, ...sameData ];
	}else{
		allCartData=oldkartData;
	}			
	for( let i=0; i<allCartData.length; i++){
		if(allCartData[i].hasOwnProperty("extension_attributes")){
			let data={};
			if(allCartData[i].isAddIteamId)
			data.item_id = allCartData[i].item_id; 
			data.sku = allCartData[i].sku;
			data.qty = allCartData[i].qty;
			data.name = allCartData[i].name;
			data.price = allCartData[i].price; 
			data.product_type = allCartData[i].product_type; 
			data.quote_id = cart_id;
			finalDataAllCart.push(data);
		}else{
			allCartData[i].quote_id = cart_id;
			finalDataAllCart.push( allCartData[i]);
		}
	}
	return finalDataAllCart
}

function updateAllCartProducts( cartData, cart_id, totalResponseBy){
	let totalResponse=[];
	let tempPromise=[];
	for( const [key] in  cartData){
		 let product={}
		 product.item_id = cartData[key].item_id;
		//  product.name = cartData[key].name;
		//  product.price = cartData[key].price;
		 product.product_type = cartData[key].product_type;
		 product.qty = cartData[key].qty;
		 product.quote_id = cartData[key].quote_id;
		 product.sku = cartData[key].sku;

		tempPromise.push (new Promise ( function (resolve,reject){
			// update api call
			AddToCart(product , cart_id)
			.then(res => {
				totalResponse[key] = res;
				resolve(totalResponse );
			})
		}));
		Promise.all( tempPromise ).then( values=> {
			totalResponseBy( values)
		});
	}
}

export const placeOrder =( orderData)=> {
	return dispatch => {
		dispatch({ type : SET_SPINNER_STARTED});
		placeOrderApi(orderData).then(res => {
			dispatch({ type : SET_SPINNER_COMPLETED});
			if(res.code === 200) { 
				dispatch({
					type: SET_USER_CART_DATA,
					payload:  []
				}); 
			}
		}).catch(err => {
			dispatch({ type : SET_SPINNER_COMPLETED});
			if( err.response && err.response.data.result)
			alert(err.response.data.result)
		})
	}
}

export function reOrderProducts(orders){
	checkProductStock( orders, function( checkStockRes ){
		let ids = new Set( checkStockRes.map(d => d.product_id));
		var reOrderProduct = orders.filter(d => ids.has(d.product_id));
		return reOrderProduct ;
	});
}
 
export function checkProductStock( orders, response){
	let totalResponse=[];
	let tempPromise=[];
	let count= 0;
	for( const [key] in  orders){
		 let product_sku= orders[key].sku;

		tempPromise.push (new Promise ( function (resolve,reject){
			// update api call
			CheckStockProductApi(product_sku)
			.then(res => {
				count+=1;
				totalResponse[key] = res.data.result;
				if(orders.length == count){
					resolve(totalResponse );
				}
				
			})
		}));
		Promise.all( tempPromise ).then( values=> {
			response( values[0])
		});
	}
}