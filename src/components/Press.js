import React , { Component } from 'react';
import Pagination from 'react-js-pagination';
import '../styles/Press.scss';
import '../styles/Home.scss';



class Press extends Component {

  constructor(props) {
    super(props);
    this.state = {
      activePage: 1
    };
  }
 
  handlePageChange(pageNumber) {
    console.log(`active page is ${pageNumber}`);
    this.setState({activePage: pageNumber});
  }	
  
  render() {

  return (
    <React.Fragment>
     <section className="page_banner">
      <div><img className="image" src={require('../images/press_header.jpg')}/></div>

	    <div className="container-fluid press-section pl-5 pr-5">  
	       <div className="row">

             <div className ="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12">
                <div className="press-list">
                  <div className="press-date">June 2020 </div>
                    <div className="press-img"><img src={require('../images/elegraph.png')}/></div>
                     <div className="press-content">
                        <p>6 Things You Need To Know About Caroline Hirons’s Skincare Routine FEAT. Molecular Multi-Nutrient Day Cream</p>
                     </div>
                </div>
             </div>

             <div className ="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12">
               <div className="press-list">
                  <div className="press-date">June 2020 </div>
                    <div className="press-img"><img src={require('../images/elle.png')}/></div>
                     <div className="press-content">
                        <p>These Are The 20 Results-Driven Moisturisers You Need To Know FEAT. Molecular Multi-Nutrient Day Cream</p>
                     </div>
                </div>
             </div>

             <div className ="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12">
               <div className="press-list">
                  <div className="press-date">June 2020 </div>
                    <div className="press-img"><img src={require('../images/vogue.png')}/></div>
                     <div className="press-content">
                        <p>From Threading Your Own Brows To Cutting Your Fringe, Our Beauty Experts Tell You How FEAT. Triple Hyaluronic Antioxidant Hydration Serum</p>
                     </div>
                </div>
             </div>

              <div className ="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12">
               <div className="press-list">
                  <div className="press-date">June 2020 </div>
                    <div className="press-img"><img src={require('../images/female.png')}/></div>
                     <div className="press-content">
                        <p>Light Moisturisers For Fresh, Hydrated Summer Skin FEAT. Molecular Multi-Nutrient Day Cream</p>
                     </div>
                </div>
             </div>

              <div className ="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12">
               <div className="press-list">
                  <div className="press-date">June 2020 </div>
                    <div className="press-img"><img src={require('../images/mail-online.jpg')}/></div>
                     <div className="press-content">
                        <p>Allies Of Skin Molecular Silk Amino Hydrating Cleanser FEAT. Molecular Silk Amino Hydrating Cleanser</p>
                     </div>
                </div>
             </div>


	       </div>

         {/* <div className ="row pages">
		        <Pagination
		          activePage={this.state.activePage}
		          itemsCountPerPage={10}
		          totalItemsCount={250}
		          pageRangeDisplayed={5}
		          onChange={this.handlePageChange.bind(this)}
		        />
        </div> */}

	    </div> 
      
      </section>
    </React.Fragment>
  );

 }

}

export default Press;