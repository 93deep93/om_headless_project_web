
import React, { useEffect, useState, useRef } from 'react';
import ReactDOM from 'react-dom';
import '../styles/MobileMenu.scss';
import { Link } from 'react-router-dom';
import { Navbar, Nav } from 'react-bootstrap';
import { connect } from 'react-redux';
import { Button, Col, Overlay, Popover, Tooltip, Row } from 'react-bootstrap';
import { getNumbers } from '../actions/getAction';
import { addBasket } from '../actions/addAction';
import { increaseQty } from '../actions/increaseQty';
import Cart from './Cart';
import { isAuthenticated } from '../account/Repository';
import { changeLoginStatus } from '../actions/changeLoginStatus'
const MobileMenu = (props) => {
    const [show, setShow] = useState(false);
    const [ showMenu, setShowMenu ] = useState(false);
    const [ activeClass, setactiveClass ] = useState('');
    const target = useRef(null);

    useEffect(() => {
        window.onscroll = () => {
            let activeClass = 'fixed-header';
            if(window.scrollY === 0){
                activeClass = '';
            }
            setactiveClass(activeClass);
         }
        },
      []);

      const logout = ()=>{
        // localStorage.clear()
        localStorage.removeItem('basketData');
        localStorage.removeItem('x-access-token');
        localStorage.removeItem('x-access-token-expiration');
        localStorage.removeItem('user-info-allies');
        sessionStorage.removeItem('cart-id');
        sessionStorage.removeItem('checkout_shipping_data');
        props.changeLoginStatus(isAuthenticated())
        props.getNumbers();
      }

    return (
        <React.Fragment>
            <header class={"page-header mobile-menu " + (activeClass)}>
                <div id="custom-header-id" class="customDiv">

                    <div class="menu-over-banner">
                        <div class="menu-over-banner-container">
                            <div class="menu-section">
                                <div onClick={ ()=>{ setShowMenu( !showMenu) } } class="main-menu">
                                    <span class="header-main-menu">Menu</span>
                                </div>
                                <div class= {"slide-menu-show " + ( showMenu ? 'active' : '')}>
                                    <div class="slide-menu-show-conatiner">
                                        <div class="inner-menu">
                                            <div class="inner-menu-heading">
                                                <Link to="/" className="mobilelogo"><img src={require('../icons/menu-logo.png')} /></Link>

                                            </div>
                                            <a class="close-inner-menu" onClick={ ()=>{ setShowMenu( false) } }>X</a>
                                            <div class="inner-menu-container">
                                                <ul class="inner-menu-parent-container-ul">

                                                    <li className="parent-menu"> <Link to="/" onClick={ ()=>{ setShowMenu( false) } } > Home </Link></li>
                                                    <li className="parent-menu"> <Link to="/Shop" onClick={ ()=>{ setShowMenu( false) } }> Shop </Link></li>
                                                    <li className="parent-menu"> <Link to="/About" onClick={ ()=>{ setShowMenu( false) } }> About Us </Link></li>
                                                    <li className="parent-menu"> <Link to="/Press" onClick={ ()=>{ setShowMenu( false) } }> Press </Link> </li>
                                                    <li className="parent-menu"> <Link to="/Retailers" onClick={ ()=>{ setShowMenu( false) } }> Retailers </Link> </li>
                                                    {/* <li className="parent-menu"> <Link to="/regimen-recommender" onClick={ ()=>{ setShowMenu( false) } }> Regimen Recommender  </Link></li> */}
                                                </ul>
                                            </div>
                                        </div>

                                        {props.basketProps.isLogin ?<>
                                            <ul class="header links">
                                                <li class="authorization-link mobile-only">
                                                <Link onClick={ ()=>{ setShowMenu( false) } } to="/accountinfo">
                                                    My account    
                                                </Link>
                                                </li>
                                            </ul>
                                            <ul class="header links custom-register">
                                                <li class="authorization-link register-link">
                                                    <Link onClick={()=> { logout(); setShowMenu( false); }}>Logout</Link>
                                                </li>
                                            </ul>
                                        </> :
                                        <>
                                        <ul class="header links custom-register">
                                            <li class="order-return">
                                                {/* <a href="#">Order Return</a> */}
                                            </li>
                                        </ul>
                                        <ul class="header links"><li class="authorization-link mobile-only">
                                            <Link onClick={ ()=>{ setShowMenu( false) } } to="/Login">
                                                Login    
                                            </Link>
                                        </li>
                                        </ul>
                                        <ul class="header links custom-register">
                                            <li class="authorization-link register-link">
                                                <Link onClick={ ()=>{ setShowMenu( false) } } to="/Signup">sign up for allies underground</Link>
                                            </li>
                                        </ul>
                                        </>
                                        }

                                    </div>
                                </div>
                                <div class="main-logo">
                                    <strong class="logo">
                                        <Link to="/" className="mobilelogomenu"><img src={require('../icons/logo.png')} /></Link>
                                    </strong>

                                </div>
                                <div class="minicart-wrapper">
                                    <div class="action showcart" >
                                    <span className="text" ref={target} onClick={() => setShow(!show)}> Bag (<span >{props.basketProps.basketNumbers}</span>)</span>
                                        {/* <span class="counter qty empty">
                                            <span class="counter-number"></span>
                                            <span class="counter-label">
                                            </span>
                                        </span> */}
                                    </div>
                                    <Overlay target={target.current} show={show} placement="left">
                                        {(props) => (
                                        <Tooltip className="cart_popover mobile-fixed" {...props}>
                                        
                                            <div className="cart_list">
                                                <Cart handleCloseBag={()=>{ setShow(!show) }}/>                     
                                            </div>

                                            <span className="close-icon" onClick={() => setShow(!show)}>&times;</span>
                                        </Tooltip>
                                        )}

                                    </Overlay>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </React.Fragment>
    );
}

const mapStateToProps = state => ({
    basketProps: state.basketState
})
export default connect(mapStateToProps, { addBasket, increaseQty, changeLoginStatus, getNumbers })(MobileMenu);