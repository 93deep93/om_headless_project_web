import React, { useState, Fragment, Component } from 'react';
import { Form, Button, Tooltip, Accordion, Card } from 'react-bootstrap';
// import '../../styles/Checkout.scss';
import { Redirect, Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import ShippingForm from '../../Checkout/ShippingForm/shippingForm';
import PaymentExpressCheckout from './paypalExpressCheckout';
import CODcheckout from './codCheckout';
import ApplyCoupon from './applyCoupon';
import equal from 'fast-deep-equal';
const PaymentMethod = (props) => {
    const [ paymet_method, setPayment_method] = useState('cashondelivery');
    return (
        <React.Fragment> 
            <div className="pament-option">
                <section className="payment-option">
                    <div className="step-title">Payment Method</div>
                    <div className="payment-group">
                        <PaymentExpressCheckout 
                            paymet_method={paymet_method} 
                            setPayment_method={(data)=> setPayment_method(data)}
                        />
                        <CODcheckout 
                            paymet_method={paymet_method} 
                            setPayment_method={(data)=> setPayment_method(data)}
                            user_info_address={props.user_info_address}
                            shipmentAddress= {props.shipmentAddress}
                            order_error = {props.order_error}
                            handle_place_order = { (shipmentAddress, billingAddress)=>{ props.handle_place_order( shipmentAddress, billingAddress, paymet_method) }}
                            />
                        {/* <ApplyCoupon/> */}
                    </div>
                </section>
            </div>
        </React.Fragment >
    );
};

export default PaymentMethod;