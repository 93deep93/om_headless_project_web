import { combineReducers } from 'redux';
import basketReducer from './basketReducer';
import spinner from './spinner';
import customerInfoReducer from './customerinfoReducer';
import alertsDialog from './alertsDialogReducer'

export default  combineReducers ({
  basketState:basketReducer,
  spinnerState: spinner,
  userinfoState: customerInfoReducer,
  alertsDialog
});