import { ADD_PRODUCT_BASKET, SET_SPINNER_STARTED, SET_SPINNER_COMPLETED, SET_ADD_BASKET_ERROR_ , CHECK_UNAUTHORIZED} from './types';
import { createCart, AddToCart} from '../account/Repository';

export const addBasket = (product) => {
	return dispatch => {
		dispatch({ type : SET_SPINNER_STARTED});
		
			if(sessionStorage.getItem('cart-id')){
				let cartId = sessionStorage.getItem('cart-id');
				let productDetails ={};
				productDetails.sku =  product.sku;
				productDetails.quoteId=  cartId;
				productDetails.qty= product.numbers ? product.numbers : 1;
				productDetails.product_option= {extension_attributes : {bundle_options: [],
					configurable_item_options: [],
					custom_options: []}};
				AddToCart(productDetails , cartId)
				.then(res => {
				if(res.code === 200) { 
						product.quoteId = cartId;
						product.item_id = res.result.item_id;	
						dispatch({ type : SET_SPINNER_COMPLETED});			
						dispatch({
							type: ADD_PRODUCT_BASKET,
							payload: product
						}); 
					}
				}).catch(err => {
					dispatch({ type : SET_SPINNER_COMPLETED});
					if( err.response && err.response.data.result)
					dispatch({ type : SET_ADD_BASKET_ERROR_, error : err.response.data});
				});
			}else{
				createCart(product)
				.then(res => {
				if(res.code === 200) { 
					sessionStorage.setItem('cart-id', res.result)
					let cartId = res.result;
					let productDetails ={};
					productDetails.sku =  product.sku;
					productDetails.quoteId=  cartId;
					productDetails.qty= product.numbers ? product.numbers : 1;
					productDetails.product_option= {extension_attributes : {bundle_options: [],
						configurable_item_options: [],
						custom_options: []}};
					AddToCart(productDetails , cartId)
					.then(res => {
					if(res.code === 200) { 
							product.quoteId = cartId;
							product.item_id = res.result.item_id;	
							dispatch({ type : SET_SPINNER_COMPLETED});			
							dispatch({
								type: ADD_PRODUCT_BASKET,
								payload: product
							}); 
						}
					}).catch(err => {
						dispatch({ type : SET_SPINNER_COMPLETED});
						if( err.response && err.response.data.result)
						dispatch({ type : SET_ADD_BASKET_ERROR_, error : err.response.data});
					});
				}
				}).catch(err => {
					dispatch({ type : SET_SPINNER_COMPLETED});
				if(err.response.status === 401 && err.response.statusText === "Unauthorized"){
					dispatch({ type : CHECK_UNAUTHORIZED, payload: true});
				  }
			});
		}
	}
}