import React, { Component, useState } from 'react';
import {Nav, Tab, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { HTTP } from '../../Utils';

class ProductListMenu extends Component {

	constructor(props){
		super(props);
		this.state={
			currentCategory: 0
			 
		}
	}


	componentWillMount(){
	}

   render(){

   	let {categories} = this.props;
   	

     return (
	    <React.Fragment>
          
          <section className="product_list">

		    <div className="container-fluid  pl-5 pr-5">  
		      <div className="product_list_menu">
		         <ul>  
		          <Nav variant="pills" className="flex-row">
			            <Nav.Item className={''} >
			                <Link style={{ color : Number.isInteger(parseInt(window.location.pathname.replace( /[^\d.]/g, '' ))) == true ? '':'black'}} to={`/Shop`}>All products</Link>
				        </Nav.Item>

				        {categories.map((category)=>{
		       		    return <Nav.Item className={''}>
							<Link style={{ color : category.id == parseInt(window.location.pathname.replace( /[^\d.]/g, '' ))? 'black':''}} to={`/Shop/${category.id}`}>{category.name}</Link>
						</Nav.Item>;
       		         })}					

 
			        </Nav>
		         </ul>
		      </div>

		    </div>      

      </section>

	   </React.Fragment>
  );

   }
	

     
}

export default ProductListMenu;	