import { REMOVE_PRODUCT_BASKET, SET_SPINNER_STARTED, SET_SPINNER_COMPLETED } from './types';
import { createCart, DeleteToCart} from '../account/Repository';
export const removeProduct =(product)=> {
  return(dispatch) => {
		dispatch({ type : SET_SPINNER_STARTED});
		// createCart(product)
		// 	.then(res => {
		// 	if(res.code === 200) { 
				let cartId =  sessionStorage.getItem('cart-id');
				let productDetails = {};
				productDetails.sku =  product.sku;
				productDetails.quoteId =  cartId;
				productDetails.item_id = product.item_id;
				DeleteToCart(productDetails , cartId)
				.then(res => {
				if(res.code === 200) { 
						console.log("Adding to Basket");
						console.log("Product:", product);	
						dispatch({ type : SET_SPINNER_COMPLETED});			
						dispatch({
							type: REMOVE_PRODUCT_BASKET,
							payload: product
						}); 
					}
				}).catch(err => {
					dispatch({ type : SET_SPINNER_COMPLETED});
					if( err.response && err.response.data.result)
					alert(err.response.data.result)
				});
			// }
			// }).catch(err => {
			// 	dispatch({ type : SET_SPINNER_COMPLETED});
			// console.log(err)
			// });
		
	}
}