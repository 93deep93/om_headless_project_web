import React, { Component } from 'react';
import countriesList from '../../../data/countries_reagions.json';
import Addressinfo from '../assets/address_info';
import {Link} from 'react-router-dom';
class UserInfo extends Component {

    constructor(props) {
      super(props);
      this.state = {
        default_billing_address : '',
        default_shipping_address : '',
        countriesList : countriesList,
        recenOrders : [],
        isLoaded :true,
      };
    }

    UNSAFE_componentWillReceiveProps( newProps){
        if( this.props.propsData &&this.props.propsData.addresses){
          let default_billing_address = this.props.propsData.addresses.filter(x=> x.default_billing== true);
          if(default_billing_address.length>0)
          default_billing_address[0].country_name = this.state.countriesList.filter(x=> x.id == default_billing_address[0].country_id)[0].full_name_english;
          let default_shipping_address = this.props.propsData.addresses.filter(x=> x.default_shipping== true);
          if(default_shipping_address.length>0)
          default_shipping_address[0].country_name = this.state.countriesList.filter(x=> x.id == default_shipping_address[0].country_id)[0].full_name_english;
          this.setState({ default_shipping_address : default_shipping_address.length>0 ? default_shipping_address[0]:'', 
              default_billing_address : default_billing_address.length > 0? default_billing_address[0] : '', isLoaded : false })
        }

        if( newProps.userMyOrders !== this.props.userMyOrders ){
          if(  newProps.userMyOrders && newProps.userMyOrders.items.length>0){
            this.setState({recenOrders : newProps.userMyOrders.items.slice(0, 5)});
         }
        }
    }
    
    render() {
        const {default_billing_address, default_shipping_address } = this.state;
        let {isLoaded, items} = this.state;

    return (
      <>
      {isLoaded ? 
        <div className="text-center mt-5 mb-5">loading...</div>
        :
                <div>
                    <div className="block block-dashboard-info">
                      <div className="block-title"><strong>Account Information</strong></div>
                      <div className="block-content">
                        <div className="row">
                          <div className="col-md-6">
                            <div className="box box-information">
                              <strong className="box-title">
                                <span>Contact Information</span>
                              </strong>
                              <div className="box-content">
                                <p>
                                  {this.props.propsData.firstname +' ' +this.props.propsData.lastname}
                                </p>
                                <p>
                                  {this.props.propsData.email}
                              </p>
                              </div>
                              <div className="box-actions">
                                
                                <Link className="action edit" to={{pathname: 'accountinfo-edit', state : { userInfo:this.props.propsData, changeEmailCheck : false, changePasswordCheck : false} }} >
                                  <span> Edit</span>
                                </Link>
                                <Link className="action change-password" to={{pathname: 'accountinfo-edit', state : { userInfo:this.props.propsData, changeEmailCheck : false, changePasswordCheck : true} }} >
                                  <span> Change Password  </span>
                                </Link>
                              </div>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div style={{ display:'none'}} className="box box-newsletter">
                              <strong className="box-title">
                                <span>Newsletters</span>
                              </strong>
                              <div className="box-content">
                                <p>
                                  You aren't subscribed to our newsletter.
                              </p>
                              </div>
                              <div className="box-actions">
                                <a className="action edit" href="#"><span>Edit</span></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="block block-dashboard-addresses">
                      <div className="block-title">
                        <strong>Address Book</strong>
                        
                      </div>
                      <div className="block-content">
                        <div className="row">
                          <div className="col-md-6">
                            <div className="box box-billing-address">
                              <strong className="box-title">
                                <span>Default Billing Address</span>
                              </strong>
                              <div className="box-content">
                              {default_billing_address === ''? ' Not find address' :
                                
                                < Addressinfo billing_address={default_billing_address}/>
                                }   
                              </div>
                              <div className="box-actions">
                                <Link  to={{pathname: 'address-edit', state : { address_details : default_billing_address } }} >
                                  Edit Address
                                </Link>
                              </div>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="box box-shipping-address">
                              <strong className="box-title">
                                <span>Default Shipping Address</span>
                              </strong>
                              <div className="box-content">
                              {default_shipping_address === ''? ' Not find address' :
                               < Addressinfo billing_address={default_shipping_address}/>
                                }   
                                </div>
                              <div className="box-actions">
                                <Link  to={{pathname: 'address-edit', state : { address_details : default_shipping_address } }} >
                                  Edit Address
                                </Link>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    {this.state.recenOrders.length > 0 ?
                    <div className="block block-dashboard-orders">
                    <div className="block-title order"> <strong>Recent Orders</strong>
                      <a className="action viewall view" href="#"> <span>View All</span> </a>
                    </div>
                   <div className="block-content">
                      <div className="table-wrapper orders-recent">
                        <table className="data table table-order-items recent" id="my-orders-table">
                          <thead>
                            <tr>
                              <th scope="col" className="col id">Order #</th>
                              <th scope="col" className="col date">Date</th>
                              <th scope="col" className="col shipping">Ship To</th>
                              <th scope="col" className="col total">Order Total</th>
                              <th scope="col" className="col status">Status</th>
                              <th scope="col" className="col actions">Action</th>
                              <th>&nbsp;</th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.state.recenOrders.map(( order , index)=>{
                              return(
                                <tr>
                                  <td data-th="Order #" className="col id">{order.increment_id}</td>
                                  <td data-th="Date" className="col date">{order.created_at}</td>
                                  <td data-th="Ship To" className="col shipping">{ order.customer_firstname +' '+ order.customer_lastname}</td>
                                  <td data-th="Order Total" className="col total"><span className="price">${order.total_due}</span></td>
                                  <td data-th="Status" className="col status">{order.status}</td>
                                  <td data-th="Actions" className="col actions">
                                  <Link className=" view" to={{ pathname: '/Orderdetails', state: { orderDetails : order } }}> <span>View Order</span>  </Link> {/* className="action view" */}
                                    {/* <a onClick={()=> this.props.reOrderProducts( order.items )} className="action order"> <span>Reorder</span> </a> */}
                                  </td>
                                  <td></td>
                                </tr>
                              )
                            })}
              
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>:''}
                  </div>  }
                  </>
                  )
    }
}
export default UserInfo