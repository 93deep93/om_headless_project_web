import React, {Component} from 'react';
import './Login.scss';
import { signup } from './Repository';
import {Modal} from 'react-bootstrap';


class CreateAccount extends Component {

   constructor() {
        super();
        this.state = { email: '', firstname: '', lastname: '', isPopup: false,  password: '', message: ''};
      }

      handleClose = () => {
         this.setState({isPopup: false})
      }


      handleInputChange = (event) => this.setState({[event.target.name]: event.target.value});

      submitSignup = (event) => {
        event.preventDefault();
        signup(this.state)
          .then(token => {
            // window.location = '/';
           if(token.code === 200) {           
            this.setState({isPopup:true, message: "You are successfully Create Account !", error: ''});
           }
          })
            .catch(err => {
            this.setState({error:err});
            console.log(err)
          });
      }

   render() {

     let {isPopup, error, message} = this.state;

      return (
    <React.Fragment> 
    <div className="login-container container mt-5 mb-5">
      <div className="row">

        <div className="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-xs-12 p-0 mx-auto my-auto">

            <div className="customer-login">
                     
                  <form className="signup" onSubmit={this.submitSignup}>

                      <div className ="fist-step">

                     <h2 className="heading-title">PERSONAL INFORMATION</h2>
                    
                        <div className="form_group">
                       <label for="fname">First Name</label>
                      <input type="text" name="first name" placeholder="" onChange={this.handleInputChange} />  
                </div>

                        <div className="form_group">
                            <label for="lname">Last Name</label>
                            <input type="text" name="last name" placeholder="" onChange={this.handleInputChange} />  
                        </div>

                        <div class="checkbox">
                            <span><input type="checkbox" onChange={this.handleInputChange}/> Sign Up For Newsletter </span>
                        </div>

                        </div>

                        <div className = "second-step">

                         <h2 className="heading-title">SIGN-IN INFORMATION</h2>
                    
                        <div className="form_group">
                             <label for="fname">Email</label>
                            <input type="email" name="email" placeholder="" onChange={this.handleInputChange} />  
                        </div>

                        <div className="form_group">
                            <label for="lname">Password</label>
                            <input type="password" name="password" placeholder="" onChange={this.handleInputChange} />  
                        </div>

                        <div className="form_group">
                            <label for="lname">Confirm Password</label>
                            <input type="password" name="confirm password" placeholder="" onChange={this.handleInputChange} />
                        </div>

                        <div class="checkbox">
                            <span><input type="checkbox" /> I AGREE TO THE T&CS </span>
                        </div>
                       </div>


                      <div className="account-btn text-center">
                   <input type="submit" value="Create An Account" />
                      </div>
                  </form>
            </div>  
          </div> 
      </div> 
    </div>

            <Modal className="message_popup" show={isPopup} onHide={this.handleClose} animation={false}>
                  <Modal.Header closeButton> </Modal.Header>
                    <Modal.Body className= "text-center mt-5 mb-5">{message}</Modal.Body>
                </Modal>
 
    </React.Fragment>
  ); 
   }
  
}

export default CreateAccount;