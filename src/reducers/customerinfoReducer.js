import { SET_USER_INFO_FULL_DETAILS, SET_USER_INFO_MY_ORDERS } from '../actions/types';

const initialState = {
    isUserInfo :  '',
    userMyOrders : { total_count : '', items : []}
}

const customerInfoReducer = ( state= initialState, action)=>{
    switch( action.type){
        case SET_USER_INFO_FULL_DETAILS: // -- user authentication --
		state.isUserInfo = action.payload;
		localStorage.setItem('user-info-allies', JSON.stringify(state));
		return { ...state}

        case SET_USER_INFO_MY_ORDERS: // -- user authentication --
		state.userMyOrders = action.payload;
		// localStorage.setItem('user-info-allies', JSON.stringify(state));
        return { ...state}
        
        default : 
        return state;
    }
}

export default customerInfoReducer;