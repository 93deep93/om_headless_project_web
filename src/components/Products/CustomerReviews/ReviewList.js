import React from 'react';

const ReviewsList = () => {

    return (
        <React.Fragment>
            <div className="card gedf-card">
                <div className="card-header">
                    <div className="d-flex justify-content-between align-items-center">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="mr-2">
                                <div className="stamped-review-avatar-content">JT</div>
                            </div>

                            <div className="ml-2">
                                <div className="h5 m-0">JACQUELINE T <span>Verified Buyer</span></div>
                                <div className="h7 text-muted">Singapore</div>
                            </div>
                        </div>
                        <div>
                            <div className="dropdown">
                                <span> 26/06/2020 </span>

                            </div>
                        </div>
                    </div>
                </div>
                <div className="card-body">
                    <a className="card-link" href="#">
                        <h5 className="card-title">AVERAGE CLEANSER</h5>
                    </a>

                    <p className="card-text">
                        My first time using this cleanser was great! I noticed a difference to my skin right away
                        One thing to note is that is doesn't lather into a foam
                    </p>
                </div>
                <div className="card-footer">
                    <a href="#" className="card-link"><i className="fa fa-mail-forward"></i> Share</a>
                    <span className="float-right social">
                        <span className="stamped-rating-holder">Was this review helpful?</span>
                        <span className="thumb"><i className="fa fa-thumbs-up"></i> 1 </span>
                        <span><i className="fa fa-thumbs-down"></i> 0 </span>
                    </span>
                </div>
            </div>

            <div className="card gedf-card">
                <div className="card-header">
                    <div className="d-flex justify-content-between align-items-center">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="mr-2">
                                <div className="stamped-review-avatar-content">EC</div>
                            </div>
                            <div className="ml-2">
                                <div className="h5 m-0">EMILY C <span>Verified Buyer</span></div>
                                <div className="h7 text-muted">United States</div>
                            </div>
                        </div>
                        <div>
                            <div className="dropdown">
                                <span> 26/06/2020 </span>
                            </div>
                        </div>
                    </div>

                </div>
                <div className="card-body">

                    <a className="card-link" href="#">
                        <h5 className="card-title">FAVORITE NEW CLEANSER!</h5>
                    </a>

                    <p className="card-text">
                        My first time using this cleanser was great! I noticed a difference to my skin right away
                    </p>
                </div>
                <div className="card-footer">
                    <a href="#" className="card-link"><i className="fa fa-mail-forward"></i> Share</a>
                    <span className="float-right social">
                        <span className="stamped-rating-holder">Was this review helpful?</span>
                        <span className="thumb"><i className="fa fa-thumbs-up"></i> 1 </span>
                        <span><i className="fa fa-thumbs-down"></i> 0 </span>
                    </span>
                </div>
            </div>
        </React.Fragment>
    )
}

export default ReviewsList
