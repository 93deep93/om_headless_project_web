import React, { useEffect , useState} from 'react';
const KeyProducts = (props) =>{
    const [attributes_key_data, set_attributes_key_data] = useState([]);
    const [activelink, setActiveLink] = useState('');
    const [activelinkData, setActiveLinkData] = useState('');

    

    const keyArr = ["silk_amino_hyaluronic_acid","silk_amino_silk_amino_acids","silk_amino_organic_safflower","silk_amino_organic_moringa","silk_amino_ethylated_acid","silk_amino_white_tea","silk_amino_lecithin","silk_amino_lactic_acid","promise_dioic_mandelic","promise_potassium_azeloyl","promise_oraganic_rosehip_oil","promise_colloidal_silver","promise_pomegranate_enzymes","promise_niacinamide","promise_medical_grade","promise_hyaluronic_acid","promise_phospholipids","promise_palmitoyl_tripeptide","bright_lactic_glycolic","bright_hyaluronic","bright_prickly_pear","bright_diglucosyl","bright_ally_rtm","bright_yoghurt","bright_silk_amino","bright_medical_grade","rhassoul_kaolin_clay","colloidal_silver_medical_grade","attr_1_bha_salicylic_acid","diglucosyl_gallic_acid","astaxanthin","water_activated_vitamin_c","hyaluronic_acid","teprenone","lactobacillus_ferment","fermented_black_tea_and_persia","plantago_lanceolata_leaf_extra","organic_argan_oil","cacao","hemp_seed_oil","safflower_oil","fresh_clay_masque_hyaluronic","hydrolyzed_verbascum_thapsus_f","palmitoyl_tripeptide_5_repair","palmitoyl_tripeptide_8","caffeine","raspberry_seed_oil_acai_oil","moringa_extract","marrubium_stem_cells","day_cream_niacinamide","day_cream_mediacl_grade","marrubium_vulgare_extract","niacinamide_repair_mask","medical_grade_manuka_honey_umf","tetrahexyldecyl_ascorbate","glutathione_astaxanthin_thiota","low_molecular_weight_high_mole","cassia_angustifolia_seed_polys","fresh_hyaluronic_masque","silk_amino_acids","prickly_pear_enzyme_niacinamid","glycolic_lactic_salicylic_acid","yoghurt","phospholipids","pomegranate_enzymes","organic_rosehip_oil","potassium_azeloyl_diglycinate","colloidal_silver","low_molecular_weight_lmw_hyalu","ally_rtm","cxa_6_panacea_complextm","overnight_lower_molecular","overnight_cassia_angustifolia","ellagic_acid","indian_ginseng","pterostilbene","prodiziatm","nutrient_rich_oil_blend_organi","tamanu_oil","superoxide_dismutase_green_tea","jojoba_oil","dioic_mandelic_lactic_pyruvic","antioxidant_blend","mandelic_lactic_salicylic_acid","hyaluronic_acid_niacinamide","organic_rosehip_tamanu_oil","superoxide_dismutase","rumex_occidentalis_extract","bakuchiol","palmitoyl_tripeptide_5","nonapeptide_1","acetyl_tetrapeptide_40","resveratrol_ferment","superoxide_dismutase_two","niacinamide","palmitoyl_tripeptide_38","fragaria_vesca_strawberry_frui","liquorice_root_extract","vitamin_e_astaxanthin","epigallocatechin_gallate","ethylated_l_ascorbic_acid","ten_tetrahexyldecyl_ascorbate","superoxide_dismutase_vit","glutathione","ergothioneine","healing_oil_blend","acai_berry","charged_rose_hydrosol","fresh_cellstm_mango_extract","molecular_marrubium_stem_cells","molecular_moringa_extract","molecular_fermented_black_tea","molecular_lactobacillius","molecular_plantago_lanceolata","molecular_hyaluronic_acid","molecular_niacinamide","molecular_medical_grade_manuka","molecular_tamanu_oil","molecular_superoxide_dismutase","molecular_green_tea","molecular_caffeine","molecular_licorice_root","molecular_curcumin","molecular_phospholipids","molecular_ceramides","pollution_repair_teprenone","pollution_repair_marrubium","pollution_repair_palmitoyl","pollution_repair_palmitoyl_tri","pollution_repair_tetrahexy","pollution_repair_glutathione","pollution_repair_astaxanthin","pollution_repair_thiotaine","pollution_repair_spin","pollution_phospholipids","pollution_repair_ceramides","pollution_repair_lactobacilliu","polltuion_repair_hydrolyzed","pollution_repair_hyaluronic","pollution_repair_caffeine","pollution_repair_raspberry","pollution_repair_moringa","pollution_repair_niacinamide","pollution_repair_medical"]
    
    useEffect(() => {
        var ids = new Set( keyArr.map(d => d));
        var attributes_key_data =  props.attributes_key_data.filter( d => ids.has(d.attribute_code))
        if(attributes_key_data.length>0){
            set_attributes_key_data(attributes_key_data);
            setActiveLink( attributes_key_data[0].attribute_code);
            setActiveLinkData( props.product[ attributes_key_data[0].attribute_code ]);
        }  
      }, [])

      const setActiveLinkHandle =( attribute_code)=>{
            setActiveLink(attribute_code);
            setActiveLinkData(props.product[attribute_code]);
      }
    
    return(
        <div className="learn-more-tab-content">
            <div className="key-allies-title-anchor-container">
                { attributes_key_data.map( ( item )=>{
                    return <div className={"key-allies "+( activelink === item.attribute_code ? "active":'')}  onClick ={ ()=>{ setActiveLinkHandle( item.attribute_code) }}><a> {item.default_frontend_label } \ </a></div>  
                })}
            </div>
            
            <span className="purpose-label">PURPOSE</span>
            <div className="tab-text"><div dangerouslySetInnerHTML={{__html: activelinkData}}></div></div>
           
        </div>
    )
} 

export default KeyProducts