import React, { useState, Fragment, Component } from 'react';
import { Form, Button, Tooltip, Accordion, Card } from 'react-bootstrap';
import '../../styles/Checkout.scss';
import { Redirect, Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import ShippingForm from '../Checkout/ShippingForm/shippingForm';
import OrderSummary from '../Checkout/orderSummary';
import CheckoutProcess from '../Checkout/CheckoutProcess';


import countriesList from '../../data/countries_reagions.json';
import equal from 'fast-deep-equal';
const Payment = (props) => {

    return (
        <React.Fragment>
            <CheckoutProcess />
            <section>
                <div className="container-fluid checkout_page pl-5 pr-5 mt-5 mb-5">
                    <div className="row">
                        <div className="col-xl-8 col-lg-8 col-md-7 col-sm-12 col-12">
                            <section className="payment-option">
                                <div className="step-title">Payment Method</div>
                                <div className="payment-group">
                                    <div className="payment-method-title field choice">
                                        <input type="radio" name="payment[method]" className="radio" id="paypal_express" value="paypal_express" />
                                        <label className="label" htmlFor="paypal_express">
                                            <img className="payment-icon" src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/pp-acceptance-medium.png" alt="Acceptance Mark" />
                                            <span>PayPal Express Checkout</span>
                                            <a className="action action-help" >What is PayPal?</a>
                                        </label>
                                        <div className="choiceName">
                                            <input className="checkbox valid" type="checkbox" id="amgdpr_agree" name="amgdpr_agree" value="1" />
                                            <label htmlFor="amgdpr_agree">
                                                <span className="action">I have read and accept the <a href="#">privacy policy</a></span>
                                            </label>

                                        </div>
                                    </div>
                                    <div className="payment-method _active">
                                        <div className="payment-method-title field choice">
                                            <input type="radio" name="payment[method]" className="radio" id="cashondelivery" value="cashondelivery" />
                                            <label className="label" htmlFor="cashondelivery"><span>Preorder - Choose this option if your order includes Preordered Items, and our customer representative will get in touch with you regarding payment</span></label>
                                        </div>
                                        <div className="payment-method-billing-address">

                                            <div className="checkout-billing-address">
                                                <div className="billing-address-same-as-shipping-block field choice">
                                                    <input type="checkbox" name="billing-address-same-as-shipping" id="billing-address-same-as-shipping-cashondelivery" />
                                                    <label htmlFor="billing-address-same-as-shipping-cashondelivery"><span>My billing and shipping address are the same</span></label>
                                                </div>
                                                <div className="billing-address-details">
                                                    deepak yadav <br />
                                                    Meerut New Jersey 11122 <br />
                                                    United States<br />
                                                    <a href="tel:5187463347">5187463347</a><br />

                                                    <button type="button" className="action action-edit-address">
                                                        <span>Edit</span>
                                                    </button>
                                                </div>
                                                <fieldset className="fieldset" >

                                                    <div className="field field-select-billing">
                                                        <label className="label"><span>Billing Address</span></label>
                                                        <div className="control">
                                                            <select className="select" name="billing_address_id">
                                                                <option value="">deepak yadav, adfad, Meerut, New Jersey 11122, United States</option>
                                                                <option value="">New Address</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div className="billing-addresschnage">

                                                        <div className="billing-address-form">

                                                            <ShippingForm />

                                                        </div>
                                                        <div className="choiceName">
                                                            <input className="checkbox valid" type="checkbox" id="amgdpr_agree" name="amgdpr_agree" value="1" />
                                                            <label htmlFor="amgdpr_agree">
                                                                <span className="action">I have read and accept the <a href="#">privacy policy</a></span>
                                                            </label>

                                                        </div>

                                                    </div>

                                                    <div className="actions-toolbar">
                                                        <div className="primary">
                                                            <button className="action action-update" type="button">
                                                                <span>Update</span>
                                                            </button>
                                                            <button className="action action-cancel" type="button">
                                                                <span>Cancel</span>
                                                            </button>
                                                        </div>
                                                    </div>

                                                </fieldset>
                                            </div>

                                        </div>
                                        <div className="actions-toolbar placebutton">
                                            <div className="primary">
                                                <button className="action primary checkout" type="submit" title="Place Order">
                                                    <span>Place Order</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="applycoupon order_summary">
                                        <Accordion defaultActiveKey="1">
                                            <Card>
                                                <Accordion.Toggle as={Card.Header} eventKey="1">
                                                    <div className="itemin_cart"><span>Apply Discount Code</span></div>
                                                </Accordion.Toggle>
                                                <Accordion.Collapse eventKey="1">
                                                    <Card.Body>
                                                        <div className="payment-option-content">
                                                            <form className="form form-discount" id="discount-form">
                                                                <div className="payment-option-inner">
                                                                    <div className="field">
                                                                        <label className="label" for="discount-code">
                                                                            <span>Enter discount code</span>
                                                                        </label>
                                                                        <div className="control">
                                                                            <input className="input-text" type="text" id="discount-code" name="discount_code" placeholder="Enter discount code" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="actions-toolbar">
                                                                    <div className="primary">
                                                                        <button className="action action-apply" type="submit" value="Apply Discount">
                                                                            <span><span>Apply Discount</span></span>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </Card.Body>
                                                </Accordion.Collapse>
                                            </Card>
                                        </Accordion>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div className="col-xl-4 col-lg-4 col-md-5 col-sm-12 col-12">
                            <div className="opc-block-summary">
                                <span className="title">Order Summary</span>

                                <table className="data table table-totals">
                                    <tbody>

                                        <tr className="totals sub">
                                            <th className="marks" scope="row">Cart Subtotal</th>
                                            <td className="amount">
                                                <span className="price">$92.00</span>
                                            </td>
                                        </tr>

                                        <tr className="totals shipping excl">
                                            <th className="marks" scope="row">
                                                <span className="label">Shipping</span>

                                                <span className="value">Shipping - USPS First className Mail</span>
                                            </th>
                                            <td className="amount">

                                                <span className="price">$0.00</span>

                                            </td>
                                        </tr>

                                        <tr className="grand totals">
                                            <th className="marks" scope="row">
                                                <strong data-bind="i18n: title">Order Total</strong>
                                            </th>
                                            <td className="amount">
                                                <strong><span className="price">$92.00</span></strong>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>

                            </div>
                            <div className="incartitem">

                            </div>
                            <div className="shipping-information">
                                <div className="ship-to">
                                    <div className="shipping-information-title">
                                        <span>Ship To:</span>
                                        <button className="action action-edit">
                                            <span>edit</span>
                                        </button>
                                    </div>
                                    <div className="shipping-information-content">
                                        deepak<br />
                                        adfad<br />
                                        Meerut, New Jersey 11122<br />
                                        United States<br />
                                        5187463347
                                    </div>
                                </div>
                                <div className="ship-via">
                                    <div className="shipping-information-title">
                                        <span>Shipping Method:</span>
                                        <button className="action action-edit">
                                            <span>edit</span>
                                        </button>
                                    </div>
                                    <div className="shipping-information-content">
                                        <span className="value">Shipping - USPS First className Mail</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </React.Fragment >
    );
};

export default Payment;