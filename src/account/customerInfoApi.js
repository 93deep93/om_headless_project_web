import axios from 'axios';

const BASE_URL = 'http://139.180.155.160';


export function getUserInfoApi() {
        return axios.get(`${BASE_URL}/api/user/me?token=${localStorage.getItem('x-access-token')}`)
                .then(response => response.data);
}

export function getUserMyOrdersApi() {
    return axios.get(`${BASE_URL}/api/user/order-history?token=${localStorage.getItem('x-access-token')}`)
            .then(response => response.data);
}

export function postUserInfoApi(data) {    
        return axios.post(`${BASE_URL}/api/user/me?token=${localStorage.getItem('x-access-token')}`, 
                  {customer:data})
                  .then(response => response.data)
                  .catch(err => Promise.reject(err));
      }

export function updateUserPasswordApi(data){
        return axios.post(`${BASE_URL}/api/user/change-password?token=${localStorage.getItem('x-access-token')}`, data)
                .then(response => response.data)
                .catch(err => Promise.reject(err));
}