
import React from 'react';
const CheckoutProcess = (props) => {
    return props.view === 'payment'?
     (
        <section className="checkout-process">
            <ul className="opc-progress-bar payment-sec">
                    <li style={{ cursor : 'pointer'}} onClick={ ()=>props.gotoCheckoutPage()} className="opc-progress-bar-item complete">
                        <span>Shipping</span>
                    </li>
                    <li className="opc-progress-bar-item _active">
                        <span>Review &amp; Payments</span>
                    </li>
            </ul>
        </section>
        
    ): props.view === 'checkout'? (<section className="checkout-process">
    <ul className="opc-progress-bar">
        <li className="opc-progress-bar-item _active">
            <span>Shipping</span>
        </li>
        <li className="opc-progress-bar-item">
            <span>Review &amp; Payments</span>
        </li>
    </ul></section>):''
}
export default CheckoutProcess