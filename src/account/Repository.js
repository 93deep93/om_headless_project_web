import axios from 'axios';

const BASE_URL = 'http://139.180.155.160';


export function getProducts() {
        return axios.get(`${BASE_URL}/api/catalog/vue_storefront_catalog/product/_search`)
                .then(response => response.data);
}

export function getCartProducts(cart) {
        return axios.post(`${BASE_URL}/api/products`, {cart})
                .then(response => response.data);
}

export function login(data) {
  return axios.post(`${BASE_URL}/api/user/login`, 
            {username:data.email,password:data.password})

    .then(response => {
       localStorage.setItem('x-access-token', response.data.result);
       localStorage.setItem('x-access-token-expiration', 
                            Date.now() + 2 * 60 * 60 * 1000);
      return response.data})
    .catch(err => Promise.reject('Authentication Failed!'));
}

export function forgetPassword(data) {
  return axios.post(`${BASE_URL}/api/user/reset-password`, 
            {email:data.email})

    .then(response => {
       localStorage.setItem('x-access-token', response.data.result);
       localStorage.setItem('x-access-token-expiration', 
                            Date.now() + 2 * 60 * 60 * 1000);
      return response.data})
    .catch(err => Promise.reject('Authentication Failed!'));
}


export function signup(data) {
  return axios.post(`${BASE_URL}/api/user/create`, 
      { customer: {email:data.email,firstname:data.firstname,lastname:data.lastname},
        password:data.password
      })


    .then(response => {
       localStorage.setItem('x-access-token', response.data.token);
       localStorage.setItem('x-access-token-expiration', 
                            Date.now() + 2 * 60 * 60 * 1000);
      return response.data})
    .catch(err => Promise.reject('Authentication Failed!'));
}

export function setUserProfileApi () {
  return axios.get(`${BASE_URL}/api/user/me?token=${localStorage.getItem('x-access-token')}`)
    .then(response => 
      {localStorage.setItem('user-info-allies', JSON.stringify(response.data.result));
      return  response.data
      })
    .catch(err => Promise.reject(err));
}

export function pay (data) {
        console.log('pay data', data);
        return axios.get(`${BASE_URL}/api/pay`, 
            { params: { 'x-access-token': localStorage.getItem('x-access-token')} })
                .then(response => response.data)
                .catch(err => Promise.reject(err));
}
export function isAuthenticated(){
        return localStorage.getItem('x-access-token') !== null && localStorage.getItem('x-access-token-expiration') > Date.now()
}

export function getShipmentMethods(data) {    
  return axios.post(`${BASE_URL}/api/cart/shipping-methods?token=${localStorage.getItem('x-access-token')}&cartId=${data.cart_id}`, 
            {address :{ country_id : data.country_code}})
            .then(response => response.data)
            .catch(err => {Promise.reject(err)});
}

export function createCart(product) {
  return axios.post(`${BASE_URL}/api/cart/create?token=${localStorage.getItem('x-access-token')? localStorage.getItem('x-access-token') : '' }`)
          .then(response => {
            // sessionStorage.setItem('cart-id', response.data.result);
            return response.data
          })
          .catch(err => Promise.reject(err));
}

export function AddToCart(productDetails, cart_id) {
  return axios.post(`${BASE_URL}/api/cart/update?token=${localStorage.getItem('x-access-token') ? localStorage.getItem('x-access-token') : '' }&cartId=${cart_id}`,
            { cartItem: productDetails})
            .then(response => response.data)
            .catch(err => Promise.reject(err));
}


export function getCartAddedProducts(cart_id) {
  return axios.get(`${BASE_URL}/api/cart/pull?token=${localStorage.getItem('x-access-token')}&cartId=${cart_id}`)
      .then(response => response.data);
}

export function DeleteToCart(productDetails, cart_id) {
  return axios.post(`${BASE_URL}/api/cart/delete?token=${localStorage.getItem('x-access-token') ? localStorage.getItem('x-access-token') : '' }&cartId=${cart_id}`,
  { cartItem: productDetails})
  .then(response => response.data)
  .catch(err => 
    {
      if(err.response.status === 401 && err.response.statusText === "Unauthorized"){
        sessionStorage.setItem('Unauthorized', true);
      }
    Promise.reject(err)
    });
}

export function placeOrderApi(productDetails) {
  return axios.post(`${BASE_URL}/api/order`,
    {  
      addressInformation : productDetails.addressInformation,
      products: productDetails.products,
      transmited : true,
      user_id : productDetails.user_id,
      cart_id : productDetails.cart_id
    })
    .then(response => response.data)
    .catch(err =>
    {
      if(err.response.status === 401 && err.response.statusText === "Unauthorized"){
        sessionStorage.setItem('Unauthorized', true);
      }
    Promise.reject(err)
    });
}


export function CheckStockProductApi(sku) {
  return axios.get(`${BASE_URL}/api/stock/check?sku=${sku}`)
      .then(response => response);
}