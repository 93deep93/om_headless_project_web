import React from 'react';
import '../styles/MobileBanner.scss';
import { Link } from 'react-router-dom';

const MobileBanner = () => {
  return (
    <React.Fragment>
     <div class="mobile-banner-new">
         <img class="mobile-banner" src={require('../images/Home_background_mobile_new1612.jpg')} alt="Allies of skin banner Mobile"/>
        <div class="mobile-banner-content">
        <p>Try our new pH balanced cleansing gel</p>
        <Link href="/shop"><span>SHOP NOW</span></Link></div>
        </div>
    </React.Fragment>
  );
}

export default MobileBanner;