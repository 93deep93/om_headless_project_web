import React, { useState } from 'react';
import styled from 'styled-components'
import { Button, Modal} from 'react-bootstrap';
import '../styles/WidgetPopup.scss';
// import $ from 'jquery';
// import * as emailjs from 'emailjs-com'
// import { Mailer } from 'nodemailer-react'
 


const ContactUsPopup = (props) => {

	const [mail_to, setMailTo] = useState('93deep93@gmail.com');
	const [subject, setSubject] = useState('');
	const [message, setMessage] = useState('');

	const [mail_to_error, setMailToError] = useState('');
	const [subject_error, setSubjectError] = useState('');
	const [message_error, setMessageError] = useState('');

	  const [show, setShow] = useState(false);
	  const handleClose = () => setShow(false);
	  const handleShow = () => setShow(true);

	function validateEmail( email){
        const regex_email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex_email.test(String(email).toLowerCase());
    }

	  function validate(){
		let flag = false;
		if( (mail_to=== "" || mail_to=== null || mail_to === undefined) ){
            setMailToError( 'This field is required');
            flag = true;
        }else if(( mail_to !== "" || mail_to !== null || mail_to !== undefined)){
            if(!validateEmail( mail_to)){
                setMailToError('Please enter valid email id !');
                flag = true;
            }
        }
		if( subject === "" || subject=== null || subject === undefined){
            setSubjectError('This field is required');
            flag = true;
		}
		if( message === "" || message=== null || message === undefined){
            setMessageError('This field is required');
            flag = true;
		}
		return flag
	  }

	  function handleSubmit(){
		if(!validate()){

			// let mailerConfig = {
			// 	transport: 
			// 	defaults: Optional NodeMailer Message Options
			// }

			// const mailer = Mailer(mailerConfig, emailsList)

			// mailer.send(templateName, props, nodeMailerMessage)

			// let templateParams = {
			// 	from_name: 'yadav.deepak@orangemantra.in',
			// 	to_name: mail_to,
			// 	subject: subject,
			// 	message_html: message,
			//    }
			//    emailjs.send(
			// 	'gmail',
			// 	'c21bb4ba5604ac2f11c326b514050b5b',
			// 	'template_9t6zi2z',
			// 	 templateParams,
			// 	'user_mGhgObN9bLRAqeU9VfDbY'
			//    )

		// 	var data = {
		// 		// service_id: 'default_service',
		// 		template_id: 'template_9t6zi2z',
		// 		user_id: 'user_mGhgObN9bLRAqeU9VfDbY',
		// 		template_params: {
		// 			'username': 'James',
		// 			'g-recaptcha-response': '03AHJ_ASjnLA214KSNKFJAK12sfKASfehbmfd...'
		// 		}
		// 	};
			 
		// 	$.ajax('https://api.emailjs.com/api/v1.0/email/send', {
		// 		type: 'POST',
		// 		data: JSON.stringify(data),
		// 		contentType: 'application/json'
		// 	}).done(function() {
		// 		alert('Your mail is sent!');
		// 	}).fail(function(error) {
		// 		alert('Oops... ' + JSON.stringify(error));
		// 	});
		}
	  }


		return (
			<>
			<div className = "modal_contact_popup">
                {/* <Button className="widget-popup" onClick={handleShow} >Contact Us</Button> */}

                <Modal className="contact-popup" show={show} onHide={handleClose} animation={false}>
			        <Modal.Header closeButton>
			          <Modal.Title>Contact Us</Modal.Title>
			        </Modal.Header>
			        <Modal.Body>
                        <div className="row mt-3">

				            <div className="col-12">
				              <div className="input-group mb-3">
								  <div className="input-group-prepend">
								    <span className="input-group-text" id="basic-addon1"><i className="fa fa-envelope" aria-hidden="true"></i></span>
								  </div>
								  <input type="email" name ='mail_to' onChange={(e)=>{ setMailTo(e.target.value); setMailToError('');}} value={mail_to} className="form-control" placeholder="Requester" aria-label="Requester"/>
								</div>
								{mail_to_error ? <div  className = "error">{mail_to_error}</div> : ''}
				            </div>


				            <div className="col-12 mt-3">
				              <div className="input-group mb-3">
								  <div className="input-group-prepend">
								    <span className="input-group-text" id="basic-addon1"><i className="fa fa-question-circle" aria-hidden="true"></i></span>
								  </div>
								  <input type="text" name ='subject' onChange={(e)=>{ setSubject(e.target.value); setSubjectError('');}} value={subject}  className="form-control" placeholder="Subject" aria-label="Subject"/>
								</div>
								{subject_error ? <div  className = "error">{subject_error}</div> : ''}
				            </div>

				            <div className="col-12 mt-3">
				              <textarea name ='message' value={message} onChange={(e)=>{ setMessage(e.target.value); setMessageError('');}}  className="contact-input-form" rows="4" cols="50">
							</textarea>
							{message_error ? <div  className = "error">{message_error}</div> : ''}
				            </div>
				          </div>
			        </Modal.Body>
			        <Modal.Footer>
			          <Button variant="primary" onClick={handleSubmit}>
			            Email US
			          </Button>
			        </Modal.Footer>
			      </Modal>
			</div>

			
		</>	
		);
}

export default ContactUsPopup; 