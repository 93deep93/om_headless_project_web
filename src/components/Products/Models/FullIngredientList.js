import React from 'react';
const FullIngredientList = () =>{
    return(
        <div className="tab-text">
            <p className="poppara">Aqua (Water), Polyacrylate-1 Crosspolymer, Sodium C14-16 Olefin Sulfonate, Glycerin, Cocamidopropyl Hydroxysultaine, Sodium Cocoamphoacetate, Carthamus Tinctorius (Safflower) Seed Oil (Organic), Propanediol, Helianthus Annuus (Sunflower) Seed Oil (Organic), Leuconostoc/Radish Root Ferment Filtrate, Lactic Acid (L), Hydrolyzed Jojoba Esters, Moringa Oleifera Seed Oil (Organic), 3-O-Ethyl Ascorbic Acid, Cocodimonium Hydroxypropyl Silk Amino Acids, Hydrolyzed Silk, Camellia Sinensis (White Tea) Leaf Extract, Camellia Oleifera (Green Tea) Leaf Extract, Sodium Hyaluronate (L), Lecithin, Astaxanthin, Xanthan Gum, Citric Acid, Lauryl Glucoside, Sodium Cocoyl Glutamate, Sodium Lauryl Glucose Carboxylate, Caprylyl Glycol, O-cymen-5-OL, Polysorbate 20, Tetrasodium EDTA, Phenethyl Alcohol, Potassium Sorbate</p> 
            <p className="poppara">This product is Vegan. </p>
            <p className="poppara">Formulated without Silicones, Sulfates, Drying Alcohol, Retinyl Palmitate, Synthetic Dyes, Synthetic Fragrances & Essential Oils. </p>
            <p className="poppara">We do not test on animals. 100% cruelty-free.</p>
            
        </div>
    )
} 

export default FullIngredientList
