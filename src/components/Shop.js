import React, {Component} from 'react';
import '../styles/Shop.scss';
import '../styles/Home.scss';
import ProductListMenu from './Products/ProductListMenu';
import { Tab } from 'react-bootstrap';
import ProductList from './Products/ProductList';
import { connect } from 'react-redux';
import {  addBasket } from '../actions/addAction';
import { HTTP } from '../Utils';
import { Link } from 'react-router-dom';


class Shop extends Component{

  constructor(props){
    super(props);
    this.state = {
      categories: []
    };
  }

  componentDidMount(){
    HTTP().get('/api/catalog/vue_storefront_catalog/category/_search')
    .then(res => {
        const cats = res.data.hits.hits.reduce((result, cat, index)=>{
          result.push(cat._source);
          return result;
        },[]);

        const pageCategory = cats.find((cat)=>cat.id ==this.props.match.params.id);
            this.setState({ 
                pageCategory:pageCategory,
             })
        

       

        HTTP().get('/api/catalog/vue_storefront_catalog/product/_search')
        .then(res => {
        var items = res.data.hits.hits.reduce((result, set, index)=>{
          for(let s in set){
            let options = set[s].attributes_metadata || [];
            let productSizeOption = options.length ? options.find((attr)=>attr.default_frontend_label === "Product Size") : null;
            productSizeOption = productSizeOption ? productSizeOption.options[0] || [] : [];
            set._source.productSizeOptions = productSizeOption;
          }

          result.push(set._source);
          return result;
        },[]);
            // const {categories} = this.state;
            items = items.filter(x=> x.extension_attributes.website_ids[0] === 1);
            this.setState({ 
                    items:items,
                    categories: cats
             })
             localStorage.setItem('allProductsAlliesProducts',  JSON.stringify(items));
        });


  });
}

componentWillReceiveProps( props){
  
  if(props.basketProps.unAuthorizeUser){
    localStorage.removeItem('x-access-token');
    localStorage.removeItem('x-access-token-expiration');
    localStorage.removeItem('user-info-allies');
    sessionStorage.removeItem('cart-id');
    this.props.history.push({
    pathname :'/Login'
    })
  }
}

gotoReviewPage=( product_id ,error)=>{
  this.props.history.push( {pathname: `/productDetails/${product_id}` , state:{ error : error}});
}

render(){
  let  {categories, items, pageCategory} = this.state;
  let page = Object.assign({}, pageCategory);
  const  catId =  this.props.match.params.id;
  items = items || [];
  let categoriedItem = [];
  if(catId){
     categoriedItem = items.filter((item, index)=>{
      for(let cat of item.category_ids){if(cat==catId)return item;}
    });
  }else{
     categoriedItem = items;
  }

  return (
    <React.Fragment>
	      <div className ="page_banner">
	         <img className="image" src={require('../images/Shop_Header.jpg')}/>
         </div>
         <div>
   
          <Tab.Container id="left-tabs-example" defaultActiveKey="first">
            <ProductListMenu categories={categories} />
           <div className="container-fluid pr-5 pl-5">
            <Tab.Content>
              <Tab.Pane eventKey="first">
                < ProductList postData={categoriedItem} gotoReviewPage={this.gotoReviewPage}  />
              </Tab.Pane>
            </Tab.Content>
            </div>
         
         </Tab.Container>
          
        </div>
    </React.Fragment>
  );
  }
}


const mapStateToProps = state => ({
	basketProps : state.basketState,
});


export default connect(mapStateToProps, { addBasket })(Shop);