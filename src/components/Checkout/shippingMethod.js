import React, {Fragment, Component} from 'react';
import {Form, Button, Tooltip, Accordion, Card } from 'react-bootstrap';
import '../../styles/Checkout.scss';
import { Redirect, Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import countriesList from '../../data/countries_reagions.json';
import equal from 'fast-deep-equal';

const ShippingMethod =( props)=> {
        return (
            <React.Fragment>
                <div className="shipping_method">
                    <h3 className="step_title">Shipping Method</h3>                   
                    { props.shipping_method_data.length>0 ? 

                        <table className="table-checkout-shipping-method">
                            <tbody>
                            { props.shipping_method_data.map( ( methodlist, index) => {
                                return (
                                    
                                    <tr>     
                                        <td className='col-method'> < input
                                                    type="radio"
                                                    value="paymethodlist.method_codepal" 
                                                    checked={props.selected_shipping_method === methodlist.method_code ? true : false} 
                                                    onChange={props.handleShippingMethodOnChange.bind(this,methodlist.method_code)} />
                                        </td>
                                        <td className='col-price'> {'$'+ methodlist.amount} </td>
                                        <td className='col-method'>  { methodlist.method_title} </td>
                                        <td className='col-carrier'>{ methodlist.carrier_title} </td>          
                                    </tr>
                                )
                                })
                            }
                            
                            </tbody>
                        </table>
                        
                        : ''}
        
                    {props.selected_shipping_method_error.length !== ''? <div className='error'>{props.selected_shipping_method_error}</div>  : ''}
          
                </div>

            </React.Fragment >
        );


}


export default ShippingMethod;