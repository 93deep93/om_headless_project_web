import React, {Fragment} from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import {  increaseQty } from '../actions/increaseQty';
import {  decreaseQty } from '../actions/decreaseQty';
import { removeProduct } from '../actions/removeProduct';
import '../styles/Cart.scss';


const Cart = (props) => {
  let { cartCost, products } = props.basketProps;
	let productsInCart = products.productList; 
	
	if(!productsInCart.length){
		return <Fragment>
		<span>YOU HAVE NO ITEMS IN YOUR SHOPPING CART!</span>
		</Fragment>
	}
   productsInCart = productsInCart.map( (productList, index) => {
   	 console.log ("my product is");
   	 console.log (productList);


   	return (
   		  <Fragment>
			<div className ="row cart_details">
                <div className="col-xl-6 col-lg-6 col-md-4 col-sm-12 col-12">

                 <h2> {productList.name} </h2>

                </div>
                <div className="col-xl-6 col-lg-6 col-md-4 col-sm-12 col-12">

                   <div className="row cart_detail">
                     <div className="col-3 cart">
                        <span className="cart_title_name">Size</span>
                        <span className="cart_title_value"> {productList.productSizeOptions ? productList.productSizeOptions.label : ''}</span>
                     </div>
                     <div className="col-3 cart">
                     <span className="cart_title_name">Price</span>
                        <span className="cart_title_value"> {'$'+productList.price.toFixed(2) }</span>
                     </div>
                     <div className="col-3 cart">
                     <span className="cart_title_name">QTY</span>
                       <div className ="incrment_decrment">
                        <span onClick= {() => props.decreaseQty(productList)}><i class="fa fa-minus" aria-hidden="true"></i></span>
                        <span className="cart_title_value"> {productList.numbers ? productList.numbers : productList.qty? productList.qty :0} </span>
                        <span onClick= {() => props.increaseQty(productList)}><i className="fa fa-plus" aria-hidden="true"></i></span>
                       </div>
                     </div>
                     <div className="col-3 cart cart-use">
                    
                        <span onClick={()=>props.removeProduct(productList)}> <i className="fa fa-trash" aria-hidden="true"></i></span>
                        
                     </div>           
                   </div>
                </div>                 
                </div>
			

   		  </Fragment>
   		)
   })


 return (
 	  <>
       <div className="container-fluid">

         <div className="products">
           {productsInCart}
         </div>
         <div className="cart subtotal">
         <span className="cart_title_name">Cart Subtotal</span>
         <span className="cart_title_value">{cartTotal(cartCost)} </span>    
           <div className="checkout-form">
           <a style={{ display:'none'}} href="/ViewandEdit" className="view-edit">view and Edit Card</a>
           <span onClick={props.handleCloseBag} className="checkout-btn"><Link onClick={()=>{ sessionStorage.removeItem('checkout_shipping_data');}} to = "/Checkout">Proceed to checkout </Link></span>
           </div>
         </div>
       </div>
 	  </>
	);

}

const mapStateToProps = state => ({
	basketProps : state.basketState
});

function cartTotal(amount){
	return '$'+amount.toFixed(2);
}

export default connect(mapStateToProps, {increaseQty, decreaseQty, removeProduct })(Cart);