import { DECREASE_QUANTITY, SET_SPINNER_STARTED, SET_SPINNER_COMPLETED } from './types';
import { createCart, AddToCart} from '../account/Repository';

export const decreaseQty = (product) => {
	return dispatch => {
		dispatch({ type : SET_SPINNER_STARTED});
		if(product.numbers>1 || product.qty>1){
		// 	createCart(product)
		// .then(res => {
		// if(res.code === 200) { 
			let productDetails ={};
			productDetails.sku =  product.sku;
			productDetails.quoteId= sessionStorage.getItem('cart-id');
			productDetails.qty= (product.qty ||product.numbers)-1;
			if(product.item_id)
			productDetails.item_id = product.item_id;
			AddToCart(productDetails , sessionStorage.getItem('cart-id'))
			.then(res => {
			if(res.code === 200) { 
				
					console.log("decressing the Basket");
					console.log("Product:", product);					
					dispatch({
						type: DECREASE_QUANTITY,
						payload: product
					}); 
					dispatch({ type : SET_SPINNER_COMPLETED});	
				}
			}).catch(err => {
				dispatch({ type : SET_SPINNER_COMPLETED});	
				if( err.response && err.response.data.result)
				alert(err.response.data.result)
			});
		// }
		// }).catch(err => {
		// 	dispatch({ type : SET_SPINNER_COMPLETED});	
		// console.log(err)
		// });
	
		}else{
			dispatch({ type : SET_SPINNER_COMPLETED});	
			return false
		}
	}
}