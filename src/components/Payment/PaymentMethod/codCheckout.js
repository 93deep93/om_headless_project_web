import React, { useState, Fragment, Component, useEffect , useRef} from 'react';
import ShippingForm from '../../Checkout/ShippingForm/shippingForm';
import countriesList from '../../../data/countries_reagions.json';
const CODcheckout = (props) => {
    const childRef = useRef();
    // const [ shippingPropsData, SetShippingPropsData] = useState(props.shippingData);
    const [ shipmentAddress, updateShipmentAddress] = useState(props.shipmentAddress);
    const [ billingAddress, updateBillingAddress] = useState(props.shipmentAddress);
    const [ address_list_data, updateAddressListData] = useState(props.user_info_address);
    const [ check_same_address, setCheckSameAddress ] = useState(false);
    const [ show_edit_address, setShowEditAddress ] = useState(false);
    const [ show_address_block, setShowAddressBlock ] = useState(true);
    const [ selected_address, setSelectedAddress ]= useState('new_address');
    const [ show_selected_address, setShowSelectedAddress ]= useState(true);
    const [ checkPrivacyPolicy, setCheckPrivacyPolicy ]= useState( false);
    const [ placeOrderBtnDisable, setPlaceOrderBtnDisable ]= useState(true);
    const [ privacy_policy_error, setPrivacyPolicyError] = useState('Accept privacy policy');

    useEffect(()=>{
        if( !localStorage.getItem('x-access-token')){
            setShowSelectedAddress(false)

        }
        if( props.shipmentAddress.default_billing == true){
            setCheckSameAddress(true);
        }else{
            setShowEditAddress(true)
        }
        if(props.shipmentAddress.id){
            setSelectedAddress(props.shipmentAddress.id)
        }
    },[]);

    function handle_check_same_address (){
        if(check_same_address && show_edit_address){
            setCheckSameAddress(!check_same_address);
            setShowEditAddress(!show_edit_address);
            setShowAddressBlock(!show_address_block);
        }else if(!check_same_address && show_edit_address){
            setCheckSameAddress(true)
            setShowEditAddress(false)
            updateBillingAddress(shipmentAddress)
        }else if(check_same_address && !show_edit_address){
            setShowEditAddress(true);
            updateBillingAddress(shipmentAddress)
        }else{
            setCheckSameAddress(true)
            setShowAddressBlock(!show_address_block);
        }
    }

    function handle_showEditAdd (){
        setShowAddressBlock(!show_address_block);
    }

    function handle_onChange_SelectAddress( e){
        setCheckSameAddress(false);
        setSelectedAddress(e.target.value)
    }

    function handle_Update_billing_Address(){
        if(selected_address ==='new_address'){
            if(!childRef.current.validate()){
                let data = childRef.current.getMyState();
                data.isNewAddress = true;
                updateBillingAddress( data)
                setShowAddressBlock(!show_address_block);
                setShowEditAddress(true);
                setPlaceOrderBtnDisable(false)
              }
        }else{
            let address = address_list_data.filter( item=> item.id== selected_address);
            if(address.length>0){
                address[0].region = address[0].region.region ? address[0].region.region : address[0].region;
                address[0].address = address[0].street.join(',');
                updateBillingAddress( address[0]);
            }
            setShowAddressBlock(!show_address_block);
            setShowEditAddress(true);
            setPlaceOrderBtnDisable(false)
        }
    }

    function handle_place_order(){
        if(checkPrivacyPolicy){
            props.handle_place_order( shipmentAddress, billingAddress)
        }
    }

    function _handlePrivacyCheck(){
        setCheckPrivacyPolicy(!checkPrivacyPolicy)
        if(checkPrivacyPolicy){
            setPlaceOrderBtnDisable(true)
            setPrivacyPolicyError('Accept privacy policy')
        }else{
            setPlaceOrderBtnDisable(false)
            setPrivacyPolicyError('')
        }   
    }

    return (
        <React.Fragment> 
            <div className="payment-method _active">
                <div className="payment-method-title field choice">
                    <input type="radio" onClick={()=> props.setPayment_method('cashondelivery')} className="radio" checked={props.paymet_method === 'cashondelivery'? true:false} />
                    <label className="label" htmlFor="cashondelivery"><span>Preorder - Choose this option if your order includes Preordered Items, and our customer representative will get in touch with you regarding payment</span></label>
                </div>
                {props.paymet_method === 'cashondelivery'? <>
                    <div className="payment-method-billing-address">

                        <div className="checkout-billing-address">
                            <div className="billing-address-same-as-shipping-block field choice">
                                <input type="checkbox" checked={check_same_address} onClick={ handle_check_same_address} />
                                <label htmlFor="billing-address-same-as-shipping-cashondelivery"><span>My billing and shipping address are the same</span></label>
                            </div>
                            {show_address_block ?
                                <div className="billing-address-details">
                                    {billingAddress ? 
                                        <div className="shipping-information-content">
                                        <span>{ billingAddress.firstname +''+ billingAddress.lastname}</span><br/>
                                        <span>{ billingAddress.address}</span><br/>
                                        <span>{ billingAddress.city}</span><br/>
                                        {/* <span>{ billingAddress.region}</span><br/> */}
                                        <span>{countriesList.length>0 ? countriesList.map( country => { 
                                                                if(country.id === billingAddress.country_id)  
                                                                return country.full_name_english; }):'' }</span><br/>
                                        <span><a >{ billingAddress.telephone}</a></span><br/>
                                    </div>
                                    :''}

                                    {show_edit_address ? 
                                        <button type="button" onClick={()=>{ handle_showEditAdd()}} className="action action-edit-address">
                                            <span>Edit</span>
                                        </button>:''}
                                </div>
                                :
                                <fieldset className="fieldset" >

                                    <div className="field field-select-billing">
                                        {/* <label className="label"><span>Billing Address</span></label> */}
                                        <div className="control"  style={ {display : show_selected_address ? 'block':'none' }}>
                                            <select className="select" value={ selected_address} onChange={ (e)=>{handle_onChange_SelectAddress(e)}}  name="select_address">
                                                { address_list_data.map( ( item, index) => {
                                              return <option key={item.id} value = {item.id}>
                                                      {item.firstname ? item.firstname :'' +' '+ item.lastname ? item.lastname : ''},
                                                      {item.street? item.street.join(', '):''},
                                                      {item.city? item.city : ''}, {item.region? item.region.region : ''} {item.postcode? item.postcode : ''},
                                                      {countriesList.length>0 ? countriesList.map( country => { 
                                                          if(country.id === item.country_id)  
                                                          return country.full_name_english; }):'' },
                                                      {item.telephone ? item.telephone : ''},
                                                  </option>
                                            })}
                                            <option value="new_address">New Address</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div className="billing-addresschnage">

                                        <div className="billing-address-form">
                                            {selected_address === 'new_address' ?
                                                <ShippingForm shippingEmail={ props.shipmentAddress.email } ref={childRef} address_exists={ billingAddress.isNewAddress ? billingAddress : false} setShippingMethod ={(e)=>{ } }/>
                                                :''}
                                            
                                            <div className="actions-toolbar">
                                                <div className="primary">
                                                    <button onClick={ ()=>{ handle_Update_billing_Address()}} className="action action-update" type="button">
                                                        <span>Update</span>
                                                    </button>
                                                    <button onClick={()=>{ setShowEditAddress(true); setShowAddressBlock(!show_address_block) }} className="action action-cancel" type="button">
                                                        <span>Cancel</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </fieldset>
                            }
                        </div>

                    </div>

                    <fieldset className="fieldset" >
                            <div className="choiceName">
                                <input className="checkbox valid" checked={checkPrivacyPolicy} type="checkbox"  onClick={ ()=>{ _handlePrivacyCheck()}} />
                                <label htmlFor="amgdpr_agree">
                                    <span className="action">I have read and accept the <a href="#">privacy policy</a></span>
                                </label>
                            </div>
                            <div className='error' style={{ color: "#e02b2"}}>{privacy_policy_error}</div>
                    </fieldset>


                    <div className="actions-toolbar placebutton">
                    <div className="primary">
                        <button className="action primary checkout" onClick={()=>{ handle_place_order()}} disabled={placeOrderBtnDisable} title="Place Order">
                            <span>Place Order</span>
                        </button>
                    </div>
                    <div className='error' style={{ color: "#e02b2" }}>{props.order_error}</div>       
                </div>
                </>:''}
            </div>
                        
        </React.Fragment>
    )
}

export default CODcheckout