export const ADD_PRODUCT_BASKET = 'ADD_PRODUCT_BASKET';
export const GET_NUMBERS_BASKET ='GET_NUMBERS_BASKET';
export const INCREASE_QUANTITY ='INCREASE_QUANTITY';
export const DECREASE_QUANTITY ='DECREASE_QUANTITY';
export const REMOVE_PRODUCT_BASKET ='REMOVE_PRODUCT_BASKET';
export const CHANGE_LOGIN_STATUS ='CHANGE_LOGIN_STATUS';
export const SET_USER_CART_DATA ='SET_USER_CART_DATA';
export const SET_SPINNER_STARTED = 'SET_SPINNER_STARTED';
export const SET_SPINNER_COMPLETED = 'SET_SPINNER_COMPLETED';
export const SET_USER_INFO_FULL_DETAILS = 'SET_USER_INFO_FULL_DETAILS';
export const SET_USER_INFO_MY_ORDERS = 'SET_USER_INFO_MY_ORDERS';
export const SET_ADD_BASKET_ERROR_ = "SET_ADD_BASKET_ERROR_";
export const CHECK_UNAUTHORIZED = "CHECK_UNAUTHORIZED";