import {ADD_PRODUCT_BASKET, GET_NUMBERS_BASKET, INCREASE_QUANTITY, DECREASE_QUANTITY, REMOVE_PRODUCT_BASKET, CHANGE_LOGIN_STATUS, SET_USER_CART_DATA, SET_ADD_BASKET_ERROR_, CHECK_UNAUTHORIZED} 
from '../actions/types';

const initialState = 
// JSON.parse(localStorage.getItem('basketData')) || 
{
	basketNumbers: 0,
	cartCost: 0,
	addBasketError: {},
	products: {
		productList : [],
	},
	isLogin: false,
	unAuthorizeUser : false,
}	

export default (state = initialState , action) => {
	let product, cartData;
	switch(action.type){
		case CHANGE_LOGIN_STATUS: // -- user authentication --
		state.unAuthorizeUser = false;
		state.isLogin = action.payload;
		console.log('reducer state', state);
		localStorage.setItem('basketData', JSON.stringify(state));

		return { ...state}
		case ADD_PRODUCT_BASKET:

		product = action.payload;
		product.price = product.price || 0;
		let productId = product.id;
		let checker = state.products.productList
		.reduce((result, prod, index)=> prod.id == productId ? {prod, index} : result, {});

		if(checker.constructor == Object && Object.keys(checker).length){
			checker.prod.numbers += product.numbers || 1;
			checker.prod.qty += product.qty || 1;
			state.products.productList.splice(checker.index, 1, checker.prod);
		}else{
			// not exist
			product.qty = product.qty || 1;
			product.numbers = product.numbers || 1;
			state.products.productList.push(product);
			state.basketNumbers += 1;
		}
		state.cartCost = resetTotal(state.products.productList);
		state.basketNumbers = resetBasketNumbers(state.products.productList);
		state.addBasketError = {};
		localStorage.setItem('basketData', JSON.stringify(state));
		return {...state}

		case INCREASE_QUANTITY:
		product = action.payload;
		console.log('increament', product);
		state.products.productList.map((pd, index)=>{
			if(product === pd){
				pd.numbers += 1;
				pd.qty += 1;
			}
		})
		state.cartCost = resetTotal(state.products.productList);
		state.addBasketError = {};
		state.basketNumbers = resetBasketNumbers(state.products.productList);
		localStorage.setItem('basketData', JSON.stringify(state));
		return {...state}

		case DECREASE_QUANTITY:
		product = action.payload;
		state.products.productList.map((pd, index)=>{
			if(product === pd){
				pd.numbers -= 1;
				pd.numbers = pd.numbers < 0 ? 0 : pd.numbers;
				pd.qty -= 1;
				pd.qty = pd.qty < 0 ? 0 : pd.qty;
			}
		})

		state.cartCost = resetTotal(state.products.productList);
		state.addBasketError = {};
		state.basketNumbers = resetBasketNumbers(state.products.productList);
		localStorage.setItem('basketData', JSON.stringify(state));
		return {...state}

		case REMOVE_PRODUCT_BASKET:
		product = action.payload;
		state.products.productList.map((pd, index)=>{
			if(product === pd){
				state.products.productList.splice(index,1);
				state.basketNumbers -= 1;
			}
		})
		state.cartCost = resetTotal(state.products.productList);
		state.addBasketError = {};
		state.basketNumbers = resetBasketNumbers(state.products.productList);
		localStorage.setItem('basketData', JSON.stringify(state));
		return{	...state}

		case SET_USER_CART_DATA:
			cartData = action.payload;
			state.products.productList = cartData;
			state.cartCost = resetTotal(cartData);
			state.basketNumbers = resetBasketNumbers(cartData);
			localStorage.setItem('basketData', JSON.stringify(state));
		return{...state}

		case GET_NUMBERS_BASKET:
		return {...state}
		
		case CHECK_UNAUTHORIZED : 
			localStorage.removeItem('x-access-token');
			localStorage.removeItem('x-access-token-expiration');
			localStorage.removeItem('user-info-allies');
			sessionStorage.removeItem('cart-id');
			state.unAuthorizeUser = action.payload;
			state.isLogin = false
			return{...state}

		case SET_ADD_BASKET_ERROR_ :
			state.addBasketError = { code : action.error.code, error : action.error.result}
			return {...state}

		default:
		localStorage.setItem('basketData', JSON.stringify(state));
		return state;
	}
}

function resetTotal(products){
	let total = 0;
	products.forEach((product, index)=>{
		total += Number(product.price || 0) * Number(product.numbers || product.qty);
	});
	return total;
}

function resetBasketNumbers(products){
	let total = 0;
	products.forEach((product, index)=>{
		total +=  Number(product.numbers || product.qty);
	});
	return total;
}