import React ,{Component}from 'react';
import Banner from './components/Banner';
import Header from './components/Header';
import Home from './components/Home';
import About from './components/About';
import Contact from './components/Contact';
import Shop from './components/Shop';
import Checkout from './components/Checkout';
import CheckoutNew from './components/Checkout/CheckoutNew';
import PaymentSucess from './components/Checkout/PaymentSucess';
import Press from './components/Press';
import Retailers from './components/Retailers';
import Regimen from './components/Regimen';
import ViewandEdit from './components/ViewandEdit';
import ContactUsPopup from './components/ContactUsPopupScript';
import RewardPoint from './components/RewardPoint';
import Orderinfo from './guest/Orderinfo';
import Login from './account/Login';
import Signup from './account/Signup'; 
import Create from './account/Create';
import Payment from './components/paypal/Payment';
import ProductDetails from './components/Products/ProductDetails';
import Forgetpassword from './account/Forgetpassword';
import Privacypolicy from './components/Privacypolicy';
import Faq from './components/Faq';
import TermOfService from './components/TermOfService';
import Footer from './components/Footer';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/font-awesome/css/font-awesome.min.css'; 
import './App.scss';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import { connect } from 'react-redux'; 
import { changeLoginStatus } from './actions/changeLoginStatus';
import {getNumbers, checkOldCartAndUpdate } from './actions/getAction';
import { HTTP } from './Utils';
import store from './store';
import AccountInfo from './components/CustomerInfo/AccountDashboard/accountView';
import MyOrders from './components/CustomerInfo/Myorders/myOrdersView';
import AddressBook from './components/CustomerInfo/AddressBook/addressBook';
import OrderDetails from './components/CustomerInfo/Myorders/orderDetail';
import AlertDialogBox from './components/AlertDialogBox/alertDialogBox';
import AccountInfoEdit from './components/CustomerInfo/AccountDashboard/accountEdit';
import Settings from './components/CustomerInfo/Settings/settings';
import Subscription from './components/CustomerInfo/Newsletter/subscription';
import EditAddress from './components/CustomerInfo/AddressBook/editAddress';
import SwellRewardsPoint from './components/SwellRewards/SwellRewards';
import PaymentNew from './components/Payment/payment';
import PaymentView from './components/Payment'

class App extends Component {

  
  componentWillMount(){
    if( !localStorage.getItem('allProductsAlliesProducts')){
      HTTP().get('/api/catalog/vue_storefront_catalog/product/_search')
      .then(res => {
      var items = res.data.hits.hits.reduce((result, set, index)=>{
        for(let s in set){
          let options = set[s].attributes_metadata || [];
          let productSizeOption = options.length ? options.find((attr)=>attr.default_frontend_label === "Product Size") : null;
          productSizeOption = productSizeOption ? productSizeOption.options[0] || [] : [];
          set._source.productSizeOptions = productSizeOption;
        }

        result.push(set._source);
        return result;
      },[]);
      items = items.filter(x=> x.extension_attributes.website_ids[0] === 1);
          // const {categories} = this.state;
          localStorage.setItem('allProductsAlliesProducts',  JSON.stringify(items));
      });
    }
  
    if(this.props.basketProps.isLogin){
      //check cart is already 
      this.props.dispatch(getNumbers());
    }else if( localStorage.getItem('x-access-token')){
      this.props.dispatch(changeLoginStatus(true));
      this.props.dispatch(getNumbers());
      //check cart is already 
      // this.props.dispatch( checkOldCartAndUpdate( this.props.backketProps));
    }
    if(sessionStorage.getItem('cart-id')){
      this.props.dispatch(getNumbers());
    }
  }


  render() {

    let showDialogBox = "";
    if (this.props.showDialog === true) {
      // alert(this.state.showMessage);
      showDialogBox = <div id="alert-dialog">
        {/* <div className="alert-box-container">
            <div className="alert-box" style={{borderRadius: "5px"}}>
                 <div style={{borderTopLeftRadius: "5px",borderTopRightRadius: "5px",backgroundColor: "blue",color: "white",height:"20%",textAlign: "center",display: "flex",alignItems: "center",justifyContent: "center"}}>
                     Alert !!!
                 </div>
                 <div style={{height:"60%",textAlign: "center",color: "black"}}>
                         {this.state.showMessage}
                     </div>
                     <div style={{borderTop: "1px solid gray",height:"20%",textAlign: "center",display: "flex",alignItems: "center",justifyContent: "center"}}>
                         <button className="list-btn" onClick={this.onDialogClose}>Ok</button>
                     </div>
                 </div>
            </div> */}
        <AlertDialogBox
          displayShow={true}
          alertType={'confirmalert'}
          displayMessage={'confirmalert'}
          alertData={'this.state.alertData'}
          alertPushRoute = {''}
        />

      </div>
     
    } else {
      showDialogBox = "";
    }
    
    let showSpinner ="";
    if(this.props.loading){
      showSpinner =  <div  className='spinner-div'>
        <div className='lds-roller'>
          <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
          
        </div>
        <p>Please Wait...</p>
      </div>
    }
    
  return (
    <BrowserRouter>
    <div>
    
     <Banner />
     { window.location.pathname.includes('/regimen-recommender') ===true ? 
     <Route path ="/regimen-recommender" component={Regimen} />
      :
      <>
      <Header />
      <Switch>
        <Route path ="/" exact component={Home} />
        <Route path ="/About" component={About} />
        <Route path ="/Contact" component={Contact} />
        <Route path ="/Press" component={Press} />
        <Route path ="/Retailers" component={Retailers} />
        
        <Route path ="/Orderinfo" component={Orderinfo} />
        
        <Route path ="/Signup" component={Signup} />
        <Route path ="/Create" component={Create} />
        <Route path ="/Forgetpassword" component={Forgetpassword} />
        <Route path ="/ViewandEdit" component={ViewandEdit} />
        <Route path ="/Privacypolicy" component={Privacypolicy} />
        <Route path ="/Faq" component={Faq} />
        <Route path ="/TermOfService" component={TermOfService} />
        <Route path ="/ProductDetails/:id" component={ProductDetails}/>
        <Route path ="/Shop/:id" component={Shop} />
        <Route path ="/Shop" component={Shop} />
        {/* <Route path ="/Checkout" component={Checkout} /> */}
        <Route path ="/Checkout" component={CheckoutNew} />
        <Route path ="/Payment" component={PaymentView} />
        <Route path ="/accountinfo" component={AccountInfo} />
        <Route path ="/accountinfo-edit" component={AccountInfoEdit} />
        <Route path ="/myorders" component={MyOrders} />
        <Route path ="/address" component={AddressBook} />
        <Route path ="/Orderdetails" component={OrderDetails} />
        <Route path ="/settings" component={Settings}/>
        <Route path ="/newslettermanage" component={Subscription} />
        <Route path ="/address-edit" component={EditAddress} />
        <Route path ="/address-new" component={EditAddress} />
        <Route path ="/swell-rewards" component={SwellRewardsPoint} />
        <Route path ="/payment-sucess" component={PaymentSucess} />
        <Route path ="/Login" component={Login} />

        {/* <Route path ="/payment-new" component={PaymentNew} />
        <Route path ="/payment-view" component={PaymentView} /> */}
        
      </Switch> 
      <Footer /> 
      <RewardPoint /> 
      <ContactUsPopup />
      {showDialogBox} 
      {showSpinner}
     
      {/* <script type="text/javascript" src="https://s3.amazonaws.com/assets.freshdesk.com/widget/freshwidget.js"></script>
      <script type="text/javascript">{`
      FreshWidget.init("", {"queryString": "&widgetType=popup&formTitle=Contact+Us&submitTitle=EMAIL+US&submitThanks=Thank+you.%0D%0AYour+message+has+been+sent.&captcha=yes&searchArea=no", "utf8": "✓", "widgetType": "popup", "buttonType": "text", "buttonText": "CONTACT US", "buttonColor": "white", "buttonBg": "#000000", "alignment": "2", "offset": "500px", "submitThanks": "Thank you.\r\nYour message has been sent.", "formHeight": "500px", "captcha": "yes", "url": "https://alliesofskin.freshdesk.com"} ); */}
      </>
      }
 
    </div>
    </BrowserRouter>
    );
  }
}

const mapStateToProps = state => ({
  basketProps:state.basketState,
  loading:state.spinnerState.loading,
  showAlert: state.alertsDialog.showAlert,
  alertType: state.alertsDialog.alertType,
  alertMessage: state.alertsDialog.alertMessage,
  alertData: state.alertsDialog.alertData,
  alertPushRoute : state.alertsDialog.alertPushRoute,
})
export default connect(mapStateToProps)(App);
