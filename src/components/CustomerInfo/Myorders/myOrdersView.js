import React, { Component } from 'react';
import PaginationView from '../assets/pagination';
import AccountDashMenu from '../myAccountDashboardMenu';
import '../../../styles/AccountInfo.scss';
import { getUserMyOrders} from '../../../actions/customerInfo';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';
import { isAuthenticated } from '../../../account/Repository';


const limit=10;

class MyOrders extends Component {

  constructor(props) {
    super(props);
    this.state = {
      recenOrders: [],
      currentPage: 1
    };
  }

  componentDidMount(){
    if(  this.props.userMyOrders && this.props.userMyOrders.items>0){
      this.setState({recenOrders : this.props.userMyOrders.items});
   }else{
    this.props.dispatch( getUserMyOrders());
   }
  }

  UNSAFE_componentWillReceiveProps(newProps){
    if( newProps.userMyOrders !== this.props.userMyOrders ){
      if(  newProps.userMyOrders && newProps.userMyOrders.items.length>0){
        this.setState({recenOrders : newProps.userMyOrders.items});
     }else{ this.setState({ recenOrders : [] })}
    }
  }

  handlePageChange = (page, e) => {
    this.setState({
      currentPage: page
    });
  };

  goToOrders = ( orderDetails) => {
    this.props.history.push({
      path:'/orderdetails',
      state :{ orderDetails : orderDetails }
    });
  }

  render() {
    if (!isAuthenticated()) return (<Redirect to="/login" />);
    const { currentPage } = this.state;
    let start = Number( (currentPage-1) *10);
    let end = Number(currentPage*limit);
    return (
      <React.Fragment>
        <section className="page_banner">
          <div className="rowsec-account" id="maincontent">
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-4">
                <AccountDashMenu/>
                </div>
                <div className="col-md-8">
                  <div className="block block-dashboard-orders myorders">                  
                    <div className="block-content">
                    {this.state.recenOrders.length > 0 ?
                      <div className="table-wrapper orders-recent">
                        <table className="data table table-order-items recent" id="my-orders-table">
                          
                          <thead>
                            <tr>
                              <th scope="col" className="col id">Order #</th>
                              <th scope="col" className="col date">Date</th>
                              <th scope="col" className="col shipping">Ship To</th>
                              <th scope="col" className="col total">Order Total</th>
                              <th scope="col" className="col status">Status</th>
                              <th scope="col" className="col actions">Action</th>
                              <th>&nbsp;</th>
                            </tr>
                          </thead>
                          <tbody>
                          {this.state.recenOrders.slice( start, end).map(( order , index)=>{
                              return(
                                <tr>
                                  <td data-th="Order #" className="col id">{order.increment_id}</td>
                                  <td data-th="Date" className="col date">{order.created_at}</td>
                                  <td data-th="Ship To" className="col shipping">{ order.customer_firstname +' '+ order.customer_lastname}</td>
                                  <td data-th="Order Total" className="col total"><span className="price">${order.total_due}</span></td>
                                  <td data-th="Status" className="col status">{order.status}</td>
                                  <td data-th="Actions" className="col actions">
                                  <Link  to={{ pathname: '/Orderdetails', state: { orderDetails : order } }}> <span>View Order</span>  </Link> {/* className="action view" */}
                                    {/* <a href="#" data-post="#" className="action order"> <span>Reorder</span> </a> */}
                                  </td>
                                  <td></td>
                                </tr>
                              )
                            })}
                          </tbody>
                        </table>
                        <div className="customer-addresses-toolbar toolbar bottom">
                            <div className="pager">
                                <p className="toolbar-amount">
                                    <span className="toolbar-number">
                                    Items  { ((this.state.currentPage-1)*limit)+1 } to { ((this.state.currentPage-1)*limit) + (this.state.recenOrders.slice( start, end).length)} of {this.state.recenOrders.length} total
                                    </span>
                                </p>

                                <div className="limiter">
                                <PaginationView 
                                  total={ this.state.recenOrders.length} 
                                  pageCount ={ Number(this.state.recenOrders.length/10)} 
                                  limit ={limit} 
                                  currentPage ={this.state.currentPage}
                                  handlePageChange={this.handlePageChange}
                                  />
                                </div>
                            </div>
                        </div>
                      </div>
                    : 'You have placed no orders.'}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );

  }

}

const mapStateToProps = state => ({
  userMyOrders  : state.userinfoState.userMyOrders,
})

export default connect(mapStateToProps) (MyOrders);