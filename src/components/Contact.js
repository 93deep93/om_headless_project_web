import React from 'react';
import { useState } from 'react';
import '../styles/About.scss';
import axios from 'axios';

const Contact = () => {
  const [ message_error, setMessageError ] = useState('');
  const [ email_error, setEmailError ] = useState('');
  const [ email, setEmail ] = useState('');
  const [ message, setMessage ] = useState('');
  const [ name, setName] = useState('');
  const [ name_error, setNameError ] = useState('');

  function validateEmail( email){
    const regex_email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex_email.test(String(email).toLowerCase());
}

function validate(){
let flag = false;
if( (email=== "" || email=== null || email === undefined) ){
        setEmailError( 'This field is required');
        flag = true;
    }else if(( email !== "" || email !== null || email !== undefined)){
        if(!validateEmail( email)){
            setEmailError('Please enter valid email id !');
            flag = true;
        }
    }
if( name === "" || name=== null || name === undefined){
        setNameError('This field is required');
        flag = true;
}
if( message === "" || message=== null || message === undefined){
        setMessageError('This field is required');
        flag = true;
}
return flag
}

function handleSubmit(){
  
  if(!validate()){
    // debugger
    // var myHeaders = new Headers();
    // myHeaders.append("Cookie", "PHPSESSID=9qv7avt56gkf3jpkgn94b26ju4");
    
    // var requestOptions = {
    //   method: 'POST',
    //   headers: myHeaders,
    //   redirect: 'follow'
    // };

    var config = {
      method: 'post',
      url: 'http://demo43.laraveldevloper.in/rest/V1/contactus?contactForm[name]=deepak%20yadav&contactForm[email]=yadav.deepak@orangemantra.in&contactForm[comment]=message',
      headers: {
        authorization: 'OAuth oauth_consumer_key=\\"l25jlvkyy8g5ni6sx6u4wmotjdepmwj7\\",oauth_token=\\"fct3iclvi3nztskdquh4auvwbxe7vzyf\\",oauth_signature_method=\\"HMAC-SHA256\\",oauth_timestamp=\\"1594293050\\",oauth_nonce=\\"syDq7t\\",oauth_version=\\"1.0\\",oauth_signature=\\"bV7FqjjNF%2Bcdnf2ispucXdMTiFHLmqoXvW%2F2l9vLCiY%3D\\"'
     }
    };
    
    axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
    })
    .catch(function (error) {
      console.log(error);
    });
    
    
  //   fetch("http://demo43.laraveldevloper.in/rest/V1/contactus?contactForm[name]="+name+
  //   "&contactForm[email]="+email+"&contactForm[comment]="+ message, requestOptions)
  //     .then(response =>{
  //       debugger;
  //       console.log(response);
  //     })
  //     .catch(error => {
  //       debugger
  //       console.log('error', error)});

  }
}

  return (
    <React.Fragment>
     <div className="contact-section">
       <h3 className="text-center">CONTACT US</h3>
       <div className="container-fluid about-container contact-container mt-5">
          <div className="row">

            <div className ="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
              <div className="about-left-section border-section">
                <div class="left_sec">
                 <span><img src={require('../images/contact1.jpg')}/></span>           
                </div>
              </div>
            </div>

            <div className ="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 pl-0">
               <div className="about-right-section content_section">
                   <h2 className="section_title">Get in Touch </h2>
                    <div className="section_content mt-5">
                        <h4 className="text-left font-weight-bold">Allies Group Pte Ltd</h4>
                       <p>71 Lor 23 Geylang, #04-16 <br/>  Singapore 388386 <br/>  Company Registration No. 201315916D <br/> ask@alliesofskin.com  <br /> +65 9646 9631</p>
                    </div>
               </div>
            </div>

          </div>

          <div className="row">
            <div className ="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
             <div className="about-left-section content_section border-section">
                 <h2 className="section_title">NEED SOME HELP? </h2>
                    <div className="section_content">
                       <h4 className="text-right font-weight-bold">THAT'S WHAT WE ARE HERE FOR.</h4>
                 <p>If you have questions about an order, our products, or navigating our website, send us an email or check out our Help & FAQs page - we might already have an answer for you!</p>
                     
                 {/* <div className="address-cont bottom-address">
                    <div className="address-block">General Enquiries
                      <p><a href="mailto:ask@alliesofskin.com">ask@alliesofskin.com</a></p>
                    </div>
                    <div className="address-block">Press Enquiries (Singapore)
                      <p><a href="mailto:press@alliesofskin.com">press@alliesofskin.com</a></p>
                    </div>
                    <div className="address-block">Press Enquiries (US)
                      <p><strong>Emily Parr</strong> <a href="mailto:emily@poke-pr.com">emily@poke-pr.com</a></p>
                    </div>
                    <div className="address-block">Press Enquiries (UK)
                      <p><strong>Olivia Wilson-young</strong> <a href="mailto:olivia@huntergracelondon.com">olivia@huntergracelondon.com</a></p>
                    </div>
                  </div> */}
                  </div>
             </div>
            </div>
            <div className ="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <div className="about-right-section">
                  <div className="right_sec">
                  <div className="address-cont bottom-address" style={{paddingTop :'25%'}}>
                    <div className="address-block">General Enquiries
                      <p><a href="mailto:ask@alliesofskin.com">ask@alliesofskin.com</a></p>
                    </div>
                    <div className="address-block">Press Enquiries (Singapore)
                      <p><a href="mailto:press@alliesofskin.com">press@alliesofskin.com</a></p>
                    </div>
                    <div className="address-block">Press Enquiries (US)
                      <p><strong>Emily Parr</strong> <a href="mailto:emily@poke-pr.com">emily@poke-pr.com</a></p>
                    </div>
                    <div className="address-block">Press Enquiries (UK)
                      <p><strong>Olivia Wilson-young</strong> <a href="mailto:olivia@huntergracelondon.com">olivia@huntergracelondon.com</a></p>
                    </div>
                  </div>
                  
                        {/* <div className="form-box">
                  <div className="form contact">
                
                  <fieldset className="fieldset">
                  
                    <div className="field name">
                      <label className="label required" for="name"><span>Name</span></label>
                      <div className="control">
                        <input name="name" title="Name" value={name} onChange={(e)=>{ setName(e.target.value); setNameError('')} } className="input-text" type="text" data-validate="{required:true}" aria-required="true"/>
                      </div>
                      {name_error ? <div  className = "error">{name_error}</div> : ''}
                    </div>
                    
                    <div className="field email">
                      <label className="label" for="email"><span>Email Address</span></label>
                      <div className="control">
                        <input name="email"  title="Email" value={email} onChange={(e)=>{ setEmail(e.target.value); setEmailError('')}} className="input-text" type="email" data-validate="{required:true, 'validate-email':true}" aria-required="true"/>
                      </div>
                      {email_error ? <div  className = "error">{email_error}</div> : ''}
                    </div>
                    
                    
                    <div className="field comment">
                      <label className="label" for="comment"><span>Message</span></label>
                      <div className="control">
                        <textarea name="message"value={message} onChange={(e)=>{ setMessage(e.target.value); setMessageError('')}} className="input-text" cols="5" rows="1" data-validate="{required:true}" aria-required="true"></textarea>
                      </div>
                      {message_error ? <div  className = "error">{message_error}</div> : ''}
                    </div>
                    
                                        
                  </fieldset>
                  
                  <div className="actions-toolbar">
                    <div className="primary">
                      <input type="hidden" name="hideit" id="hideit" value=""/>
                      <button onClick={()=>{ handleSubmit()}} className="action submit primary"><span>Send</span> </button>
                    </div>
                  </div>
                  
                  </div>
                </div>
                   */}
                  </div>
                </div>
            </div>
          </div>

            <div className="row">

            <div className ="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
              <div className="about-left-section border-section">
                <div class="left_sec">
                 <span><img src={require('../images/contact2.jpg')}/></span>           
                </div>
              </div>
            </div>

            <div className ="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 pl-0">
               <div className="about-right-section content_section">
                   <h2 className="section_title">Careers</h2>
                    <div className="section_content">
                       <p>We are always on the lookout for brilliant individuals with the brightest vibes. Tell us what you want to do at Allies of Skin and why you'd be a great fit and send your thoughts to
                          <a className="ml-2" href="mailto:careers@alliesofskin.com">careers@alliesofskin.com</a>
                        </p>
                    </div>
               </div>
            </div>

          </div>

       </div>
     </div>
    </React.Fragment>
  );
}

export default Contact;