import React from 'react';
import { Link } from 'react-router-dom';
import '../../../styles/CustomerReviews.scss';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import ReviewsForm from './ReviewForm';
import QuestionForm from './QuestionForm';
import ReviewsList from './ReviewList';
import QuestionList from './QuestionList';

const CustomerReviews = () => {

    return (
        <React.Fragment>

            <div className="container-fluid customer-review-section pl-5 pr-5">
                <div className="stamped-header-title"> Customer Reviews </div>
                <div className="row">
                    <div className="col-xl-6 col-lg-6 col-md-5 col-sm-12 co-12">
                        <div className="customer-review-left-section">
                            <div className="stamped-summary">
                                <span className="stamped-summary-num"> 48 </span>
                                <span className="stamped-summary-start">
                                    <i className="fa fa-star"></i>
                                    <i className="fa fa-star"></i>
                                    <i className="fa fa-star"></i>
                                    <i className="fa fa-star"></i>
                                    <i className="fa fa-star"></i>
                                </span>
                                <span className="stamped-summary-txt">Based on 263 Reviews</span>
                            </div>

                            <div className="stamped-summary-action">
                                <Link >Write a review</Link>
                                <Link >Ask a Question</Link>
                            </div>

                        </div>
                    </div>
                    <div className="col-xl-6 col-lg-6 col-md-7 col-sm-12 co-12">
                        <div className="customer-review-right-section">

                            <Tabs>
                                <TabList>
                                    <Tab>Reviewes (263) </Tab>
                                    <Tab> Questions (14) </Tab>
                                </TabList>

                                <TabPanel>

                                    <div className="card gedf-card">
                                        <div className="review-form">
                                            <div className="question-form-wrapper">
                                                
                                                {/* <div className="stamped-form-message stamped-form-message-success">
                                                    <p>Thank you for posting a question!<br />Your question will appear on the site once we've answered it.</p>
                                                </div> */}
                                                <ReviewsForm/>
                                            </div>
                                        </div>
                                        <ReviewsList/>
                                      </div>
                                </TabPanel>

                                <TabPanel>
                                    <div className="card gedf-card">
                                        <div className="question-form">
                                            <div className="question-form-wrapper">
                                                {/* <div className="stamped-form-message stamped-form-message-success">
                                                    <p>Thank you for posting a question!<br />Your question will appear on the site once we've answered it.</p>
                                                </div> */}
                                                <QuestionForm/>
                                            </div>
                                        </div>
                                        <QuestionList/>
                                    </div>

                                </TabPanel>

                            </Tabs>
                        </div>
                    </div>
                </div>

            </div>

        </React.Fragment>
    );
}

export default CustomerReviews;