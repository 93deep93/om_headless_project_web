import React, { useState, Fragment, Component } from 'react';

const PaymentExpressCheckout = (props) => {

    return (
        <React.Fragment> 
            <div className="payment-method-title field choice">
                <input type="radio" onClick={()=> props.setPayment_method('paypal_express')} className="radio" checked={props.paymet_method === 'paypal_express'? true:false} />
                <label className="label" htmlFor="paypal_express">
                    <img className="payment-icon" src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/pp-acceptance-medium.png" alt="Acceptance Mark" />
                    <span>PayPal Express Checkout</span>
                    <a className="action action-help" >What is PayPal?</a>
                </label>
                <div className="choiceName">
                    { props.paymet_method === 'paypal_express'? 
                            <>
                            <input className="checkbox valid" type="checkbox" id="amgdpr_agree" name="amgdpr_agree" value="1" />
                            <label htmlFor="amgdpr_agree">
                                <span className="action">I have read and accept the <a href="#">privacy policy</a></span>
                            </label>
                            </>
                        :
                        ''
                    }
                </div>
            </div>
        </React.Fragment>
    )
}

export default PaymentExpressCheckout