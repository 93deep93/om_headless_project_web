import React, { useState, Fragment, Component } from 'react';
import { Form, Button, Tooltip, Accordion, Card } from 'react-bootstrap';
import '../../styles/Checkout.scss';
import { Redirect, Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import OrderSummaryView from './OrderSummary';
import PaymentMethod from './PaymentMethod';
import CheckoutProcess from '../Checkout/CheckoutProcess';
import { isAuthenticated , placeOrderApi} from '../../account/Repository';
import { SET_USER_CART_DATA, SET_SPINNER_STARTED, SET_SPINNER_COMPLETED } from '../../actions/types';
import { HTTP } from '../../Utils';

const client = {
    sandbox:    'AR4QmiLo_pYFTLmNUArk2QbO3-FVdeoOJxd7mCfPqZOvhECIU6JnNS0awZYMyRkhK8onIrHNqzKezX32',
    production: 'YOUR-PRODUCTION-APP-ID',
    }
   class PaymentView extends Component {

    constructor(props) {
        super(props);
          this.state ={
            paymentMethod : '',
            shippingData : this.props.location.state ? this.props.location.state.shippingData : '',
            shipmentAddress: this.props.location.state ? this.props.location.state.shippingData.shippingAddress:'',
            validated: false,
            copy_address : false,
            order_error : '',
            user_info: localStorage.getItem('user-info-allies') ? 
            JSON.parse(localStorage.getItem('user-info-allies')).isUserInfo ?
              JSON.parse(localStorage.getItem('user-info-allies')).isUserInfo
              : JSON.parse(localStorage.getItem('user-info-allies')) 
              : '',
            user_info_address: localStorage.getItem('user-info-allies') ? 
            JSON.parse(localStorage.getItem('user-info-allies')).isUserInfo ?
                JSON.parse(localStorage.getItem('user-info-allies')).isUserInfo.addresses
                : JSON.parse(localStorage.getItem('user-info-allies')).addresses 
                : [],
          }
        }

        gotoCheckoutPage =()=>{
            this.props.history.push('/Checkout');
        }

        getAddressDataFormat=( data)=>{
            return  {
                city: data.city,
                company: data.company,
                country_id: data.country_id,
                email: data.email,
                firstname: data.firstname,
                lastname: data.lastname,
                postcode:  data.postcode,
                region: data.region,
                region_id: data.region_id,
                street: data.street ? data.street : [data.address],
                telephone: data.telephone
            }
           }

        handle_place_order =( shipmentAddress, billingAddress, paymet_method)=>{
            let user_email = billingAddress.email;
            this.setState({ order_error : ''});
            const info = this.state.user_info;
            let data ={};
            let finelArrProducts = [];
            let arrProducts =this.props.basketProps.products.productList;
            for(let i=0; i< arrProducts.length; i++){
                let obj ={};
                obj.qty = (arrProducts[i].qty || arrProducts[i].numbers);
                obj.sku = arrProducts[i].sku;
                finelArrProducts.push(obj);
            }
            data.addressInformation = { 
                shippingAddress : this.getAddressDataFormat( shipmentAddress), 
                billingAddress : this.getAddressDataFormat( billingAddress),
                
                // payment_method_additional:  { payment}, 
                payment_method_code: paymet_method,
                shipping_carrier_code : this.state.shippingData.shipping_carrier_code,
                shipping_method_code : this.state.shippingData.shipping_method_code,
            };
            data.products= finelArrProducts;
            // data.transmited = true;
            if( localStorage.getItem('x-access-token')){
                data.user_id = info.id? info.id:info.isUserInfo.id;
            }
            data.cart_id = sessionStorage.getItem('cart-id');
            this.orderplaceSubmit( data, user_email);
        }

        orderplaceSubmit=(data, user_email)=>{
            
            console.log(data);
            //call order api
            this.props.dispatch({ type : SET_SPINNER_STARTED});
            return HTTP().post('/api/order',{  
                addressInformation : data.addressInformation,
                products: data.products,
                transmited : true,
                user_id : data.user_id,
                cart_id : data.cart_id
              })
              .then(res => {
                this.props.dispatch({ type: SET_USER_CART_DATA, payload:  []}); 
                  this.props.dispatch({ type : SET_SPINNER_COMPLETED});
                  sessionStorage.removeItem('cart-id');
                  sessionStorage.removeItem('checkout_shipping_data');
                  if( localStorage.getItem('x-access-token')){
                    this.props.history.push({
                        pathname: '/Orderdetails', state: { orderDetails : false, orderPlace: true, orderPlaceData : res.data.result } 
                    })
                    }else{
                        this.props.history.push({
                            pathname: '/payment-sucess', state: { orderPlaceEmail: user_email, orderPlaceData : res.data.result } 
                        })
                    }
                    //   orderPlaceData : {
                    //   "magentoOrderId": "11535",
                    //   "orderNumber": "3000005108",
                    //   "backendOrderId": "11535",
                    //   "transferedAt": "2020-10-02T06:24:59.687Z"
                    // }
                 
              }).catch(err => {
                this.props.dispatch({ type : SET_SPINNER_COMPLETED});
                debugger
                if( err.response && err.response.data.result){
                    if(err.response.data.result.includes('Error while adding shipping address')){
                        this.setState({ order_error : 'Shipping not available for this address !'});
                    }else{
                        this.setState({ order_error : err.response.data.result});
                    }

                    
                }

                
              })
        }

        render() {
            // if (!isAuthenticated()) return (<Redirect to="/login" />);

            const { shippingData, user_info_address, shipmentAddress, order_error } = this.state;

            if(this.state.shippingData !='' && ( this.props.basketProps.products.productList.length > 0)){
                return (
                    <React.Fragment>
                        <CheckoutProcess gotoCheckoutPage = {this.gotoCheckoutPage} view={'payment'} />
                        <section>
                            <div className="container-fluid checkout_page pl-5 pr-5 mt-5 mb-5">
                                <div className="row">
                                    <div className="col-xl-8 col-lg-8 col-md-7 col-sm-12 col-12">
                                        <PaymentMethod 
                                            basketProps = {this.props.basketProps}
                                            user_info_address = {user_info_address}
                                            shipmentAddress = {shipmentAddress}
                                            shippingData={shippingData}
                                            handle_place_order={this.handle_place_order}
                                            order_error = {order_error}
                                        />
                                    </div>
                                    <div className="col-xl-4 col-lg-4 col-md-5 col-sm-12 col-12">
                                        <OrderSummaryView 
                                            shippingData={shippingData} 
                                            basketProps = {this.props.basketProps}
                                            gotoCheckoutPage = {this.gotoCheckoutPage}
                                        />
                                    </div>
                                </div>
                            </div>
                        </section>
                    </React.Fragment >
                );
            }else{
                return <section>
                <div className="container-fluid checkout_page pl-5 pr-5 mt-5 mb-5">  
                  <div className="row">
                      <div className="col-xl-8 col-lg-8 col-md-7 col-sm-12 col-12">
                        <div>You have no items in your shopping cart.</div>
                        <div>Click <Link to ='/'>here</Link> to continue shopping.</div>
                      </div>
                  </div>
                </div>
                </section>
            }
            
        }
}
  
  const mapStateToProps = state => ({
    basketProps : state.basketState
  })
  
  export default connect(mapStateToProps) (PaymentView); 