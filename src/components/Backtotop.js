import React, { Component } from 'react';
import { Button} from 'react-bootstrap';


class Backtotop extends Component {
     constructor(props) {
       super(props);
         this.state = {
        is_visible: false
      };
  }

  componentDidMount() {
       var scrollComponent = this;
      document.addEventListener("scroll", function(e) {
        scrollComponent.toggleVisibility();
      });
  }

  toggleVisibility() {

    if (window.pageYOffset > 200) {
      this.setState({
        is_visible: true
      });
    } else {
      this.setState({
        is_visible: false
      });
    }

  }

  scrollToTop() {

    window.scrollTo({
      top: 0,
      behavior: "smooth"
    });

  }

  render() {

      const { is_visible } = this.state;

      return (
        <React.Fragment> 

          <div className="scroll-to-top">
            {is_visible && (
              <div onClick={() => this.scrollToTop()}>
                <div className="scroll_top">
                  <Button>BACK TO TOP</Button>
                </div>
              </div>
            )}
          </div>

        </React.Fragment> 
    );
  }  
}

export default Backtotop;


