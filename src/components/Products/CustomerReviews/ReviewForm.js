import React from 'react';

const ReviewsForm = () => {

    return (
        <React.Fragment>
            <form method="post" id="new-review-form" className="new-question-form stamped-visible">
                <div className="spr-form-title stamped-form-title">Write a review</div>
                <fieldset className="spr-form-contact stamped-form-contact">

                    <legend>Author</legend>
                    <div className="spr-form-contact-name stamped-form-contact-name">
                        <label className="spr-form-label stamped-form-label" forHtml="question_author_31">Name</label>
                        <input className="spr-form-input spr-form-input-text stamped-form-input stamped-form-input-text" id="question_author_31" type="text" name="name" value="" placeholder="Enter your name" required="" />
                    </div>
                    <div className="spr-form-contact-email stamped-form-contact-email">
                        <label className="spr-form-label stamped-form-label" forHtml="question_email_31">Email</label>
                        <input className="spr-form-input spr-form-input-email stamped-form-input stamped-form-input-email" id="question_email_31" type="email" name="email" value="" placeholder="john.smith@example.com" required="" />
                    </div>
                </fieldset>
                <fieldset className="stamped-form-review">
                    <legend>Rating</legend>
                    <div className="stamped-form-review-rating">
                        <label className="stamped-form-label" htmlFor="reviewRating">Rating</label>
                        <input type="text" id="reviewRating" name="reviewRating" required="" />

                        <div className="stamped-form-input stamped-starrating">
                            <a className="fa fa-star" ><span>1</span></a>
                            <a className="fa fa-star" ><span>2</span></a>
                            <a className="fa fa-star-o"><span>3</span></a>
                            <a className="fa fa-star-o"><span>4</span></a>
                            <a className="fa fa-star-o"><span>5</span></a>
                        </div>
                    </div>
                    <div className="stamped-form-review-title">
                        <label className="stamped-form-label" htmlFor="review_title_31">Title of Review</label>
                        <input className="stamped-form-input stamped-form-input-text" id="review_title_31" type="text" name="reviewTitle" required="" value="" placeholder="Give your review a Title" />
                    </div>
                    <div className="stamped-form-review-body">
                        <label className="stamped-form-label" htmlFor="review_body_31">How was your overall experience?</label>
                        <div className="stamped-form-input">
                            <textarea className="stamped-form-input stamped-form-input-textarea" id="review_body_31" name="reviewMessage" required="" rows="3" maxlength="5000"></textarea>
                        </div>
                    </div>
                </fieldset>
                
                <fieldset className="spr-form-actions stamped-form-actions">
                    <legend>Submit</legend>
                    <input type="submit" className="stamped-button stamped-button-primary button button-primary btn btn-primary" value="Submit" />
                </fieldset>
            </form>
        </React.Fragment>
    )
}
export default ReviewsForm