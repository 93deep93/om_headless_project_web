import React from 'react';

const QuestionForm = () => {

    return (
        <React.Fragment>
            <form method="post" id="new-question-form" className="new-question-form stamped-visible">
                <div className="spr-form-title stamped-form-title">Ask a Question</div>
                <fieldset className="spr-form-contact stamped-form-contact">

                    <legend>Author</legend>
                    <div className="spr-form-contact-name stamped-form-contact-name">
                        <label className="spr-form-label stamped-form-label" forHtml="question_author_31">Name</label>
                        <input className="spr-form-input spr-form-input-text stamped-form-input stamped-form-input-text" id="question_author_31" type="text" name="name" value="" placeholder="Enter your name" required="" />
                    </div>
                    <div className="spr-form-contact-email stamped-form-contact-email">
                        <label className="spr-form-label stamped-form-label" forHtml="question_email_31">Email</label>
                        <input className="spr-form-input spr-form-input-email stamped-form-input stamped-form-input-email" id="question_email_31" type="email" name="email" value="" placeholder="john.smith@example.com" required="" />
                    </div>
                </fieldset>
                <fieldset className="spr-form-review stamped-form-review">
                    <legend>Question</legend>
                    <div className="spr-form-review-body stamped-form-review-body">
                        <label className="spr-form-label stamped-form-label" forHtml="question_body_31">Question</label>
                        <div className="spr-form-input stamped-form-input">
                            <textarea className="spr-form-input spr-form-input-textarea stamped-form-input stamped-form-input-textarea" id="question_body_31" name="reviewBody" rows="3" maxLength="1000" placeholder="Write your question here" required=""></textarea>
                        </div>
                    </div>
                </fieldset>
                <fieldset className="spr-form-actions stamped-form-actions">
                    <legend>Submit</legend>
                    <input type="submit" className="stamped-button stamped-button-primary button button-primary btn btn-primary" value="Submit Question" />
                </fieldset>
            </form>

        </React.Fragment>
    )
}
export default QuestionForm