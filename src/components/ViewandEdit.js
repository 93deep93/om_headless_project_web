import React, {Fragment} from 'react';
import { connect } from 'react-redux';
import '../styles/ViewandEdit.scss'
import { Link } from 'react-router-dom';
import {Card, Accordion, useAccordionToggle } from 'react-bootstrap';
import {  increaseQty } from '../actions/increaseQty';
import {  decreaseQty } from '../actions/decreaseQty';
import { removeProduct } from '../actions/removeProduct';


const ViewandEdit = (props) => {

	 let { cartCost, products } = props.basketProps;
	let productsInCart = products.productList; 
	
	if(!productsInCart.length){
		return <Fragment>
		<span className="view_editcart">YOU HAVE NO ITEMS FOR VIEW AND EDIT IN CART!</span>
		</Fragment>
	}
    productsInCart = productsInCart.map( (productList, index) => {
   	 console.log ("my product is");
   	 console.log (productList);


   	 return (
   		  <Fragment>
   		        <div className="container-fluid view_edit">
                        <div className="row header-description">
                            <div className="col-xl-2 col-lg-2 col-md-2 col-sm-3 col-4 mb-2"><img src={'http://139.180.155.160/img/310/300/resize'+ productList.image} /></div>
                             <div className="col-xl-6 col-lg-6 col-md-6 col-sm-9 col-8 mb-2"> <h3> {productList.name} </h3><span className="preorder">{productList.amasty_preorder_note}</span></div>
                                <div className="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-6 mb-2 text-center"><span className="title_price">{'$'+productList.price.toFixed(2) }</span></div>
                               <div className="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-6 mb-2">
	                               <div className ="incrment_decrment">
			                        <span onClick= {() => props.decreaseQty(productList)}><i class="fa fa-minus" aria-hidden="true"></i></span>
			                        <span className="cart_title_value"> {productList.numbers} </span>
			                        <span onClick= {() => props.increaseQty(productList)}><i className="fa fa-plus" aria-hidden="true"></i></span>
	                             </div>
                              </div>

                              <div className="col-12 delete-product">
                                 <span onClick={()=>props.removeProduct(productList)}> <i className="fa fa-trash" aria-hidden="true"></i></span>
                                  <Link to ={{pathname: `productDetails/${productList.id}`}}><i className="fa fa-pencil" aria-hidden="true"></i></Link>
                              </div>
                             
                       </div>

                </div>                
   		  </Fragment>
   		)
   })


		return (
              <>
               <div className="container-fluid pr-5 pl-5 mt-5 mb-5">
                 <div className="row"> 
                     <div className="col-xl-8 col-lg-7 col-md-12 col-sm-12 col-12">

                        <div className="row header-title">
                               <div className="col-8"><span className="title_name">Item</span></div>
                               <div className="col-2"><span className="title_name">Price</span></div>
                               <div className="col-2"><span className="title_name">Qty</span></div>
                           </div>

                         <div>{productsInCart}</div>

                     </div>
                     <div className="col-xl-4 col-lg-5 col-md-12 col-sm-12 col-12">
                        <div className="container-fluid">
                           <div className="order-summary">
                             <h2 className="text-center">Summary</h2>
                             <div className="col-12">
                                <div className="Shipping-tax">
                                   <Accordion defaultActiveKey="0">
          							      <Card>
          							        <Card.Header>
          							          <CustomToggle eventKey="1">Estimate Shipping and Tax<span className="fa fa-angle-down"></span></CustomToggle>
          							        </Card.Header>
          							        <Accordion.Collapse eventKey="1">
          							          <Card.Body>
                                    <form action="">
          										  <div class="form-group">
          										    <label for="Country">Country</label>
          										    <input type="text" className="form-control" id="email"/>
          										  </div>
          										  <div class="form-group">
          										    <label for="State">State/Province</label>
          										    <input type="text" className="form-control" id="pwd"/>
          										  </div>
          										  <div class="form-group">
          										    <label for="Zip">Zip/Postal Code</label>
          										    <input type="text" className="form-control" id="pwd"/>
          										  </div>

          										  <div class="form-group">
          										    <label for="Shipping">Shipping</label><br/>
          										       <div className="select-tax">
          										         <input type="radio" id="male" name="gender" value="Mail"/>
                                        <label for="Mail">USPS First Class Mail $0.00</label><br/>
                                        <input type="radio" id="male" name="gender" value="Priority"/>
                                          <label for="Priority">USPS Priority $5.00</label><br/>
                                     </div>
          										  </div>										 
          										</form>
          							          </Card.Body>
          							        </Accordion.Collapse>
          							      </Card>
          							    </Accordion>
                                </div>
	                             <div className="row total">
	                              <div className="col-6 cart-totals">
	                                <span className="order-price-name">Sub Total</span>
	                                </div>
	                               <div className="col-6 cart-totals text-right">
	                               <span className="order-price-value">{cartTotal(cartCost)}</span>
	                               </div>
	                            </div>

                                <div className="row total mb-5">
	                              <div className="col-6 cart-totals">
	                                <span className="order-price-name">Order Total</span>
	                                </div>
	                               <div className="col-6 cart-totals text-right">
	                               <span className="order-price-value">{cartTotal(cartCost)}</span>
	                               </div>
                               </div>
                           </div>


                             <div className="col-12 text-center">
                               <Link to ="/checkout" className="proceed-checkout">Proceed to checkout</Link>
                            </div>

                        </div>
                        </div>
                     </div>
                  </div>
			       
			    </div>
           </>
			)


}

const mapStateToProps = state => ({
	basketProps : state.basketState
});

function cartTotal(amount){
	return '$'+amount.toFixed(2);
}

function CustomToggle({ children, eventKey }) {
  const decoratedOnClick = useAccordionToggle(eventKey, () =>
    console.log('totally custom!'),
  );

  return (
    <button type="button" onClick={decoratedOnClick} >
      {children}
    </button>
  );
}


export default connect(mapStateToProps, {increaseQty, decreaseQty, removeProduct }) (ViewandEdit);