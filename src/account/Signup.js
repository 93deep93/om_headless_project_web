import React, {Component} from 'react';
import './Login.scss';
import {Link} from 'react-router-dom';
import {Modal} from 'react-bootstrap';
import { signup } from './Repository';
import {Form, Button, Tooltip } from 'react-bootstrap';

class Signup extends Component {

    constructor() {
        super();
        this.state = { error : '', email: '', firstname: '', lastname: '', isPopup: false,  password: '', message: '', email_error: '', firstname_error: '', lastname_error: '',  password_error: '', confirm_password: '', confirm_password_error : ''};
      }

      handleClose = () => {
         this.setState({isPopup: false})
      }


      handleInputChange = (event) => this.setState({[event.target.name]: event.target.value, [event.target.name + '_error']: ''});

      validateEmail=( email)=>{
        const regex_email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex_email.test(String(email).toLowerCase());
      }

      validatePassword=( password)=>{
        const regex_password = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/;
        return regex_password.test(password);
      }

      validateForm=()=>{
        let flag = false;
        var { email, firstname, lastname, password, email_error, confirm_password, firstname_error, lastname_error,  password_error, confirm_password_error} = this.state;
        if( email === "" || email=== null || email === undefined){
          email_error = 'This field is required';
          this.setState({ email_error : email_error})
          flag = true;
        }else if( email !== "" || email !== null || email !== undefined){
          if(!this.validateEmail(email)){
            email_error = 'Please enter valid email id !'
            this.setState({ email_error : email_error})
            flag = true;
          }}
        if( firstname === "" || firstname=== null || firstname === undefined){
          firstname_error = 'This field is required';
          this.setState({ firstname_error : firstname_error})
          flag = true;
        }
        if( lastname === "" || lastname=== null || lastname === undefined){
          lastname_error = 'This field is required';
          this.setState({ lastname_error : lastname_error})
          flag = true;
        }
        if( password === "" || password=== null || password === undefined){
          password_error = 'This field is required';
          this.setState({ password_error : password_error})
          flag = true;
        }else if( password !== "" || password !== null || password !== undefined){
          if(!this.validatePassword(password)){
            password_error = 'Password must have at least 8 letters, one uppercase character, one lowercase character, one digit and one special character !'
            this.setState({ password_error : password_error})
            flag = true;
          }}
        if( confirm_password === "" || confirm_password=== null || confirm_password === undefined){
          confirm_password_error = 'This field is required';
          this.setState({ confirm_password_error : confirm_password_error})
          flag = true;
        }
        
        if( (password !== "") &&  (confirm_password !== "")){
          if( confirm_password  !== password){
            confirm_password_error = "Password and confirm password didn't match !";
            this.setState({ confirm_password_error : confirm_password_error})
            flag = true;
          }
        }


        return flag
      }

      submitSignup = (event) => {
        event.preventDefault();
        if(!this.validateForm()){
          
          signup(this.state)
            .then(token => {
              // window.location = '/';
             if(token.code === 200) {           
              this.setState({isPopup:true, message: "You are successfully Register !", error: ''});
             }
            })
              .catch(err => {
              this.setState({error:err});
              console.log(err)
            });
        }
      }

  render () {

     let {isPopup, error, message,  email, firstname, lastname, password, confirm_password, email_error, firstname_error, lastname_error,  password_error, confirm_password_error} = this.state;
    
     return (
    <React.Fragment> 
    <div className="login-container sign-up-container container mt-5 mb-5">
      <div className="row">

        <div className="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-xs-12 p-0 mx-auto my-auto">

            <div className="customer-login">
                     
                      <form className="signup" onSubmit={this.submitSignup}>

                         <h2>Register</h2>

                            <Form.Group controlId="formBasicEmail" className="form_group">
                              <Form.Label>Email address</Form.Label>
                              <Form.Control name="email" value={email } type="email" onChange={this.handleInputChange} placeholder=""  isInvalid={!!email_error}/>
                                <Form.Control.Feedback style={{ paddingLeft : '0%' }} type="invalid"> {email_error} </Form.Control.Feedback>
                            </Form.Group>
                            
                            <Form.Group controlId="formBasicEmail" className="form_group">
                              <Form.Label>First Name</Form.Label>
                              <Form.Control name="firstname" value={firstname } type="text" onChange={this.handleInputChange} placeholder=""  isInvalid={!!firstname_error}/>
                                <Form.Control.Feedback style={{ paddingLeft : '0%' }} type="invalid"> {firstname_error} </Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group controlId="formBasicEmail" className="form_group">
                              <Form.Label>Last Name</Form.Label>
                              <Form.Control name="lastname" value={lastname } type="text" onChange={this.handleInputChange} placeholder=""  isInvalid={!!lastname_error}/>
                                <Form.Control.Feedback style={{ paddingLeft : '0%' }} type="invalid"> {lastname_error} </Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group controlId="formBasicEmail" className="form_group">
                              <Form.Label>Enter Password</Form.Label>
                              <Form.Control name="password" value={password } type="password" onChange={this.handleInputChange} placeholder=""  isInvalid={!!password_error}/>
                                <Form.Control.Feedback style={{ paddingLeft : '0%' }} type="invalid"> {password_error} </Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group controlId="formBasicEmail" className="form_group">
                              <Form.Label>Confirm Password</Form.Label>
                              <Form.Control name="confirm_password" value={confirm_password } type="password" onChange={this.handleInputChange} placeholder=""  isInvalid={!!confirm_password_error}/>
                                <Form.Control.Feedback style={{ paddingLeft : '0%' }} type="invalid"> {confirm_password_error} </Form.Control.Feedback>
                            </Form.Group>
                            {error ?  
                            <div style={{ color : 'red', textAlign:'center', textAlign:'center', paddingTop:'10px'}}>{error}</div>
                            : ''}
                            <input type="submit" value="Register" />
                            <span className="login-account"><Link to="/Login">Login Your Account?</Link></span>
                      </form>
            </div>  
        </div> 

        </div> 

    </div>


                <Modal className="message_popup" show={isPopup} onHide={this.handleClose} animation={false}>
                  <Modal.Header closeButton> </Modal.Header>
                    <Modal.Body className= "text-center mt-5 mb-5">{message}</Modal.Body>
                </Modal>

 
    </React.Fragment>
  );

  }
 
}

export default Signup;