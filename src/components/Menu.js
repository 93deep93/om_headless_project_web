import React, { useEffect, useState, useRef } from 'react';
import ReactDOM from 'react-dom';
import '../styles/Menu.scss';
import {Link} from 'react-router-dom';
import {Navbar, Nav } from 'react-bootstrap';
import {connect} from 'react-redux';
import {Button, Col, Overlay, Popover, Tooltip, Row} from 'react-bootstrap';
import {getNumbers} from '../actions/getAction';
import {addBasket} from '../actions/addAction';
import {  increaseQty } from '../actions/increaseQty';
import Cart from './Cart';
import { isAuthenticated } from '../account/Repository';
import { changeLoginStatus } from '../actions/changeLoginStatus'


const Menu = (props) => {

    useEffect(() => {
      getNumbers();
    }, []);

    const [show, setShow] = useState(false);
    const target = useRef(null);

    const logout = ()=>{
      // localStorage.clear()
      localStorage.removeItem('basketData');
      localStorage.removeItem('x-access-token');
      localStorage.removeItem('x-access-token-expiration');
      localStorage.removeItem('user-info-allies');
      sessionStorage.removeItem('cart-id');
      sessionStorage.removeItem('checkout_shipping_data');
      props.changeLoginStatus(isAuthenticated())
      props.getNumbers();
    }

  return (
    <React.Fragment>
      <div className="menu-over-banner">
         <nav className="navbar navbar-inverse">
		  <div className="container-fluid">
		    <div className="navbar-header ml-auto mr-auto">
		      <Link to="/" className="text-center logo"><img src={require('../icons/logo.png')}  height="27px;"/></Link>
		    </div>
		     <ul className="nav navbar-right">

		      <li className="dropdown">
            <Link to="" className="dropbtn"><img src={require('../icons/user.png')}  height="16px;"/></Link>
            <div className="dropdown-content">
              {props.basketProps.isLogin ? <Link to="/accountinfo">My account</Link> : <Link to="/Login">Login</Link>}
              {props.basketProps.isLogin ? '':<Link to="/Signup">Signup</Link> }
              {props.basketProps.isLogin ? <Link to="" onClick={logout}>Logout</Link> :'' }          {/* <Link to="Orderinfo">Order Return</Link> */}
            </div>
		      </li>
		      <li>
		      <div className="cart_popover">
                <Row>             
                  <span className="text" ref={target} onClick={() => setShow(!show)}> Bag (<span >{props.basketProps.basketNumbers}</span>)
                  </span>

                  <Overlay target={target.current} show={show} placement="left">
                    {(props) => (
                      <Tooltip className="cart_popover" {...props}>
                       
                         <div className="cart_list">
                             <Cart handleCloseBag={()=>{ setShow(!show) }}/>                     
                         </div>

                        <span className="close-icon" onClick={() => setShow(!show)}>&times;</span>
                      </Tooltip>
                    )}

                  </Overlay>

                </Row>
            </div>
		      </li>
		    </ul>
		  </div>
        </nav>
      </div>
      <Navbar collapseOnSelect expand="md">
    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
    <Navbar.Collapse id="responsive-navbar-nav">
    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Nav className="mr-auto ml-auto">
      <li className="nav-item text-white"> <Link to="/"> Home </Link></li>
          <li className="nav-item text-white"> <Link to="/Shop"> Shop </Link></li>
          <li className="nav-item text-white"> <Link to="/About"> About Us </Link></li>
          <li className="nav-item text-white"> <Link to="/Press"> Press </Link> </li>
          <li className="nav-item text-white"> <Link to="/Retailers"> Retailers </Link> </li>
          <li className="nav-item text-white"> <Link to="/regimen-recommender"> Regimen Recommender  </Link></li>
      </Nav>  
    </Navbar.Collapse>
    
  </Navbar>
    </React.Fragment>
  );
}

const mapStateToProps = state => ({
  basketProps:state.basketState
})
export default connect(mapStateToProps, { addBasket, increaseQty, changeLoginStatus, getNumbers  })(Menu);