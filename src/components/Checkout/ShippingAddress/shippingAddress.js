import React, {Fragment, Component} from 'react';
import {Form, Button, Tooltip, Accordion, Card } from 'react-bootstrap';
import { Redirect, Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import countriesList from '../../../data/countries_reagions.json';
import equal from 'fast-deep-equal';
import AddAddress from './AddressPopup';

import ShippingMethod from '../shippingMethod';
import { getShipmentMethods, isAuthenticated, getProducts, pay } from '../../../account/Repository';
class ShippingAddress extends Component {

   constructor(props) {
      super(props);
      this.state = { 
        cart_id : sessionStorage.getItem('cart-id'),
        productItem: [], 
        total: 0,
        formData: {},
        errors:{ },
        validated: false,
        selected_shipping_method : '',
        selected_shipping_method_error : '',
        shipping_method_data : [],
        state_arr:[],
        countriesList : countriesList,
        cartCost:'', products:[],
        selected_Address : '',
        default_Is_selected_address : true,
        openFormModel : false,
        exists_newAddress : false,
        user_info_address : [],
        user_info: localStorage.getItem('user-info-allies') ? 
          JSON.parse(localStorage.getItem('user-info-allies')).isUserInfo ?
            JSON.parse(localStorage.getItem('user-info-allies')).isUserInfo
            : JSON.parse(localStorage.getItem('user-info-allies')) 
            : '',
       }
       
      
      }

      componentDidMount(){
        
        if(this.props.previous_checkout_shipping_data && Object.keys(this.props.previous_checkout_shipping_data).length > 0){
          this.setShippingMethod( this.props.previous_checkout_shipping_data.shippingAddress.country_id);
          const shippingAddress = this.props.previous_checkout_shipping_data.shippingAddress;
          const user_info_address = this.props.user_info_address;
          let exists_newAddress = false;
          if(shippingAddress.id = 'new'){
            user_info_address.push(shippingAddress);
            exists_newAddress = true;
          }

          this.setState({ 
              selected_shipping_method : this.props.previous_checkout_shipping_data.shipping_method_code,
              selected_Address : this.props.previous_checkout_shipping_data.shippingAddress, 
              user_info_address :   user_info_address, exists_newAddress : exists_newAddress});
        }else{
          let default_shipping = this.props.user_info_address.filter( x=> x.default_shipping);
          if( default_shipping.length>0){
            this.setState({ selected_Address : default_shipping[0] , user_info_address :  this.props.user_info_address});
          }else{
            this.setState({ selected_Address : this.props.user_info_address[0], user_info_address :  this.props.user_info_address});
          }
            this.setShippingMethod(this.props.user_info_address[0].country_id)
        }
        
      }

      UNSAFE_componentWillReceiveProps( props,nextProps){
        if(  nextProps.user_info_address && (props.user_info_address != nextProps.user_info_address) ){
          
          let default_shipping = this.props.user_info_address.filter( x=> x.default_shipping);
        if( default_shipping.length>0){
          this.setState({ selected_Address : default_shipping[0] , user_info_address :  this.props.user_info_address});
        }else{
          this.setState({ selected_Address : this.props.user_info_address[0], user_info_address :  this.props.user_info_address});
        }
          this.setShippingMethod(this.props.user_info_address[0].country_id)
        }
      }

      setShippingMethod =(country_code)=>{
        this.props.showLoader();
        getShipmentMethods( { country_code : country_code , cart_id : this.state.cart_id})
        .then(res => {
          this.props.closeLoader();
          if(res.code === 200) {   
            this.setState({ shipping_method_data : res.result});
          }
        }).catch(err => {
          this.props.closeLoader()
          console.log(err)
        });
      }

      handleShippingMethodOnChange =( val)=>{
        this.setState({ selected_shipping_method : val , selected_shipping_method_error: ''});
      }

      setSelectAddress =(item)=>{
        this.setShippingMethod(item.country_id)
        debugger
        this.setState({ selected_Address : item , default_Is_selected_address: true, selected_shipping_method: ''})
      }

      validate=() =>{
        let flag = false;
        let selected_shipping_method_error_val ='';
        const { errors, formData, selected_shipping_method , selected_shipping_method_error} = this.state;
        if( selected_shipping_method === "" || selected_shipping_method ===null || selected_shipping_method === undefined){
            selected_shipping_method_error_val = 'Please select shipping method';
            flag = true
          }
          this.setState({ errors : errors, selected_shipping_method_error : selected_shipping_method_error_val});
          return flag;
      }

      gotoPaymentPage=()=>{
        if(!this.validate( ) && this.state.selected_Address !=''){
          let data = {};
          const shipping_data_state = this.state.selected_Address;
            data.cart_id=this.state.cart_id;
            data.shipping_carrier_code = this.state.shipping_method_data.filter(item=> item.method_code === this.state.selected_shipping_method)[0].carrier_code;
            data.shipping_method_code = this.state.selected_shipping_method;
            data.shipping_method_Details = this.state.shipping_method_data.filter(item=> item.method_code === this.state.selected_shipping_method)[0];
            let shipping_data ={};
          if( shipping_data_state.id != 'new'){
              shipping_data.id = shipping_data_state.id;
              shipping_data.address = shipping_data_state.street[0];
              shipping_data.address2 = shipping_data_state.street.length>1 ? shipping_data_state.street[1]: '';
              shipping_data.city = shipping_data_state.city;
              shipping_data.company = shipping_data_state.company;
              shipping_data.country_id = shipping_data_state.country_id;
              shipping_data.email = this.state.user_info.email;
              shipping_data.firstname = shipping_data_state.firstname;
              shipping_data.lastname = shipping_data_state.lastname;
              shipping_data.postcode = shipping_data_state.postcode;
              shipping_data.region = shipping_data_state.region.region;
              shipping_data.region_code = shipping_data_state.region.region_code;
              shipping_data.region_id = shipping_data_state.region_id;
              shipping_data.state = shipping_data_state.region.region_code;
              shipping_data.street = shipping_data_state.street;
              shipping_data.telephone = shipping_data_state.telephone;
              shipping_data.default_billing= shipping_data_state.default_billing? true: false;
              shipping_data.default_shipping= shipping_data_state.default_shipping? true: false;
            
            data.shippingAddress = shipping_data;
          }else{
            data.shippingAddress = this.state.selected_Address;
          }
          
          
          
          localStorage.setItem('shipmentAddress' ,  JSON.stringify(data));
          this.props.gotoPaymentPage(data);
        }
      }

    

      setShippingAddress=( data)=>{
        data.id= 'new';
        data.street = [ data.address, data.address2 ];
        var user_info_address = this.state.user_info_address;
        if( this.state.exists_newAddress === true){
          var removeIndex = user_info_address.map(function(item) { return item.id; }).indexOf('new');
        // remove object
        user_info_address.splice(removeIndex, 1);        
        }
        user_info_address.push( data);
        this.setState({  openFormModel : false, exists_newAddress : true, user_info_address : user_info_address , selected_shipping_method: ''});
        this.setSelectAddress( data)
      }

      editAddress=( data)=>{
        this.setState({  openFormModel : true});
      }
      
      
      render() {

        const {selected_Address, user_info_address, exists_newAddress} = this.state;
        
  
        return (
            <React.Fragment>
                     <div id="checkout-step-shipping" className="step-content">
                      <div className="field addresses">
                        <div className="control">
                            <div className="shipping-address-items">
                              <div className="row">
                                { user_info_address.map((item, index)=>{
                                    return(
                                      <div className="col-md-4">
                                        <div className= {"shipping-address-item "+ (selected_Address.id === item.id ?  "selected-item" :'')} onClick={()=>{ this.setSelectAddress(item) } }>
                                            {item.firstname ? item.firstname :'' +' '+ item.lastname ? item.lastname : ''}<br/>
                                            {item.street? item.street.join(', '):''}<br/>
                                            {item.city? item.city : ''}, {item.region? item.region.region : ''} {item.postcode? item.postcode : ''}<br/>
                                            {countriesList.length>0 ? countriesList.map( country => { 
                                                if(country.id === item.country_id)  
                                                return country.full_name_english; }):'' }<br/>
                                            {item.telephone ? item.telephone : ''}<br/>
                                            {item.id ? item.id === 'new'?
                                              <a onClick={()=>{ this.editAddress(item)}}>Edit</a> : <div></div>:''
                                            }
                                            
                                        </div>
                                        </div>
                                    )
                                } )}
                               </div>
                            </div>
                        </div>
                    </div>
                <div className="new-address-popup">
                  {exists_newAddress ? '':<button onClick={ ()=>{ this.setState({ openFormModel : true})} } className="action action-show-popup">
                        <span>New Address</span>
                    </button>}
                </div>

                <div className="shipping_method">
                      { this.state.shipping_method_data.length>0 ? 
                        <Fragment>
                          <ShippingMethod  
                            shipping_method_data ={ this.state.shipping_method_data}
                            selected_shipping_method={ this.state.selected_shipping_method}
                            selected_shipping_method_error={ this.state.selected_shipping_method_error}
                            handleShippingMethodOnChange={this.handleShippingMethodOnChange}
                           />
                           
                            <div className="actions-toolbar" id="shipping-method-buttons-container">
                              <div className="primary">
                                <button onClick={this.gotoPaymentPage} className="button action continue primary">
                                    <span>Next</span>
                                </button>
                              </div>
                          </div>
                        </Fragment>
                        :
                        <div className="shipping_method">
                            <h3 className="step_title">Shipping Method</h3>        
                            <span>Sorry, no quotes are available for this order at this time</span>
                        </div>
                      } 
                    </div>
              
                </div>
                <AddAddress 
                  isShow ={ this.state.openFormModel} 
                  setShippingAddress ={this.setShippingAddress}
                  closeModal ={ ()=>{ this.setState({openFormModel:false})}}
                  />

                   
            </React.Fragment >
        );

    }

}


export default ShippingAddress;