import React, { Component } from 'react';
import Pagination from 'react-js-pagination';
import { isMouseMoveEvent } from 'react-multi-carousel';
import '../../../styles/AccountInfo.scss';
import AccountDashMenu from '../myAccountDashboardMenu';
import { getUserInfo, getUserMyOrders } from '../../../actions/customerInfo';
import { reOrderProducts , checkProductStock, checkOldCartAndUpdate} from '../../../actions/getAction';
import { getShipmentMethods, isAuthenticated, getProducts, pay } from '../../../account/Repository';
import { SET_USER_CART_DATA, SET_SPINNER_STARTED, SET_SPINNER_COMPLETED, CHECK_UNAUTHORIZED } from '../../../actions/types';
import { Redirect} from 'react-router-dom';
import UserInfo from './userInfo';
import { connect } from 'react-redux';
class AccountInfo extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoaded :true,
      postDataState :'',
    };
  }


  componentDidMount(){
    // if (!isAuthenticated())
    this.props.dispatch( getUserMyOrders());
    this.props.dispatch( getUserInfo());
    
  }

  UNSAFE_componentWillReceiveProps( newProps){
    if( newProps.userinfoProps  && newProps.userinfoProps.isUserInfo){
      this.setState({postDataState : newProps.userinfoProps.isUserInfo,  isLoaded : false})
    }
  }

  reOrderProducts=( orders)=>{
    // this.props.dispatch({ type : SET_SPINNER_STARTED});
    // reOrderProducts( orders).then(res => {
    // }).catch(err => {});

    let varOrder = orders;
    checkProductStock ( orders,  (res)=>{

      let ids = new Set( res.map(d => d.product_id));
          let reOrderProduct = varOrder.filter(d => ids.has(d.product_id));
      //direct go to add to cart
      let orders = [];
      for(let i=0; i<reOrderProduct.length; i++){
        let data={};
        data.item_id = reOrderProduct[i].product_id; 
        data.sku = reOrderProduct[i].sku;
        data.qty = reOrderProduct[i].qty_ordered;
        data.name = reOrderProduct[i].name;
        data.price = reOrderProduct[i].price; 
        data.product_type = reOrderProduct[i].product_type;
        orders.push(data);
      }
      this.props.dispatch(checkOldCartAndUpdate(orders ));
    })
  }


  render() {
    if (!isAuthenticated()) return (<Redirect to="/login" />);
    let {isLoaded, items} = this.state;
    let PostData = this.state.postDataState || []; //Object.assign([],items);
    console.log('Product List', PostData);
  
      
    return (
      <React.Fragment>
        <section className="page_banner">
          <div className="rowsec-account account-inof-grid" id="maincontent">
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-4">
                  <AccountDashMenu/>
                </div>
                <div className="col-md-8">
                {isLoaded ? 
                  <div className="text-center mt-5 mb-5">loading...</div>
                  :
                  <UserInfo propsData = {this.state.postDataState} userMyOrders ={this.props.userMyOrders}  reOrderProducts ={this.reOrderProducts}/>
                }
                   
                </div>
              </div>
            </div>
          </div>

        </section>
      </React.Fragment>
    );
    
  }

}

const mapStateToProps = state => ({
  userinfoProps : state.userinfoState,
  userMyOrders  : state.userinfoState.userMyOrders,
})

export default connect(mapStateToProps) (AccountInfo);