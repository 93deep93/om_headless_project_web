import { SET_USER_INFO_FULL_DETAILS, SET_SPINNER_STARTED, SET_SPINNER_COMPLETED , SET_USER_INFO_MY_ORDERS} from './types';
import { getUserInfoApi, getUserMyOrdersApi} from '../account/customerInfoApi';

export const getUserInfo = () => {
	return dispatch => {
		dispatch({ type : SET_SPINNER_STARTED});
		getUserInfoApi()
		.then(res => {
		if(res.code === 200) { 
			
            dispatch({ type : SET_SPINNER_COMPLETED});			
            dispatch({
                type: SET_USER_INFO_FULL_DETAILS,
                payload: res.result
            }); 
        }
		}).catch(err => {
			dispatch({ type : SET_SPINNER_COMPLETED});
		console.log(err)
		});
	}
}

//getUserMyOrders
export const getUserMyOrders = () => {
	return dispatch => {
		dispatch({ type : SET_SPINNER_STARTED});
		getUserMyOrdersApi()
		.then(res => {
		if(res.code === 200) { 
			
            dispatch({ type : SET_SPINNER_COMPLETED});			
            dispatch({
                type: SET_USER_INFO_MY_ORDERS,
                payload: res.result
            }); 
        }
		}).catch(err => {
			dispatch({ type : SET_SPINNER_COMPLETED});
		console.log(err)
		});
	}
}


